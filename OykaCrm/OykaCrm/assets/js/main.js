jQuery(document).ready(function (jQuery) {
    $(".loading-div").delay(2500).fadeOut(400);
    $(".kurulum-form").delay(3000).fadeIn(300);
    "use strict";
    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
        new SelectFx(el);
    });
    jQuery('.selectpicker').selectpicker;

    jQuery("#address").val(jQuery(".firma-adres-al").val() + "," + jQuery(".firma-ilce-al option:selected").text() + "," + jQuery(".firma-il-al option:selected").text());

    jQuery('.firma-adres-al').change(function () {
        var firmaadresi = jQuery(this).val();
        jQuery("#address").val(firmaadresi);
        jQuery("#submit").trigger("click");
    });

    jQuery(document).on("click", ".show_more", function () {
        var id = jQuery(this).attr("data-record");
        jQuery(".show_more").hide();
        jQuery(".loading-kdv").show();
        jQuery.ajax({
            url: "/ajax-kdv-raporlari.php",
            data: { id: id },
            type: "post",
            success: function (e) {
                jQuery(".menu").attr("data-record", "menu_" + id).remove();
                jQuery(".kdv-aylari-listeleme").append(e);
            }
        })
    });


    jQuery(document).on("click", ".kdv-aylari-tr", function () {
        var id = jQuery(this).attr("data-ay");
        jQuery.ajax({
            url: "/ajax-kdv-ay.php",
            data: { id: id },
            type: "post",
            success: function (e) {
                jQuery(".kdv-aylari-detay").html(e);
            }
        })
    });



    jQuery('#submit').click(function () {
        var firmail = jQuery(".firma-il-al option:selected").text();
        var firmailce = jQuery(".firma-ilce-al option:selected").text();
        var firmaadresi = jQuery(".firma-adres-al").val();
        var firmailveadres = firmaadresi + "," + firmailce + "," + firmail;
        jQuery("#address").val(firmailveadres);


    });



    // Talep Start
    jQuery("#form-talep").submit(function (e) {
        jQuery.post(
            jQuery(this).attr('action'),
            jQuery(this).serialize(),
            function (data) {
                jQuery('#sonuc').html(data);
                jQuery(".del").val("");
            }
        );
        e.preventDefault();
    });
    // Talep End

    // Talep Cevap Start
    jQuery(".form-talep-cevap").submit(function (e) {
        jQuery.post(
            jQuery(this).attr('action'),
            jQuery(this).serialize(),
            function (data) {
                jQuery('.sonuc2').html(data);
                jQuery(".del").val("");
            }
        );
        e.preventDefault();
    });
    // Talep Cevap End	

    jQuery('#menuToggle').on('click', function (event) {
        jQuery('body').toggleClass('open');
    });

    jQuery('.search-trigger').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        jQuery('.search-trigger').parent('.header-left').addClass('open');
    });

    jQuery('.search-close').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        jQuery('.search-trigger').parent('.header-left').removeClass('open');
    });

    jQuery(".search-filter-button").click(function () {
        jQuery(".search-filter").toggle("medium");
    });

    jQuery(".sistemkullanicisiinput").change(function () {
        jQuery("#kullaniciyetkiler").toggle("medium");
    });

    jQuery(".mevccutsifredegis").change(function () {
        jQuery("#mevccutsifredegisekran").toggle("medium");
    });

    jQuery('.firsatperiyot').on('change', function () {
        var firsatperiyotsecili = jQuery("option:selected", this).val();
        if (firsatperiyotsecili == "0") {
            jQuery('.firsatsurec').val("");
            jQuery('.firsatsurec').hide();
        } else if (firsatperiyotsecili == "1") {
            jQuery('.firsatsurec').show();
            jQuery('.firsatsurec').val("");
        } else if (firsatperiyotsecili == "2") {
            jQuery('.firsatsurec').show();
            jQuery('.firsatsurec').val("");
        } else if (firsatperiyotsecili == "3") {
            jQuery('.firsatsurec').show();
            jQuery('.firsatsurec').val("");
        }
    });

    jQuery('.clockpicker').clockpicker();

    // Datepicker Start
    var date_input1 = jQuery('input[name="siparistarihi1"]'); //our date input has the name "date"
    var date_input2 = jQuery('input[name="siparistarihi2"]'); //our date input has the name "date"
    var date_input3 = jQuery('.tarih');
    var container = jQuery('.bootstrap-iso form').length > 0 ? jQuery('.bootstrap-iso form').parent() : "body";
    var options = {
        format: 'dd.mm.yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    };
    date_input1.datepicker(options);
    date_input2.datepicker(options);
    date_input3.datepicker(options);
    // Datepicker End

    jQuery(".search-customer-result").hide();
    jQuery(".search-customer").on("keyup", function () {
        var value = encodeURIComponent(jQuery(this).val());
        var konu = "value=" + value;
        jQuery(".customer-info").html("");
        jQuery(".search-customer-small").html("&nbsp;");
        jQuery(".musteriid").val("");
        jQuery.ajax({
            type: "post",
            url: "/ajax.php?sayfa=musteriad",
            data: konu,
            beforeSend: function () {
                jQuery("#sonuc").fadeIn().html('<img src="/assets/images/loading.gif" width="20" height="20" />');
            },
            success: function (sonuc) {
                jQuery(".search-customer-result").show().html(sonuc);
                jQuery('.search-customer-result-list').click(function () {
                    var jQuerySonraki = jQuery(this).next(".bilgiler-genel");
                    var str = jQuery(this).html();
                    var res = str.replace("&amp;", "&");
                    jQuery('.search-customer').val(res);
                    jQuery('.search-customer-small').html(jQuerySonraki.find(".search-customer-bakiye").html());
                    jQuery('.customer-info').html(jQuerySonraki.find(".search-customer-info").html());
                    jQuery(".search-customer-result").hide();
                    jQuery(".search-customer-result").hide();


                    var value2 = encodeURIComponent(jQuery(".musteriid").val());
                    var konu2 = "value=" + value2;
                    jQuery.ajax({
                        type: "post",
                        url: "/ajax.php?sayfa=aktivite",
                        data: konu2,
                        success: function (sonuc2) {
                            jQuery(".bagli-aktivite").html(sonuc2);
                        }
                    });
                    jQuery.ajax({
                        type: "post",
                        url: "/ajax.php?sayfa=firsat",
                        data: konu2,
                        success: function (sonuc2) {
                            jQuery(".bagli-firsat").html(sonuc2);
                        }
                    });



                });
            }
        });
    });

    jQuery(".search-customer-result-yetkili").hide();
    jQuery(".search-customer-yetkili").on("keyup", function () {
        var value = encodeURIComponent(jQuery(this).val());
        var konu = "value=" + value;
        jQuery(".customer-info-yetkili").html("");
        jQuery(".search-customer-small-yetkili").html("&nbsp;");
        jQuery(".musteriid").val("");
        jQuery.ajax({
            type: "post",
            url: "/ajax-aktivite-yetkili.php?sayfa=musteriad",
            data: konu,
            beforeSend: function () {
                jQuery("#sonuc").fadeIn().html('<img src="/assets/images/loading.gif" width="20" height="20" />');
            },
            success: function (sonuc) {
                jQuery(".search-customer-result-yetkili").show().html(sonuc);
                jQuery('.search-customer-result-list-yetkili').click(function () {
                    var jQuerySonraki = jQuery(this).next(".bilgiler-genel");
                    var str = jQuery(this).html();
                    var res = str.replace("&amp;", "&");
                    jQuery('.search-customer-yetkili').val(res);
                    jQuery('.search-customer-small-yetkili').html(jQuerySonraki.find(".search-customer-bakiye").html());
                    jQuery('.customer-info-yetkili').html(jQuerySonraki.find(".search-customer-info").html());
                    jQuery(".search-customer-result-yetkili").hide();

                    var value2 = encodeURIComponent(jQuery(".musteriid").val());
                    var konu2 = "value=" + value2;
                    jQuery.ajax({
                        type: "post",
                        url: "/ajax.php?sayfa=firsat",
                        data: konu2,
                        success: function (sonuc2) {
                            jQuery(".bagli-firsat").html(sonuc2);
                        }
                    });
                });
            }
        });
    });

    jQuery("#button-tahsilat-ekle").click(function () {
        jQuery("#container-tahsilat-ekle").show("medium");
        jQuery("#button-tahsilat-ekle").hide("medium");
    });
    jQuery("#button-tahsilat-vazgec").click(function () {
        jQuery("#container-tahsilat-ekle").hide("medium");
        jQuery("#button-tahsilat-ekle").show("medium");
    });
    jQuery(".tahsilat-tip-1").click(function () {
        jQuery(".tahsilat-hesap").show();
        jQuery(".tahsilat-vade-tarihi").hide();
        jQuery(".tahsilat-cekler").hide();
        jQuery(".tahsilat-meblag").show();
        jQuery(".tahsilat-aciklama").show();
    });
    jQuery(".tahsilat-tip-2").click(function () {
        jQuery(".tahsilat-vade-tarihi").show();
        jQuery(".tahsilat-cekler").show();
        jQuery(".tahsilat-hesap").hide();
    });
    jQuery("#button-musteri-bilgileri-goster").click(function () {
        jQuery(".container-musteri-bilgileri").toggle("medium");
    });

    function formatMyMoney(price) {
        var currency_symbol = "TT";
        var formattedOutput = new Intl.NumberFormat('tr-TR', {
            currency: 'TRY',
            minimumFractionDigits: 2,
        });
        return formattedOutput.format(price).replace(currency_symbol, '');
    }

    jQuery(".numberFormat1").mask("99", { reverse: true });
    jQuery(".numberFormat2").mask("########0,00", { reverse: true });
    jQuery(".numberFormat3").mask("999", { reverse: true });
    jQuery(".numberFormat4").mask("9999999", { reverse: true });
    jQuery(".numberFormat5").mask("-9999999", { reverse: true });
    jQuery(".numberFormatTel").mask("9 999 999 9999", { reverse: true });

    jQuery(".tarih").mask("99.99.9999", { reverse: true });
    jQuery(".tarih").attr('autocomplete', 'off');

    jQuery("#urunaciklamagenel_1").hide();
    jQuery("#urunlerindirimgenel_1").hide();
    jQuery("#urunotvgenel_1").hide();
    jQuery("#urunoivgenel_1").hide();

    jQuery("#aciklamaekle_1").click(function () {
        jQuery("#urunaciklamagenel_1").show("medium");
    });
    jQuery("#indirimekle_1").click(function () {
        jQuery("#urunlerindirimgenel_1").show("medium");
        jQuery(".satir-indirimi-genel").show("medium");
    });
    jQuery("#urunotvekle_1").click(function () {
        jQuery("#urunotvgenel_1").show("medium");
        jQuery(".otv-toplam-genel").show("medium");
    });
    jQuery("#urunoivekle_1").click(function () {
        jQuery("#urunoivgenel_1").show("medium");
        jQuery(".oiv-toplam-genel").show("medium");
    });
    jQuery("#aciklamasil_1").click(function () {
        jQuery("#urunaciklamagenel_1").hide("medium");
        jQuery("#urunaciklamagenel_1 input").val("");
    });
    jQuery("#indirimsil_1").click(function () {
        jQuery("#urunlerindirimgenel_1").hide("medium");
        jQuery("#urunlerindirimgenel_1 input").val("0");
    });
    jQuery("#urunotvsil_1").click(function () {
        jQuery("#urunotvgenel_1").hide("medium");
        jQuery("#urunotvgenel_1 input").val("0");
    });
    jQuery("#urunoivsil_1").click(function () {
        jQuery("#urunoivgenel_1").hide("medium");
        jQuery("#urunoivgenel_1 input").val("0");
    });

    jQuery(".ara-toplam-indirimi-ekle").click(function () {
        jQuery(".ara-toplam-indirimi").show("medium");
    });
    jQuery(".ara-toplam-indirimi-sil").click(function () {
        jQuery(".ara-toplam-indirimi input").val("0");
        jQuery(".ara-toplam-indirimi").hide("medium");
    });

    jQuery(".stopaj-ekle").click(function () {
        jQuery(".stopaj").show("medium");
    });
    jQuery(".stopaj-sil").click(function () {
        jQuery(".stopaj").hide("medium");
    });

    jQuery(".tevkifat-ekle").click(function () {
        jQuery(".tevkifat").show("medium");
    });
    jQuery(".tevkifat-sil").click(function () {
        jQuery(".tevkifat").hide("medium");
    });

    jQuery(document).on('keydown', '.urunadi', function () {
        var id = this.id;
        var splitid = id.split('_');
        var index = splitid[1];
        jQuery('#' + id).autocomplete({
            source: function (request, response) {
                jQuery.ajax({
                    url: "/Offer/SearchProduct",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term, request: 1
                    },
                    beforeSend: function () {
                        jQuery("#sonuc").fadeIn().html('<img src="/assets/images/loading.gif" width="20" height="20" />');
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return { label: item.label, value: item.label };
                        }))
                    },
                    error: function (data) {
                        jQuery("input[data-id='urunid_" + index + "']").val("");
                    }
                });
            },
            select: function (event, ui) {
                jQuery(this).val(ui.item.label); // display the selected text
                var urunid = ui.item.value; // selected id to input                 
                var musteriid = jQuery(".musteriid").val(); // selected id to input
                // AJAX
                jQuery.ajax({
                    url: '/Offer/PriceSet',
                    type: 'post',
                    data: { urunid: urunid },
                    dataType: 'json',
                    success: function (response) {

                        var len = response.length;
                        if (len > 0) {
                            var fiyat = response[0];
                            $("#selectedprice").val(fiyat);
                            document.getElementById('urunfiyat_' + index).value = formatMyMoney(fiyat);
                            jQuery("#urunfiyat_" + index).trigger("keyup");
                            //var turunid = response[0]['urunid'];
                            //var turunbirim = response[0]['urunbirim'];                             
                            //var turunkdv = response[0]['urunkdv'];
                            //var turuntoplam = response[0]['uruntoplam'];
                            //var turunotv = response[0]['urunotv'];
                            //var turunoiv = response[0]['urunoiv'];								
                            //var stokmiktar = response[0]['stokmiktar'];						
                            //var stokadi = response[0]['stokadi'];						
                            //var stokuyarimiktar = response[0]['stokuyarimiktar'];								
                            //document.getElementById('urunid_'+index).value = urunid;
                            //document.getElementById('stokid_'+index).value = urunid;
                            //document.getElementById('urunbirim_'+index).value = turunbirim;
                            //document.getElementById('urunfiyat_'+index).value = formatMyMoney(turunfiyat);
                            //document.getElementById('urunkdv_'+index).value = turunkdv;
                            //document.getElementById('uruntoplam_'+index).value = formatMyMoney(turuntoplam);	
                            //document.getElementById('urunotv_'+index).value = turunotv;	
                            //document.getElementById('urunoiv_'+index).value = turunoiv;	
                            jQuery("#urunfiyat_" + index).trigger("keyup");

                            //if(parseInt(stokmiktar)<=parseInt(stokuyarimiktar)){
                            //	jQuery("#kritik-stok").show(function(){
                            //		jQuery(this).find(".danger-text-stok").html(stokadi+" isimli ürün stoklarda azalmıştır. Stok Miktarı : "+stokmiktar+" "+turunbirim);
                            //	});
                            //}

                        }
                    },
                    error: function (data) {
                        jQuery("input[data-id='urunid_" + index + "']").val("");
                    }

                });
                jQuery.ajax({
                    url: '/getDetails.php',
                    type: 'post',
                    data: { urunid: urunid, request: 3, musteriid: musteriid },
                    success: function (response) {
                        jQuery("#musteriurunmain_" + index).html(response);
                    }
                });
                return false;
            }
        });
    });

    jQuery(".close-my-button").click(function () {
        jQuery(".kritik-stok-uyari-box").hide();
    });



    jQuery(document).on('keydown', '.urunadiuretim', function () {
        var id = this.id;
        var splitid = id.split('_');
        var index = splitid[1];
        jQuery('#' + id).autocomplete({
            source: function (request, response) {
                jQuery.ajax({
                    url: "/getDetails2.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term, request: 1
                    },
                    success: function (data) {
                        response(data);
                        jQuery("#urunid_" + index).val("");
                    },
                });
            },
            select: function (event, ui) {
                jQuery(this).val(ui.item.label); // display the selected text
                var urunid = ui.item.value; // selected id to input
                var musteriid = jQuery(".musteriid").val(); // selected id to input
                // AJAX
                jQuery.ajax({
                    url: '/getDetails2.php',
                    type: 'post',
                    data: { urunid: urunid, request: 2 },
                    dataType: 'json',
                    success: function (response) {
                        var len = response.length;
                        if (len > 0) {
                            var turunbirim = response[0]['urunbirim'];
                            document.getElementById('urunid_' + index).value = urunid;
                            document.getElementById('urunbirim_' + index).value = turunbirim;
                            jQuery("#urunfiyat_" + index).trigger("keyup");
                        } else {
                            jQuery("#urunid_" + index).val("");
                        }
                    },
                });
                return false;
            }

        });
    });

    jQuery(document).on('keydown', '.mamuladiuretim', function () {
        jQuery('#mamuladi').autocomplete({
            source: function (request, response) {
                jQuery.ajax({
                    url: "/getDetails3.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term, request: 1
                    },
                    success: function (data) {
                        response(data);
                        jQuery("#mamulid").val("");
                    },
                });
            },
            select: function (event, ui) {
                jQuery(this).val(ui.item.label); // display the selected text
                var uruniduretim = ui.item.value; // selected id to input
                // AJAX
                jQuery.ajax({
                    url: '/getDetails3.php',
                    type: 'post',
                    data: { uruniduretim: uruniduretim, request: 2 },
                    dataType: 'json',
                    success: function (response) {
                        var len = response.length;
                        if (len > 0) {
                            document.getElementById('mamulid').value = uruniduretim;
                        } else {
                            jQuery("#mamulid").val("");
                        }
                    },
                });
                return false;
            }
        });
    });

    jQuery('.currency').turkLirasi();
    jQuery('.usd-tl-karsiligi').hide();
    jQuery('.eur-tl-karsiligi').hide();
    jQuery('.gbp-tl-karsiligi').hide();
    jQuery('.parabirimi').on('change', function () {
        var parabirimisecili = jQuery("option:selected", this).val();
        if (parabirimisecili == "TL") {
            jQuery('.table-tutar small').html("<i class='fa fa-turkish-lira'></i>");
            jQuery('.doviz-tl-karsiligi input').val("");
            jQuery('.usd-tl-karsiligi').hide();
            jQuery('.eur-tl-karsiligi').hide();
            jQuery('.gbp-tl-karsiligi').hide();
        } else if (parabirimisecili == "$") {
            jQuery('.table-tutar small').html("<i class='fa fa-dollar'></i>");
            jQuery('.usd-tl-karsiligi').show();
            jQuery('.eur-tl-karsiligi').hide();
            jQuery('.gbp-tl-karsiligi').hide();
        } else if (parabirimisecili == "€") {
            jQuery('.table-tutar small').html("<i class='fa fa-eur'></i>");
            jQuery('.usd-tl-karsiligi').hide();
            jQuery('.eur-tl-karsiligi').show();
            jQuery('.gbp-tl-karsiligi').hide();
        } else if (parabirimisecili == "£") {
            jQuery('.table-tutar small').html("<i class='fa fa-gbp'></i>");
            jQuery('.usd-tl-karsiligi').hide();
            jQuery('.eur-tl-karsiligi').hide();
            jQuery('.gbp-tl-karsiligi').show();
        }
    });
    jQuery('.parabirimi').on('change', function () {
        var parabirimisecilihizli = jQuery("option:selected", this).val();
        if (parabirimisecilihizli == "TL") {
            jQuery('.hizli-para-birimi').html("<i class='fa fa-turkish-lira'></i>");
            jQuery('.usd-tl-karsiligi').hide();
            jQuery('.eur-tl-karsiligi').hide();
            jQuery('.gbp-tl-karsiligi').hide();
        } else if (parabirimisecilihizli == "$") {
            jQuery('.hizli-para-birimi').html("<i class='fa fa-dollar'></i>");
            jQuery('.usd-tl-karsiligi').show();
            jQuery('.eur-tl-karsiligi').hide();
            jQuery('.gbp-tl-karsiligi').hide();
        } else if (parabirimisecilihizli == "€") {
            jQuery('.hizli-para-birimi').html("<i class='fa fa-eur'></i>");
            jQuery('.usd-tl-karsiligi').hide();
            jQuery('.eur-tl-karsiligi').show();
            jQuery('.gbp-tl-karsiligi').hide();
        } else if (parabirimisecilihizli == "£") {
            jQuery('.hizli-para-birimi').html("<i class='fa fa-gbp'></i>");
            jQuery('.usd-tl-karsiligi').hide();
            jQuery('.eur-tl-karsiligi').hide();
            jQuery('.gbp-tl-karsiligi').show();
        }
    });

    jQuery("#addmore").click(function () {
        var lastname_id = jQuery('.tr_input input[type=text]:nth-child(1)').last().attr('id');
        var split_id = lastname_id.split('_');
        var index = Number(split_id[1]) + 1;
        var html = "<tr class='tr_input'><td width='380'><input type='text' placeholder='Hizmet veya ürünü aratabilirsiniz' id='urunadi_" + index + "' name='urunadi' class='hesap urunadi form-control' autocomplete='off'><div id='urunaciklamagenel_" + index + "'><div class='input-group'><div class='input-group-addon'>AÇIKLAMA</div><input type='text' name='urunaciklamasi' placeholder='Ürün Açıklaması' class='form-control' id='urunaciklama_" + index + "'><a class='btn btn-new-1 btn-sm' id='aciklamasil_" + index + "'><i class='fa fa-times'></i></a><input type='hidden' value='' id='urunid_" + index + "' name='urun_id' /><input type='hidden' value='' id='stokid_" + index + "' name='stok_id' /></div></div><div id='urunlerindirimgenel_" + index + "'><div class='input-group'><div class='input-group-addon'>İNDİRİM</div><input type='text' name='urunindirim' placeholder='İndirim' class='hesap form-control urunindirim currency' id='urunindirim_" + index + "'><select name='urunindirimsec' class='hesap form-control urunindirimsec'><option value='1'>% İndirim</option><option value='2'>Fiyat İndirim</option></select><a class='btn btn-new-1 btn-sm' id='indirimsil_" + index + "'><i class='fa fa-times'></i></a><input type='hidden' class='satirindirim' /></div></div><div id='urunotvgenel_" + index + "'><div class='input-group'><div class='input-group-addon'>ÖTV</div><input type='text' name='urunotv' placeholder='ÖTV' id='urunotv_" + index + "' class='hesap form-control numberFormat3 currency urunotv' value='0'><a class='btn btn-new-1 btn-sm' id='urunotvsil_" + index + "'><i class='fa fa-times'></i></a><input type='hidden' class='urunotvtutar' /></div></div><div id='urunoivgenel_" + index + "'><div class='input-group'><div class='input-group-addon'>ÖİV</div><input type='text' name='urunoiv' placeholder='ÖİV' class='hesap form-control numberFormat3 currency urunoiv' id='urunoiv_" + index + "' value='0'><a class='btn btn-new-1 btn-sm' id='urunoivsil_" + index + "'><i class='fa fa-times'></i></a><input type='hidden' class='urunoivtutar' /></div></div></td><td width='80'><input type='text' placeholder='1' name='urunmiktar' id='urunmiktar_" + index + "' class='hesap form-control urunmiktar currency' value='1'></td><td width='105'><input type='text' placeholder='Adet,kg..' name='urunbirim' id='urunbirim_" + index + "' class='urunbirim form-control'></td><td  width='150'><div class='input-group'><input type='text' placeholder='Ürün Fiyatı' name='urunfiyat' id='urunfiyat_" + index + "' class='hesap form-control urunfiyat currency'><input type='hidden' class='urunfiyatmiktarli' /><div class='input-group-addon musteri-urun-button' id='musteriurunbutton_" + index + "'><i class='fa fa-info-circle'></i><div style='display:block;position:relative'><div class='musteri-urun-main' id='musteriurunmain_" + index + "'></div></div></div></div></td><td width='105'><select name='urunkdv' class='hesap form-control urunkdv' id='urunkdv_" + index + "'><option value='18'>18% KDV</option><option value='8'>8% KDV</option><option value='1'>1% KDV</option><option value='0'>0% KDV</option></select><input type='hidden' class='urunkdvfiyat' /></td><td width='150'><input type='text' placeholder='Toplam' name='uruntoplam' id='uruntoplam_" + index + "' class='hesap form-control currency uruntoplam' ></td><td width='120'><a class='btn btn-new-1 btn-sm satir-sil removefield'><i class='fa fa-times'></i></a><div class='btn-group' role='group'><div class='btn-group' role='group'><a class='btn btn-new-2 btn-sm dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fa fa-plus'></i><span class='caret'></span></a><ul class='dropdown-menu'><li><a id='aciklamaekle_" + index + "'>AÇIKLAMA EKLE</a></li><li><a id='indirimekle_" + index + "'>İNDİRİM EKLE</a></li> <li><a id='urunotvekle_" + index + "'>ÖTV EKLE</a></li><li><a id='urunoivekle_" + index + "'>ÖİV EKLE</a></li><li class='switching-toplamdan-bul'><label class='switch switch-default switch-warning-outline-alt switch-pill mr-2 switch-sm'><input class='switch-input toplamdanbul' type='checkbox' name='toplamdanbul' value='1'> <span class='switch-label'></span> <span class='switch-handle'></span> </label>TOPLAMDAN BUL</li></ul></div></div></td></tr>";

        jQuery('.multi-fields tbody').append(html);
        jQuery("#musteriurunbutton_" + index).hover(function () {
            jQuery(this).find("#musteriurunmain_" + index).show();
        }, function () {
            jQuery(this).find("#musteriurunmain_" + index).hide();
        });
        jQuery("#urunaciklamagenel_" + index).hide();
        jQuery("#urunlerindirimgenel_" + index).hide();
        jQuery("#urunotvgenel_" + index).hide();
        jQuery("#urunoivgenel_" + index).hide();
        jQuery(".removefield").click(function () {
            jQuery(this).parent().parent('.tr_input').remove();
        });
        jQuery(".birim-ekle").click(function () {
            jQuery(".urunler-birim").show("medium");
        });
        jQuery("#aciklamaekle_" + index).click(function () {
            jQuery("#urunaciklamagenel_" + index).show("medium");
        });
        jQuery("#indirimekle_" + index).click(function () {
            jQuery("#urunlerindirimgenel_" + index).show("medium");
        });
        jQuery("#urunotvekle_" + index).click(function () {
            jQuery("#urunotvgenel_" + index).show("medium");
        });
        jQuery("#urunoivekle_" + index).click(function () {
            jQuery("#urunoivgenel_" + index).show("medium");
        });

        jQuery("#aciklamasil_" + index).click(function () {
            jQuery("#urunaciklamagenel_" + index).hide("medium");
            jQuery("#urunaciklamagenel_" + index + " input").val("");
        });
        jQuery("#indirimsil_" + index).click(function () {
            jQuery("#urunlerindirimgenel_" + index).hide("medium");
            jQuery("#urunlerindirimgenel_" + index + " input").val("0");
        });
        jQuery("#urunotvsil_" + index).click(function () {
            jQuery("#urunotvgenel_" + index).hide("medium");
            jQuery("#urunotvgenel_" + index + " input").val("0");
        });
        jQuery("#urunoivsil_" + index).click(function () {
            jQuery("#urunoivgenel_" + index).hide("medium");
            jQuery("#urunoivgenel_" + index + " input").val("0");
        });
        jQuery('#urunmiktar_' + index).turkLirasi();
        jQuery('#urunfiyat_' + index).turkLirasi();
        jQuery('#uruntoplam_' + index).turkLirasi();
        jQuery('#urunindirim_' + index).turkLirasi();
    });

    jQuery(".satir-indirimi-genel").hide();
    jQuery(".brut-toplam-genel").hide();
    jQuery(".otv-toplam-genel").hide();
    jQuery(".oiv-toplam-genel").hide();



    var toplamhesapla = function () {
        var ara_toplam = 0,
            satir_indirimi = 0,
            ara_toplam_indirimi = 0,
            ara_toplam_indirimi_sec = 0,
            brut_toplam = 0,
            otv_toplam = 0,
            oiv_toplam = 0,
            kdv_toplam = 0,
            genel_toplam,
            kurbilgisi1 = 0,
            kurbilgisi2 = 0,
            kurbilgisi3 = 0,
            kurtoplam1 = 0,
            kurtoplam2 = 0,
            kurtoplam3 = 0;



        ara_toplam_indirimi = parseFloat(jQuery('.ara_toplam_indirimi .currency').val().replace(".", "").replace(",", "."));
        ara_toplam_indirimi_sec = jQuery('.ara_toplam_indirimi select').val();

        jQuery(".urunfiyatmiktarli").each(function () {
            ara_toplam += +(jQuery(this).val());
        });
        jQuery(".satirindirim").each(function () {
            satir_indirimi += +(jQuery(this).val());
        });
        jQuery(".urunotvtutar").each(function () {
            otv_toplam += +(jQuery(this).val());
        });
        jQuery(".urunoivtutar").each(function () {
            oiv_toplam += +(jQuery(this).val());
        });
        jQuery(".urunkdvfiyat").each(function () {
            kdv_toplam += +(jQuery(this).val());
        });

        if (ara_toplam_indirimi_sec == 1) {
            var ara_toplam_indirim_orani = ara_toplam_indirimi;
        } else {
            var ara_toplam_indirim_orani = (100 * ara_toplam_indirimi) / (ara_toplam - satir_indirimi);
        }

        brut_toplam = (ara_toplam - satir_indirimi) - (((ara_toplam - satir_indirimi) * ara_toplam_indirim_orani) / 100);
        otv_toplam = otv_toplam - (otv_toplam * ara_toplam_indirim_orani) / 100;
        oiv_toplam = oiv_toplam - (oiv_toplam * ara_toplam_indirim_orani) / 100;
        kdv_toplam = kdv_toplam - (kdv_toplam * ara_toplam_indirim_orani) / 100;
        genel_toplam = brut_toplam + otv_toplam + oiv_toplam + kdv_toplam;

        if ((satir_indirimi != 0) || (ara_toplam_indirimi != 0)) {
            jQuery(".satir-indirimi-genel").show("medium");
            jQuery(".brut-toplam-genel").show("medium");
        } else {
            jQuery(".satir-indirimi-genel").hide("medium");
            jQuery(".brut-toplam-genel").hide("medium");
        }
        if (otv_toplam != 0) {
            jQuery(".otv-toplam-genel").show("medium");
        } else {
            jQuery(".otv-toplam-genel").hide("medium");
        }
        if (oiv_toplam != 0) {
            jQuery(".oiv-toplam-genel").show("medium");
        } else {
            jQuery(".oiv-toplam-genel").hide("medium");
        }
        kurbilgisi1 = jQuery(".usd-tl-karsiligi>.kur input").val();
        kurbilgisi2 = jQuery(".eur-tl-karsiligi>.kur input").val();
        kurbilgisi3 = jQuery(".gbp-tl-karsiligi>.kur input").val();
        kurtoplam1 = kurbilgisi1 * genel_toplam;
        kurtoplam2 = kurbilgisi2 * genel_toplam;
        kurtoplam3 = kurbilgisi3 * genel_toplam;
        jQuery(".ara-toplam>span").html(formatMyMoney(ara_toplam.toFixed(2)));
        jQuery(".ara-toplam>input").val(formatMyMoney(ara_toplam.toFixed(2)));
        jQuery(".satir-indirimi>span").html(formatMyMoney(satir_indirimi.toFixed(2)));
        jQuery(".satir-indirimi>input").val(formatMyMoney(satir_indirimi.toFixed(2)));
        jQuery(".brut-toplam>span").html(formatMyMoney(brut_toplam.toFixed(2)));
        jQuery(".brut-toplam>input").val(formatMyMoney(brut_toplam.toFixed(2)));
        jQuery(".otv-toplam>span").html(formatMyMoney(otv_toplam.toFixed(2)));
        jQuery(".otv-toplam>input").val(formatMyMoney(otv_toplam.toFixed(2)));
        jQuery(".oiv-toplam>span").html(formatMyMoney(oiv_toplam.toFixed(2)));
        jQuery(".oiv-toplam>input").val(formatMyMoney(oiv_toplam.toFixed(2)));
        jQuery(".ara-kdv>span").html(formatMyMoney(kdv_toplam.toFixed(2)));
        jQuery(".ara-kdv>input").val(formatMyMoney(kdv_toplam.toFixed(2)));
        jQuery(".genel-toplam>span").html(formatMyMoney(genel_toplam.toFixed(2)));
        jQuery(".genel-toplam>input").val(formatMyMoney(genel_toplam.toFixed(2)));
        jQuery(".usd-tl-karsiligi>.kur>span").html(formatMyMoney(kurtoplam1.toFixed(2)));
        jQuery(".eur-tl-karsiligi>.kur>span").html(formatMyMoney(kurtoplam2.toFixed(2)));
        jQuery(".gbp-tl-karsiligi>.kur>span").html(formatMyMoney(kurtoplam3.toFixed(2)));
    }

    var urun_miktar = 0,
        urun_birim_fiyat = 0,
        urun_indirim = 0,
        urun_indirim_tipi = 0,
        urun_otv = 0,
        urun_oiv = 0,
        urun_kdv = 0,
        bosfiyat = 0,
        urun_indirim_yuzde = 0,
        urun_indirim_sabit = 0,
        urun_toplam_fiyat = 0,
        urun_otvli_fiyat = 0,
        urun_sade_otv = 0,
        urun_oivli_fiyat = 0,
        urun_sade_oiv = 0,
        urun_sade_kdv = 0;

    var uruntoplamhesapla = function () {
        var jQuerytr = jQuery(this).closest("tr");
        if (jQuerytr.find(".toplamdanbul").prop('checked') == false) {


            var urun_miktar = parseFloat(jQuerytr.find('.urunmiktar').val().replace(".", "").replace(",", ".")),
                urun_birim_fiyat = parseFloat(jQuerytr.find('.urunfiyat').val().replace(".", "").replace(",", ".")),
                urun_indirim = parseFloat(jQuerytr.find('.urunindirim').val().replace(".", "").replace(",", ".")),
                urun_indirim_tipi = jQuerytr.find('.urunindirimsec').val(),
                urun_otv = parseFloat(jQuerytr.find('.urunotv').val().replace(".", "").replace(",", ".")),
                urun_oiv = parseFloat(jQuerytr.find('.urunoiv').val().replace(".", "").replace(",", ".")),
                urun_kdv = parseFloat(jQuerytr.find('.urunkdv').val().replace(".", "").replace(",", ".")),
                bosfiyat = urun_miktar * urun_birim_fiyat,
                urun_indirim_yuzde = (bosfiyat - ((bosfiyat * urun_indirim) / 100)),
                urun_indirim_sabit = bosfiyat - urun_indirim,
                urun_toplam_fiyat = 0;

            if (urun_indirim_tipi == 1) {
                var urun_indirimli_fiyati = urun_indirim_yuzde;
            } else {
                var urun_indirimli_fiyati = urun_indirim_sabit;
            }

            var urun_otvli_fiyat = urun_indirimli_fiyati + ((urun_indirimli_fiyati * urun_otv) / 100),
                urun_sade_otv = ((urun_indirimli_fiyati * urun_otv) / 100), // inputtan aşağıya yazılacak miktarı
                urun_oivli_fiyat = urun_otvli_fiyat + ((urun_otvli_fiyat * urun_oiv) / 100),
                urun_sade_oiv = ((urun_otvli_fiyat * urun_oiv) / 100),
                urun_toplam_fiyat = urun_oivli_fiyat + ((urun_otvli_fiyat * urun_kdv) / 100),
                urun_sade_kdv = ((urun_otvli_fiyat * urun_kdv) / 100);

            jQuerytr.find('.urunfiyatmiktarli').val((urun_miktar * urun_birim_fiyat).toFixed(2));
            jQuerytr.find('.satirindirim').val((bosfiyat - urun_indirimli_fiyati).toFixed(2));
            jQuerytr.find('.uruntoplam').val(formatMyMoney((urun_toplam_fiyat.toFixed(2)).toString()));
            jQuerytr.find('.urunotvtutar').val(((urun_sade_otv.toFixed(2)).toString()));
            jQuerytr.find('.urunoivtutar').val(((urun_sade_oiv.toFixed(2)).toString()));
            jQuerytr.find('.urunkdvfiyat').val(((urun_sade_kdv.toFixed(2)).toString()));
        } else {
            var urun_miktar = parseFloat(jQuerytr.find('.urunmiktar').val().replace(".", "").replace(",", ".")),
                urun_birim_fiyat = parseFloat(jQuerytr.find('.urunfiyat').val().replace(".", "").replace(",", ".")),
                urun_toplam_fiyat = parseFloat(jQuerytr.find('.uruntoplam').val().replace(".", "").replace(",", ".")),
                urun_kdv = parseFloat(jQuerytr.find('.urunkdv').val().replace(".", "").replace(",", ".")),
                urun_birim_fiyat = ((((urun_toplam_fiyat / urun_miktar) * 100) / (urun_kdv + 100))),
                urun_sade_kdv = ((urun_birim_fiyat * urun_kdv) / 100) * urun_miktar;
            jQuerytr.find('.urunfiyatmiktarli').val((urun_miktar * urun_birim_fiyat).toFixed(2));
            jQuerytr.find('.urunfiyat').val(formatMyMoney((urun_birim_fiyat.toFixed(2)).toString()));
            jQuerytr.find('.urunkdvfiyat').val(((urun_sade_kdv.toFixed(2)).toString()));
        }
        toplamhesapla();
    };


    var hesaplamafonksiyonlar = function () {
        jQuery('.urunindirim').change(uruntoplamhesapla);
        jQuery('.urunindirimsec').change(uruntoplamhesapla);
        jQuery('.urunotv').keyup(uruntoplamhesapla);
        jQuery('.urunoiv').keyup(uruntoplamhesapla);
        jQuery('.urunmiktar').change(uruntoplamhesapla);
        jQuery('.urunbirim').change(uruntoplamhesapla);
        jQuery('.urunfiyat').keyup(uruntoplamhesapla);
        jQuery('.urunkdv').change(uruntoplamhesapla);
        jQuery('.uruntoplam').change(uruntoplamhesapla);
        jQuery('.satir-sil').click(uruntoplamhesapla);
        jQuery('.ara_toplam_indirimi select').change(toplamhesapla);
        jQuery('.ara_toplam_indirimi input').change(toplamhesapla);
        jQuery('.urunotv').next("a").click(uruntoplamhesapla);
        jQuery('.urunoiv').next("a").click(uruntoplamhesapla);
    };

    jQuery(".form-teklif").ready(function () {
        hesaplamafonksiyonlar();
    });
    jQuery('#addmore').click(function () {
        hesaplamafonksiyonlar();
    });
    /* jQuery('.urunadi').click(function(){
        console.log("def");
        uruntoplamhesapla();
    });	 */



    jQuery("#addmoremusteri").click(function () {
        var lastname_id = jQuery('.tr_input_musteri input[type=text]:nth-child(1)').last().attr('id');
        var split_id = lastname_id.split('_');
        var index = Number(split_id[1]) + 1;
        var html = "<tr class='tr_input_musteri'><td width='280'><input type='text' placeholder='Yetkili Kişinin Adı ve Soyadı' id='yetikiliadi_" + index + "' name='yetkiliadi[]' class='form-control'></td><td width='200'><input type='text' placeholder='E-posta adresi' id='yetkilieposta_" + index + "' name='yetkilieposta[]' class='form-control'></td><td width='200'><input type='text' placeholder='Telefon numarası' id='yetkilitelefon_" + index + "' name='yetkilitelefon[]' class='form-control'></td><td width='320'><textarea rows='4' placeholder='Kişi hakkında notlar' id='yetkilinotlar_" + index + "' name='yetkilinotlar[]' class='form-control'></textarea></td><td width='20'><a id='addmusteriToCustomer' class='add-field btn btn-new-1 btn-sm'>&nbsp;Ekle&nbsp;</a></td><td width='20'><a class='btn btn-new-1 btn-sm satir-sil removefield' id='removemusteri_" + index + "'><i class='fa fa-times'></i></a></td></tr>";
        jQuery('.multi-fields tbody').append(html);
        jQuery(".removefield").click(function () {
            jQuery(this).parent().parent('.tr_input_musteri').remove();
        });
    });



    jQuery("#addmore2").click(function () {
        var lastname_id = jQuery('.tr_input input[type=text]:nth-child(1)').last().attr('id');
        var split_id = lastname_id.split('_');
        var index = Number(split_id[1]) + 1;
        var html = "<tr class='tr_input'><td width='380'><input type='text' placeholder='Hizmet veya ürünü aratabilirsiniz' id='urunadi_" + index + "' name='urunadi[]' class='hesap urunadiuretim form-control' autocomplete='off'><input type='hidden' value='' id='urunid_" + index + "' name='urun_id[]' /></td><td width='80'><input type='text' placeholder='1' name='urunmiktar[]' id='urunmiktar_" + index + "' class='hesap form-control numberFormat4 urunmiktar currency' value='1'></td><td width='105'><input type='text' placeholder='Adet,kg..' name='urunbirim[]' class='urunbirim form-control' id='urunbirim_" + index + "'></td><td width='20'><a class='btn btn-new-1 btn-sm satir-sil remove-field' id='removeinputuretimurun_" + index + "'><i class='fa fa-times'></i></a></td></tr>";
        jQuery('.multi-fields tbody').append(html);
        jQuery("#removeinputuretimurun_" + index).click(function () {
            jQuery(this).parent().parent('.tr_input').remove();
        });
        jQuery('#urunmiktar_' + index).turkLirasi();
    });

    jQuery('body').on('keyup change', 'input, select', function () {
        jQuery(".form-hizli input, .form-hizli select").on('keyup change', function (event) {
            var gider_genel_toplam,
                gider_urunkdv_orani,
                hizli_genel_toplam,
                kurbilgisi,
                kurtoplam1,
                kurtoplam2,
                kurtoplam3,
                hizli_kdv_toplam,
                kurbilgisi1,
                kurbilgisi2,
                kurbilgisi3;
            gider_genel_toplam = parseFloat(jQuery('.gider_genel_toplam').val().replace(".", "").replace(",", ".")),
                gider_urunkdv_orani = parseFloat(jQuery('.gider_urunkdv_orani').val().replace(".", "").replace(",", ".")),
                hizli_kdv_toplam = gider_genel_toplam - (gider_genel_toplam / (1 + (gider_urunkdv_orani / 100)));
            jQuery(".gider_urunkdv_tutari").val(formatMyMoney(hizli_kdv_toplam.toFixed(2)));
            kurbilgisi1 = jQuery(".usd-tl-karsiligi>.kur>input").val();
            kurbilgisi2 = jQuery(".eur-tl-karsiligi>.kur>input").val();
            kurbilgisi3 = jQuery(".gbp-tl-karsiligi>.kur>input").val();
            kurtoplam1 = kurbilgisi1 * gider_genel_toplam;
            kurtoplam2 = kurbilgisi2 * gider_genel_toplam;
            kurtoplam3 = kurbilgisi3 * gider_genel_toplam;
            jQuery(".usd-tl-karsiligi>.kur>span").html(formatMyMoney(kurtoplam1.toFixed(2)));
            jQuery(".eur-tl-karsiligi>.kur>span").html(formatMyMoney(kurtoplam2.toFixed(2)));
            jQuery(".gbp-tl-karsiligi>.kur>span").html(formatMyMoney(kurtoplam3.toFixed(2)));
        });
    });

    jQuery('body').on('keyup change', 'input, select', function () {
        jQuery(".form-uretim-fisi input, .form-uretim-fisi select").on('keyup change', function (event) {
            var genel_miktar = parseFloat(jQuery('.genelmiktar').val().replace(".", "").replace(",", "."));
            jQuery('input[id^="birimbasituketilen_"]').each(function () {
                var toplam_tuketilen = genel_miktar * $(this).val();
                jQuery(this).parent().next().next().next().find(".span_uretilenmamulmiktari").html(genel_miktar);
                jQuery(this).parent().next().next().next().find(".input_uretilenmamulmiktari").val(genel_miktar);
                jQuery(this).parent().next().next().next().next().find(".span_toplamtuketilen").html(toplam_tuketilen);
                jQuery(this).parent().next().next().next().next().find(".input_toplamtuketilen").val(toplam_tuketilen);
            });
        });
    });

    jQuery(".kayitlicekler").click(function () {
        jQuery(".tahsilat-vade-tarihi").hide();
        jQuery(".tahsilat-meblag").hide();
        jQuery(".tahsilat-aciklama").hide();
    });
    jQuery(".yenicek").click(function () {
        jQuery(".tahsilat-vade-tarihi").show();
        jQuery(".tahsilat-meblag").show();
        jQuery(".tahsilat-aciklama").show();
    });
    jQuery(".teklifnotsilme").click(function () {
        var href = jQuery(this).attr("data-id");
        jQuery(".silineceknotid").val(href);
    });
    jQuery("#teklifolustur").click(function () {
        var teklif_id = jQuery(this).attr("data-id");
        jQuery("#teklifdosyasi").load("/satis-teklif-olustur.php?faturaid=" + teklif_id);
    });
    jQuery("#siparisolustur").click(function () {
        var siparis_id = jQuery(this).attr("data-id");
        jQuery("#siparisdosyasi").load("/satis-siparisler-olustur.php?faturaid=" + siparis_id);
    });
    jQuery("#siparisolustururetimekrani").click(function () {
        var siparis_id = jQuery(this).attr("data-id");
        jQuery("#siparisdosyasiuretim").load("/uretim-ekran-siparisler-olustur.php?faturaid=" + siparis_id);
    });
    jQuery(".tooltip-button").hover(function () {
        jQuery(this).find(".tooltip-main").show();
    }, function () {
        jQuery(this).find(".tooltip-main").hide();
    });
    jQuery("#musteriurunbutton_1").hover(function () {
        jQuery(this).find("#musteriurunmain_1").show();
    }, function () {
        jQuery(this).find("#musteriurunmain_1").hide();
    });
    jQuery(".fileinput").change(function () {
        var fileinputname = jQuery(this).val().replace(/C:\\fakepath\\/i, '')
        jQuery(".filename").html("<p><strong><small> Yüklenecek Fotoğraf : </small></strong><em>" + fileinputname + "</em></p>");
    });
    jQuery(".silme-bilgi").hide();
    jQuery(".file-delete").click(function () {
        jQuery(".mevcutresim").val("");
        jQuery(".mevcut-resim").hide(500);
        jQuery(".mevcut-resim-sil-buton").hide(500);
        jQuery(".silme-bilgi").show(500);
    });

    $("#all_sayfa_acik").click(function () {
        $("#kullaniciyetkiler table>tbody>tr input[name*='sayfa_acik[]']:checkbox").not(this).prop('checked', this.checked);
    });
    $("#all_sayfa_goruntuleme").click(function () {
        $("#kullaniciyetkiler table>tbody>tr input[name*='sayfa_goruntuleme[]']:checkbox").not(this).prop('checked', this.checked);
    });
    $("#all_sayfa_duzenleme").click(function () {
        $("#kullaniciyetkiler table>tbody>tr input[name*='sayfa_duzenleme[]']:checkbox").not(this).prop('checked', this.checked);
    });




    $('.editable').on('click', function () {
        var that = $(this);
        if (that.find('textarea').length > 0) {
            return;
        }
        var currentText = that.text();

        var pagestyle = $(this).attr("page-style");

        if (pagestyle == 1) {
            var $input = $('<textarea class="form-control editable-form"></textarea>').val(currentText)
                .css({
                    'position': 'absolute',
                    top: that.position().top,
                    left: "1%",
                    width: "98%",
                    height: that.height() + 15,
                    opacity: 1,
                    padding: "3px",
                });
        } else {
            var $input = $('<textarea class="form-control editable-form"></textarea>').val(currentText)
                .css({
                    'position': 'absolute',
                    top: that.position().top,
                    left: that.position().left,
                    width: that.width(),
                    height: that.height() + 12,
                    opacity: 1,
                });
        }



        $(this).append($input);

        var idno = $(this).attr("data-id");
        var pageid = $(this).attr("page-id");


        $(document).click(function (event) {
            if (!$(event.target).closest('.editable').length) {
                if ($input.val()) {
                    that.text($input.val());
                }
                that.find('input').remove();
                var konu = "value=" + $input.val();
                jQuery.ajax({
                    type: "post",
                    url: "/ajax-notlar.php?id=" + idno + "&pageid=" + pageid,
                    data: konu,
                    dataType: "text",
                    success: function (sonuc) {
                        $("td").filter("[data-id='" + idno + "']").each(function () {
                            $(this).html(sonuc);
                        });

                    }
                });

            }
        });
    });




    jQuery(".kosulsablonu").on('change', function () {
        var kosulsablonu = jQuery(this).val();
        var cesit = jQuery(this).attr("data-type");
        jQuery.ajax({
            type: 'post',
            url: '/ajax-sablon-sec.php',
            data: { kosulsablonu, cesit },
            success: function (data) {
                if (cesit == 1) {
                    jQuery("textarea[name='kosullar']").val(data);
                }
            }

        });
    });

    jQuery(".stock-add a").on('click', function () {
        var inputmiktar = jQuery(".stock-add-input").val();
        var inputid = jQuery(".stock-add-input-id").val();
        var cesit = jQuery(this).attr("data-type");
        jQuery.ajax({
            type: 'post',
            url: '/ajax-stok-add.php',
            data: { inputid, inputmiktar, cesit },
            success: function (data) {
                //jQuery(".view-stock").html(data);	
                jQuery(location).attr('href', '/stoklar/goruntuleme/' + inputid)
            }

        });
    });


    jQuery(".stock-add-uretim a").on('click', function () {
        var inputmiktar = jQuery(".stock-add-input").val();
        var inputid = jQuery(".stock-add-input-id").val();
        var cesit = jQuery(this).attr("data-type");
        jQuery.ajax({
            type: 'post',
            url: '/ajax-stok-add.php',
            data: { inputid, inputmiktar, cesit },
            success: function (data) {
                //jQuery(".view-stock").html(data);	
                jQuery(location).attr('href', '/uretim-ekrani-stoklar/goruntuleme/' + inputid)
            }

        });
    });







});

function nl2br(str, is_xhtml) {

    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '';

    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');

}

var firmailceAL = function () {
    jQuery('.firma-ilce-al select').change(function () {
        var firmail = jQuery(".firma-il-al option:selected").text();
        var firmaadresi = jQuery(".firma-adres-al").val();
        var firmailveadres = firmaadresi + "," + firmail;
        jQuery("#address").val(firmailveadres);
        jQuery("#submit").trigger("click");


    });
}




function IlceleriAl() {
    jQuery("#semti").html('<select id="semt"><option value="0">- Semt Seçiniz -</option></select>');
    il = jQuery("#il").val();
    jQuery.ajax({
        type: 'POST',
        url: '/search-ilce.php',
        data: 'il=' + il,
        success: function (msg) {
            jQuery('#ilcesi').html(msg);
        }
    });
}
function IlceleriAl2() {
    jQuery("#semti2").html('<select id="semt2"><option value="0">- Semt Seçiniz -</option></select>');
    il2 = jQuery("#il2").val();
    jQuery.ajax({
        type: 'POST',
        url: '/search-ilce2.php',
        data: 'il2=' + il2,
        success: function (msg) {
            jQuery('#ilcesi2').html(msg);
            firmailceAL();
        }
    });

}