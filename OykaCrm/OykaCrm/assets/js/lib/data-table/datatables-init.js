﻿(function ($) {
    //    "use strict";


    /*  Data Table
    -------------*/

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
"date-uk-pre": function ( a ) {
    var ukDatea = a.split('.');
    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
},

"date-uk-asc": function ( a, b ) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
},

"date-uk-desc": function ( a, b ) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
}
} );



    $('#bootstrap-data-table').DataTable({
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
    });



    $('#bootstrap-data-table-export').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ]
                 },
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ]
                 },
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ]
                },
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'print',
				 exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ],
                 },
				title: $("#yazdirmabaslik").html()
			}
			
        ]
		
		
    });
	
	$('#bootstrap-data-table-export-2').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
         buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	$('#bootstrap-data-table-export-3').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
			null,
			null,
		  ],
         buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	
	
	
	$('#bootstrap-data-table-export-4').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	$('#bootstrap-data-table-export-5').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	$('#bootstrap-data-table-export-6').DataTable({
        dom: 'lBfrtip',
		"aaSorting": [],
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
         buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	$('#bootstrap-data-table-export-7').DataTable({
        dom: 'lBfrtip',
		"aaSorting": [],
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
         buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	 $('#bootstrap-data-table-export-8').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ]
                 },
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ]
                 },
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ]
                },
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'print',
				 exportOptions: {
                  
                    columns: [ 1, 2, 3, 4, 5 ],
                 },
				title: $("#yazdirmabaslik").html()
			}
			
        ]
		
		
    });
	
	

	$('#bootstrap-data-table-export-10').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
		  ],
         buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-11').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[20, 30, 50, -1], [20, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-12').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });

	$('#bootstrap-data-table-export-13').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			{ "sType": "date-uk" },
			{ "sType": "date-uk" },
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	
	$('#bootstrap-data-table-export-14').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			{ "sType": "date-uk" },
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-15').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-16').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-17').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-18').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-19').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			{ "sType": "date-uk" },
			null,
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-20').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-21').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-22').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	$('#bootstrap-data-table-export-23').DataTable({
        dom: 'lBfrtip',
        lengthMenu: [[15, 30, 50, -1], [15, 30, 50, "All"]],
		columns: [
			null,
			null,
			null,
			null,
			{ "sType": "date-uk" },
			null,
			null,
			null,
			null,
		  ],
        buttons: [
			'copy',
			{
				extend: 'csv',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'excel',
				title: $("#yazdirmabaslik").html()
			},
			{
				extend: 'pdf',
				title: $("#yazdirmabaslik").html()
			},
			'print'
        ]
    });
	
	

	
	$('#row-select').DataTable( {
			initComplete: function () {
				this.api().columns().every( function () {
					var column = this;
					var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
	 
							column
								.search( val ? '^'+val+'$' : '', true, false )
								.draw();
						} );
	 
					column.data().unique().sort().each( function ( d, j ) {
						select.append( '<option value="'+d+'">'+d+'</option>' )
					} );
				} );
			}
		} );






})(jQuery);