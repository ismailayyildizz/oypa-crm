﻿function ProductSearch() {

    var searchkey = $("#ProductSearchBox").val();
    console.log(searchkey);
    $.ajax({
        method: "GET",
        url: "/Offer/SearchProduct",
        data: { id: searchkey },
        beforeSend: function () {

        }
    }).done(function (d) {
        $("#FoundedProducts").html(d);

    }).always(function () {

    });
};


function AppProduct(id) {
    console.log(id);
    var OfferIDx = $("#hidOfferID").val();
    console.log(OfferIDx);
    $.ajax({
        method: "POST",
        url: "/Offer/AddProduct",
        data: { id: id, OfferID: OfferIDx },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-home").html(d);

    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};

function AddProduct(id) {
    console.log(id);
    var OfferIDx = $("#hidOfferID").val();
    console.log(OfferIDx);
    $.ajax({
        method: "Get",
        url: "/Offer/AddProductForMachine",
        data: { id: id, OfferID: OfferIDx },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-profile").html(d);

    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};

function UpdateOfferRow(rowid) {
    var productName = $("#ProductName" + rowid).val();
    var quantity = $("#Quantity" + rowid).val();
    var unit = $("#Unit" + rowid).val();
    var unitPrice = $("#UnitPrice" + rowid).val();
    var costprice = $("#CostPrice" + rowid).val();
    var rate = $("#profitRate").val();


    //var discountRate = $("#DiscountRate"+rowid).val();

    $.ajax({
        method: "GET",
        url: "/Offer/UpdateOfferRow",
        data: { rowid: rowid, productName: productName, quantity: quantity, unit: unit, unitPrice: unitPrice, costPrice: costprice },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });

        }
    }).done(function (d) {
        $("#nav-home").html(d);


    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};

function UpdateOfferRowMD(rowid) {
    var productName = $("#ProductNameMD" + rowid).val();
    var quantity = $("#QuantityMD" + rowid).val();
    var unit = $("#UnitMD" + rowid).val();
    var unitPrice = $("#UnitPriceMD" + rowid).val();
    var costprice = $("#CostPriceMD" + rowid).val();
    //var rate = $("#profitRateMD").val();
    //amortismanlari da ekle


    //var depreciation = $("#Depreciation" + rowid).val();

    //if (depreciation != null && depreciation != "") {
    //    document.getElementById("MonthCountMD" + rowid).readOnly = false;
    //} else {
    //    document.getElementById("MonthCountMD" + rowid).readOnly = true;
    //}

    //var monthlyDepreciation = $("#MonthlyDepreciation").val();
    var monthCount = $("#MonthCountMD" + rowid).val();

    $.ajax({
        method: "GET",
        url: "/Offer/UpdateOfferRowForMachine",
        data: { rowid: rowid, productName: productName, quantity: quantity, unit: unit, unitPrice: unitPrice, costPrice: costprice, /*depreciation: depreciation,*/ monthCount: monthCount },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });

        }
    }).done(function (d) {
        $("#nav-profile").html(d);


    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};

function UpdateOfferPositions(id) {

    var brut = $("#Brutucret" + id).val();
    var offerid = $("#positionsquantity" + id).val();
    alert(offerid);
    alert(brut);
    alert(id);

    $.ajax({
        method: "GET",
        url: "/Offer/SearchPositionsPartial",
        data: { offerid: offerid, id: id },
        beforeSend: function () {


        }
    }).done(function () {



    }).always(function () {
    });
};

function AddProfitRate(offerID) {
    var rate = $("#profitRate").val();
    $.ajax({
        method: "GET",
        url: "/Offer/AddProfitRate",
        data: { offerid: offerID, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-home").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddProfitRateHumanResources(offerID) {
    var rate = $("#profitRateHumanResources").val();
    $.ajax({
        method: "GET",
        url: "/Offer/AddProfitRateHumanResources",
        data: { offerid: offerID, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-contact").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddProfitRateForOther(offerID) {

    var rate = $("#profitRateOther").val();

    $.ajax({
        method: "GET",
        url: "/Offer/AddProfitRateForOther",
        data: { offerid: offerID, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-other2").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddProfitRateForMachine(offerID) {

    var rate = $("#profitRateforMAD").val();

    $.ajax({
        method: "GET",
        url: "/Offer/AddProfitRateForMachine",
        data: { offerid: offerID, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-profile").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddManagementExpenseRateForHR(offerId) { // isletme gideri ik
    var rate = $("#managementExpenseHR").val();

    $.ajax({
        method: "GET",
        url: "/Offer/AddManagementExpenseRateForHR",
        data: { offerID: offerId, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-contact").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddManagementExpenseRateForProduct(offerId) { //isletme gideri malzeme/stok

    var rate = $("#managementExpenseProduct").val();

    $.ajax({
        method: "GET",
        url: "/Offer/AddManagementExpenseRateForProduct",
        data: { offerID: offerId, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-home").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });

}

function AddManagementExpenseRateForOther(offerId) {

    var rate = $("#managementExpenseOther").val();

    $.ajax({
        method: "GET",
        url: "/Offer/AddManagementExpenseRateForOther",
        data: { offerID: offerId, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-other2").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddManagementExpenseRateForMAD(offerId) {

    var rate = $("#managementExpenseMAD").val();

    $.ajax({
        method: "GET",
        url: "/Offer/AddManagementExpenseRateForMachine",
        data: { offerID: offerId, profitRate: rate },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-profile").html(d);
        RefreshOfferFooter();

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
}

function AddOtherRows(rowID) {

    var desc = $("#otherDescription").val();
    var price = $("#otherPrice").val();

    if (desc == "" || price == "") {
        bootbox.alert({
            size: "small",
            message: "Lütfen açıklama ve tutarı giriniz!",
            buttons: {
                ok: {
                    label: 'Tamam',
                    className: 'btn-success'
                },
            },
            callback: function () { /* your callback code */ }
        })
    }
    else {
        $.ajax({
            method: "GET",
            url: "/Offer/AddOfferOthersRow",
            data: { offerID: rowID, description: desc, price: price },
            beforeSend: function () {
                $.blockUI({ message: $('#domMessage') });
            }
        }).done(function (d) {
            if (d.failed) {
                $("#warningModal2 .modal-body").html(d.message);
                $("#warningModal2").modal('show');
            }
            else {
                $("#nav-other2").html(d);
                $("#otherDescription").val('');
                $("#otherPrice").val('');
            }


        }).always(function () {
            RefreshOfferFooter();
            $.unblockUI();
        });
    }


}

function RemoveOthersRow(rowId) {
    bootbox.confirm({
        message: "Silmek istediğinizden emin misiniz?",
        buttons: {
            confirm: {
                label: 'Evet',
                className: 'btn-success'
            },
            cancel: {
                label: 'Hayır',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    method: "GET",
                    url: "/Offer/RemoveOfferOthersRow",
                    data: { rowID: rowId },
                    beforeSend: function () {
                        $.blockUI({ message: $('#domMessage') });
                    }
                }).done(function (d) {
                    if (d.failed) {
                        $("#warningModal2 .modal-body").html(d.message);
                        $("#warningModal2").modal('show');
                    }
                    else {
                        $("#nav-other2").html(d);
                    }

                }).always(function () {
                    RefreshOfferFooter();
                    $.unblockUI();
                });
            }
        }
    });


}

function RemoveRow(rowid) {
    bootbox.confirm({
        message: "Silmek istediğinizden emin misiniz?",
        buttons: {
            confirm: {
                label: 'Evet',
                className: 'btn-success'
            },
            cancel: {
                label: 'Hayır',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    method: "GET",
                    url: "/Offer/RemoveOfferRow",
                    data: { rowid: rowid },
                    beforeSend: function () {
                        $.blockUI({ message: $('#domMessage') });
                    }
                }).done(function (d) {
                    $("#nav-home").html(d);
                    RefreshOfferFooter();

                }).always(function () {
                    RefreshOfferFooter();
                    $.unblockUI();
                });
            }
        }
    });


};




function RemoveRowForMachine(rowid) {

    bootbox.confirm({
        message: "Silmek istediğinizden emin misiniz?",
        buttons: {
            confirm: {
                label: 'Evet',
                className: 'btn-success'
            },
            cancel: {
                label: 'Hayır',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    method: "GET",
                    url: "/Offer/RemoveOfferRowForMachine",
                    data: { rowid: rowid },
                    beforeSend: function () {
                        $.blockUI({ message: $('#domMessage') });
                    }
                }).done(function (d) {
                    $("#nav-profile").html(d);
                    RefreshOfferFooter();

                }).always(function () {
                    RefreshOfferFooter();
                    $.unblockUI();
                });
            }
        }
    });

};

function RemoveRowPositions(rowid) {


    bootbox.confirm({
        message: "Silmek istediğinizden emin misiniz?",
        buttons: {
            confirm: {
                label: 'Evet',
                className: 'btn-success'
            },
            cancel: {
                label: 'Hayır',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                var OfferIDx = $("#hidOfferID").val();
                $.ajax({
                    method: "GET",
                    url: "/Offer/RemoveOfferPositionsRow",
                    data: { rowid: rowid, OfferID: OfferIDx },
                    beforeSend: function () {
                        $.blockUI({ message: $('#domMessage') });
                    }
                }).done(function (d) {
                    $("#nav-contact").html(d);

                }).always(function () {
                    RefreshHRProfitCalculate();
                    RefreshOfferFooter();
                    $.unblockUI();
                });
            }
        }
    });


};

function RefreshOfferFooter() {
    var offerID = $("#hidOfferID").val();
    $.ajax({
        method: "GET",
        url: "/Offer/RefreshOfferFooter",
        data: { offerID: offerID },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#AllPriceOffer").html(d);

    }).always(function () {
        $.unblockUI();
    });
};



function UpdateOfferDiscount() {

    var offerID = $("#OfferID").val();
    var discount = $("#OfferDiscount").val();
    var discountrate = $("#OfferDiscountRate").val();

    $.ajax({
        method: "GET",
        url: "/Offer/UpdateOfferDiscount",
        data: { offerID: offerID, discount: discount, discountrate: discountrate },
        beforeSend: function () {
            $("#Loading").show();
        }
    }).done(function (d) {
        $("#OfferFooterList").html(d);

    }).always(function () {
        $("#Loading").hide();
    });
};


function RemoveDiscount(offerID) {
    $.ajax({
        method: "GET",
        url: "/Offer/RemoveDiscount",
        data: { offerID: offerID },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {

        $("#OfferFooterList").html(d);

    }).always(function () {
        $.unblockUI();
    });
};


function CalculateCost(price) {
    $.ajax({
        method: "GET",
        url: "/Person/CalculateCost",
        data: { price: price },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#detailselectx").show();
        $("#getcalculateucret").html(d);

    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};

function CalculateBrutToNet(offerposid) {

    var brutAmount = $("#Brutucret" + offerposid).val();
    var quantity = $("#positionsquantity" + offerposid).val();


    console.log(brutAmount);
    $.ajax({
        method: "GET",
        url: "/Offer/CalculateBrutToNet",
        data: { rowid: offerposid, brut: brutAmount, quantity: quantity },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#nav-contact").html(d);
        $("#exampleModal").modal('show');
    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};

function CalculateBrutToNet2(offerposid) {

    var brutAmount = $("#Brutucret" + offerposid).val();
    var quantity = $("#positionsquantity" + offerposid).val();
    if (brutAmount == "") {
        $("#warningModal2 .modal-body").html("Lütfen Brut Ücret Giriniz!");
        $("#warningModal2").modal('show');
    } else {
        console.log(brutAmount);
        $.ajax({
            method: "GET",
            url: "/Offer/CalculateBrutToNet",
            data: { rowid: offerposid, brut: brutAmount, quantity: quantity },
            beforeSend: function () {
                $.blockUI({ message: $('#domMessage') });
            }
        }).done(function (d) {
            $("#nav-contact").html(d);
            //$("#exampleModal").modal('show');


        }).always(function () {
            RefreshHRProfitCalculate();
            RefreshOfferFooter();
            $.unblockUI();
        });
    }
};

function GetPositionItemsValue(offerposid) {
    //$("#aa").modal('show');
    $.ajax({
        method: "GET",
        url: "/Offer/GetOfferPositionItemsValue",
        data: { rowID: offerposid },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#lg_modal2 .modal-body").html(d);
        $("#rowID").val(offerposid);

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
};

function AllCalculateItems(offerid) {
    //$("#aa").modal('show');
    $.ajax({
        method: "GET",
        url: "/Offer/GetAllCalculateItems",
        data: { offerID: offerid },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#lg_modal .modal-body").html(d);
        //$("#rowID").val(offerposid);

    }).always(function () {
        //RefreshOfferFooter();
        $.unblockUI();
    });
};

function AddAllPrice() {

    $("#CalculateAllModal").modal('hide');

    var array = null;
    array = [];
    //var arrayList = new array(2);
    $('#CalculateAllModal input:checkbox:checked').each(function () {
        array.push($(this).attr('value'));
        //arrayList[0] = $(this).attr('value');
        //arrayList[1] =
    });

    var brut = $("#ttx1").val();
    var yillikizin = $("#yillikizindiv input[name='t_yillikIzin']:checked").val();
    var kidem = $("#ttx5").val();
    var ihbar = $("#ihbardiv input[name='ihbarTazminati']:checked").val();
    var bayramMes = $("#ttx7").val();
    var bordroYol = $("input[name='bordro']:checked").val();
    var saglikGideri = $("#ttx11").val();
    var yol = $("#yolttx").val();
    var isgGideri = $("#ttx17").val();
    var policeGideri = $("#ttx18").val();
    var ihaleBelgeMasrafi = $("#ttx19").val();
    var egitimBelgeGideri = $("#ttx20").val();
    var fazlaMesai = $("#ttx21").val();
    var saglikSigortasi = $("#ttx22").val();
    var bireyselEmeklilik = $("#ttx23").val();
    var offerID = $("#offer_ID").val();
    var yemek = $("#yemekttx").val();
    var radioYemek = $("#yemekdiv input[name='t_radioyemek']:checked").val();
    var fazlaMesaiRadio = $("#fazlaMesaidiv input[name='fazlaMesair']:checked").val();
    var gvOrani = $("#ttx15").val();

    if (array.length == "0") {
        $("#warningModal2 .modal-body").html("Lütfen Personel Seçiniz!");
        $("#warningModal2").modal('show');
    }
    else if (brut == "") {
        $("#warningModal2 .modal-body").html("Lütfen Brut Ücret Giriniz!");
        $("#warningModal2").modal('show');
    }
    else {
        $.ajax({

            method: "GET",
            url: "/Offer/CalculateAllPrice",
            data: {
                positions: array,
                Brut: brut,
                YillikIzin: yillikizin,
                KidemTaz: kidem,
                IhbarTaz: ihbar,
                BayramMesai: bayramMes,
                Yol: yol,
                SaglikGideri: saglikGideri,
                IsgGideri: isgGideri,
                PoliceGideri: policeGideri,
                IhaleBelgeMasrafi: ihaleBelgeMasrafi,
                FazlaMesai: fazlaMesai,
                SaglikSigortasi: saglikSigortasi,
                BireyselEmeklilik: bireyselEmeklilik,
                RadioYol: bordroYol,
                EgitimBelgeGideri: egitimBelgeGideri,
                OfferID: offerID,
                Yemek: yemek,
                RadioYemek: radioYemek,
                radioFazlaMesai: fazlaMesaiRadio,
                GvOrani: gvOrani
            },
            traditional: true,
            beforeSend: function () {
                $.blockUI({ message: $('#domMessage') });
            }

        }).done(function (data) {

            $("#nav-contact").html(data)
        }).always(function () {
            RefreshHRProfitCalculate();
            RefreshOfferFooter();
            $.unblockUI();
        });
    }
};


function AddOpsiyonelItemsValue() {
    debugger;
    $("#aa").modal('hide');
    var brut = $("#txt1").val();
    var yillikizin = $("#yillikizindiv input[name='yillikIzin']:checked").val();
    var kidem = $("#txt5").val();
    var ihbar = $("#ihbardiv input[name='ihbarTazminati']:checked").val();
    var bayramMes = $("#txt7").val();
    var bordroYol = $("input[name='bordro']:checked").val();
    var saglikGideri = $("#txt11").val();
    var yol = $("#yoltxt").val();
    var isgGideri = $("#txt17").val();
    var policeGideri = $("#txt18").val();
    var ihaleBelgeMasrafi = $("#txt19").val();
    var egitimBelgeGideri = $("#txt20").val();
    var fazlaMesai = $("#txt21").val();
    var saglikSigortasi = $("#txt22").val();
    var bireyselEmeklilik = $("#txt23").val();
    var rowID = $("#rowID").val();
    var yemek = $("#txtyemek").val();
    var radioYemek = $("#yemekdiv input[name='radioyemek']:checked").val();
    var offerID = $("#offer_ID").val();
    var fazlaMesaiRadio = $("#fazlaMesaidiv input[name='fazlaMesair']:checked").val();
    var gvOrani = $("#txt15").val();



    $.ajax({

        method: "GET",
        url: "/Offer/CalculateOpsiyonelItemsValue",
        data: {
            Brut: brut,
            YillikIzin: yillikizin,
            KidemTaz: kidem,
            IhbarTaz: ihbar,
            BayramMesai: bayramMes,
            Yol: yol,
            SaglikGideri: saglikGideri,
            IsgGideri: isgGideri,
            PoliceGideri: policeGideri,
            IhaleBelgeMasrafi: ihaleBelgeMasrafi,
            FazlaMesai: fazlaMesai,
            SaglikSigortasi: saglikSigortasi,
            BireyselEmeklilik: bireyselEmeklilik,
            RadioYol: bordroYol,
            EgitimBelgeGideri: egitimBelgeGideri,
            rowID: rowID,
            Yemek: yemek,
            RadioYemek: radioYemek,
            OfferID: offerID,
            IhbarTaz: ihbar,
            radioFazlaMesai: fazlaMesaiRadio,
            GvOrani: gvOrani
        },
        //traditional: true,
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }

    }).done(function (d) {

        $("#nav-contact").html(d);

    }).always(function () {
        RefreshHRProfitCalculate();
        RefreshOfferFooter();
        $.unblockUI();
    });
}



function newPosition(rowID) {
    var title = $("#titleName").val();
    var status = $("#statusID").val();
    var oID = rowID;

    if (title != "" && status != "") {

        $.ajax({
            method: "GET",
            url: "/Offer/AddPosition",
            data: { titleName: title, statusID: status, offerID: oID },
            beforeSend: function () {

            }
        }).done(function (d) {
            if (d.result == "Error") {
                bootbox.alert({
                    size: "small",
                    message: "Pozisyon sistemde tanımlı!",
                    buttons: {
                        ok: {
                            label: 'Tamam',
                            className: 'btn-danger'
                        },
                    },

                })
            } else {
                $("#largeModal .modal-body").html(d);
                $("#newPositionModal").modal('hide');
                $("#largeModal").modal("show");
                $("#titleName").val('');
                $('.modal').on('shown.bs.modal', function (e) {
                    $('body').addClass('modal-open');
                });
            }


        }).always(function () {

        });
    } else {
        bootbox.alert({
            size: "small",
            message: "Pozisyon adı ve durumu boş geçilemez!",
            buttons: {
                ok: {
                    label: 'Tamam',
                    className: 'btn-danger'
                },
            },

        })
    }

};

function AddPositionsOffer(id, offerid) {
    console.log(id);
    $.ajax({
        method: "POST",
        url: "/Offer/AddPositionsOffer",
        data: { id: id, offerid: offerid },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        if (d.result == "Fail") {
            $("#warningModal2 .modal-body").html(d.message);
            $("#warningModal2").modal('show');
        } else {

            $("#nav-contact").html(d);
        }

    }).always(function () {

        $.unblockUI();
    });
};

function Mesaj() {
    $.notify({
        // options
        icon: 'glyphicon glyphicon-warning-sign',
        title: ' ',
        message: 'Ürün Ekleme İşlemi Başarılı.',
        url: '',
        target: '_blank'
    }, {
        // settings
        element: 'body',
        position: null,
        type: "success",
        allow_dismiss: true,
        newest_on_top: true,
        showProgressbar: true,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 250,
        timer: 500,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="100" aria-valuemin="90" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
};

function OpenRefreshOfferView(offerID) {

    $.ajax({
        method: "GET",
        url: "/Offer/RefreshOfferView",
        data: { OfferID: offerID },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        setInterval(7000);
        $("#largeModalReOffer").modal('show');
        $("#largeModalReOffer .modal-body").html(d);

    }).always(function () {
        $.unblockUI();
    });
};

function ReOfferConfirm() {
    $("#ReOfferConfirm").modal('show');
}


function CreateReOffer() {
    var begindate = $("#DateBegin").val();
    var enddate = $("#DateEnd").val();
    var offerID = $("#reOfferID").val();
    var oppName = $("#oppName").val();

    if (begindate == "" || enddate == "" || oppName == "") {
        $("#ReOfferConfirm").modal('hide');
        $("#warningModal2 .modal-body").html("Teklif bilgileri boş geçilemez!");
        $("#warningModal2").modal('show');
    } else if (enddate < begindate) {

        $("#ReOfferConfirm").modal('hide');
        $("#warningModal2 .modal-body").html("Bitiş tarihi düzenleme tarihinden önce olamaz!");
        $("#warningModal2").modal('show');
    }
    else {
        $("#ReOfferConfirm").modal('hide');
        $("#largeModalReOffer").modal('hide');
        $.ajax({
            method: "GET",
            url: "/Offer/CreateReOffer",
            data: { BeginDate: begindate, EndDate: enddate, OfferID: offerID, OppName: oppName },
            beforeSend: function () {
                $.blockUI({ message: $('#domMessage') });
            }
        }).done(function (d) {

            if (d.oppID > 0) {

                $(document).ready(function () {
                    $("#Message").css('display', '');
                    $("#Message").animate({ left: '250px' });
                    //setInterval(function () { $("#Message").fadeOut() }, 3500);
                });
                setInterval(function () { window.location.href = "/Offer/Detail?OpportunityID=" + d.oppID + "&&AutoOpportunity=1#"; }, 1500);


            } else {
                $("#warningModal2 .modal-body").html("Teklif yenileme başarısız!");
                $("#warningModal2").modal('show');
            }


        }).always(function () {

            $.unblockUI();
        });
    }
};

function CalculateIsOneTimeOfferPosition(rowID) {


    var quantity = $("#positionsQuantity" + rowID).val();
    var workingHour = $("#workingHour" + rowID).val();
    var overtimePay = $("#overtimePay" + rowID).val();

    if (quantity == "" || workingHour == "" || overtimePay == "") {

        $("#warningModal2 .modal-body").html("Hesaplama kalemleri boş geçilemez!");
        $("#warningModal2").modal('show');
    } else {
        $.ajax({
            method: "GET",
            url: "/Offer/CalculateIsOneTimeOfferPosition",
            data: { RowID: rowID, Quantity: quantity, WorkingHour: workingHour, OvertimePay: overtimePay },
            beforeSend: function () {
                $.blockUI({ message: $('#domMessage') });
            }
        }).done(function (d) {
            $("#nav-contact").html(d);

        }).always(function () {
            RefreshOfferFooter();
            $.unblockUI();
        });
    }
};

function GetIsOneTimeOfferPositionItemsValue(offerposid) {

    $.ajax({
        method: "GET",
        url: "/Offer/GetIsOneTimeOfferPositionItemsValue",
        data: { rowID: offerposid },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#IsOneTimeOpsiyonelHesaplama .modal-body").html(d);
        $("#rowID").val(offerposid);

    }).always(function () {
        RefreshOfferFooter();
        $.unblockUI();
    });
};
function CalculateIsOneTimeOtherExpense() {
    var roadExpense = $("#ttt1").val();
    var foodExpense = $("#ttt2").val();
    var offerID = $("#offerID").val();
    if (foodExpense == "" || roadExpense == "") {

        $("#warningModal2 .modal-body").html("Yol ve yemek tutarı boş geçilemez!");
        $("#warningModal2").modal('show');
    } else {
        $.ajax({
            method: "GET",
            url: "/Offer/CalculateIsOneTimeOtherExpense",
            data: { OfferID: offerID, FoodExpense: foodExpense, RoadExpense: roadExpense },
            beforeSend: function () {
                $.blockUI({ message: $('#domMessage') });
            }
        }).done(function (d) {
            $("#nav-other2").html(d);

        }).always(function () {
            RefreshOfferFooter();
            $.unblockUI();
        });
    }
};


function OpenHrProfitCalculateModal(offerId) {
    $.ajax({
        method: "GET",
        url: "/Offer/HrCalculateProfitModalPreView",
        data: { offerId: offerId },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {

        $("#HRCalculateProfitModal").modal("show");
        $("#HRCalculateProfitModal .modal-body").html(d);
        ////sonra da modalimizi göster

    }).always(function () {
        $.unblockUI();
    });
}


function OpenHrProfitCalculateModalPreview(offerId) {
    $.ajax({
        method: "GET",
        url: "/Offer/HrCalculateProfitModalPreView",
        data: { offerId: offerId },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {

        $("#HRCalculateProfitModalPreview").modal("show");
        $("#HRCalculateProfitModalPreview .modal-body").html(d);
        ////sonra da modalimizi göster

    }).always(function () {
        $.unblockUI();
    });
}

function OpenHrProfitCalculateModalHistory(offerId, versionDigit) {
    $.ajax({
        method: "GET",
        url: "/Offer/HrCalculateProfitModalHistory",
        data: { offerId: offerId, versionDigit: versionDigit },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {

        $("#HRCalculateProfitModalHistory").modal("show");
        $("#HRCalculateProfitModalHistory .modal-body").html(d);
        ////sonra da modalimizi göster

    }).always(function () {
        $.unblockUI();
    });
}



function HRProfitCalculate() {
    var offerId = $("#offerId").val();
    var array = null;
    array = [];
    //var arrayList = new array(2);
    $('#HRCalculateProfitModal input:checkbox:checked').each(function () {
        array.push($(this).attr('value'));

    });
    var ManagementExpense = $("#HRManagementExpenseTxt").val();
    var HRProfitRate = $("#HRProfitRateTxt").val();
    if (ManagementExpense != "" || HRProfitRate != "") {
        //if (array.length > 0) {

        $.ajax({
            method: "GET",
            url: "/Offer/HRCalculateProfit",
            data: { items: array, managementExpenseTxt: ManagementExpense, profitRateTxt: HRProfitRate, offerId: offerId },
            traditional: true,
            beforeSend: function () {

                $.blockUI({ message: $('#domMessage') });
                $("#HRCalculateProfitModal").modal('hide');
            }
        }).done(function (d) {
            if (d.result == "Success") {
                $("#offerTotalHrProfit").val(d.message);
            } else {
                $("#warningModal2 .modal-body").html(d.message);
                $("#warningModal2").modal('show');
            }
            //$("#HRCalculateProfitModal .modal-body").html(d);

            ////sonra da modalimizi göster

        }).always(function () {
            RefreshOfferFooter();
            $.unblockUI();
        });



        //} else {
        //    $("#warningModal2 .modal-body").html("Hesaplama kalemi seçiniz!");
        //    $("#warningModal2").modal('show');
        //}

    } else {
        $("#warningModal2 .modal-body").html("Lütfen Karlılık Oranı Giriniz!");
        $("#warningModal2").modal('show');
    }

}
function RefreshHRProfitCalculate() {
    var offerId = $("#offerId").val();

    $.ajax({
        method: "GET",
        url: "/Offer/HRCalculateProfit",
        data: { offerId: offerId },
        traditional: true,
        beforeSend: function () {
        }
    }).done(function (d) {
        if (d.result == "Success") {
            $("#offerTotalHrProfit").val(d.message);
        }
    }).always(function () {

    });


}







function RequestNewProduct() {

    bootbox.confirm({
        message: "Satın alma biriminden yeni ürün kayıt talabinde bulunmak istediğinizden emin misiniz?",
        buttons: {
            confirm: {
                label: 'Evet',
                className: 'btn-success'
            },
            cancel: {
                label: 'Hayır',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('.modal').modal('hide');
                var newProductName = $("#NewProductName").val();
                var newProductBrand = $("#NewProductBrand").val();
                var newProductModel = $("#NewProductModel").val();
                var newProductDescription = $("#NewProductDescription").val();

                if (newProductModel != "" && newProductBrand != "" && newProductDescription != "") {
                    $.ajax({
                        method: "GET",
                        url: "/Offer/RequestNewProduct",
                        data: { newProductName: newProductName, newBrand: newProductBrand, newModel: newProductModel, description: newProductDescription },
                        traditional: true,
                        beforeSend: function () {
                            $.blockUI({ message: $('#domMessage') });
                        }
                    }).done(function (d) {
                        if (d.result == "Success") {
                            bootbox.alert({
                                message: d.message,
                                size: 'medium'
                            });
                        } else if (d.result = "FailMail") {
                            bootbox.alert({
                                message: d.message,
                                size: 'medium'
                            });
                        } else {
                            bootbox.alert({
                                message: d.message,
                                size: 'medium'
                            });
                        }
                    }).always(function () {
                        $.unblockUI();
                    });
                } else {
                    $("#warningModal2 .modal-body").html("Malzeme adı, marka ve açıklama boş bırakılamaz!");
                    $("#warningModal2").modal('show');
                }
            }

        }
    });






}

function ShowProductDescription(rowid) {

    $.ajax({
        method: "GET",
        url: "/Offer/ShowProductDescription",
        data: { rowid: rowid },
        beforeSend: function () {
            $.blockUI({ message: $('#domMessage') });
        }
    }).done(function (d) {
        $("#DescriptionModal").modal("show");
        $("#DescriptionDetail").html(d);


    }).always(function () {

        $.unblockUI();
    });




};

function RemoveOffer(offerID) {

    bootbox.confirm({
        message: "Teklifi silmek istediğinizden emin misiniz?",
        buttons: {
            confirm: {
                label: 'Evet',
                className: 'btn-success'
            },
            cancel: {
                label: 'Hayır',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    method: "GET",
                    url: "/Offer/RemoveOffer",
                    data: { offerId: offerID},
                    beforeSend: function () {
                         $.blockUI({ message: $('#domMessage') });
                    }
                }).done(function (d) {
                    if (d.result == "Success") {
                        bootbox.alert({
                            size: "small",
                            message: d.message,
                            buttons: {
                                ok: {
                                    label: 'Tamam',
                                    className: 'btn-success'
                                },
                            },
                            callback: function () { window.location.href = "/Offer/Index";}
                        });
                    } else {
                        bootbox.alert({
                            size: "small",
                            message: d.message,
                            buttons: {
                                ok: {
                                    label: 'Tamam',
                                    className: 'btn-success'
                                },
                            },
                            callback: function () { }
                        });
                    }



                }).always(function () {

                    $.unblockUI();
                });
            }

        }
    });






}