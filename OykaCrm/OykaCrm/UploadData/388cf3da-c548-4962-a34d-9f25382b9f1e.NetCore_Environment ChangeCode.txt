$env:ASPNETCORE_ENVIRONMENT='Development'



 var sp = @"CREATE PROCEDURE SPAppLog
                            @RecordUserId uniqueidentifier,
                            @UpdateUserId uniqueidentifier,
                            @RecordDate datetime2,
                            @UpdateDate datetime2,
                            @IsDeleted bit,
                            @Source nvarchar(MAX),
                            @ControllerName nvarchar(MAX),
                            @ActionName nvarchar(MAX),
                            @Browser nvarchar(MAX),
                            @IPAddress nvarchar(max),
                            @OldValue nvarchar(max),
                            @NewValue nvarchar(max),
                            @Description nvarchar(max)
                        AS
                        BEGIN
                            SET NOCOUNT ON;
                            Insert into AppLogs(
                                   [RecordUserId]
                                   ,[UpdateUserId]
                                   ,[RecordDate]
                                   ,[UpdateDate]
                                   ,[IsDeleted]
                                   ,[Source]
                                   ,[ControllerName]
                                   ,[ActionName]
                                   ,[Browser]
                                   ,[IPAddress]
                                   ,[OldValue]
                                   ,[NewValue]
                                   ,[Description]
                                   )
                         Values ( @RecordUserId,  @UpdateUserId,@RecordDate,@UpdateDate,@IsDeleted,@Source, @ControllerName,@ActionName,@Browser,@IPAddress,@OldValue,@NewValue,@Description)
                        END
                        GO";
            migrationBuilder.Sql(sp);