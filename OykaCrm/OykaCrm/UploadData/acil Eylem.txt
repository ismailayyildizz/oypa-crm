function ShowEmergencyActionItemDetail(id) {
    $.get("/EmergencyAction/EmergencyActionItemDetail", { id }, function (response) {
        $("#EmergencyActionItemDetailContent").html(response);
        ShowModalBegin("EmergencyActionItemDetail");
    });
}

function EmergencyActionItemDelete(id) {
    $.swalDelete({
        title: "Acil Eylem Madde Sil",
        text: "Seçili Kaydı Silmek İstediğinizden Emin misiniz? Bu işlem geri alınamaz.",
        url: "/EmergencyAction/EmergencyActionItemDelete",
        data: { id },
        success: function (response) {
            if (response.IsSuccess) {
                UpdateDataTable("table_Modal_EmergencyActionItemList_1");
                UpdateDataTable("table_EmergencyActionList");
            }
            Notify(response.Message, response.Class);
        }
    });
}

var onEmergencyActionItemSaveSuccess = function (response) {
    if (response.IsSuccess) {
        UpdateDataTable("table_Modal_EmergencyActionItemList_1");
        CloseModalBegin("EmergencyActionItemDetail");
    }
    Notify(response.Message, response.Class);
};

//Acil Eylem
function ShowEmergencyActionDetail(id) {
    $.get("/EmergencyAction/EmergencyActionDetail", { id }, function (response) {
        $("#EmergencyActionDetailContent").html(response);

        ShowModalBegin("EmergencyActionDetail");
        initializeValidation();
    });
}

var onEmergencyActionSaveSuccess = function (response) {
    if (response.IsSuccess) {
        UpdateDataTable("table_EmergencyActionList");
        CloseModalBegin("EmergencyActionDetail");
    }
    Notify(response.Message, response.Class);
};

function EmergencyActionDelete(id) {
    $.swalDelete({
        title: "Acil Eylem Sil",
        text: "Seçili Kaydı Silmek İstediğinizden Emin misiniz? Bu işlem geri alınamaz.",
        url: "/EmergencyAction/EmergencyActionDelete",
        data: { id },
        success: function (response) {
            if (response.IsSuccess) {
                UpdateDataTable("table_EmergencyActionList");
            }
            Notify(response.Message, response.Class);
        }
    });
}

//Acil Eylem Detail
var onEmergencyActionDetailSaveSuccess = function (response) {
    if (response.IsSuccess) {
        $("#EmergencyActionDetailId").val(response.ReturnId);

        $('.hide-if-no-record').removeClass('d-none');
    }
    Notify(response.Message, response.Class);
};

//Ekip Görevi seçildiğinde görev detayı otomatik değiştirir.
$(document).on("mouseout", "#Job", GetJob);
$(document).on("blur", "#Job", GetJob);
$(document).on("hidden.bs.modal", "#ModalEmergencyActionItemListModal_1", GetJob);

function GetJob() {
    var job = $("#Job").val();
    var groupItem = $("#GroupItem").val();

    if (job === "Ekip Başkanı") {
        if (groupItem === "YANGINLA MÜCADELE") {
            $("#Action").val("Acil bir durumda (yangın, deprem vb.) ilk müdahaleyi yapmak üzere görevli olan söndürme ekibini yönlendirme, içeride kalmış kişilerin tahliyesini sağlamak.");
        }
        if (groupItem === "ARAMA KURTARMA VE TAHLİYE EKİBİ") {
            $("#Action").val("Söndürme ekibinden alacağı talimat ile acil bir durumda öncelikle acil durumdan zarar görmüş kişileri daha sonra önemli belge ve malzemeleri kurtarmak üzere ekibi yönlendirmek");
        }
        if (groupItem === "İLK YARDIM EKİBİ") {
            $("#Action").val("Yaralı personele sağlık ekipleri gelinceye kadar almış olduğu eğitim doğrultusunda ilk yardımda bulunmak, gerektiğinde sağlık ekiplerine yardım etmek");
        }

    } else if (job === "Ekip Başkanı Yardımcısı") {
        if (groupItem === "YANGINLA MÜCADELE") {
            $("#Action").val("Ekip başkanına yardım etmek, ekip başkanı bulunmadığı durumlarda, onun görevini yerine getirmek");
        }
        if (groupItem === "ARAMA KURTARMA VE TAHLİYE EKİBİ") {
            $("#Action").val("Ekip başkanına yardım etmek, ekip başkanı bulunmadığı durumlarda, onun görevini yerine getirmek");
        }
        if (groupItem === "İLK YARDIM EKİBİ") {
            $("#Action").val("Yaralı personelin bulunduğu alanda, diğer personeli kontrol altına alıp paniği önlemek; ilk yardım eğitimi almamış personelin müdahelesine engel olmak");
        }

    } else if (job === "Üye") {
        if (groupItem === "YANGINLA MÜCADELE") {
            $("#Action").val("Acil bir durumda, ekip başkanının talimatları doğrultusunda yangına ilk müdahaleyi sağlamak");
        }
        if (groupItem === "ARAMA KURTARMA VE TAHLİYE EKİBİ") {
            $("#Action").val("Ekip başkanının talimatı ile zarar görmüş kişilerin ve malzemenin tahliyesini sağlamak");
        }
        if (groupItem === "İLK YARDIM EKİBİ") {
            $("#Action").val("Ekip başkanının talimatı doğrultusunda yardımda bulunmak");
        }

    }

}

//Acil Eylem Yazdır
function ShowEmergencyActionListPrintModal() {
    $.post("/EmergencyAction/EmergencyActionItemList", function (response) {
        var $dropdown = $("#DropdownEmergencyActionItemList");
        $dropdown.attr("class", "form-control custom-select");
        $dropdown.append($("<option />").val(0).text("Hepsi"));

        $.each(response, function () {
            $dropdown.append($("<option />").val(this.EmergencyActionItemId).text(this.Item));
        });
    });

    ShowModalBegin("EmergencyActionList");
}

function EmergencyActionListPrint() {
    var printWithSignature = $("#PrintWithSignature:checked").val();
    var groupId = $("#DropdownEmergencyActionItemList").val();

    var withSignature = false;
    if (printWithSignature === "true") {
        withSignature = true;
    }

    if (groupId === "0") {
        window.open("/Report/PrintEmergencyActionGroupByList?isPrintWithSignature=" + withSignature);

    } else {
        window.open("/Report/PrintEmergencyActionSingleGroupList?id=" + groupId + "&isPrintWithSignature=" + withSignature);
    }
}