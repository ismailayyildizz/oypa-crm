GetItemCardList metodu tüm ürünleri getirir.
	Code:11.022 (11 ile başlaması 11. category ürünü olduğunu gösterir)
	STGRPCode:11 // Bizdeki CategoryId
	Serviceten dönen lastInPrice alanını birim olarak mı kullanacağız? Logodan gelen birimler ile bizde kayıtlı birim fiyatlar uyuşmuyor.
	Serviceten dönen STGRPCode'lar arasında bizde olmayan CatId'ler var onları çekmeyecek miyiz?
	 Biz ürünleri logodan çektiğimize göre neden ürünlere güncelleme ekranı koyduk ?
	 Bu metottan bize amortisman ile ilgili bir field dönmüyor, biz ürün kartının içerisine amortisman koymuşuz.Ür