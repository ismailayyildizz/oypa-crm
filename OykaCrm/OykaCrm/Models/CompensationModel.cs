﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ConstantParameterModel
    {
        public int Id { get; set; }
        public int ConstantParemeterId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public int PositionStatus { get; set; }
        public DateTime UsedDate { get; set; } //Hangi dönemde değiştirilmiş
        public string FormatDate { get; set; }
    }
}