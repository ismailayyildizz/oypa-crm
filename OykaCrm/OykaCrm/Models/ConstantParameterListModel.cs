﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ConstantParameterListModel
    {
        public List<Compensation> CompensationList { get; set; } = new List<Compensation>();
        public List<GrossRateate> GrossRateateList { get; set; } = new List<GrossRateate>();
    }
    public class BaseClass
    {
        public int Id { get; set; }
        public string ParameterName { get; set; }
        public int PositionStatus { get; set; }
        public string ParameterValue { get; set; }
        public DateTime? UsedDate { get; set; } //Hangi dönemde değiştirilmiş
        public bool? IsActive { get; set; }
    }
    public class Compensation : BaseClass
    {


    }

    public class GrossRateate : BaseClass
    {


    }
}