﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm
{
    public class LogModel
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}