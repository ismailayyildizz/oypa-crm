﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OpportunityDetailViewModel:LayoutControlModel
    {

        public List<Customers> CustomersList { get; set; }
        public List<Projects> ProjectList { get; set; }

        public string OpportunityNumber { get; set; }

        public List<OpportunityStatus> StatusList { get; set; }

        public Opportunity SingleOpportunity { get; set; }

        public List<RoleGroupPermissions> RolePermissionList { get; set; }

        public List<Documents> DocumentsList { get; set; }
        public string FormatDateBegin { get; set; }
        public string FormatDateEnd { get; set; }
    }
}