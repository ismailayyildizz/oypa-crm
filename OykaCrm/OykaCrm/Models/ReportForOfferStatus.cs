﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ReportForOfferStatus
    {
        public int OfferID { get; set; }
        public string OfferNumber { get; set; }
        public Projects Projects { get; set; }
        public Customers Customers { get; set; }
        public DateTime? DateBegin { get; set; }
        public DateTime? DateEnd { get; set; }
        public double? GenelToplam { get; set; }
        public int? OpportunityID { get; set; }
        public OfferStatus OfferStatus { get; set; }
        public int StatusLevel { get; set; }
    }
}