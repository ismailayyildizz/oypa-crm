﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferControlModel
    {
        public Opportunity singleopportuniy { get; set; }
        public Offer singleoffer;
        public OfferContract SingleOfferContract { get; set; }
        public List<OfferProducts> offerproducts;
        public List<OfferEquipment> offerequipments;
        public List<OfferPositions> offerpositions;
        public List<Positions> positions;
        public List<PositionStatus> PositionStatuses { get; set; }
        public List<Products> productlist { get; set; }

        public List<Documents> DocumentsList { get; set; }

        public List<OfferStatus> OfferStatusList { get; set; }

        public List<Projects> ProjectList { get; set; }

        public List<OfferContentTemplate> OfferContentTemplateList { get; set; }

        public List<IsOneTimeOfferPositions> IsOneTimeOfferPosList { get; set; }

        //Checxbox İle seçmek için eklendi.
        public List<int?> OfferPositionID { get; set; }
        public List<PositionItems> PositionItems { get; set; }


        public List<Notes> Notes { get; set; } = new List<Notes>();

        public int RowID { get; set; }

        public List<PositionOptions> PositonsOptions { get; set; }

        public double? KidemTazValue { get; set; }

        public List<OfferOtherExpenses> OtherExpenses { get; set; }

        public string OpportunityNumber { get; set; }
        public Employee Employee{ get; set; }

        public List<IsOneTimePositionItems> IsOneTimePositionItems { get; set; }

        public List<IsOneTimeOfferOtherExpenseItems> IsOneTimeOfferOtherExpenseItems{ get; set; }
        public List<IsOneTimeOtherExpense> IsOneTimeOfferOtherExpenseList{ get; set; }
        public int RoleGroup { get; set; }

        public List<CustomerContact> CustomerContactList { get; set; }

    }
}