﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Hosting;

namespace OykaCrm.Models
{
    public class OypaHelper
    {
        public OypaDbEntities db { get; set; }
        public OypaHelper()
        {
            db = new OypaDbEntities();
        }
        public string GetUpdOfferNumber()
        {
            var OfferNumberCount = db.ParamCount.FirstOrDefault(i => i.Code == "Offer").ParamCount1;
            var Prefix = db.ParamCount.FirstOrDefault(i => i.Code == "Offer").Param;
            var Year = DateTime.Now.Year;
            var OfferNumber = OfferNumberCount + 1;

            var Params = db.ParamCount.FirstOrDefault(i => i.Code == "Offer");
            Params.ParamCount1 = OfferNumber;
            db.SaveChanges();

            string FinishOfferNumber = Prefix + Year + "-" + OfferNumber.ToString();
            return FinishOfferNumber;
        }

        public string GetOpportunityNumber()
        {
            var OpportunityCount = db.ParamCount.FirstOrDefault(i => i.Code == "Opportunity").ParamCount1;
            var Prefix = db.ParamCount.FirstOrDefault(i => i.Code == "Opportunity").Param;
            var Year = DateTime.Now.Year;

            var OpportunityNumber = OpportunityCount + 1;

            var Params = db.ParamCount.FirstOrDefault(i => i.Code == "Opportunity");
            Params.ParamCount1 = OpportunityNumber;
            db.SaveChanges();

            string FinishOfferNumber = Prefix + Year + "-" + OpportunityNumber.ToString();
            return FinishOfferNumber;
        }

        public static int RecordEmployee()
        {
            var LoginUser = HttpContext.Current.Session["profile"] as Employee;
            var RecordEmp = LoginUser.ID;
            return RecordEmp;

        }
        public static string RecordEmployeeFullName()
        {
            var LoginUser = HttpContext.Current.Session["profile"] as Employee;
            var RecordEmp = LoginUser.FullName;
            return RecordEmp;

        }

        public static int EmployeeRoleGroup()
        {
            var LoginUser = HttpContext.Current.Session["profile"] as Employee;
            var EmpRoleGroup = LoginUser.RolGroupID;
            return EmpRoleGroup.Value;

        }
        public static int EmployeeId()
        {
            var LoginUser = HttpContext.Current.Session["profile"] as Employee;
            var id = LoginUser.ID;
            return id;

        }

        public static string UserIpAdress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public int? EmployeeRegionID()
        {

            var LoginUser = HttpContext.Current.Session["profile"] as Employee;
            var RegionEmployeeID = LoginUser.RegionID;

            return RegionEmployeeID;

        }

        public static IEnumerable<LogModel> PublicInstancePropertiesEqual<T>(T self, T to, params string[] ignore) where T : class
        {
            List<LogModel> loglist = new List<LogModel>();

            if (self != null && to != null)
            {
                Type type = typeof(T);
                List<string> ignoreList = new List<string>(ignore);
                foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(to, null);

                        if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                        {
                            loglist.Add(new LogModel() { FieldName = pi.Name, OldValue = selfValue?.ToString(), NewValue = toValue?.ToString(), TableName = type.Name });
                        }
                    }
                }

            }
            return loglist;
        }

        public static string[] getIgnorelist()
        {
            List<string> ignores = new List<string>() {
                " "
            };

            return ignores.ToArray();
        }

        public static void AddApplicationLog(string Source, string ActionType, string ModelName, long ModelID, string TableName, bool IsSuccess, string Description, int RecordUser, string RecordIP, IEnumerable<LogModel> differents)
        {
            using (OypaDbEntities db = new OypaDbEntities())
            {
                if (differents != null && differents.Count() > 0)
                {
                    foreach (var item in differents)
                    {
                        db.AddLog(Source, ActionType, ModelName, ModelID, item.TableName, item.FieldName, item.OldValue, item.NewValue, IsSuccess, Description, RecordUser, RecordIP, DateTime.Now);
                    }
                }
                else
                {
                    db.AddLog(Source, ActionType, ModelName, ModelID, TableName, string.Empty, string.Empty, string.Empty, IsSuccess, Description, RecordUser, RecordIP, DateTime.Now);

                }
            }
        }

        public static bool SendMail(string konu, string icerik, string aliciemail, string filepath)
        {

            MailMessage eposta = new MailMessage();

            bool kontrol = true;
            if (!string.IsNullOrEmpty(aliciemail))
            {
                eposta.From = new MailAddress("tyhbilgi@oypa.com.tr");
                eposta.To.Add(aliciemail);
                eposta.Subject = konu;
                eposta.Body = icerik;
                eposta.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();

                if (filepath != null)
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(HostingEnvironment.MapPath("~/pdf/" + filepath));
                    eposta.Attachments.Add(attachment);

                }
                smtp.Host = "mail.oypa.com.tr";
                smtp.Host = "172.18.100.22";
                smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential("tyhbilgi@oypa.com.tr", "As123456");
                smtp.EnableSsl = false;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                object userState = eposta;


                try
                {

                    smtp.Send(eposta);
                }

                catch (SmtpException ex)
                {

                    kontrol = false;

                }


            }
            return kontrol;




        }
        public static bool SendErrorMail(string konu, string icerik)
        {

            MailMessage eposta = new MailMessage();

            eposta.From = new MailAddress("tyhbilgi@oypa.com.tr");
            eposta.To.Add("tahsin.tiryaki@sibertechno.com");
            eposta.CC.Add("ismail.ayyildiz@farmakod.com");


            eposta.Subject = konu;
            eposta.Body = icerik;
            eposta.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();


            smtp.Host = "mail.oypa.com.tr";
            smtp.Host = "172.18.100.22";
            smtp.Port = 25;
            smtp.Credentials = new System.Net.NetworkCredential("tyhbilgi@oypa.com.tr", "As123456");
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            object userState = eposta;

            bool kontrol = true;

            try
            {

                smtp.Send(eposta);
            }

            catch (SmtpException ex)
            {

                kontrol = false;

            }

            return kontrol;

            //MailMessage eposta = new MailMessage();
            //eposta.From = new MailAddress("bilgi@farmakod.com");

            //eposta.To.Add("tahsin.tiryaki@sibertechno.com");
            ////eposta.CC.Add("ismail.ayyildiz@farmakod.com");

            //eposta.Subject = konu;
            //eposta.Body = icerik;
            //eposta.IsBodyHtml = true;
            //SmtpClient smtp = new SmtpClient();

            //smtp.Host = "mail.farmakod.com";
            //smtp.Port = 587;
            //smtp.Credentials = new System.Net.NetworkCredential("bilgi@farmakod.com", "FArm010203##");
            //smtp.EnableSsl = false;
            //smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //object userState = eposta;

            //bool kontrol = true;

            //try
            //{

            //    smtp.Send(eposta);
            //}

            //catch (SmtpException ex)
            //{

            //    kontrol = false;
            //}

            //return kontrol;


        }
        //public static bool SendMail(string konu, string icerik, string aliciemail, string filepath)
        //{

        //    MailMessage eposta = new MailMessage();
        //    eposta.From = new MailAddress("bilgi@farmakod.com");

        //    eposta.To.Add(aliciemail);

        //    eposta.Subject = konu;
        //    eposta.Body = icerik;
        //    eposta.IsBodyHtml = true;
        //    SmtpClient smtp = new SmtpClient();
        //    if (filepath != null)
        //    {
        //        System.Net.Mail.Attachment attachment;
        //        attachment = new System.Net.Mail.Attachment(HostingEnvironment.MapPath("~/pdf/" + filepath));
        //        eposta.Attachments.Add(attachment);

        //    }
        //    smtp.Host = "mail.farmakod.com";
        //    smtp.Port = 587;
        //    smtp.Credentials = new System.Net.NetworkCredential("bilgi@farmakod.com", "FArm010203##");
        //    smtp.EnableSsl = false;
        //    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //    object userState = eposta;

        //    bool kontrol = true;

        //    try
        //    {

        //        smtp.Send(eposta);
        //    }

        //    catch (SmtpException ex)
        //    {

        //        kontrol = false;
        //    }

        //    return kontrol;

        //}
        public static bool SendMailWithMailGroup(string konu, string icerik, string aliciemail, string filepath, string ccMailGroup, string ccTYHSupportMailGroup)
        {

            //MailMessage eposta = new MailMessage();
            //eposta.From = new MailAddress("bilgi@farmakod.com");

            //eposta.To.Add(aliciemail);
            //if (ccMailGroup != null)
            //{
            //    eposta.CC.Add(ccMailGroup);
            //}
            //if (ccTYHSupportMailGroup != null)
            //{
            //    eposta.CC.Add(ccTYHSupportMailGroup);
            //}
            //eposta.Subject = konu;
            //eposta.Body = icerik;
            //eposta.IsBodyHtml = true;
            //SmtpClient smtp = new SmtpClient();
            //if (filepath != null)
            //{
            //    System.Net.Mail.Attachment attachment;
            //    attachment = new System.Net.Mail.Attachment(HostingEnvironment.MapPath("~/pdf/" + filepath));
            //    eposta.Attachments.Add(attachment);

            //}
            //smtp.Host = "mail.farmakod.com";
            //smtp.Port = 587;
            //smtp.Credentials = new System.Net.NetworkCredential("bilgi@farmakod.com", "FArm010203##");
            //smtp.EnableSsl = false;
            //smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //object userState = eposta;

            //bool kontrol = true;

            MailMessage eposta = new MailMessage();

            eposta.From = new MailAddress("tyhbilgi@oypa.com.tr");
            eposta.To.Add(aliciemail);
            if (ccMailGroup != null)
            {
                eposta.CC.Add(ccMailGroup);
            }
            if (ccTYHSupportMailGroup != null)
            {
                eposta.CC.Add(ccTYHSupportMailGroup);
            }
            eposta.Subject = konu;
            eposta.Body = icerik;
            eposta.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();

            if (filepath != null)
            {
                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(HostingEnvironment.MapPath("~/pdf/" + filepath));
                eposta.Attachments.Add(attachment);

            }
            smtp.Host = "mail.oypa.com.tr";
            smtp.Host = "172.18.100.22";
            smtp.Port = 25;
            smtp.Credentials = new System.Net.NetworkCredential("tyhbilgi@oypa.com.tr", "As123456");
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            object userState = eposta;

            bool kontrol = true;

            try
            {

                smtp.Send(eposta);
            }

            catch (SmtpException ex)
            {

                kontrol = false;
            }

            return kontrol;

        }


        public static bool FillinEmailContentWithMailGroup(string message1, string message2, string mailContentTitle, string mailTo, string mailSubject, string link, string filepath, string EmailDocName, string ccMailGroup, string ccTYHSupportMailGroup)
        {
            string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Email/" + EmailDocName));
            //#To# Hitap edilecek kişi
            // Message1 #CreatedBy# tarafından #OpportunityName# isimli bir fırsat oluşturuldu.
            // Message2 Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak..

            Body = Body.Replace("#MailContentTitle#", mailContentTitle); //Örn Sn Direktör
            Body = Body.Replace("#Message1#", message1);
            Body = Body.Replace("#Message2#", message2);
            Body = Body.Replace("#Link#", link);

            bool mailres = OypaHelper.SendMailWithMailGroup(mailSubject, Body, mailTo, filepath, ccMailGroup, ccTYHSupportMailGroup);
            return mailres;
        }


        public static bool FillinEmailContent(string message1, string message2, string mailContentTitle, string mailTo, string mailSubject, string link, string filepath, string EmailDocName)
        {
            string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Email/" + EmailDocName));
            //#To# Hitap edilecek kişi
            // Message1 #CreatedBy# tarafından #OpportunityName# isimli bir fırsat oluşturuldu.
            // Message2 Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak..

            Body = Body.Replace("#MailContentTitle#", mailContentTitle); //Örn Sn Direktör
            Body = Body.Replace("#Message1#", message1);
            Body = Body.Replace("#Message2#", message2);
            Body = Body.Replace("#Link#", link);

            bool mailres = OypaHelper.SendMail(mailSubject, Body, mailTo, filepath);
            return mailres;
        }

        public static bool FillinEmailContentForNewProduct(string message1, string message2, string message3, string message4, string message5, string mailContentTitle, string mailTo, string mailSubject, string link, string filepath, string EmailDocName)
        {
            string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Email/" + EmailDocName));
            //#To# Hitap edilecek kişi
            // Message1 #CreatedBy# tarafından #OpportunityName# isimli bir fırsat oluşturuldu.
            // Message2 Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak...

            Body = Body.Replace("#MailContentTitle#", mailContentTitle); //Örn Sn Direktör
            Body = Body.Replace("#Message1#", message1);
            Body = Body.Replace("#Message2#", message2);
            Body = Body.Replace("#Message3#", message3);
            Body = Body.Replace("#Message4#", message4);
            Body = Body.Replace("#Message5#", message5);
            Body = Body.Replace("#Link#", link);

            bool mailres = OypaHelper.SendMail(mailSubject, Body, mailTo, filepath);
            return mailres;
        }


        public static string ReplaceContract(string html, string opportunityName, string customer, string customerAddress, string price) //Detaylı Fiyat Listesi Şablonu Seçildi ise
        {
            //var replacePrice =  html.Replace("#fiyat#", price+"₺");
            var replaceOffer = html.Replace("#teklifadı#", opportunityName);
            var replaceCustomer = replaceOffer.Replace("#firma#", customer);
            var replaceCustomerAddress = replaceCustomer.Replace("#firmaAddress#", customerAddress);

            //var replaceKDV = replaceCustomer.Replace("#KDV#", KDV+"₺");
            //var replaceTotapPrice = replaceKDV.Replace("#GenelToplam#", totalPrice+"₺");

            return replaceCustomerAddress;

        }
        public static string ReplaceContract(string html, string customer, string customerAddress) //Temel şablon seçildi ise
        {
            var result = html.Replace("#Customer#", customer);
            return result.Replace("#CustomerAddress#", customerAddress);
        }


        public static string ReplaceContract(string html, string price, string KDV, string totalPrice) //Teklifin gönderme noktasında Fiyatlar replace ediliyor ise
        {
            var replacePrice = html.Replace("#fiyat#", price + "₺");
            var replaceKDV = replacePrice.Replace("#KDV#", KDV + "₺");
            var replaceTotapPrice = replaceKDV.Replace("#GenelToplam#", totalPrice + "₺");
            return replaceTotapPrice;

        }

        public static string ReplaceContractTemplate(string html, string opportunityName, string customer, string customerAddress, string beginDate, string endDate)
        //Sozlesme icin sablon replace metodu
        {
            var oppNameReplace = html.Replace("#OpportunityName#", opportunityName);
            var customerReplace = oppNameReplace.Replace("#Customer#", customer);
            var customerAddressReplace = customerReplace.Replace("#CustomerAddress#", customerAddress);
            var beginDateReplace = customerAddressReplace.Replace("#BeginDate#", beginDate);

            return beginDateReplace.Replace("#EndDate#", endDate);
        }

        public static string ReplaceContractTemplate(string html, string opportunityName, string customer, string customerAddress, string beginDate, string endDate, string montlyPrice)
        //Sozlesme icin sablon replace metodu
        {
            var oppNameReplace = html.Replace("#OpportunityName#", opportunityName);
            var customerReplace = oppNameReplace.Replace("#Customer#", customer);
            var customerAddressReplace = customerReplace.Replace("#CustomerAddress#", customerAddress);
            var beginDateReplace = customerAddressReplace.Replace("#BeginDate#", beginDate);
            var endDateReplace = beginDateReplace.Replace("#EndDate#", endDate);

            return endDateReplace.Replace("#mothlyPrice#", montlyPrice);
        }


        public static string ReplaceOfferContract(string html, string customer, string customerAddress, string year, string offerName, string date, int contractId) //Temel şablon seçildi ise
        {
            html = html.Replace("#Year#", year);
            html = html.Replace("#Customer#", customer);
            html = html.Replace("#CustomerAddress#", customerAddress);
            html = html.Replace("#OfferName#", offerName);
            html = html.Replace("#Date", date);
            if (contractId == 3)
            {
                return html;

            }
            else if (contractId == 4)
            {
                html = html.Replace("#Date", date);
            }


            var result = html.Replace("#Customer#", customer);
            return result.Replace("#CustomerAddress#", customerAddress);
        }
        public static string TurkceKarakter(string text)
        {

            text = text.Replace("İ", "\u0130");

            text = text.Replace("ı", "\u0131");

            text = text.Replace("Ş", "\u015e");

            text = text.Replace("ş", "\u015f");

            text = text.Replace("Ğ", "\u011e");

            text = text.Replace("ğ", "\u011f");

            text = text.Replace("Ö", "\u00d6");

            text = text.Replace("ö", "\u00f6");

            text = text.Replace("ç", "\u00e7");

            text = text.Replace("Ç", "\u00c7");

            text = text.Replace("ü", "\u00fc");

            text = text.Replace("Ü", "\u00dc");

            return text;
        }
    }
}