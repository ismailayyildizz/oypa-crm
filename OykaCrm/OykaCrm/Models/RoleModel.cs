﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public int? RoleLevelId { get; set; }
        public List<RoleLevelList> RoleLevelList { get; set; } = new List<RoleLevelList>();
    }
   
}