//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OykaCrm.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesInvoiceItemReturnLogo
    {
        public int ID { get; set; }
        public Nullable<int> SalesInvoiceID { get; set; }
        public string Type { get; set; }
        public string MasterCode { get; set; }
        public string TrCode { get; set; }
        public string IoCode { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
        public string UnitCode { get; set; }
        public string VatRate { get; set; }
        public string VatIncluded { get; set; }
        public string VatAmount { get; set; }
        public string VatBase { get; set; }
        public string Billed { get; set; }
        public string TotalNet { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<int> RecordUserID { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public Nullable<int> UpdateUserID { get; set; }
        public string Ohp_Code1 { get; set; }
        public string Description { get; set; }
    }
}
