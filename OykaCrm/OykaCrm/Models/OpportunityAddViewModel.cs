﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OpportunityAddViewModel
    {
        public int ID { get; set; }
        public string OpportunityNumber { get; set; }
        public string OpportunityName { get; set; }

        public DateTime DateBegin { get; set; }

        public DateTime DateEnd { get; set; }

        public int StatusLevel { get; set; }

        public string Summary { get; set; }
        public string MainContent { get; set; }

        public string ReasonDescription { get; set; }

        public int CustomerID { get; set; }

        public int EmployeeID { get; set; }

        public int RegionID { get; set; }

        public int EnviromentID { get; set; }

        public int ProjectID { get; set; }

        public bool IsOneTime { get; set; }

        public int EnvironmentID { get; set; }

        public DateTime RecordDate { get; set; }

        public int IsOneTimeForm { get; set; }




    }
}