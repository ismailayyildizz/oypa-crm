﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OvertimeModel
    {
        public int PositionId { get; set; }
        public string Status { get; set; }
        public string GrossPrice { get; set; }//brüt ücret
        public string HIRate { get; set; }//hafta içi oranı
        //HIFM1 : Hafta içi fazla mesai
        public string HIHour { get; set; }//hafta içi saat
        public string HSRATE { get; set; }//Hafta sonu oran
        //BT=Bayram tatili
        public string HSHour { get; set; }
        public string BTRate { get; set; }
        //BTFM1 : Bayram Tatili fazla mesai
        public string BTHour { get; set; }
        public string TotalOvertimeGrossPrice { get; set; }//Toplam fazla mesai brüt ücerti
        public string EmployerShare { get; set; }//işveren payı
        public string EmployerUnemployement { get; set; }//işveren işsizlik
        public string TotalOvertimeCost { get; set; }//toplam fazla mesai maaliyet
        public string Spread { get; set; }//kar oranı
        public string InvoiceAmountForOvertime { get; set; }//Fatura edilecek tutar

    }
}