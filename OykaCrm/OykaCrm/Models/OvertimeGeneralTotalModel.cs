﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OvertimeGeneralTotalModel
    {
        //genel toplam
        public string OvertimeGeneralTotal { get; set; }
        public int InvoiceRowId { get; set; }
    }
}