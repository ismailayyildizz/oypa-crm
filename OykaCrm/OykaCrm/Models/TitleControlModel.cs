﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class TitleControlModel
    {
        public List<Positions> PositionList { get; set; }
        public List<PositionStatus> StatusList { get; set; }
        public Positions SinglePosition { get; set; }



    }
}