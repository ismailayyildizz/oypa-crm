﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class DocumentModel
    {
        public List<Documents>  documents { get; set; }
        public List<DocumentCategory> documentCategories { get; set; }
        public List<Employee> employees { get; set; }
    }

}