﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ProductModel:LayoutControlModel
    {
        public int ID { get; set; }

        public int? CategoryID { get; set; }

        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public string Unit { get; set; }

        public double? UnitPrice { get; set; }

        public string Currency { get; set; }

        public int? Depreciation { get; set; }

        public int? LogoID { get; set; }

        public int? LogoCode { get; set; }

        //public DateTime? RecordDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        //public bool IsActive { get; set; }

        public IEnumerable<ProductCategory> ProductCategories { get; set; }
    }
}