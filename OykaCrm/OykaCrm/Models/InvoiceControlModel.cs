﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class InvoiceControlModel
    {
        public int MonthCount { get; set; }
        public double MonthlyInvoice { get; set; }
        public List<InvoiceRow> InvoiceRows { get; set; }
        public string ProjectName { get; set; }
        public string CustomerName { get; set; }
        public string OfferNumber { get; set; }
        public DateTime? DateBegin { get; set; }
        public DateTime? DateEnd { get; set; }
        public double Total { get; set; }
        public double KDV { get; set; }
        public int i { get; set; }
        public double? Remaining { get; set; }
        public int RemainInvoiceCount { get; set; }

        public double? TotalPaidedInvoiceTotal { get; set; }
        public List<OfferPositions> OfferPositons { get; set; }

        public List<int?> OfferPositonsID { get; set; }
        public List<Positions> Positons { get; set; }
        public List<OfferPositions> OfferPositionList { get; set; }
        public List<ExpenseCenter> ExpenseCenterList { get; set; }

        public InvoiceRow SingleInvoice { get; set; }

        public Offer SingleOffer { get; set; }

        public int OfferID { get; set; }

    }


}