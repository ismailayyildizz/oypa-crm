//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OykaCrm.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocumentType
    {
        public int ID { get; set; }
        public string TypeName { get; set; }
        public string SortBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
