﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class LayoutControlModel
    {
        public Employee CurrentUser { get; set; }
        public LayoutControlModel()
        {
            if (HttpContext.Current.Session["profile"] != null)
            {
                CurrentUser = HttpContext.Current.Session["profile"] as Employee;
            } 
        }
    }
}