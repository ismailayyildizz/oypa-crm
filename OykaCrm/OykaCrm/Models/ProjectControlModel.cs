﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ProjectControlModel
    {
        public int ProjectID { get; set; }
        public string Code { get; set; }
        public string ProjectName { get; set; }
        public Nullable<int> RegionID { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<int> RecordEmployeeID { get; set; }
        public string RecordIP { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectSubject { get; set; }
        public string ProjectArea { get; set; }
        public Projects SingleProject { get; set; }
        public List<Projects> Projects { get; set; }
        public List<Offer> Offers { get; set; }
        public List<Opportunity> Opportunities { get; set; }
        public List<Offer> Contracts { get; set; }
        public List<Offer> Invoices { get; set; }
        public List<Documents> Documents { get; set; }
        public List<OfferContractTemplate> ContractTemplateList { get; set; } = new List<OfferContractTemplate>();


    }
}