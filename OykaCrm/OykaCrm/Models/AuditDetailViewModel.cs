﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class AuditDetailViewModel
    {
        public RoleGroup RoleGroup { get; set; }
        public string rolelevelname { get; set; }
        public IEnumerable<RoleLevel> RoleLevel { get; set; }

        public IEnumerable<RoleGroupPermissions> PermList { get; set; }

        public IEnumerable<Permission> PermissionList { get; set; }
    }
}   