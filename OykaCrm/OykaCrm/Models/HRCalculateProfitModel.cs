﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class HRCalculateProfitModel
    {
        public Offer SingleOffer { get; set; }
        public OfferHistory SingleOfferHistory { get; set; }
        public List<PositionItems>  PositionItemList { get; set; }
        public List<OfferHRProfitItems>  SelectedHrProfitItemList { get; set; }
        public List<OfferHRProfitItemHistory>  SelectedHrProfitItemListHistory { get; set; }


    }
}