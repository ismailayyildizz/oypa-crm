﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm
{
    public class SalaryManager
    {
        public double BrutUcret { get; set; }
        public double IsciSSKOran { get; set; }  // 0.140  , emekli 0.075
        public double IsciSSKKesintisi { get; set; }  // 0.140  * brut
        public double IsciIssizlikOrani { get; set; } // 0.01 emekli 0
        public double IsciIssizlikKesintisi { get; set; } // 0.01 emekli 0
        public double SSKKesintilerToplami { get; set; } // IsciSSKOran + IsciIssizlikOranı
        public double GVMatrah { get; set; } // BrutUcret - SSKKesintilerToplami
        public double GelirVergisiOrani { get; set; } // 0,15
        public double DamgaVergisiOrani { get; set; } // 0.00759
        public double GelirVergisi { get; set; } // GVMatrah * 0,15
        public double IsciDamgaVergisi { get; set; } // 0.00759 * BrutUcret
        public double KesintilerToplami { get; set; } //SSKKesintilerToplami+GelirVergisi+IsciDamgaVergisi
        public double NetUcret { get; set; } // BrutUcret - KesintilerToplami
        public double SgkIsverenOrani { get; set; } //0,205 , emekli 0,245 
        public double SgkIsverenPayi { get; set; }
        public double SgkIssizlikIsverenOrani { get; set; } //0.02 emekli 0
        public double SgkIssizlikIsverenPayi { get; set; }



        public IEnumerable<PositionParameter> Parameters { get; set; }
        public OypaDbEntities db { get; set; }


        // public SalaryManager(int Status, double brutUcret) normlade metodun orjinal parametreleri bu şekildedir.
        public SalaryManager(int Status, double brutUcret, double? gvOrani)
        {
            db = new OypaDbEntities();

            Parameters = db.PositionParameter.Where(x => x.PositionID == null && x.StatusID == Status).ToList();

            IsciSSKOran = Convert.ToDouble(Parameters.FirstOrDefault(x => x.ParameterCode == "IsciSSKOran").ParameterValue.Value);
            IsciIssizlikOrani = Convert.ToDouble(Parameters.FirstOrDefault(x => x.ParameterCode == "IsciIssizlikOrani").ParameterValue.Value);
            // GelirVergisiOrani = Convert.ToDouble(Parameters.FirstOrDefault(x => x.ParameterCode == "GelirVergisiOrani").ParameterValue.Value);

            GelirVergisiOrani = gvOrani.Value;

            DamgaVergisiOrani = Convert.ToDouble(Parameters.FirstOrDefault(x => x.ParameterCode == "DamgaVergisiOrani").ParameterValue.Value);
            SgkIsverenOrani = Convert.ToDouble(Parameters.FirstOrDefault(t => t.ParameterCode == "SgkIsverenOrani").ParameterValue);
            SgkIssizlikIsverenOrani = Convert.ToDouble(Parameters.FirstOrDefault(t => t.ParameterCode == "SgkIssizlikIsverenOrani").ParameterValue);
            BrutUcret = brutUcret;

        }

        public SalaryModel GetSalary()
        {
            SalaryModel model = new SalaryModel();

            model.BrutUcret = BrutUcret;
            model.DamgaVergisiOrani = DamgaVergisiOrani;
            model.GelirVergisiOrani = GelirVergisiOrani;
            model.IsciIssizlikOrani = IsciIssizlikOrani;
            model.SgkIsverenOrani = SgkIsverenOrani;
            model.SgkIssizlikIsverenOrani = SgkIssizlikIsverenOrani;
            model.IsciSSKOran = IsciSSKOran;
            model.SgkIsverenPayi = (model.BrutUcret * model.SgkIsverenOrani);//=>
            model.SgkIssizlikIsverenPayi = (model.BrutUcret * model.SgkIssizlikIsverenOrani);//=>
            model.IsciSSKKesintisi = (model.BrutUcret * model.IsciSSKOran); // 0,014
            model.IsciIssizlikKesintisi = (model.BrutUcret * model.IsciIssizlikOrani);


            //SgkIssizlikIsverenPayi ve SgkIsverenPayi brüt ücretten çıkarıldı?
            model.SSKKesintilerToplami = (model.IsciSSKKesintisi + model.IsciIssizlikKesintisi);
            model.GVMatrah = (model.BrutUcret - model.SSKKesintilerToplami);
            model.GelirVergisi = (model.GVMatrah * model.GelirVergisiOrani);
            model.IsciDamgaVergisi = (model.BrutUcret * model.DamgaVergisiOrani);
            model.KesintilerToplami = (model.SSKKesintilerToplami + model.GelirVergisi + model.IsciDamgaVergisi);
            model.NetUcret = (model.BrutUcret - model.KesintilerToplami);




            return model;

        }

    }
}