﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm
{
    public class OfferDetailViewModel
    {
        public Offer singleoffer;
        public List<OfferProducts> offerproducts;
        public List<OfferEquipment> offerequipments;
        public List<OfferPositions> offerpositions;

    }
}