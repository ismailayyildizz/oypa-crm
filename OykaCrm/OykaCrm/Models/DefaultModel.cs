﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class DefaultModel
    {
        public int ProjectCount { get; set; }
        public int OpportunityCount { get; set; }
        public int OfferCount { get; set; }
        public int CustomerCount { get; set; }
        public int IsOneTimeOffer { get; set; }
        public int NormalOffer { get; set; }
        public IEnumerable<Opportunity> OppList { get; set; }
        public IEnumerable<Offer> OfferList { get; set; }
        public List<PopulerCustomer> PopulerCustomerList { get; set; }
        public MonthlyOpportunityCount SingleMonthlyOpportunityCount { get; set; }
        public OfferStatusCount SingleOfferStatusCount { get; set; }


    }
    public class PopulerCustomer
    {
        public string Customer { get; set; }
        public int Count { get; set; }
    }
    public class MonthlyOpportunityCount
    {
       
        public int ConfirmJan { get; set; }
        public int WaitingJan { get; set; }
        public int RefuseJan { get; set; }

        public int ConfirmFeb { get; set; }
        public int WaitingFeb { get; set; }
        public int RefuseFeb { get; set; }

        public int ConfirmMar { get; set; }
        public int WaitingMar { get; set; }
        public int RefuseMar { get; set; }

        public int ConfirmApr { get; set; }
        public int WaitingApr { get; set; }
        public int RefuseApr { get; set; }

        public int ConfirmMay { get; set; }
        public int WaitingMay { get; set; }
        public int RefuseMay { get; set; }

        public int ConfirmJune { get; set; }
        public int WaitingJune { get; set; }
        public int RefuseJune { get; set; }

        public int ConfirmJuly { get; set; }
        public int WaitingJuly { get; set; }
        public int RefuseJuly { get; set; }


        public int ConfirmAug { get; set; }
        public int WaitingAug { get; set; }
        public int RefuseAug { get; set; }

        public int ConfirmSep { get; set; }
        public int WaitingSep { get; set; }
        public int RefuseSep { get; set; }

        public int ConfirmOct { get; set; }
        public int WaitingOct { get; set; }
        public int RefuseOct { get; set; }

        public int ConfirmNov { get; set; }
        public int WaitingNov { get; set; }
        public int RefuseNov { get; set; }

        public int ConfirmDec { get; set; }
        public int WaitingDec { get; set; }
        public int RefuseDec { get; set; }
        
    }

    public class OfferStatusCount
    {
        public int IlkKayit { get; set; }
        public int TeklifHazirlik { get; set; }
        public int SatinAlmaUzmOnay { get; set; }
        public int SatinAlmaUzmRed { get; set; }
        public int SatinAlmaMudRed { get; set; }
        public int SatinAlmaMudOnay { get; set; }
        public int IKOnay { get; set; }
        public int IKRed { get; set; }
        public int Iptal { get; set; }
        public int FiyatRevizyon { get; set; }
        public int MusteriOnayBekliyor { get; set; }
        public int SozlesmeHazirlik { get; set; }
    }
}