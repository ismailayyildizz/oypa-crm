﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferNewViewModel
    {

        public List<Customers> CustomerList { get; set; }

        public string GetOfferNumber { get; set; }
    }
}