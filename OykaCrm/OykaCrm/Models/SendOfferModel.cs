﻿using System.Collections.Generic;

namespace OykaCrm.Models
{
    public class SendOfferModel
    {
        public SendOfferModel()
        {
            CustomerEmailId = new List<string>();
        }
        public int? offerID { get; set; }
        public List<string> CustomerEmailId { get; set; }
    }
}