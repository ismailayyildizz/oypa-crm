﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class MissingDayControlModel
    {
        public List<MissingDay> MdList { get; set; }
        public bool IsActive { get; set; }
        public int RowID { get; set; }
        public double? TotalTermInvoiceAmount { get; set; }

        public string Description { get; set; }
        public string MonthName { get; set; }

        public bool IsEmpty { get; set; }
    }
}