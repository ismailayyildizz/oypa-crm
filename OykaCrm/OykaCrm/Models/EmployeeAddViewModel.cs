﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OykaCrm
{
    public class EmployeeAddViewModel : LayoutControlModel
    {
        public int ID { get; set; }
        public string FullName { get; set; }

        [Required(ErrorMessage = "*")]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",ErrorMessage ="E-Posta yanliş!")]
        public string EMail { get; set; }

        public string Password { get; set; }

        public string Code { get; set; }

        public string Title { get; set; }

        public IEnumerable<RoleGroup> RoleGroup { get; set; }

        public int? RoleGroupID { get; set; }

        public IEnumerable<Region> Region { get; set; }

        public int? RegionID { get; set; }

        public int? ManagerID { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? RecordDate { get; set; }

        public int? RecordEmployeeId { get; set; }

        public IEnumerable<Employee> Employees { get; set; }

    }
}