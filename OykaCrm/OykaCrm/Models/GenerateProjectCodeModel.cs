﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class GenerateProjectCodeModel
    {
        public string OpportunityNumber { get; set; }
        public int CityId { get; set; }
        public int CountyId { get; set; }
        public string CustomerName { get; set; }
    }
}