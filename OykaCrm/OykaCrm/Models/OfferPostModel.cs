﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferPostModel
    {
        public string teklifaciklamasi { get; set; }
        public string musteriadi { get; set; }
        public string duzenlemetarihi { get; set; }
        public string vadetarihi { get; set; }
        public string parabirimi { get; set; }
        public string kosullar { get; set; }

        public List<string> urunadi { get; set; }

        public List<string> urunaciklamasi { get; set; }

        public List<string> urunfiyat { get; set; }

        public List<string> urunbirim { get; set; }
        public List<string> urunmiktar { get; set; }

        public string aratoplam { get; set; }

        public string bruttoplam { get; set; }

        public string toplamkdv { get; set; }

        public string geneltoplam { get; set; }



    }
}