﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferArchiveControlModel
    {
        public bool IsOneTime { get; set; }
        public List<OfferProductsHistory> PrdHistoryList { get; set; } 
        public List<OfferEquipmentHistory> EquipmentHistoryList { get; set; }
        public List<OfferPositionsHistory> PositionHistoryList { get; set; }
        public List<OfferOtherExpenseHistory> OtherExpenseHistoryList { get; set; }
        public OfferHistory SingleOfferHistory { get; set; }

        public List<IsOneTimeOfferPositonHistory> IsOneTimePositionHistoryList { get; set; }
        public List<IsOneTimeOtherExpenseHistory> IsOneTimeOtherExpenseHistoryList { get; set; }
    }
}