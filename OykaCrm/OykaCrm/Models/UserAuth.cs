﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Models
{
    public class UserAuth: AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session["username"] != null)
            {
                return true;
            }
            else
            {
                httpContext.Response.Redirect("/Login/Index");
                return false;
            }
        }
    }
}