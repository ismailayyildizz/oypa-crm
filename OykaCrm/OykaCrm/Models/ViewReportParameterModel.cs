﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ViewReportParameterModel
    {
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ReportTypeID { get; set; }
    }
}