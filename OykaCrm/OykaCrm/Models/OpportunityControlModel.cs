﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OpportunityControlModel
    {
        public List<Opportunity> OpportunityList { get; set; }
        public List<Offer> OfferList{ get; set; }
    }
}