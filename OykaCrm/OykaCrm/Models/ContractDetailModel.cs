﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ContractDetailModel
    {
        public List<ContractTemplate> ContractTemplates { get; set; }
        public Offer singleoffer;
        public ContractTemplate singletemplate { get; set; }
        public int OfferID { get; set; }
        public OfferContractTemplate SingleOfferContract { get; set; }

    }
}