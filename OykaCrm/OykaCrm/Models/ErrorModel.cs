﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public ModelStatee ModelState { get; set; } = new ModelStatee();
    }
    public class ModelStatee
    {
        public List<string> ValError0 { get; set; }
        public List<string> ValError1 { get; set; }
    }
}