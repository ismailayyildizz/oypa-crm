//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OykaCrm.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IsOneTimeOfferPositonHistory
    {
        public int ID { get; set; }
        public Nullable<int> VersionNumber { get; set; }
        public Nullable<int> VersionDigit { get; set; }
        public Nullable<int> OfferID { get; set; }
        public Nullable<int> PositionID { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> PositionItemID { get; set; }
        public Nullable<double> Price { get; set; }
        public string Currency { get; set; }
        public Nullable<double> WorkingHour { get; set; }
        public Nullable<double> OvertimePay { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
    
        public virtual IsOneTimePositionItems IsOneTimePositionItems { get; set; }
        public virtual Positions Positions { get; set; }
    }
}
