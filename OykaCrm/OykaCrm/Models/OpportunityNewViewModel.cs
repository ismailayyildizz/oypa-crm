﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OpportunityNewViewModel : LayoutControlModel
    {
        public List<Customers> CustomersList { get; set; }

        public string OpportunityNumber { get; set; }

        public List<OpportunityStatus> StatusList { get; set; }

        public int ProjectID { get; set; }
        public List<Projects> ProjectList { get; set; }

        public List<Cities> CityList { get; set; } = new List<Cities>();
    }
}