﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferPDFControlModel
    {
        public List<Offer> OfferList { get; set; }
        public string ReportName { get; set; }
        public DateTime DateBegin { get; set; }
        public DateTime DateEnd { get; set; }
    }
}