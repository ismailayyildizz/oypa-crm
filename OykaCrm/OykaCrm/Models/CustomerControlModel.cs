﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class CustomerControlModel
    {
        public int CustomerID { get; set; }
        public string AccountCode { get; set; }
        public string CustomerFullName { get; set; }
        public Nullable<int> RegionID { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public string EMail { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? CityID { get; set; }
        public string CountyName { get; set; }
        public int CountyId { get; set; }
        public string CityName { get; set; }
        public string PostCode { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string LogoID { get; set; }
        public string LogoCode { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Customers SingleCustomer { get; set; }
        public List<Customers> Customers { get; set; }
        public List<Cities> CityList { get; set; }
        public List<County> Counties { get; set; }

        
 
        public List<Currency> Currencies { get; set; }
        public string Title { get; set; }
        public string FaxNumber { get; set; }
        public string IBAN { get; set; }
        public System.DateTime OpeningDate { get; set; }
        public string CurrencyType { get; set; }
        public string OpeningBalance { get; set; }
        public string NotesForContact { get; set; }
        public string County_City { get; set; }
        public int i { get; set; }
        public List<CustomerContact> CustomerContacts { get; set; }

        public string FormatDate { get; set; }

    }
}