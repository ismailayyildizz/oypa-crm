﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferAddPostModel
    {

        public string OfferNumber { get; set; }

        public string select { get; set; }

        public string OfferDetail { get; set; }

        public string Customer { get; set; }

        public DateTime duzenlemetarihi { get; set; }

        public DateTime vadetarihi { get; set; }


    }
}