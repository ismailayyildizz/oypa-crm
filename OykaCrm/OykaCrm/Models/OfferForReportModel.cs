﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferForReportModel
    {
        public int OfferID { get; set; }
        public string OfferNumber { get; set; }
        public Projects Projects { get; set; }
        public Customers Customers { get; set; }
        public DateTime? DateBegin { get; set; }
        public DateTime? DateEnd { get; set; }
        public double? GenelToplam { get; set; }
        public int ReportTypeID { get; set; }
        public int? OpportunityID { get; set; }
        public string StatusName { get; set; }

        public DateTime? FilterDateBegin { get; set; }
        public DateTime? FilterDateEnd { get; set; }

        public double? BrutToplamProfitability { get; set; }
        public double? OtherProfitability { get; set; }
        public double? MachineProfitability { get; set; }
        public double? AraToplamProfitability { get; set; }


    }
}