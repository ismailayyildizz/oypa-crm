﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ContractControlModel
    {
        public List<Offer> Contracts { get; set; } //statusLevel=8 olan offers
        public List<OfferContractTemplate> ContractTemplateList { get; set; } = new List<OfferContractTemplate>();

    }
}