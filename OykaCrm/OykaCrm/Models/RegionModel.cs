﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class RegionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GroupMail { get; set; }
    }
}