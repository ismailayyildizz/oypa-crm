﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class CalculateAllPrice
    {
        public string Brut { get; set; }

        public string YillikIzin { get; set; }

        public string KidemTaz { get; set; }

        public string IhbarTaz { get; set; }

        public string BayramMesai { get; set; }

        public string Yol { get; set; }
        public string Yemek { get; set; }

        public string SaglikGideri { get; set; }

        public string IsgGideri { get; set; }

        public string PoliceGideri { get; set; }

        public string IhaleBelgeMasrafi { get; set; }

        public string FazlaMesai { get; set; }

        public string SaglikSigortasi { get; set; }

        public string BireyselEmeklilik { get; set; }
        public string EgitimBelgeGideri { get; set; }

        public string RadioYol { get; set; }

        public int OfferID { get; set; }

        public string RadioYemek { get; set; }

        public int rowID { get; set; }
        public string radioFazlaMesai { get; set; }

        public string GvOrani { get; set; }

    }
}