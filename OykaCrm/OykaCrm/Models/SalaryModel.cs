﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm
{
    public class SalaryModel
    {
        public double BrutUcret { get; set; }
        public double IsciSSKOran { get; set; }  // 0.140  , emekli 0.075
        public double IsciSSKKesintisi { get; set; }
        public double IsciIssizlikKesintisi { get; set; }
        public double IsciIssizlikOrani { get; set; } // 0.01 emekli 0
        public double SSKKesintilerToplami { get; set; } // IsciSSKOran + IsciIssizlikOranı
        public double GVMatrah { get; set; } // BrutUcret - SSKKesintilerToplami
        public double GelirVergisiOrani { get; set; } // 0,15
        public double DamgaVergisiOrani { get; set; } // 0.00759
        public double GelirVergisi { get; set; } // GVMatrah * 0,15
        public double IsciDamgaVergisi { get; set; } // 0.00759 * BrutUcret
        public double KesintilerToplami { get; set; } //SSKKesintilerToplami+GelirVergisi+IsciDamgaVergisi
        public double NetUcret { get; set; } // BrutUcret - KesintilerToplami

        public double SgkIsverenOrani { get; set; } //0,205 , Emekli:0,245
        public double SgkIsverenPayi { get; set; } //Normal Çalışan: brüt * 0,205 , Emekli: brüt*0,245

        public double  SgkIssizlikIsverenOrani{ get; set; }  //0.02 emekli 0
        public double  SgkIssizlikIsverenPayi { get; set; }//Normal Çalışan: brüt * 0,205 , Emekli: brüt*0

    }
}