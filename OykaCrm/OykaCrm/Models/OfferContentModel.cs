﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OfferContentModel
    {
        public OfferContentTemplate singletemplate { get; set; }
        public OfferPreContent precontent { get; set; }

        public int OfferID { get; set; }
        public Offer SingleOffer { get; set; }
        public OfferContract SingleOfferContract { get; set; }
    }
}