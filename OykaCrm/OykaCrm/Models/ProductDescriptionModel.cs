﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class ProductDescriptionModel
    {
        public OfferProducts Product { get; set; }
        public Offer Offer{ get; set; }
    }
}