﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class AddMissingDayValues
    {
        public int PositionID { get; set; }

        public int RowID { get; set; }

        public string Quantity { get; set; }
        public string UnitePrice { get; set; }
        public string ExpectHRPrice { get; set; }
        public string HrUnitePrice { get; set; }
        public string MonthlyWorkingDay { get; set; }
        public string MonthlyMissingDay { get; set; }
        public string TermWorkingDay { get; set; }
        public string TermHRUnitPrice { get; set; }
        public string TermInvoiceAmount { get; set; }
    }
}