﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class CalculateMissingDayParamaters
    {

        public string HRUnitPrice { get; set; }
        public string MonthlyWorkingDay { get; set; }
        public string MonthlyMissingDay { get; set; }
        public int RowID { get; set; }
        public int PositionID { get; set; }
        public string Description { get; set; }
    }
}