//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OykaCrm.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class OfferOtherExpenses
    {
        public int ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> OfferID { get; set; }
        public Nullable<double> Price { get; set; }
        public string Description { get; set; }
        public Nullable<int> RecordUserID { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<int> UpdateUserID { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    }
}
