﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class IsOneTimeCalculate
    {
        public int Quantity { get; set; }

        public int RowID { get; set; }

        public string WorkingHour { get; set; }

        public string OveritmePay { get; set; }

        public string RoadExpense { get; set; }

        public string FoodExpense{ get; set; }
    }
}