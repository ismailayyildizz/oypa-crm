﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OykaCrm.Models
{
    public class OvertimeDetailModel
    {
        public List<Overtime> ListOvertime { get; set; }
        public List<Positions> Positons { get; set; }
        public InvoiceRow InvoiceRow { get; set; }
        public List<OfferPositions> OfferPositionList { get; set; }
        public string Description { get; set; }
        public int? OfferID { get; set; }

    }

}