﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Data.Entity;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.UI;

namespace OykaCrm.Controllers
{
    [UserAuth]
    public class ReportController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        //Tarih aralıgında raporlar
        public ActionResult DateFilteredReports()
        {
            var model = Db.ReportTypes.ToList();
            return View(model);
        }

        //Tarih aralıgındaki raporları görüntüleme method
        public List<OfferForReportModel> GetReportForDateFiltered(DateTime BeginDate, DateTime EndDate, List<Offer> ListOfOffers, int reportTypeID)
        {
            List<OfferForReportModel> offerListModel = new List<OfferForReportModel>();

            foreach (var item in ListOfOffers)
            {
                var reportModel = new OfferForReportModel();
                reportModel.OfferID = item.ID;
                reportModel.OfferNumber = item.OfferNumber;
                reportModel.Projects = item.Projects;
                reportModel.Customers = item.Customers;
                reportModel.DateBegin = item.DateBegin;
                reportModel.DateEnd = item.DateEnd;
                reportModel.GenelToplam = item.GenelToplam.Value;
                reportModel.OpportunityID = item.OpportunityID;
                reportModel.ReportTypeID = reportTypeID;

                reportModel.FilterDateBegin = BeginDate;
                reportModel.FilterDateEnd = EndDate;

                if (reportTypeID == 3)
                {
                    var offer = Db.Offer.Where(i => i.ID == item.ID).FirstOrDefault();

                    if (offer.IsOneTime == true)
                    {
                        //brüt toplam karlılık oranı
                        reportModel.BrutToplamProfitability = offer.TotalHrProfit;

                        //Diger karlılık oranı
                        reportModel.OtherProfitability = offer.SumOtherExpenseProfit;

                        //Malzeme Stok 
                        reportModel.AraToplamProfitability = offer.SumAraToplamProfit;

                        //Makine demirbaş
                        offer.TotalMachineExpense = Db.OfferEquipment.Where(i => i.OfferID == offer.ID).Sum(i => (i.CostPrice * i.Quantity));
                        offer.TotalMonthlyDepreciation = Db.OfferEquipment.Where(i => i.OfferID == offer.ID).Sum(i => i.MonthlyDepreciation);
                        double? sumMachineExpenseProfitRate = 0;

                        if (offer.TotalMachineExpense == null)
                        {
                            offer.TotalMachineExpense = 0;
                        }
                        if (offer.ManagementExpenseRateMD != null) // makine/demirbas icin isletme gideri orani 
                        {
                            sumMachineExpenseProfitRate += offer.ManagementExpenseRateMD;
                        }
                        if (offer.ProfitRateForMachine != null)  // makine/demirbas icin karlilik orani 
                        {
                            sumMachineExpenseProfitRate += offer.ProfitRateForMachine;
                        }
                        if (offer.ManagementExpenseRateMD == null && offer.ProfitRateForMachine == null)
                        {
                            offer.SumMachineExpenseProfit = 0;
                        }

                        if (offer.TotalHrProfit == null)
                        {
                            offer.TotalHrProfit = 0;
                        }

                        offer.SumMachineExpenseProfit = (offer.TotalMachineExpense * (sumMachineExpenseProfitRate / 100));
                        //Aylik Amortisman fiyatınada karlılık oranı eklenecek!
                        offer.TotalMonthlyDepreciation = (offer.TotalMonthlyDepreciation * (sumMachineExpenseProfitRate / 100));
                        // amortisman toplam ucreti

                        reportModel.MachineProfitability = offer.SumMachineExpenseProfit+ offer.TotalMonthlyDepreciation;

                    }
                    else
                    {
                        //İk
                        reportModel.BrutToplamProfitability = offer.TotalHrProfit;
                        //Diğer
                        reportModel.OtherProfitability = offer.SumOtherExpenseProfit;
                        //Malzeme Stok 

                        reportModel.AraToplamProfitability = offer.SumAraToplamProfit;
                    }
                }

                if (reportTypeID == 4)
                {
                    var offer = Db.Offer.Include(i => i.OfferStatus).Where(i => i.ID == item.ID).FirstOrDefault();

                    if (offer.OfferStatus != null)
                    {
                        reportModel.StatusName = offer.OfferStatus.StatusName;
                    }
                    else
                    {
                        reportModel.StatusName = "-";
                    }
                }

                offerListModel.Add(reportModel);
            }

            return offerListModel;
        }


        //Tarih aralıgında raporları görüntüleme(Partial)
        [HttpPost]
        public ActionResult ViewReportForDateFiltered(ViewReportParameterModel model)
        {
            var offerListModel = new List<OfferForReportModel>();
            var offerList = new List<Offer>();

            if (model.BeginDate > model.EndDate)
            {
                return Json(new { failed = true, message = "Başlangıç tarihi bitiş tarihinden büyük olamaz.." }, JsonRequestBehavior.AllowGet);
            }
            if (model.ReportTypeID == 1) //Verilen Teklifler Raporu
            {
                offerList = Db.Offer.Include(x => x.Projects).Include(x => x.Customers).Where(i => (i.DateBegin >= model.BeginDate && i.DateBegin <= model.EndDate) && i.IsActive == true).ToList();

                if (offerList != null && offerList.Count > 0)
                {
                    ViewBag.typeID = 1;
                    offerListModel = GetReportForDateFiltered(model.BeginDate, model.EndDate, offerList, 1);

                    OypaHelper.AddApplicationLog("Management", "ViewReportForDateFiltered", "Report", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.BeginDate} - {model.EndDate} tarihleri arasındaki Verilen Teklifler Raporunu görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return PartialView(@"~\Views\Report\_PartialOfferReports.cshtml", offerListModel);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında teklif bulunamadı, tekrar deneyiniz.." }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (model.ReportTypeID == 2) //Kesilen Faturalar Raporu
            {
                //en az bir faturasi kesilmis offerlar
                var invoicedOffers = Db.InvoiceRow.Where(i => i.IsPaid == true).Select(i => i.OfferID).Distinct();

                foreach (var item in invoicedOffers)
                {
                    var offer = Db.Offer.Where(i => i.ID == item && (i.DateBegin >= model.BeginDate && i.DateBegin <= model.EndDate) && i.IsActive == true).FirstOrDefault();
                    if (offer != null)
                    {
                        offerList.Add(offer);
                    }
                }

                if (offerList != null && offerList.Count > 0)
                {
                    ViewBag.typeID = 2;
                    offerListModel = GetReportForDateFiltered(model.BeginDate, model.EndDate, offerList, 2);

                    OypaHelper.AddApplicationLog("Management", "ViewReportForDateFiltered", "Report", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.BeginDate} - {model.EndDate} tarihleri arasındaki Kesilen Faturalar Raporunu görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return PartialView(@"~\Views\Report\_PartialOfferReports.cshtml", offerListModel);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında faturalandırılmış teklif bulunamadı, tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (model.ReportTypeID == 3) //Teklif bazında karlılık raporu
            {
                offerList = Db.Offer.Where(i => (i.DateBegin >= model.BeginDate && i.DateBegin <= model.EndDate) && i.IsActive == true).ToList();
                if (offerList != null && offerList.Count > 0)
                {
                    ViewBag.typeID = 3;
                    offerListModel = GetReportForDateFiltered(model.BeginDate, model.EndDate, offerList, 3);

                    OypaHelper.AddApplicationLog("Management", "ViewReportForDateFiltered", "Report", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.BeginDate} - {model.EndDate} tarihleri arasındaki Teklif Bazında Karlılık Raporunu görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return PartialView(@"~\Views\Report\_PartialOfferReports.cshtml", offerListModel);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında karlılık oranları gösterilecek teklif bulunamadı, tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (model.ReportTypeID == 4) //Teklif Durum Raporu
            {
                offerList = Db.Offer.Where(i => (i.DateBegin >= model.BeginDate && i.DateBegin <= model.EndDate) && i.IsActive == true).ToList();
                if (offerList != null && offerList.Count > 0)
                {
                    ViewBag.typeID = 4;
                    offerListModel = GetReportForDateFiltered(model.BeginDate, model.EndDate, offerList, 4);

                    OypaHelper.AddApplicationLog("Management", "ViewReportForDateFiltered", "Report", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.BeginDate} - {model.EndDate} tarihleri arasındaki Teklif Durum Raporunu görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return PartialView(@"~\Views\Report\_PartialOfferReports.cshtml", offerListModel);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında teklif bulunamadı, tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                }
            }

            else
            {
                return Json(new { failed = true, message = "Rapor tipini seçiniz.." }, JsonRequestBehavior.AllowGet);
            }
        }

        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Teklif Durum Raporları
        public ActionResult OfferStatusReports()
        {
            var status = Db.OfferStatus.Where(x => x.IsActive == true).ToList();
            return View(status);

        }


        //Teklif Durum Raporları Görüntüleme (Partial)
        [HttpPost]
        public ActionResult ViewReportForOfferStatus(string selectOfferStatus)
        {
            var convertOfferStatus = Convert.ToInt32(selectOfferStatus);
            var offerList = Db.Offer.Include(x => x.OfferStatus).Where(x => x.StatusLevel == convertOfferStatus && x.IsActive == true).ToList();

            var model = new List<ReportForOfferStatus>();

            if (offerList.Count != 0)
            {
                foreach (var item in offerList)
                {
                    var md = new ReportForOfferStatus();
                    md.OfferID = item.ID;
                    md.OfferNumber = item.OfferNumber;
                    md.Projects = item.Projects; ;
                    md.Customers = item.Customers;
                    md.DateBegin = item.DateBegin;
                    md.DateEnd = item.DateEnd;
                    md.OpportunityID = item.OpportunityID;
                    md.StatusLevel = item.OfferStatus != null ? item.OfferStatus.StatusLevel : 0;
                    md.OfferStatus = item.OfferStatus;
                    md.GenelToplam = item.GenelToplam;

                    model.Add(md);
                }

                OypaHelper.AddApplicationLog("Management", "ViewReportForOfferStatus", "Report", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, durumu {convertOfferStatus} numaralı olan tekliflerin Teklif Durum Raporunu görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return PartialView(@"~\Views\Report\_PartialViewReportForOfferStatus.cshtml", model);
            }
            else
            {
                return Json(new { failed = true, message = "Seçilen sürece ait teklif bulunamadı." }, JsonRequestBehavior.AllowGet);
            }

        }


        // ///////////////////////////////////////////// Müşteri Bazlı Teklif Raporları  ///////////////////////////////////////////////////

        //Müşteri Bazlı Teklifler listesi 

        public ActionResult CustomersReports()
        {

            var customers = Db.Customers.ToList();
            return View(customers);
        }

        //Müşteri Raporları Görüntüleme (Partial)
        [HttpPost]
        public ActionResult ViewReportForCustomer(string selectCustomer)
        {
            var convertCustomer = Convert.ToInt32(selectCustomer);

            var offerList = Db.Offer.Include(x => x.OfferStatus).Where(x => x.CustomerID == convertCustomer && x.IsActive == true).ToList();

            if (offerList.Count != 0)
            {
                OypaHelper.AddApplicationLog("Management", "ViewReportForCustomer", "Report", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {convertCustomer} Id'li müşterinin tekliflerini görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return PartialView(@"~\Views\Report\_PartialCustomerReports.cshtml", offerList);
            }
            else
            {
                return Json(new { failed = true, message = "Seçilen müşteriye ait teklif bulunamadı." }, JsonRequestBehavior.AllowGet);
            }
        }

        // /////////////////////////////////////  PDF Dönüşüm ////////////////////////////////////////////////////

        //Tarih Filtreli Pdf Raporları
        public ActionResult DownPdf(int reportTypeID, string beginDate, string endDate)
        {
            List<Offer> offerList = new List<Offer>();
            OfferPDFControlModel model = new OfferPDFControlModel();
            var bDate = Convert.ToDateTime(beginDate);
            var eDate = Convert.ToDateTime(endDate);
            if (reportTypeID == 1)
            {
                offerList = Db.Offer.Include(x => x.Projects).Include(x => x.Customers).Where(i => (i.DateBegin >= bDate && i.DateBegin <= eDate) && i.IsActive == true).ToList();

                if (offerList != null && offerList.Count > 0)
                {
                    model.OfferList = offerList;
                    model.ReportName = Db.ReportTypes.Find(reportTypeID) != null ? Db.ReportTypes.Find(reportTypeID).Description : " ";
                    model.DateBegin = bDate;
                    model.DateEnd = eDate;
                    return new PartialViewAsPdf("_PartialOfferReportsForPDF", model); //butonsuz falan farklı partial, pdf icin
                }

                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında teklif bulunamadı, tekrar deneyiniz.." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (reportTypeID == 2)
            {
                //en az bir faturasi kesilmis offerlar
                var invoicedOffers = Db.InvoiceRow.Where(i => i.IsPaid == true).Select(i => i.OfferID).Distinct();

                foreach (var item in invoicedOffers)
                {
                    var offer = Db.Offer.Where(i => i.ID == item && (i.DateBegin >= bDate && i.DateBegin <= eDate) && i.IsActive == true).FirstOrDefault();
                    if (offer != null)
                    {
                        offerList.Add(offer);
                    }
                }

                if (offerList != null && offerList.Count > 0)
                {
                    model.OfferList = offerList;
                    model.ReportName = Db.ReportTypes.Find(reportTypeID) != null ? Db.ReportTypes.Find(reportTypeID).Description : " ";
                    model.DateBegin = bDate;
                    model.DateEnd = eDate;
                    return new PartialViewAsPdf("_PartialOfferReportsForPDF", model);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında faturalandırılmış teklif bulunamadı, tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                }
            }
            //type 3 ve type 4 icin de yapilacak
            else if (reportTypeID == 3)
            {
                offerList = Db.Offer.Where(i => (i.DateBegin >= bDate && i.DateBegin <= eDate) && i.IsActive == true).ToList();


                if (offerList != null && offerList.Count > 0)
                {
                    var md = GetReportForDateFiltered(bDate, eDate, offerList, 3);
                    return new PartialViewAsPdf("_PartialViewReportsOfferFilterPDF", md);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında teklif bulunamadı, tekrar deneyiniz.." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (reportTypeID == 4)
            {
                offerList = Db.Offer.Where(i => (i.DateBegin >= bDate && i.DateBegin <= eDate) && i.IsActive == true).ToList();
                if (offerList != null && offerList.Count > 0)
                {
                    var md = GetReportForDateFiltered(bDate, eDate, offerList, 4);
                    return new PartialViewAsPdf("_PartialViewOfferStatusPDF", md);
                }
                else
                {
                    return Json(new { failed = true, message = "Girilen tarih aralığında teklif bulunamadı, tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { failed = true, message = "Rapor tipini seçiniz.." }, JsonRequestBehavior.AllowGet);
            }
        }

        //Müşteri Bazlı Teklifler Pdf Raporu
        public ActionResult DownPdfForCustomerReport(int customerId)
        {
            var list = Db.Offer.Include(x => x.OfferStatus).Where(x => x.CustomerID == customerId && x.IsActive == true).ToList();

            if (list != null && list.Count() > 0)
            {
                return new PartialViewAsPdf("_PartialViewReportForCustomerPDF", list);
            }
            else
            {
                return Json(new { failed = true, message = "Rapor tipini seçiniz.." }, JsonRequestBehavior.AllowGet);
            }

        }

        //Teklif Durumu Bazlı PDF Raporu
        public ActionResult DownPdfForOfferStatusReport(int statusLevel)
        {
            var list = Db.Offer.Include(x => x.OfferStatus).Where(x => x.StatusLevel == statusLevel && x.IsActive == true).ToList();

            if (list != null && list.Count() > 0)
            {
                return new PartialViewAsPdf("_PartialViewReportForFilteredOfferStatusPDF", list);
            }
            else
            {
                return Json(new { failed = true, message = "Rapor tipini seçiniz.." }, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult GetReport(List<OfferContentTemplate> prod)
        {
            prod = Db.OfferContentTemplate.Where(i => i.IsActive == true).ToList();
            return View(prod);
        }

        // ////////////////////////////////////  Excel' Dönüşüm ///////////////////////////////////////////////////////////////////////////////////

        //Excel'e dönüştürme FilteredDateReports
        public ActionResult ExportToExcelReport(string beginDate, string endDate, string reportTypeId)
        {
            object list = null;
            var convertBeginDate = Convert.ToDateTime(beginDate);
            var convertEndDate = Convert.ToDateTime(endDate);
            var convertReportTypeId = Convert.ToInt32(reportTypeId);

            //Verilen Teklifler Raporu
            if (convertReportTypeId == 1)
            {
                var firstList = Db.Offer.Include(x => x.Projects).Include(x => x.Customers).Where(i => i.DateBegin >= convertBeginDate && i.DateBegin <= convertEndDate && i.IsActive == true).Select(x => new
                {
                    x.OfferNumber,
                    x.Projects.ProjectName,
                    x.Customers.CustomerFullName,
                    x.DateBegin,
                    x.DateEnd,
                    x.GenelToplam,
                });

                list = firstList.ToList().Select(x => new
                {
                    Teklif_Numarasi = x.OfferNumber,
                    Proje_Adi = x.ProjectName != null ? x.ProjectName : "-",
                    Müsteri = x.CustomerFullName != null ? x.CustomerFullName : "-",
                    Baslangic_Tarihi = x.DateBegin != null ? x.DateBegin.Value.ToShortDateString() : "-",
                    Bitis_Tarihi = x.DateEnd != null ? x.DateEnd.Value.ToShortDateString() : "-",
                    Toplam_Tutarı = x.GenelToplam != null ? x.GenelToplam.Value.ToString("N2") : "-",
                }).ToList();

                Response.AddHeader("content-disposition", "attachment; filename=VerilenTekliflerRaporu.xls");

            }
            //Kesilmiş faturalar listesi
            if (convertReportTypeId == 2)
            {
                var offerList = new List<object>();

                var invoicedOffers = Db.InvoiceRow.Where(i => i.IsPaid == true).Select(i => i.OfferID).Distinct();

                foreach (var item in invoicedOffers)
                {
                    var offer = Db.Offer.Where(i => i.ID == item && (i.DateBegin >= convertBeginDate && i.DateBegin <= convertEndDate) && i.IsActive == true).Select(x => new
                    {
                        Teklif_Numarasi = x.OfferNumber,
                        Proje_Adi = x.Projects.ProjectName != null ? x.Projects.ProjectName : "-",
                        Müsteri = x.Customers.CustomerFullName != null ? x.Customers.CustomerFullName : "-",
                        Baslangic_Tarihi = x.DateBegin != null ? x.DateBegin.Value : DateTime.MinValue,
                        Bitis_Tarihi = x.DateEnd != null ? x.DateEnd.Value : DateTime.MinValue,
                        Toplam_Tutarı = x.GenelToplam != null ? x.GenelToplam.Value : 0,
                    }).FirstOrDefault();

                    if (offer != null)
                    {
                        offerList.Add(offer);
                    }
                }

                list = offerList;
                Response.AddHeader("content-disposition", "attachment; filename=FaturasıKesilmisTekliflerListesi.xls");

            }
            //Teklif bazında karlılık raporu
            if (convertReportTypeId == 3)
            {
                var offerList = new List<Offer>();

                offerList = Db.Offer.Where(i => (i.DateBegin >= convertBeginDate && i.DateBegin <= convertEndDate) && i.IsActive == true).ToList();

                if (offerList != null && offerList.Count > 0)
                {
                    var offerListModel = GetReportForDateFiltered(convertBeginDate, convertEndDate, offerList, Convert.ToInt32(reportTypeId));

                    list = offerListModel.Select(x => new
                    {
                        Teklif_Numarasi = x.OfferNumber,
                        Proje_Adi = x.Projects != null ? x.Projects.ProjectName : "-",
                        Müsteri = x.Customers != null ? x.Customers.CustomerFullName : "-",
                        Baslangic_Tarihi = x.DateBegin != null ? x.DateBegin.Value.ToShortDateString() : "-",
                        Bitis_Tarihi = x.DateEnd != null ? x.DateEnd.Value.ToShortDateString() : "-",
                        Urunler_Karlılık_Tutarı = x.AraToplamProfitability != null ? x.AraToplamProfitability.Value.ToString("N2") : "-",
                        IK_Karlılık_Tutarı = x.BrutToplamProfitability != null ? x.BrutToplamProfitability.Value.ToString("N2") : "-",
                        Makine_Karlılık_Tutarı = x.MachineProfitability != null ? x.MachineProfitability.Value.ToString("N2") : "-",
                        Diger_Karlılık_Tutarı = x.OtherProfitability != null ? x.OtherProfitability.Value.ToString("N2") : "-",
                        Toplam_Tutarı = x.GenelToplam != null ? x.GenelToplam.Value.ToString("N2") : "-",
                    }).ToList();

                }

                Response.AddHeader("content-disposition", "attachment; filename=TeklifBazındaKarlılıkRaporu.xls");
            }
            //teklif Durumları Raporu
            if (convertReportTypeId == 4)
            {
                var firstList = Db.Offer.Include(x => x.Projects).Include(x => x.Customers).Include(x => x.OfferStatus).Where(i => i.DateBegin >= convertBeginDate && i.DateBegin <= convertEndDate && i.IsActive == true).Select(x => new
                {
                    x.OfferNumber,
                    x.Projects,
                    x.Customers,
                    x.DateBegin,
                    x.DateEnd,
                    x.OfferStatus,
                    x.GenelToplam,
                });

                list = firstList.ToList().Select(x => new
                {
                    Teklif_Numarasi = x.OfferNumber,
                    Proje_Adi = x.Projects != null ? x.Projects.ProjectName : "-",
                    Müsteri = x.Customers != null ? x.Customers.CustomerFullName : "-",
                    Baslangic_Tarihi = x.DateBegin != null ? x.DateBegin.Value.ToShortDateString() : "-",
                    Bitis_Tarihi = x.DateEnd != null ? x.DateEnd.Value.ToShortDateString() : "-",
                    Surec = x.OfferStatus != null ? x.OfferStatus.StatusName : "-",
                    Toplam_Tutarı = x.GenelToplam != null ? x.GenelToplam.Value.ToString("N2") : "-",
                }).ToList();

                Response.AddHeader("content-disposition", "attachment; filename=TeklifDurumlarıRaporu.xls");

            }

            var gv = new GridView();
            var AllOperatorList = Db.Offer.Where(i => i.StatusLevel == 10 && i.IsActive == true).ToList();


            gv.DataSource = list;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment; filename=SözleşmeListesi.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            return View();
        }

        //Teklif Süreçleri raporları excel dönüşümü
        public ActionResult ExportToExcelForOfferStatus(int statusLevel)
        {
            var firstList = Db.Offer.Include(x => x.OfferStatus).Where(x => x.StatusLevel == statusLevel && x.IsActive == true).Select(x => new
            {
                x.OfferNumber,
                x.Projects,
                x.Customers,
                x.DateBegin,
                x.DateEnd,
                x.OfferStatus,
                x.GenelToplam,
            });

            var list = firstList.ToList().Select(x => new
            {
                Teklif_Numarasi = x.OfferNumber,
                Proje_Adi = x.Projects != null ? x.Projects.ProjectName : "-",
                Müsteri = x.Customers != null ? x.Customers.CustomerFullName : "-",
                Baslangic_Tarihi = x.DateBegin != null ? x.DateBegin.Value.ToShortDateString() : "-",
                Bitis_Tarihi = x.DateEnd != null ? x.DateEnd.Value.ToShortDateString() : "-",
                Surec = x.OfferStatus != null ? x.OfferStatus.StatusName : "-",
                Toplam_Tutarı = x.GenelToplam != null ? x.GenelToplam.Value.ToString("N2") : "-",
            }).ToList();

            if (list.Count() > 0)
            {

                var gv = new GridView();
                var AllOperatorList = Db.Offer.Where(i => i.StatusLevel == 10 && i.IsActive == true).ToList();

                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=TeklifSurecleriListesi.xls");
                Response.ContentType = "application/ms-excel";
                Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();


            }
            return View();
        }

        //Müşteri Bazlı Teklif raporları excel dönüşümü
        public ActionResult ExportToExcelForCustomer(int customerId)
        {
            var firstList = Db.Offer.Include(x => x.OfferStatus).Where(x => x.CustomerID == customerId && x.IsActive == true).Select(x => new
            {
                x.OfferNumber,
                x.Projects,
                x.Customers,
                x.DateBegin,
                x.DateEnd,
                x.OfferStatus,
                x.GenelToplam,
            });

            var list = firstList.ToList().Select(x => new
            {
                Teklif_Numarasi = x.OfferNumber,
                Proje_Adi = x.Projects != null ? x.Projects.ProjectName : "-",
                Müsteri = x.Customers != null ? x.Customers.CustomerFullName : "-",
                Baslangic_Tarihi = x.DateBegin != null ? x.DateBegin.Value.ToShortDateString() : "-",
                Bitis_Tarihi = x.DateEnd != null ? x.DateEnd.Value.ToShortDateString() : "-",
                Surec = x.OfferStatus != null ? x.OfferStatus.StatusName : "-",
                Toplam_Tutarı = x.GenelToplam != null ? x.GenelToplam.Value.ToString("N2") : "-",
            }).ToList();

            if (list.Count() > 0)
            {

                var gv = new GridView();
                var AllOperatorList = Db.Offer.Where(i => i.StatusLevel == 10 && i.IsActive == true).ToList();

                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=MusteriBazlıTekliflerListesi.xls");
                Response.ContentType = "application/ms-excel";
                Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();


            }
            return View();
        }

    }
}
