﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class LogController : BaseController
    {
        // GET: Log
        public ActionResult Index()
        {
            var employees = Db.Employee.Where(i => i.IsActive == true).ToList();

            return View(employees);
        }

        public ActionResult GetLogs(string beginDate, string endDate, int? employeeId)
        {
            var logList = new List<ApplicationLog>();

            if (!string.IsNullOrEmpty(beginDate) && !string.IsNullOrEmpty(endDate))
            {
                var bDate = Convert.ToDateTime(beginDate);
                var eDate = Convert.ToDateTime(endDate);
                eDate = eDate.AddDays(1);

                if (employeeId != null && employeeId != 0)
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordUser == employeeId && i.RecordDate >= bDate && i.RecordDate <= eDate).OrderByDescending(t => t.RecordDate).ToList();
                }
                else
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordDate >= bDate && i.RecordDate <= eDate).OrderByDescending(t => t.RecordDate).ToList();
                }
            }
            else if (!string.IsNullOrEmpty(beginDate))
            {
                var bDate = Convert.ToDateTime(beginDate);
                if (employeeId != null && employeeId != 0)
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordUser == employeeId && i.RecordDate >= bDate).OrderByDescending(t => t.RecordDate).ToList();
                }
                else
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordDate >= bDate).OrderByDescending(t => t.RecordDate).ToList();
                }
            }
            else if (!string.IsNullOrEmpty(endDate))
            {
                var eDate = Convert.ToDateTime(endDate);
                eDate = eDate.AddDays(1);
                if (employeeId != null && employeeId != 0)
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordUser == employeeId && i.RecordDate <= eDate).OrderByDescending(t => t.RecordDate).ToList();
                }
                else
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordDate <= eDate).OrderByDescending(t => t.RecordDate).ToList();
                }
            }
            else
            {
                if (employeeId != null && employeeId != 0)
                {
                    logList = Db.ApplicationLog.Where(i => i.RecordUser == employeeId).OrderByDescending(t => t.RecordDate).ToList();
                }
                else
                {
                    return Json(new { failed = true, message = "Tarih aralığı ya da kullanıcı seçiniz." }, JsonRequestBehavior.AllowGet);
                }
            }                    

            if (logList == null || logList.Count == 0)
            {
                return Json(new { failed = true, message = "Kayıtlı Log bulunamadı." }, JsonRequestBehavior.AllowGet);
            }

            return PartialView("~/Views/Log/_PartialLogList.cshtml", logList);
        }



    }
}