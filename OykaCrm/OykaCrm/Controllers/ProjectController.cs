﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OykaCrm.Controllers
{
    public class ProjectController : BaseController
    {

        public ActionResult Index()
        {
            ProjectControlModel model = new ProjectControlModel();
            model.Projects = Db.Projects.Where(i => i.IsActive == true).OrderByDescending(t => t.UpdateDate).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Proje Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        public ActionResult Create()
        {
            ProjectControlModel model = new ProjectControlModel();
            var getProjectCode = Db.Counter.Where(i => i.ModuleName == "P").FirstOrDefault();
            var tempCounter = getProjectCode.ModuleCounter + 1;
            getProjectCode.ModuleCounter = tempCounter;
            Db.SaveChanges();
            model.ProjectCode = getProjectCode.ModuleName + "-" + getProjectCode.Year.ToString() + "-" + getProjectCode.ModuleCounter;
            return View(model);
        }


        [HttpPost]
        public ActionResult Create(ProjectControlModel model)
        {
            Projects project = new Projects();
            project.IsActive = true;
            project.ProjectArea = model.ProjectArea;
            project.ProjectName = model.ProjectName;
            project.ProjectSubject = model.ProjectSubject;
            project.RecordDate = DateTime.Now;
            project.RecordEmployeeID = OypaHelper.RecordEmployee();
            project.RecordIP = OypaHelper.UserIpAdress();
            project.Code = model.ProjectCode;
            //project.Employee=

            Db.Projects.Add(project);
            Db.SaveChanges();

            return RedirectToAction("Index");
        }


        public ActionResult Detail(int? projectID)
        {
            ProjectControlModel model = new ProjectControlModel();
            model.Offers = Db.Offer.Where(i => i.ProjectID == projectID && i.IsActive == true).ToList();
            model.Opportunities = Db.Opportunity.Where(i => i.ProjectID == projectID && i.IsActive == true).ToList();
            model.Contracts = Db.Offer.Where(i => i.StatusLevel == 10 && i.IsActive == true && i.ProjectID == projectID).ToList();

            if (model.Contracts != null && model.Contracts.Count > 0)
            {
                foreach (var item in model.Contracts)
                {
                    var existTemplate = Db.OfferContractTemplate.Where(i => i.ContractID == item.ID).FirstOrDefault();
                    if (existTemplate != null)
                    {
                        model.ContractTemplateList.Add(existTemplate);
                    }
                }
            }

            var ContractIDList = Db.OfferContractTemplate.Where(i => i.IsBilled == true).Select(i => i.ContractID).Distinct().ToList();
            List<Offer> list = new List<Offer>();
            foreach (var item in ContractIDList)
            {
                var offer = Db.Offer.Find(item);
                if (offer != null && offer.IsActive == true && offer.ProjectID == projectID)
                {
                    list.Add(offer);
                }
            }

            model.Invoices = list;
            model.Documents = Db.Documents.Where(i => i.ProjectID == projectID && i.IsActive == true).ToList();

            var selectedProject = Db.Projects.FirstOrDefault(i => i.ID == projectID);

            if (selectedProject != null)
            {
                model.SingleProject = selectedProject;
            }

            OypaHelper.AddApplicationLog("Management", "Detail", "Project", projectID != null ? projectID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {projectID} Id'li proje detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        [HttpPost]
        public ActionResult Update(ProjectControlModel model)
        {
            var updatedProject = Db.Projects.FirstOrDefault(i => i.ID == model.ProjectID);
            if (updatedProject != null)
            {
                updatedProject.ProjectArea = model.ProjectArea;
                updatedProject.ProjectName = model.ProjectName;
                updatedProject.ProjectSubject = model.ProjectSubject;
                var isSaved = Db.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        public ActionResult Delete(int projectID)
        {
            var deletedProject = Db.Projects.FirstOrDefault(i => i.ID == projectID);
            if (deletedProject != null)
            {
                deletedProject.IsActive = false;
                Db.SaveChanges();
            }

            return RedirectToAction("Index");
        }



        public ActionResult Archive()//yil bazinda projeler, tarih filtrelemesi yapilabilecek
        {
            return View();
        }


        public PartialViewResult RecordFile(int projectId)
        {
            var doc = Db.Documents.OrderByDescending(c => c.ID).FirstOrDefault();
            doc.CategoryID = 5;
            doc.ProjectID = projectId;
            Db.SaveChanges();
            ProjectControlModel model = new ProjectControlModel();
            model.Documents = Db.Documents.Where(i => i.ProjectID == projectId && i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "RecordFile", "Project", projectId, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {projectId} Id'li projeye {doc.FileName} adlı dosyayı ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Project/_ProjectDocumentsPartial.cshtml", model.Documents);
        }


        public ActionResult ExportToExcelProjects()
        {
            var gv = new GridView();
            var AllOperatorList = Db.Projects.Where(c => c.IsActive == true).ToList();

            var list = (from e in Db.Projects
                        where e.IsActive == true
                        select new
                        {
                            Proje_Kodu = e.Code,
                            Proje_Adi = e.ProjectName,
                            Olusturan = e.Employee.FullName,
                        }).ToList();

            gv.DataSource = list;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ProjeListesi.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            OypaHelper.AddApplicationLog("Management", "ExportToExcelProjects", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini excel olrak indirdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }

        public ActionResult GetProjectFromLogo()
        {
            try
            {
               
                OypaService.Authentication auth = new OypaService.Authentication();
                auth.Username = "testUser";
                auth.Password = "testKullanici";
                auth.FirmNR = "124";
                auth.PeriodNR = "1";

                OypaService.LOGOTIGERSoapClient client = new OypaService.LOGOTIGERSoapClient();
                client.Open();
                var projectList = client.GetProjectCardList(auth).ToList();
          
                var getOurProjectList = Db.Projects.Where(t => t.IsActive == true).ToList();
                List<Projects> newProjectList = new List<Projects>();
                if (projectList.Count() > 0 && projectList != null)
                {
                    if (projectList.Count() != getOurProjectList.Count())
                    {
                        // projectList.RemoveRange(0, getOurProjectList.Count());
                        foreach (var item in projectList)
                        {
                            if (getOurProjectList.Any(t => t.Code == item.CODE) == false) //Logodan gelen proje bizde kayıtlı değilse
                            {
                                Projects p = new Projects()
                                {
                                    Code = item.CODE,
                                    ProjectName = item.NAME,
                                    RecordDate = DateTime.Now,
                                    RecordIP = OypaHelper.UserIpAdress(),
                                    RecordEmployeeID = OypaHelper.RecordEmployee(),
                                    IsActive = true,
                                    UpdateDate = DateTime.Now,
                                    UpdateUserId = OypaHelper.RecordEmployee()

                                };
                                newProjectList.Add(p);

                            }
                            //else
                            //{
                            //    var find = getOurProjectList.FirstOrDefault(t => t.Code == item.CODE && t.IsActive == true);
                            //    find.Code = item.CODE;
                            //    find.ProjectName = item.NAME;
                            //    find.UpdateDate = DateTime.Now;
                            //    find.RecordIP = OypaHelper.UserIpAdress();
                            //    find.UpdateUserId = OypaHelper.RecordEmployee();
                            //    Db.SaveChanges();
                            //}
                        }
                        Db.Projects.AddRange(newProjectList);
                        int save = Db.SaveChanges();
                        if (save > 0)
                        {
                            OypaHelper.AddApplicationLog("Management", "GetProjectFromLogo", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini güncellemek için logoya istekte bulundu ve {newProjectList.Count} adet proje uygulamaya dahil edilidi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Success", message = $"Logo üzerinden yeni açılmış {newProjectList.Count} adet proje uygulamaya dahil edilidi." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "GetProjectFromLogo", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini güncellemek için logoya istekte bulundu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Success", message = $"Proje bilgileri güncel durumdadır." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "GetProjectFromLogo", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini güncellemek için logoya istekte bulundu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return Json(new { result = "Success", message = $"Proje bilgileri güncel durumdadır." }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "GetProjectFromLogo", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini güncellemek için logoya istekte bulundu fakat verilerin çekilmesi sırasında hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return Json(new { result = "NoDifference", message = $"Logodan güncel proje bilgileri çekilemedi, lütfen tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex )
            {
                OypaHelper.AddApplicationLog("Management", "GetProjectFromLogo", "Project", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini güncellemek için logoya istekte bulundu fakat verilerin çekilmesi sırasında hata ile karşılaşıldı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);


                OypaHelper.SendErrorMail("TYHErrorUpdateProductFromLogo", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, proje listesini güncellemek için logoya istekte bulundu fakat verilerin çekilmesi sırasında hata ile karşılaşıldı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");

                return Json(new { result = "NoDifference", message = $"Logodan güncel proje bilgileri getirilirken bir hata oluştu, lütfen tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
            }
           
        }


    }
}