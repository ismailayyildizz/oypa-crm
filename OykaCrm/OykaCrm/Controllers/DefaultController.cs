﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class DefaultController : BaseController
    {
        // GET: Default -Güncel Durum sekmesi
        [UserAuth]
        public ActionResult Index()
        {
            //EmployeeRoleGroup() == 1 -> RolGroupID=1(RoleGroup Tablosu-Bölge direktörü grubu)
            if (OypaHelper.EmployeeRoleGroup() == 1)
            {               
                var currentYear = DateTime.Now.Year;
                DefaultModel model = new DefaultModel();

                model.ProjectCount = Db.Projects.Where(t => t.IsActive == true).Count();
                model.OpportunityCount = Db.Opportunity.Where(t => t.IsActive == true).Count();
                model.OfferCount = Db.Offer.Where(t => t.IsActive == true).Count();
                model.CustomerCount = Db.Customers.Where(t => t.IsActive == true).Count();
                model.OppList = Db.Opportunity.Where(t => t.IsActive == true && t.DateBegin.Value.Year==currentYear).OrderByDescending(t => t.ID).Take(20).ToList();
                model.OfferList = Db.Offer.Where(t => t.IsActive == true && t.DateBegin.Value.Year == currentYear).OrderByDescending(t => t.ID).Take(20).ToList();
                model.NormalOffer = Db.Offer.Where(t => t.IsActive == true && t.IsOneTime == true).Count();
                model.IsOneTimeOffer = Db.Offer.Where(t => t.IsActive == true && t.IsOneTime == false).Count();
                #region Populer Firmalar
                var res = Db.Offer.Where(t => t.IsActive == true && t.DateBegin.Value.Year == currentYear).GroupBy(t => t.Customers).Select(t => new { Customer = (string)t.Key.CustomerFullName, Count = (int)t.Count() }).OrderByDescending(t => t.Count).Take(20).ToList();
                List<PopulerCustomer> list = new List<PopulerCustomer>();
                foreach (var item in res)
                {
                    PopulerCustomer m = new PopulerCustomer();
                    m.Count = item.Count;
                    m.Customer = item.Customer;
                    list.Add(m);
                }
                ViewBag.PopulerCustomer = list;
                #endregion

                #region Aylık Oluşturulan Fırsatlar

              
                var opportunity = Db.Opportunity.Where(t => t.DateBegin.Value.Year == currentYear).ToList();
                MonthlyOpportunityCount countModel = new MonthlyOpportunityCount();

                countModel.ConfirmJan = opportunity.Where(t => t.DateBegin.Value.Month == 1 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingJan = opportunity.Where(t => t.DateBegin.Value.Month == 1 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseJan = opportunity.Where(t => t.DateBegin.Value.Month == 1 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmFeb = opportunity.Where(t => t.DateBegin.Value.Month == 2 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingFeb = opportunity.Where(t => t.DateBegin.Value.Month == 2 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseFeb = opportunity.Where(t => t.DateBegin.Value.Month == 2 && t.StatusLevel == 4).Count(); //DirectorRed


                countModel.ConfirmMar = opportunity.Where(t => t.DateBegin.Value.Month == 3 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingMar = opportunity.Where(t => t.DateBegin.Value.Month == 3 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseMar = opportunity.Where(t => t.DateBegin.Value.Month == 3 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmApr = opportunity.Where(t => t.DateBegin.Value.Month == 4 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingApr = opportunity.Where(t => t.DateBegin.Value.Month == 4 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseApr = opportunity.Where(t => t.DateBegin.Value.Month == 4 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmMay = opportunity.Where(t => t.DateBegin.Value.Month == 5 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingMay = opportunity.Where(t => t.DateBegin.Value.Month == 5 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseMay = opportunity.Where(t => t.DateBegin.Value.Month == 5 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmJune = opportunity.Where(t => t.DateBegin.Value.Month == 6 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingJune = opportunity.Where(t => t.DateBegin.Value.Month == 6 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseJune = opportunity.Where(t => t.DateBegin.Value.Month == 6 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmJuly = opportunity.Where(t => t.DateBegin.Value.Month == 7 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingJuly = opportunity.Where(t => t.DateBegin.Value.Month == 7 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseJuly = opportunity.Where(t => t.DateBegin.Value.Month == 7 && t.StatusLevel == 4).Count(); //DirectorRed


                countModel.ConfirmAug = opportunity.Where(t => t.DateBegin.Value.Month == 8 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingAug = opportunity.Where(t => t.DateBegin.Value.Month == 8 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseAug = opportunity.Where(t => t.DateBegin.Value.Month == 8 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmSep = opportunity.Where(t => t.DateBegin.Value.Month == 9 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingSep = opportunity.Where(t => t.DateBegin.Value.Month == 9 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseSep = opportunity.Where(t => t.DateBegin.Value.Month == 9 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmOct = opportunity.Where(t => t.DateBegin.Value.Month == 10 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingOct = opportunity.Where(t => t.DateBegin.Value.Month == 10 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseOct = opportunity.Where(t => t.DateBegin.Value.Month == 10 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmNov = opportunity.Where(t => t.DateBegin.Value.Month == 11 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingNov = opportunity.Where(t => t.DateBegin.Value.Month == 11 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseNov = opportunity.Where(t => t.DateBegin.Value.Month == 11 && t.StatusLevel == 4).Count(); //DirectorRed

                countModel.ConfirmDec = opportunity.Where(t => t.DateBegin.Value.Month == 12 && t.StatusLevel == 3).Count(); //DirectorOnay
                countModel.WaitingDec = opportunity.Where(t => t.DateBegin.Value.Month == 12 && t.StatusLevel == 1).Count(); //DirectorBekleyen
                countModel.RefuseDec = opportunity.Where(t => t.DateBegin.Value.Month == 12 && t.StatusLevel == 4).Count(); //DirectorRed
                model.SingleMonthlyOpportunityCount = countModel;

                #endregion

                #region OfferStatus
                var offerList = Db.Offer.Where(t => t.DateBegin.Value.Year == currentYear && t.IsActive==true).ToList();
                OfferStatusCount statusCount = new OfferStatusCount();
                statusCount.IlkKayit = offerList.Where(t => t.StatusLevel == 1).Count();
                statusCount.TeklifHazirlik = offerList.Where(t => t.StatusLevel == 2).Count();
                statusCount.SatinAlmaUzmOnay = offerList.Where(t => t.StatusLevel == 3).Count();
                statusCount.SatinAlmaUzmRed = offerList.Where(t => t.StatusLevel == 4).Count();
                statusCount.IKOnay = offerList.Where(t => t.StatusLevel == 5).Count();
                statusCount.IKRed = offerList.Where(t => t.StatusLevel == 6).Count();
                statusCount.FiyatRevizyon = offerList.Where(t => t.StatusLevel == 7).Count();
                statusCount.Iptal = offerList.Where(t => t.StatusLevel == 8).Count();
                statusCount.MusteriOnayBekliyor = offerList.Where(t => t.StatusLevel == 9).Count();
                statusCount.SozlesmeHazirlik = offerList.Where(t => t.StatusLevel == 10).Count();
                statusCount.SatinAlmaMudOnay = offerList.Where(t => t.StatusLevel == 11).Count();
                statusCount.SatinAlmaMudRed = offerList.Where(t => t.StatusLevel == 12).Count();
                model.SingleOfferStatusCount = statusCount;
                #endregion
                ViewBag.IsDirector = true;

                //var res3= OypaHelper.FillinEmailContentWithMailGroup("message1", "message2", "test", "tahsin.tiryaki@sibertechno.com", "test", "test", null, "CustomerMail.html", "tahsin14tiryaki@gmail.com", null);
                //if (res3)
                //{
                //    OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", 0, "Offer", true, $" res testmail nolu teklif için müşteri için tanımlanan tahsin.tiryaki@sibertechno.com mail adresine teklif üst yazısı başarılı", 0, "000", null);
                //}
                //else
                //{
                //    OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", 0, "Offer", true, $" res testmailnolu teklif için müşteri için tanımlanan tahsin.tiryaki@sibertechno.com mail adresine teklif üst yazısı başarısız", 0, "000", null);
                //}


                //var res2 = OypaHelper.SendMailWithMailGroup("test", "test", "tahsin.tiryaki@sibertechno.com", null, "tahsin14tiryaki@gmail.com", null);
                //if (res2)
                //{
                //    OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", 0, "Offer", true, $" res2 testmail nolu teklif için müşteri için tanımlanan tahsin.tiryaki@sibertechno.com mail adresine teklif üst yazısı başarılı", 0, "000", null);
                //}
                //else
                //{
                //    OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", 0, "Offer", true, $" res2 testmail nolu teklif için müşteri için tanımlanan tahsin.tiryaki@sibertechno.com mail adresine teklif üst yazısı başarısız", 0, "000", null);
                //}

                return View(model);
            }
            else
            {
                return View();
            }
        }
        
        public ActionResult IndexOperator()
        {
            
     
            return View();
        }
    }
}