﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class LoginController : Controller
    {
        private OykaCrm.Models.OypaDbEntities db = new Models.OypaDbEntities();
        // GET: Login
        public ActionResult Index(string Hata)
        {
            ViewBag.ErrorMessage = Hata;
            return View();
        }


        #region Index
        //AD sorgulamasına local'den eriişm olmadığı için uygulama local'de çalıştırılmak istenildiğinde AD' kontrolü yoruma alınmalıdır.
        //Canlıya çıkarker AD kontrolü aktif edilmelidir.
        //Not: Localde çalışırken mailleri bilgi@farmkaod.com adresinden gönderilmesi gerekmektedir. canlıda ise tyhbilgi@oypa.com.tr.
        //Bu işlem model klasörü içerisinde yer alan oypaHelper classı içerisiden düzenlenmektedir. 
        #endregion

        [HttpPost]
        public ActionResult Index(string txtusername, string txtpass)
        {
            bool ADSuccessLogin = true;

            //Active Directory'den kullanıcı bilgileri doğrulanır.
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "Srvdc1.oypa.com.tr"))
            {
                //Kullanıcı ve Şifre bilgilerini doğrulamasını yapar. Doğruysa login olunmuştur.
                ADSuccessLogin = pc.ValidateCredentials(txtusername.Trim(), txtpass.Trim());
            }

            if (ADSuccessLogin)
            {

                Employee checkuser = db.Employee.Where(i => i.EMail == txtusername && i.IsActive == true).FirstOrDefault();
                if (checkuser != null)
                {
                    Session.Add("username", checkuser.FullName);
                    Session.Add("LevelID", checkuser.RolGroupID);
                    Session.Add("profile", checkuser);

                    OypaHelper.AddApplicationLog("Management", "Index", "Login", checkuser.ID, null, true, $"{checkuser.EMail} adresine sahip kulanıcı uygulamaya giriş yaptı", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    return RedirectToAction("Index", "Default");
                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "Index", "Login", 0, null, true, $"{txtusername} adresine sahip kulanıcı uygulamaya giriş yapmaya çalıştı.Uygulamada Kayıtlı olmayan E-Posta!", 99, OypaHelper.UserIpAdress(), null);
                    return RedirectToAction("Index", "Login", new { Hata = "Uygulamada Kayıtlı olmayan E-Posta!" });
                }
        }
            else
            {
                return RedirectToAction("Index", "Login", new { Hata = "Active Directory E-Posta veya şifre yanlış!" });
            }


        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Default");
        }
    }
}