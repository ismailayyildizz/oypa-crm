﻿using iTextSharp.text.pdf.qrcode;
using Newtonsoft.Json;
using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;

namespace OykaCrm.Controllers
{
    public class InvoiceController : BaseController
    {
        // GET: Invoice
        public ActionResult Index() //proje fırsat teklif müşteri bazlı faturaların listesi 
        {
            var ContractIDList = Db.OfferContractTemplate.Where(i => i.IsBilled == true).Select(i => i.ContractID).Distinct().ToList();
            var IsOneTimeOffer = Db.Offer.Where(t => t.IsActive == true && t.IsOneTime == false && t.StatusLevel == 10).ToList();
            List<Offer> list = new List<Offer>();

            foreach (var item in ContractIDList)
            {
                var offer = Db.Offer.Find(item);
                if (offer != null && offer.IsActive == true)
                {
                    list.Add(offer);
                }
            }

            if (IsOneTimeOffer != null && IsOneTimeOffer.Count() > 0)
            {
                list.AddRange(IsOneTimeOffer); // Tek seferlik teklifin sözleşmesi olmadığı için tek seferlik teklifler offer tablosundan çekildi.
            }

            list = list.OrderByDescending(t => t.ID).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Invoice", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tüm faturaları listeledi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(list);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Detail(int? ContractID, string cdata)
        {
            InvoiceControlModel model = new InvoiceControlModel();
            List<InvoiceRow> invoiceRows = new List<InvoiceRow>();
            List<InvoiceRowHistory> invoiceRowHistories = new List<InvoiceRowHistory>();

            var offer = Db.Offer.Find(ContractID);
            if (offer != null)
            {
                var checkOfferContract = Db.OfferContractTemplate.Where(t => t.ContractID == ContractID && t.IsActive == true).FirstOrDefault();
                model.ExpenseCenterList = Db.ExpenseCenter.Where(t => t.IsActive == true).ToList();
                model.OfferPositons = Db.OfferPositions.Where(t => t.OfferID == offer.ID).ToList();
                model.OfferPositonsID = model.OfferPositons.Select(t => t.PositionID).Distinct().ToList();
                model.OfferPositionList = Db.OfferPositions.Where(t => t.OfferID == offer.ID).ToList();
                model.OfferID = offer.ID;
                model.SingleOffer = offer;

                if (checkOfferContract != null)
                {
                    checkOfferContract.ContractContent = cdata;
                    checkOfferContract.UpdateDate = DateTime.Now;
                    checkOfferContract.UpdateUserID = OypaHelper.RecordEmployee();
                    checkOfferContract.IsBilled = true;
                }
                else
                {
                    OfferContractTemplate oct = new OfferContractTemplate()
                    {
                        ContractID = ContractID,
                        ContractContent = cdata,
                        IsBilled = true,
                        IsActive = true,
                        RecordDate = DateTime.Now,
                        RecordUserID = OypaHelper.RecordEmployee()
                    };
                    Db.OfferContractTemplate.Add(oct);
                }

                Db.SaveChanges();

                var rows = Db.InvoiceRow.Where(i => i.OfferID == ContractID).ToList();

                model.ProjectName = offer.Projects == null ? "-" : offer.Projects.ProjectName;
                model.CustomerName = offer.Opportunity?.Customers?.CustomerFullName;
                model.OfferNumber = offer.OfferNumber == null ? "-" : offer.OfferNumber;
                model.KDV = offer.KdvToplam.Value;
                model.Positons = Db.Positions.Where(t => t.IsActive == true).ToList();
                model.SingleOffer = offer;

                if (offer.DateBegin != null && offer.DateEnd != null)
                {
                    //var diffCount = (offer.DateEnd.Value.Month - offer.DateBegin.Value.Month) + 12 * (offer.DateEnd.Value.Year - offer.DateBegin.Value.Year);
                    TimeSpan res = offer.DateEnd.Value - offer.DateBegin.Value;
                    var totalDay = res.TotalDays;
                    var diffCount = Convert.ToInt32((totalDay / 30));
                    if (diffCount == 0)
                    {
                        model.MonthCount = 1;
                    }
                    else
                    {
                        model.MonthCount = diffCount;
                    }
                    model.DateBegin = offer.DateBegin;
                    model.DateEnd = offer.DateEnd;

                }

                if (offer.GenelToplam != 0 && model.MonthCount != 0)
                {
                    model.Total = offer.GenelToplam.Value - model.KDV;
                    var monthlyInvoice = model.Total / model.MonthCount;
                    model.MonthlyInvoice = monthlyInvoice;
                }

                var paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == true).Sum(t => t.PaidedPrice);

                //invoiceList yoksa olustur
                if (rows.Count == 0)
                {

                    for (int i = 0; i < model.MonthCount; i++)
                    {
                        InvoiceRow invoiceRow = new InvoiceRow()
                        {
                            OfferID = ContractID,
                            MonthlyInvoice = model.MonthlyInvoice,
                            IsPaid = false,
                            PaymentDate = offer.DateBegin.Value.AddMonths(i),
                            RecordDate = DateTime.Now,
                            RecordUserID = OypaHelper.RecordEmployee(),
                            PaidedPrice = 0,//ödenene tutar yok ise 0 yazılır.
                            TotalPrice = model.Total
                        };
                        //invoiceRows.Add(invoiceRow);
                        Db.InvoiceRow.Add(invoiceRow);
                        Db.SaveChanges();

                        InvoiceRowHistory history = new InvoiceRowHistory()
                        {
                            ContractID = ContractID,
                            MonthlyInvoice = model.MonthlyInvoice,
                            IsPaid = false,
                            PaymentDate = offer.DateBegin.Value.AddMonths(i),
                            RecordDate = DateTime.Now,
                            RecordUserID = OypaHelper.RecordEmployee(),
                            InvoiceRowID = invoiceRow.ID,
                            TotalPrice = invoiceRow.TotalPrice

                        };
                        invoiceRowHistories.Add(history);

                    }

                    Db.InvoiceRowHistory.AddRange(invoiceRowHistories);
                    if (Db.SaveChanges() > 0)
                    {
                        model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == ContractID).ToList();
                    }
                }
                else
                {
                    var exTotal = Db.InvoiceRow.Where(i => i.OfferID == ContractID).FirstOrDefault().TotalPrice;
                    // eger ucret degisti ise odenmemis aylarin fatura degeri artar ya da azalır
                    var genelToplam = offer.GenelToplam - offer.KdvToplam;

                    if (genelToplam != exTotal && (paidedInvoicesPrice != null || paidedInvoicesPrice != 0))
                    {

                        var invoiceList = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == false).ToList();

                        var diff = genelToplam - paidedInvoicesPrice;
                        foreach (var item in invoiceList)
                        {
                            item.MonthlyInvoice = diff / invoiceList.Count;
                            model.MonthlyInvoice = item.MonthlyInvoice.Value;
                            item.TotalPrice = genelToplam;
                        }

                        foreach (var item in rows)
                        {
                            item.TotalPrice = genelToplam;
                        }

                        Db.SaveChanges();
                    }

                    model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == ContractID).ToList();
                }

                model.i = 0;
                int invoiceCount = model.InvoiceRows.Count;

                var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == true).Count();
                model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;

                if (paidedInvoicesPrice != null)
                {
                    model.Remaining = (model.Total) - paidedInvoicesPrice; //kalan odenek
                }

                model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == true).Sum(i => i.PaidedPrice);

                OypaHelper.AddApplicationLog("Management", "Detail", "Invoice", ContractID != null ? ContractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {ContractID} Id'li fatura detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }
            else
            {
                ViewBag.Error = "Fatura Kaydı Bulunamadı";

                OypaHelper.AddApplicationLog("Management", "Detail", "Invoice", ContractID != null ? ContractID.Value : 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {ContractID} Id'li fatura detayını görüntülemek isterken fatura bulunamadı hatası ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }
        }

        public ActionResult Detail(int? ContractID)
        {
            InvoiceControlModel model = new InvoiceControlModel();
            List<InvoiceRow> invoiceRows = new List<InvoiceRow>();
            List<InvoiceRowHistory> invoiceRowHistories = new List<InvoiceRowHistory>();
            var offer = Db.Offer.Find(ContractID);
            if (offer != null)
            {
                model.SingleOffer = offer;
                model.OfferID = offer.ID;
                model.ExpenseCenterList = Db.ExpenseCenter.Where(t => t.IsActive == true).ToList();

                if (offer.IsOneTime.Value == true) //Süreli teklif
                {
                    var checkOfferContract = Db.OfferContractTemplate.Where(t => t.ContractID == ContractID && t.IsActive == true).FirstOrDefault();

                    model.OfferPositons = Db.OfferPositions.Where(t => t.OfferID == checkOfferContract.Offer.ID).ToList();
                    model.OfferPositonsID = model.OfferPositons.Select(t => t.PositionID).Distinct().ToList();
                    model.Positons = Db.Positions.ToList();
                    model.OfferPositionList = Db.OfferPositions.Where(x => x.OfferID == checkOfferContract.Offer.ID).ToList();
                    //model.OfferID = checkOfferContract.Offer.ID;
                }

                var rows = Db.InvoiceRow.Where(i => i.OfferID == ContractID).ToList();

                model.ProjectName = offer.Projects == null ? "-" : offer.Projects.ProjectName;
                model.CustomerName = offer.Opportunity?.Customers?.CustomerFullName;
                model.OfferNumber = offer.OfferNumber == null ? "-" : offer.OfferNumber;
                model.KDV = offer.KdvToplam.Value;

                if (offer.DateBegin != null && offer.DateEnd != null)
                {
                    int diffCount = 1;
                    if (offer.DateBegin != offer.DateEnd)
                    {
                        //diffCount = (offer.DateEnd.Value.Month - offer.DateBegin.Value.Month) + 12 * (offer.DateEnd.Value.Year - offer.DateBegin.Value.Year);
                        TimeSpan res = offer.DateEnd.Value - offer.DateBegin.Value;
                        var totalDay = res.TotalDays;
                        diffCount = Convert.ToInt32((totalDay / 30));
                    }

                    model.MonthCount = diffCount;
                    model.DateBegin = offer.DateBegin;
                    model.DateEnd = offer.DateEnd;
                }

                if (offer.GenelToplam != 0 && model.MonthCount != 0)
                {
                    var genelToplam = offer.GenelToplam - offer.KdvToplam;
                    model.Total = genelToplam.Value;
                    var monthlyInvoice = genelToplam / model.MonthCount;
                    model.MonthlyInvoice = monthlyInvoice.Value;
                }

                var paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == true).ToList().Sum(t => t.PaidedPrice);

                //invoiceList yoksa olustur
                if (rows.Count == 0)
                {
                    for (int i = 0; i < model.MonthCount; i++)
                    {
                        InvoiceRow invoiceRow = new InvoiceRow()
                        {
                            OfferID = ContractID,
                            MonthlyInvoice = model.MonthlyInvoice,
                            IsPaid = false,
                            PaymentDate = offer.DateBegin.Value.AddMonths(i),
                            RecordDate = DateTime.Now,
                            RecordUserID = OypaHelper.RecordEmployee(),
                            PaidedPrice = 0,//ödenene tutar yok ise 0 yazılır.
                            TotalPrice = model.Total
                        };

                        Db.InvoiceRow.Add(invoiceRow);
                        Db.SaveChanges();

                        InvoiceRowHistory history = new InvoiceRowHistory()
                        {
                            ContractID = ContractID,
                            MonthlyInvoice = model.MonthlyInvoice,
                            IsPaid = false,
                            PaymentDate = offer.DateBegin.Value.AddMonths(i),
                            RecordDate = DateTime.Now,
                            RecordUserID = OypaHelper.RecordEmployee(),
                            InvoiceRowID = invoiceRow.ID,
                            TotalPrice = invoiceRow.TotalPrice

                        };
                        invoiceRowHistories.Add(history);

                    }

                    Db.InvoiceRowHistory.AddRange(invoiceRowHistories);
                    if (Db.SaveChanges() > 0)
                    {
                        model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == ContractID).ToList();
                    }
                }
                else
                {
                    var exTotal = Db.InvoiceRow.Where(i => i.OfferID == ContractID).FirstOrDefault().TotalPrice;
                    // eger ucret degisti ise odenmemis aylarin fatura degeri artar ya da azalır
                    var genelToplam = offer.GenelToplam - offer.KdvToplam;
                    if (genelToplam != exTotal /*&& (/*paidedInvoicesPrice != null || paidedInvoicesPrice != 0)*/)
                    {

                        if (paidedInvoicesPrice != null)
                        {
                            var invoiceList = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == false).ToList();
                            var diff = genelToplam - paidedInvoicesPrice;
                            foreach (var item in invoiceList)
                            {
                                item.MonthlyInvoice = diff / invoiceList.Count;
                                model.MonthlyInvoice = item.MonthlyInvoice.Value;
                                item.TotalPrice = genelToplam;
                            }
                        }
                        foreach (var item in rows)
                        {
                            item.TotalPrice = genelToplam;
                        }

                        Db.SaveChanges();
                    }

                    model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == ContractID).ToList();
                }

                model.i = 0;
                int invoiceCount = model.InvoiceRows.Count;

                var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == true).Count();
                model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;

                if (paidedInvoicesPrice != null)
                {
                    model.Remaining = (model.Total) - paidedInvoicesPrice;
                }

                model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == ContractID && i.IsPaid == true).Sum(i => i.PaidedPrice);

                OypaHelper.AddApplicationLog("Management", "Detail", "Invoice", ContractID != null ? ContractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {offer.OfferNumber} nolu teklifin faturalarını görüntüledi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }
            else
            {
                ViewBag.Error = "Fatura Kaydı Bulunamadı";
                OypaHelper.AddApplicationLog("Management", "Detail", "Invoice", ContractID != null ? ContractID.Value : 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı Sistemde var olmayan {ContractID } İd'li teklifin faturalarını görüntülemeye çalıştı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return View(model);
            }

            return View(model);
        } //Şablon var olan ya da oluşturulması gerekmeyen(tek seferlik teklif)

        public PartialViewResult DetailModalReadOnly(int invoiceRowID)
        {
            InvoiceControlModel m = new InvoiceControlModel();
            var selectedInvoice = Db.InvoiceRow.FirstOrDefault(t => t.ID == invoiceRowID);
            if (selectedInvoice != null)
            {
                m.SingleInvoice = selectedInvoice;
                m.SingleOffer = Db.Offer.FirstOrDefault(t => t.ID == selectedInvoice.OfferID);
            }

            return PartialView("~/Views/Invoice/_PartialDetailModalReadOnly.cshtml", m);
        }

        //Fazla Mesai kaydetme
        [HttpPost]
        public PartialViewResult SaveOvertime(IEnumerable<OvertimeModel> OvertimeModel, OvertimeGeneralTotalModel GeneralModel, string Description)
        {
            //General model'de invoiceRowId var,detail sayfasında veriyi al, ajax'ta GeneralModel invoiceRowId'ye  gönder

            List<Overtime> OvertTimeList = new List<Overtime>();

            var findInvoice = Db.InvoiceRow.Find(GeneralModel.InvoiceRowId);
            findInvoice.OvertimeDescription = Description;
            double? paidedInvoicesPrice = 0;

            if (OvertimeModel != null && GeneralModel != null)
            {

                //Fazla mesaide daha önceden o ayki kayıt var mı
                var isExits = Db.Overtime.Any(x => x.InvoiceRowId == GeneralModel.InvoiceRowId && x.IsActive == true);
                //Kayıtlar varsa; sil yeniden kaydet = güncelleme işlemi

                if (isExits)
                {
                    var overtTimeList = Db.Overtime.Where(x => x.InvoiceRowId == GeneralModel.InvoiceRowId && x.IsActive == true).ToList();

                    if (overtTimeList != null)
                    {
                        Db.Overtime.RemoveRange(overtTimeList);
                        Db.SaveChanges();
                    }
                }

                foreach (var item in OvertimeModel)
                {
                    var newOvertime = new Overtime();
                    newOvertime.PositionId = item.PositionId;

                    if (item.Status.Contains("Emekli"))
                    {
                        newOvertime.StatusId = 2;
                    }
                    if (item.Status.Contains("Çalışan"))
                    {
                        newOvertime.StatusId = 1;
                    }

                    newOvertime.IsActive = true;
                    newOvertime.RecordDate = DateTime.Now;

                    newOvertime.GrossPrice = !string.IsNullOrEmpty(item.GrossPrice) ? Convert.ToDouble(item.GrossPrice.Replace(".", ""), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.HI_Rate = !string.IsNullOrEmpty(item.HIRate) ? Convert.ToDouble(item.HIRate.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.HI_Hour = !string.IsNullOrEmpty(item.HIHour) ? Convert.ToDouble(item.HIHour.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.HS_Rate = !string.IsNullOrEmpty(item.HSRATE) ? Convert.ToDouble(item.HSRATE.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.HS_Hour = !string.IsNullOrEmpty(item.HSHour) ? Convert.ToDouble(item.HSHour.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.BT_Rate = !string.IsNullOrEmpty(item.BTRate) ? Convert.ToDouble(item.BTRate.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.BT_Hour = !string.IsNullOrEmpty(item.BTHour) ? Convert.ToDouble(item.BTHour.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.TotalOvertimeGrossPrice = !string.IsNullOrEmpty(item.TotalOvertimeGrossPrice) ? Convert.ToDouble(item.TotalOvertimeGrossPrice.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.EmployerShare = !string.IsNullOrEmpty(item.EmployerShare) ? Convert.ToDouble(item.EmployerShare.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.EmployerUnemployment = !string.IsNullOrEmpty(item.EmployerUnemployement) ? Convert.ToDouble(item.EmployerUnemployement.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.TotalOvertimeCost = !string.IsNullOrEmpty(item.TotalOvertimeCost) ? Convert.ToDouble(item.TotalOvertimeCost.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.Spread = !string.IsNullOrEmpty(item.Spread) ? Convert.ToDouble(item.Spread.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.InvoiceAmountForOvertime = !string.IsNullOrEmpty(item.InvoiceAmountForOvertime) ? Convert.ToDouble(item.InvoiceAmountForOvertime.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    newOvertime.InvoiceRowId = GeneralModel.InvoiceRowId;

                    OvertTimeList.Add(newOvertime);
                }

                if (GeneralModel.InvoiceRowId != 0)
                {
                    findInvoice.OvertTimeGeneralTotal = !string.IsNullOrEmpty(GeneralModel.OvertimeGeneralTotal) ? Convert.ToDouble(GeneralModel.OvertimeGeneralTotal.Replace(".", ",").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                    findInvoice.IsOvertime = true;
                }

                Db.Overtime.AddRange(OvertTimeList);
                var isSave = Db.SaveChanges();

            }

            InvoiceControlModel model = new InvoiceControlModel();

            model.OfferID = findInvoice.OfferID.Value;
            model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID).ToList();
            model.MonthlyInvoice = findInvoice.MonthlyInvoice.Value;
            int invoiceCount = model.InvoiceRows.Count;
            paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
            var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Count();
            model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;

            if (paidedInvoicesPrice != null)
            {
                model.Remaining = (findInvoice.TotalPrice) - paidedInvoicesPrice; //kalan odenek
                                                                                  //if (Math.Floor(model.Remaining.Value) == 0)
                                                                                  //{
                                                                                  //    ViewBag.RemainZero = "Toplam Fatura Tutarına ulaşılmıştır..!";
                                                                                  //    model.Remaining = Math.Floor(model.Remaining.Value);
                                                                                  //}
            }


            model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(i => i.PaidedPrice);
            model.OfferID = findInvoice.OfferID.Value;


            OypaHelper.AddApplicationLog("Management", "SaveOvertime", "Invoice", findInvoice.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {findInvoice.Offer.OfferNumber} nolu teklifin {findInvoice.PaymentDate.Value.ToShortDateString()} tarihli faturasına fazla mesai hesaplama işlemi gerçekleştirdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
        }

        //Fazla mesai Detayları
        public PartialViewResult OvertimeModalDetail(int invoiceRowID)
        {
            var model = new OvertimeDetailModel();
            model.ListOvertime = Db.Overtime.Where(x => x.InvoiceRowId == invoiceRowID && x.IsActive == true).ToList();
            model.Positons = Db.Positions.ToList();
            model.InvoiceRow = Db.InvoiceRow.Find(invoiceRowID);
            model.OfferID = Db.InvoiceRow.Find(invoiceRowID).OfferID;
            model.OfferPositionList = Db.OfferPositions.Where(x => x.OfferID == model.OfferID).ToList();
            model.Description = model.InvoiceRow.OvertimeDescription;

            return PartialView("~/Views/Invoice/_Partial_OvertimeModalDetail.cshtml", model);

        }
        public PartialViewResult OvertimeDetailModalShowReadOnly(int id)
        {
            var model = new OvertimeDetailModel();
            model.ListOvertime = Db.Overtime.Where(x => x.InvoiceRowId == id && x.IsActive == true).ToList();
            model.Positons = Db.Positions.ToList();
            model.InvoiceRow = Db.InvoiceRow.Find(id);
            model.OfferID = Db.InvoiceRow.Find(id).OfferID;
            model.OfferPositionList = Db.OfferPositions.Where(x => x.OfferID == model.OfferID).ToList();
            model.Description = model.InvoiceRow.OvertimeDescription;
            if (model.ListOvertime.Count() == 0)
            {
                ViewBag.Message = "Fazla mesai hesabı bulunmamaktadır.";
            }

            return PartialView("~/Views/Invoice/_PartialOvertimeReadOnly.cshtml", model);
        }

        //Fazla Mesai Silme
        public PartialViewResult DeleteOvertime(int invoiceRowID)
        {
            var findInvoice = Db.InvoiceRow.Find(invoiceRowID);
            double? paidedInvoicesPrice = 0;

            findInvoice.IsOvertime = false;
            findInvoice.OvertTimeGeneralTotal = 0;

            var overtimeList = Db.Overtime.Where(x => x.InvoiceRowId == invoiceRowID && x.IsActive == true).ToList();

            foreach (var item in overtimeList)
            {
                item.IsActive = false;
            }

            Db.SaveChanges();

            InvoiceControlModel model = new InvoiceControlModel();

            model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID).ToList();
            model.OfferID = findInvoice.OfferID.Value;
            model.MonthlyInvoice = findInvoice.MonthlyInvoice.Value;
            int invoiceCount = model.InvoiceRows.Count;
            paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
            var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Count();
            model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;

            if (paidedInvoicesPrice != null)
            {
                model.Remaining = (findInvoice.TotalPrice) - paidedInvoicesPrice;
                //kalan odenek
                //if (Math.Floor(model.Remaining.Value) == 0)
                //{
                //    ViewBag.RemainZero = "Toplam Fatura Tutarına ulaşılmıştır..!";
                //    model.Remaining = Math.Floor(model.Remaining.Value);
                //}
            }

            model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(i => i.PaidedPrice);
            model.OfferID = findInvoice.OfferID.Value;

            OypaHelper.AddApplicationLog("Management", "SaveInvoice", "Invoice", findInvoice.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {findInvoice.Offer.OfferNumber} nolu teklifin {findInvoice.PaymentDate.Value.ToShortDateString()} tarihli faturasında yer alan fazla mesai tutarını sildi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
        }
        #region SaveInvoiceInformation
        //FAtura kaydı tutar ve masraf merkezi ile birlikte log web servis aracılığıyla logoya gönderilir.
        #endregion
        public ActionResult SaveInvoice(string invoiceRowID, string Price, string expenseCenter)
        {
            InvoiceControlModel model = new InvoiceControlModel();
            var id = int.Parse(invoiceRowID);
            
            double? price = Convert.ToDouble(Price);

     
            double? paidedInvoicesPrice = 0;

            if (price == null)
            {
                OypaHelper.AddApplicationLog("Management", "SaveInvoice", "Invoice", Convert.ToInt32(invoiceRowID), null, false, "Fatura tutarı boş geçilemez", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return Json(new { result = "Error", message = "Fatura turarı boş geçilemez!" }, JsonRequestBehavior.AllowGet);
            }

            var invoiceRow = Db.InvoiceRow.Find(id);
            if (invoiceRow != null)
            {
                try
                {
                   
                    //Invoice Row boş değilse 
                    //Servise git.
                    invoiceRow.ExpenseCenterCode = expenseCenter; //Masraf merkezi eklendi!
                    OypaService.Authentication auth = new OypaService.Authentication();
                    auth.Username = "testUser";
                    auth.Password = "testKullanici";
                    auth.FirmNR = "124";
                    auth.PeriodNR = "1";

                    OypaService.LOGOTIGERSoapClient client = new OypaService.LOGOTIGERSoapClient();
                    client.Open();


                   

                    OypaService.SalesInvoice modelSaleInvoice = new OypaService.SalesInvoice();
                    modelSaleInvoice.DataObjectParameter = new OypaService.DataObjectParameter();
                    modelSaleInvoice.DataObjectParameter.FillAccCodesOnPreSave = true;

                    modelSaleInvoice.LOGICALREF = "0";
                    modelSaleInvoice.TYPE = "9"; //Hizmet
                    modelSaleInvoice.NUMBER = "~";
                    modelSaleInvoice.DATE = DateTime.Now;
                    modelSaleInvoice.ARP_CODE = invoiceRow.Offer.Customers.AccountCode;
                    modelSaleInvoice.SOURCE_WH = "0";
                    modelSaleInvoice.SOURCE_COST_GRP = "0";
                    modelSaleInvoice.VAT_RATE = "18.0"; //Kdv oranı
                    modelSaleInvoice.TOTAL_DISCOUNTS = "0"; // Toplam indirim
                    modelSaleInvoice.TOTAL_DISCOUNTED = "0";//İndirime tabi tutar kdv hariç
                    modelSaleInvoice.TOTAL_VAT = ((price * 18) / 100).Value.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                    modelSaleInvoice.TOTAL_GROSS = price.Value.ToString("N2", CultureInfo.GetCultureInfo("en-US")); //Brüt tutar kdv hariç
                    var vatIncludedPrice = (price * 118) / 100;
                    modelSaleInvoice.TOTAL_NET = vatIncludedPrice.Value.ToString("N2", CultureInfo.GetCultureInfo("en-US")); //kdv dahil.
                    modelSaleInvoice.DIVISION = "0";
                    modelSaleInvoice.DEPARTMENT = "0";
                    modelSaleInvoice.FACTORY = "0";
                    modelSaleInvoice.AUXIL_CODE = "OYP";
                    //Proje kodu eklendi.
                    modelSaleInvoice.PROJECT_CODE = invoiceRow.Offer.Opportunity.Projects.Code;
                    //OypaService.SalesInvoiceItem[] itemarray = new OypaService.SalesInvoiceItem[] { };
                    List<OypaService.SalesInvoiceItem> itemarray = new List<OypaService.SalesInvoiceItem>();

                    double? shortfall = 0;
                    if (invoiceRow.IsOvertime != null)//fazla mesai kaydı
                    {
                        if (invoiceRow.IsOvertime.Value == true)
                        {
                            OypaService.SalesInvoiceItem item2 = new OypaService.SalesInvoiceItem();
                            item2.TYPE = "4";
                            item2.MASTER_CODE = "1006"; //Fazla Mesai 
                            item2.TRCODE = "9";
                            item2.DATE = DateTime.Now;
                            item2.IOCODE = "4";
                            item2.QUANTITY = "1";
                            var overtimeRound = Math.Round(invoiceRow.OvertTimeGeneralTotal.Value, 2, MidpointRounding.AwayFromZero);
                            item2.PRICE = overtimeRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            item2.TOTAL = overtimeRound.ToString("N2", CultureInfo.GetCultureInfo("en-US")); //Toplam fiyat
                            item2.UNIT_CODE = "ADET";
                            item2.VAT_RATE = "18.0";
                            item2.VAT_INCLUDED = "0";//0 kdv hariç, 1 kdv dahil
                            item2.VAT_AMOUNT = ((overtimeRound * 18) / 100).ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            item2.VAT_BASE = "0"; //kdv matharı
                            item2.BILLED = "1";
                            item2.TOTAL_NET = overtimeRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            shortfall = price - invoiceRow.OvertTimeGeneralTotal;
                            //Açıklama ve masra merkezi eklendi.
                            item2.DESCRIPTION = invoiceRow.OvertimeDescription;
                            item2.OHP_CODE1 = invoiceRow.ExpenseCenterCode;
                            itemarray.Add(item2);
                        }
                        else
                        {
                            shortfall = price - 0;
                        }
                    }
                    else
                    {
                        shortfall = price - 0;
                    }

                    if (invoiceRow.IsMissingDay != null) //Eksik gün kaydı
                    {
                        if (invoiceRow.IsMissingDay.Value == true)
                        {
                            OypaService.SalesInvoiceItem item = new OypaService.SalesInvoiceItem();
                            item.TYPE = "4";
                            item.MASTER_CODE = "1001"; //Hizmet bedeli
                            item.TRCODE = "9";
                            item.DATE = DateTime.Now;
                            item.IOCODE = "4";
                            item.QUANTITY = "1";
                            var shortfallRound = Math.Round(invoiceRow.ShortfallAmount.Value, 2, MidpointRounding.AwayFromZero);
                            item.PRICE = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            item.TOTAL = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));//Toplam fiyat
                            item.UNIT_CODE = "ADET";
                            item.VAT_RATE = "18.0";
                            item.VAT_INCLUDED = "0";//0 kdv hariç, 1 kdv dahil
                            item.VAT_AMOUNT = ((shortfallRound * 18) / 100).ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            item.VAT_BASE = "0"; //kdv matharı
                            item.BILLED = "1";
                            item.TOTAL_NET = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            //Açıklama ve masra merkezi eklendi.
                            item.DESCRIPTION = invoiceRow.MissingDayDescription;
                            item.OHP_CODE1 = invoiceRow.ExpenseCenterCode;
                            itemarray.Add(item);
                        }
                        else
                        {
                            OypaService.SalesInvoiceItem item = new OypaService.SalesInvoiceItem();
                            item.TYPE = "4";
                            item.MASTER_CODE = "1001"; //Hizmet bedeli
                            item.TRCODE = "9";
                            item.DATE = DateTime.Now;
                            item.IOCODE = "4";
                            item.QUANTITY = "1";
                            var shortfallRound = Math.Round(shortfall.Value, 2, MidpointRounding.AwayFromZero);
                            item.PRICE = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            item.TOTAL = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US")); //Toplam fiyat
                            item.UNIT_CODE = "ADET";
                            item.VAT_RATE = "18.0";
                            item.VAT_INCLUDED = "0";//0 kdv hariç, 1 kdv dahil
                            item.VAT_AMOUNT = ((shortfallRound * 18) / 100).ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            item.VAT_BASE = "0"; //kdv matharı
                            item.BILLED = "1";
                            item.TOTAL_NET = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                            //Açıklama ve masra merkezi eklendi.
                            item.DESCRIPTION = invoiceRow.MissingDayDescription;
                            item.OHP_CODE1 = invoiceRow.ExpenseCenterCode;
                            itemarray.Add(item);
                        }
                    }
                    else
                    {
                        OypaService.SalesInvoiceItem item = new OypaService.SalesInvoiceItem();
                        item.TYPE = "4";
                        item.MASTER_CODE = "1001"; //Hizmet bedeli
                        item.TRCODE = "9";
                        item.DATE = DateTime.Now;
                        item.IOCODE = "4";
                        item.QUANTITY = "1";
                        var shortfallRound = Math.Round(shortfall.Value, 2, MidpointRounding.AwayFromZero);
                        item.PRICE = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                        item.TOTAL = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                        item.UNIT_CODE = "ADET";
                        item.VAT_RATE = "18.0";
                        item.VAT_INCLUDED = "0";//0 kdv hariç, 1 kdv dahil
                        item.VAT_AMOUNT = ((shortfallRound * 18) / 100).ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                        item.VAT_BASE = "0"; //kdv matharı
                        item.BILLED = "1";
                        item.TOTAL_NET = shortfallRound.ToString("N2", CultureInfo.GetCultureInfo("en-US"));
                        //Açıklama ve masra merkezi eklendi.
                        item.DESCRIPTION = invoiceRow.MissingDayDescription;
                        item.OHP_CODE1 = invoiceRow.ExpenseCenterCode;
                        itemarray.Add(item);
                    }

                    OypaService.SalesInvoiceItem[] arrayOfStrings = itemarray.ToArray();
                    modelSaleInvoice.TRANSACTIONS = new OypaService.SalesInvoiceTransaction();
                    modelSaleInvoice.TRANSACTIONS.items = arrayOfStrings;


                    
                    var result = client.SaveSalesInvoice(auth, modelSaleInvoice);



                    if (result != null)
                    {
                        if (result.Code == 1000) //Başarılı
                        {
                            var currentLogicalRef = client.GetSalesInvoiceByLOGICALREF(auth, result.Data.LOGICALREF);
                            if (currentLogicalRef != null)
                            {
                                SalesInvoiceReturnLogo sale = new SalesInvoiceReturnLogo()
                                {
                                    InvoiceRowID = invoiceRow.ID,
                                    LogicalRef = currentLogicalRef.LOGICALREF,
                                    Type = currentLogicalRef.TYPE,
                                    Number = currentLogicalRef.NUMBER,
                                    Date = currentLogicalRef.DATE,
                                    ArpCode = currentLogicalRef.ARP_CODE,
                                    SourceWh = currentLogicalRef.SOURCE_WH,
                                    SourceCostGrp = currentLogicalRef.SOURCE_COST_GRP,
                                    VatRate = currentLogicalRef.VAT_RATE,
                                    TotalDiscounts = currentLogicalRef.TOTAL_DISCOUNTS,
                                    TotalDiscounted = currentLogicalRef.TOTAL_DISCOUNTED,
                                    TotalGross = currentLogicalRef.TOTAL_GROSS,
                                    TotalNet = currentLogicalRef.TOTAL_NET,
                                    Division = currentLogicalRef.DIVISION,
                                    Depatment = currentLogicalRef.DEPARTMENT,
                                    Factory = currentLogicalRef.FACTORY,
                                    RecordDate = DateTime.Now,
                                    RecordUserID = OypaHelper.RecordEmployee(),
                                    UpdataDate = DateTime.Now,
                                    UpdateUserID = OypaHelper.RecordEmployee(),
                                    LogoResultCode = result.Code.ToString(),
                                    LogoResultDescription = result.Description,
                                    IsCanceled = false,
                                    ProjectCode = currentLogicalRef.PROJECT_CODE
                                };

                                Db.SalesInvoiceReturnLogo.Add(sale);
                                Db.SaveChanges();
                                if (currentLogicalRef.TRANSACTIONS.items != null && currentLogicalRef.TRANSACTIONS.items.Count() > 0)
                                {
                                    foreach (var retunritem in currentLogicalRef.TRANSACTIONS.items)
                                    {
                                        SalesInvoiceItemReturnLogo items = new SalesInvoiceItemReturnLogo()
                                        {
                                            SalesInvoiceID = sale.ID,
                                            Type = retunritem.TYPE,
                                            MasterCode = retunritem.MASTER_CODE,
                                            TrCode = retunritem.TRCODE,
                                            IoCode = retunritem.IOCODE,
                                            Quantity = retunritem.QUANTITY,
                                            Price = retunritem.PRICE,
                                            Total = retunritem.TOTAL,
                                            UnitCode = retunritem.UNIT_CODE,
                                            VatRate = retunritem.VAT_RATE,
                                            VatIncluded = retunritem.VAT_INCLUDED,
                                            VatAmount = retunritem.VAT_AMOUNT,
                                            VatBase = retunritem.VAT_BASE,
                                            Billed = retunritem.BILLED,
                                            TotalNet = retunritem.TOTAL_NET,
                                            RecordDate = DateTime.Now,
                                            RecordUserID = OypaHelper.RecordEmployee(),
                                            UpdateDate = DateTime.Now,
                                            Description = retunritem.DESCRIPTION,
                                            Ohp_Code1 = retunritem.OHP_CODE1
                                        };
                                        Db.SalesInvoiceItemReturnLogo.Add(items);
                                    }
                                    Db.SaveChanges();
                                }
                            }

                            //Logoya fatura kaydı atıldı.
                            //Şimdi Kendi tablolarımızı update edelim

                            invoiceRow.IsPaid = true;
                            invoiceRow.PaidedPrice = price;
                            invoiceRow.UpdateDate = DateTime.Now;
                            invoiceRow.UpdatedUserID = OypaHelper.RecordEmployee();

                            InvoiceRowHistory history = new InvoiceRowHistory()
                            {
                                ContractID = invoiceRow.OfferID,
                                MonthlyInvoice = invoiceRow.MonthlyInvoice,
                                IsPaid = true,
                                PaymentDate = invoiceRow.PaymentDate,
                                RecordDate = DateTime.Now,
                                RecordUserID = OypaHelper.RecordEmployee(),
                                InvoiceRowID = invoiceRow.ID,
                                TotalPrice = invoiceRow.TotalPrice,
                                // PaidedPrice = price

                            };
                            Db.InvoiceRowHistory.Add(history);

                            Db.SaveChanges();

                            OypaHelper.AddApplicationLog("Management", "SaveInvoice", "Invoice", invoiceRow.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {invoiceRow.Offer.OfferNumber} nolu teklifin {invoiceRow.PaymentDate.Value.ToShortDateString()} tarihli fatura kaydı logoya başarılı bir şekilde yazıldı, Logodaki kayıt Id'si {result.Data.LOGICALREF}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            
                            OypaHelper.AddApplicationLog("Management", "SaveInvoice", "Invoice", invoiceRow.ID, null, false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {invoiceRow.Offer.OfferNumber} nolu teklifin {invoiceRow.PaymentDate.Value.ToShortDateString()} tarihli fatura logoya yazılırken '{result.Description}' mesajı ile karşılaşıldı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Error", message = "Logo servisinden işlem başarısız mesajı döndü. Fatura kaydı logoya yazılamadı, Lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "SaveInvoice", "Invoice", invoiceRow.ID, null, false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {invoiceRow.Offer.OfferNumber} nolu teklifin {invoiceRow.PaymentDate.Value.ToShortDateString()} tarihli fatura kaydı işlemi için logo servisinden cevap alınamadı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return Json(new { result = "Error", message = "Logo servisinden boş değer döndü! Lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                    }

                    //Son

                    model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID).ToList();
                    model.MonthlyInvoice = invoiceRow.MonthlyInvoice.Value;
                    model.OfferID = invoiceRow.OfferID.Value;
                    int invoiceCount = model.InvoiceRows.Count;
                    paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
                    var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == true).Count();
                    model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;

                    if (paidedInvoicesPrice != null)
                    {
                        model.Remaining = (invoiceRow.TotalPrice) - paidedInvoicesPrice;
                        //kalan odenek
                        //if (Math.Floor(model.Remaining.Value) == 0)
                        //{
                        //    ViewBag.RemainZero = "Toplam Fatura Tutarına ulaşılmıştır..!";
                        //    model.Remaining = Math.Floor(model.Remaining.Value);
                        //}
                    }


                    model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == true).Sum(i => i.PaidedPrice);
                    model.OfferID = invoiceRow.OfferID.Value;

                    var singleOffer = Db.Offer.FirstOrDefault(t => t.ID == model.OfferID);

                    if (singleOffer.IsOneTime.Value)
                    {
                        return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
                    }
                    else
                    {
                        return PartialView("~/Views/Invoice/_PartialInvoiceListIsOneTime.cshtml", model); //Tek seferlik teklif için
                    }

                }
                catch (Exception ex)
                {
                    OypaHelper.AddApplicationLog("Management", "SaveInvoice", "Invoice", invoiceRow.ID, null, false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {invoiceRow.Offer.OfferNumber} nolu teklifin {invoiceRow.PaymentDate.Value.ToShortDateString()} tarihli faturasını keserken '{ex.Message}'mesajı ile karşılaştı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    int startIndex = 0;
                    int endIndex = 0;
                    string modelMessage = string.Empty;
                    string UserInfo = " ";

                    for (int i = 0; i < ex.Message.Length; i++)
                    {
                        if (ex.Message[i] == '{')
                        {
                            startIndex = i;
                            break;
                        }
                    }
                    for (int i = 0; i < ex.Message.Length; i++)
                    {
                        if (ex.Message[i] == '}')
                        {
                            endIndex = i;

                        }
                    }

                    modelMessage = ex.Message.Substring(startIndex, (endIndex - startIndex) + 1);
                    var errormodel = JsonConvert.DeserializeObject<ErrorModel>(modelMessage);

                    if (errormodel.ModelState.ValError0 != null)
                    {
                        UserInfo = errormodel.ModelState.ValError0.FirstOrDefault();
                    }
                    if (errormodel.ModelState.ValError1 != null)
                    {
                        UserInfo += errormodel.ModelState.ValError1.FirstOrDefault();
                    }

                    return Json(new { result = "Error", message = UserInfo }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "Error", message = "İlgili fatura kaydı bulunamadı, Lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult DeleteInvoice(string invoiceRowID)
        {
            InvoiceControlModel model = new InvoiceControlModel();

            var id = int.Parse(invoiceRowID);
            double? paidedInvoicesPrice = 0;

            var invoiceRow = Db.InvoiceRow.Find(id);
            if (invoiceRow != null)
            {
                invoiceRow.IsPaid = false;
                invoiceRow.PaidedPrice = 0;
                invoiceRow.UpdateDate = DateTime.Now;
                invoiceRow.UpdatedUserID = OypaHelper.RecordEmployee();

                InvoiceRowHistory history = new InvoiceRowHistory()
                {
                    ContractID = invoiceRow.OfferID,
                    MonthlyInvoice = invoiceRow.MonthlyInvoice,
                    IsPaid = false,
                    IsCanceled = true,
                    CanceledDate = DateTime.Now,
                    PaymentDate = invoiceRow.PaymentDate,
                    RecordDate = DateTime.Now,
                    RecordUserID = OypaHelper.RecordEmployee(),
                    InvoiceRowID = invoiceRow.ID,
                    TotalPrice = invoiceRow.TotalPrice,

                };
                Db.InvoiceRowHistory.Add(history);

                Db.SaveChanges();

                model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID).ToList();
                model.MonthlyInvoice = model.InvoiceRows.FirstOrDefault().MonthlyInvoice.Value;
                model.OfferID = invoiceRow.OfferID.Value;
                int invoiceCount = model.InvoiceRows.Count;
                paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
                var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == true).Count();
                model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;
                model.OfferID = invoiceRow.OfferID.Value;
                if (paidedInvoicesPrice != null)
                {
                    model.Remaining = (invoiceRow.TotalPrice) - paidedInvoicesPrice; //kalan odenek                  
                }
                else
                {
                    paidedInvoicesPrice = 0;
                }

                // silinen aylik faturanin aylik tutarini yeniden hesaplamak icin : (toplam - odenenler) / odenmemis fatura adedi
                //var count = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == false).ToList().Count;
                //invoiceRow.MonthlyInvoice = (invoiceRow.TotalPrice - paidedInvoicesPrice) / count;
                //Db.SaveChanges();

                model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == invoiceRow.OfferID && i.IsPaid == true).Sum(i => i.PaidedPrice);

                model.SingleOffer = Db.Offer.FirstOrDefault(t => t.ID == model.OfferID);

                OypaHelper.AddApplicationLog("Management", "DeleteInvoice", "Invoice", invoiceRow.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {invoiceRow.Offer.OfferNumber} nolu teklifin {invoiceRow.PaymentDate.Value.ToShortDateString()} tarihli fatura kaydını iptal etti! ", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                if (model.SingleOffer.IsOneTime.Value)
                {
                    return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/Invoice/_PartialInvoiceListIsOneTime.cshtml", model);
                }
            }
            else
            {
                OypaHelper.AddApplicationLog("Management", "DeleteInvoice", "Invoice", invoiceRow.ID, null, false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {invoiceRowID} parametresi ile sistemde var olmayan bir faturayı görüntülemeye çalıştı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
            }
        }

        public PartialViewResult OpenMissingDayModal(int invoiceRowID)
        {
            MissingDayControlModel model = new MissingDayControlModel();
            if (invoiceRowID > 0)
            {
                var selectedInvoice = Db.InvoiceRow.FirstOrDefault(t => t.ID == invoiceRowID);
                if (selectedInvoice != null)
                {
                    model.MonthName = GetMonthName(selectedInvoice.PaymentDate.Value);

                    var checkInvoiceRowId = Db.MissingDay.FirstOrDefault(t => t.InvoiceRowID == selectedInvoice.ID && t.IsDeleted == false);
                    if (checkInvoiceRowId == null) //Daha önceden Eksik gün işlemi yapılmamış.
                    {
                        var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == selectedInvoice.OfferID);

                        TimeSpan res = selectedOffer.DateEnd.Value - selectedOffer.DateBegin.Value;
                        var totalDay = res.TotalDays;
                        int totalMonth = Convert.ToInt32((totalDay / 30));

                        //Teklifte var olan poziyonları distinct olarak çekiyorum, pozisyonları tekil olarak yazdırabilmek için
                        var posiontionsID = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).Select(t => t.PositionID).Distinct();
                        var offerpositions = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).ToList();
                        List<MissingDay> mdList = new List<MissingDay>();
                        if (posiontionsID != null && posiontionsID.Count() > 0)
                        {
                            var allHrSum = CalculateHrSum(selectedOffer, posiontionsID);
                            foreach (var item in posiontionsID)
                            {
                                MissingDay md = new MissingDay();

                                md.InvoiceRowID = selectedInvoice.ID;
                                md.PositionID = Db.Positions.FirstOrDefault(t => t.ID == item).ID;
                                //brüt ücretin satırında olan quantity'i aldım. 
                                md.Quantity = offerpositions.FirstOrDefault(t => t.PositionID == item && t.PositionItemID == 1).Quantity;

                                md.HumanResourcesUnitPrice = CalculateHRUnitPrice(selectedOffer, item);
                                md.HrSum = md.HumanResourcesUnitPrice * md.Quantity.Value;
                                var NonHrUnit = CalculateNonHrUnitPrice(selectedOffer, allHrSum);
                                md.NonHumanResourcesUnitPrice = NonHrUnit * md.Quantity.Value;
                                md.MonthlyWorkingDay = 30; // Default olarak gidiyor.
                                md.MonthlyMissingDay = 0; //Default olarak gidiyor.
                                md.TermWorkingDay = (md.Quantity * md.MonthlyWorkingDay) - md.MonthlyMissingDay;
                                md.TermHumanResourcesUnitPrice = ((md.HrSum / (md.MonthlyWorkingDay * md.Quantity)) * md.TermWorkingDay);
                                md.TermInvoiceAmount = md.TermHumanResourcesUnitPrice + md.NonHumanResourcesUnitPrice;
                                md.IsActive = false;
                                md.IsDeleted = false;
                                md.RecordDate = DateTime.Now;
                                md.RecordUserID = OypaHelper.RecordEmployee();
                                //md.UpdateDate = null;
                                //md.UpdateUserID = null;
                                mdList.Add(md);
                            }
                        }

                        Db.MissingDay.AddRange(mdList);
                        Db.SaveChanges();
                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == false && t.IsDeleted == false && t.InvoiceRowID == selectedInvoice.ID).Sum(t => t.TermInvoiceAmount);
                    }
                    else if (checkInvoiceRowId.IsActive == false)
                    {
                        //Silinmemiş ve aktif olmayan listeyi getir.
                        //Eski kayıtları silip yeniden kayıt attım. t anında  İK Brüt fiyatı değişebilir!!
                        var list = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        Db.MissingDay.RemoveRange(list);
                        Db.SaveChanges();
                        var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == selectedInvoice.OfferID);
                        TimeSpan res = selectedOffer.DateEnd.Value - selectedOffer.DateBegin.Value;
                        var totalDay = res.TotalDays;
                        int totalMonth = Convert.ToInt32((totalDay / 30));
                        //Teklifte var olan poziyonları distinct olarak çekiyorum, pozisyonları tekil olarak yazdırabilmek için
                        var posiontionsID = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).Select(t => t.PositionID).Distinct();
                        var offerpositions = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).ToList();
                        List<MissingDay> mdList = new List<MissingDay>();
                        if (posiontionsID != null && posiontionsID.Count() > 0)
                        {
                            // Burada kaldım!.
                            //var selectOffer = Db.Offer.FirstOrDefault(t=>t.)

                            var allHrSum = CalculateHrSum(selectedOffer, posiontionsID);
                            foreach (var item in posiontionsID)
                            {
                                MissingDay md = new MissingDay();

                                md.InvoiceRowID = selectedInvoice.ID;
                                md.PositionID = Db.Positions.FirstOrDefault(t => t.ID == item).ID;
                                //brüt ücretin satırında olan quantity'i aldım. 
                                md.Quantity = offerpositions.FirstOrDefault(t => t.PositionID == item && t.PositionItemID == 1).Quantity;

                                md.HumanResourcesUnitPrice = CalculateHRUnitPrice(selectedOffer, item);
                                md.HrSum = md.HumanResourcesUnitPrice * md.Quantity.Value;
                                var NonHrUnit = CalculateNonHrUnitPrice(selectedOffer, allHrSum);
                                md.NonHumanResourcesUnitPrice = NonHrUnit * md.Quantity.Value;
                                md.MonthlyWorkingDay = 30; // Default olarak gidiyor.
                                md.MonthlyMissingDay = 0; //Default olarak gidiyor.
                                md.TermWorkingDay = (md.Quantity * md.MonthlyWorkingDay) - md.MonthlyMissingDay;
                                md.TermHumanResourcesUnitPrice = ((md.HrSum / (md.MonthlyWorkingDay * md.Quantity)) * md.TermWorkingDay);
                                md.TermInvoiceAmount = md.TermHumanResourcesUnitPrice + md.NonHumanResourcesUnitPrice;
                                md.IsActive = false;
                                md.IsDeleted = false;
                                md.RecordDate = DateTime.Now;
                                md.RecordUserID = OypaHelper.RecordEmployee();
                   
                                mdList.Add(md);
                            }
                        }
                        Db.MissingDay.AddRange(mdList);
                        Db.SaveChanges();
                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == false && t.IsDeleted == false && t.InvoiceRowID == selectedInvoice.ID).Sum(t => t.TermInvoiceAmount);
                    }
                    else if (checkInvoiceRowId.IsActive == true) //Eğer Eksik gün kayıd var ise kayıtlı verileri getir.
                    {
                        //IsActive 'i true olan kayıtlar var kullanıcı eksik günde güncelleme yapabilir!
                        model.IsActive = true;
                        model.Description = selectedInvoice.MissingDayDescription;
                        model.RowID = checkInvoiceRowId.ID;
                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == true && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == true && t.IsDeleted == false && t.InvoiceRowID == selectedInvoice.ID).Sum(t => t.TermInvoiceAmount);
                        return PartialView("~/Views/Invoice/_PartialUpdateMissingDay.cshtml", model);
                    }
                }
            }

            return PartialView("~/Views/Invoice/_PartialMissingDay.cshtml", model);
        }

        public PartialViewResult OpenMissingDayModalReadOnly(int invoiceRowID)
        {
            MissingDayControlModel model = new MissingDayControlModel();
            if (invoiceRowID > 0)
            {
                var selectedInvoice = Db.InvoiceRow.FirstOrDefault(t => t.ID == invoiceRowID);
                if (selectedInvoice != null)
                {
                    if (selectedInvoice.IsMissingDay == null || selectedInvoice.IsMissingDay == false)
                    {
                        model.IsEmpty = false;
                    }
                    else
                    {
                        model.IsEmpty = true;
                    }
                    model.MonthName = GetMonthName(selectedInvoice.PaymentDate.Value);
                    var checkInvoiceRowId = Db.MissingDay.FirstOrDefault(t => t.InvoiceRowID == selectedInvoice.ID && t.IsDeleted == false);
                    if (checkInvoiceRowId == null) //Daha önceden Eksik gün işlemi yapılmamış.
                    {
                        var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == selectedInvoice.OfferID);

                        TimeSpan res = selectedOffer.DateEnd.Value - selectedOffer.DateBegin.Value;
                        var totalDay = res.TotalDays;
                        int totalMonth = Convert.ToInt32((totalDay / 30));

                        //Teklifte var olan poziyonları distinct olarak çekiyorum, pozisyonları tekil olarak yazdırabilmek için
                        var posiontionsID = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).Select(t => t.PositionID).Distinct();
                        var offerpositions = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).ToList();
                        List<MissingDay> mdList = new List<MissingDay>();
                        if (posiontionsID != null && posiontionsID.Count() > 0)
                        {


                            var allHrSum = CalculateHrSum(selectedOffer, posiontionsID);
                            foreach (var item in posiontionsID)
                            {

                                MissingDay md = new MissingDay();

                                md.InvoiceRowID = selectedInvoice.ID;
                                md.PositionID = Db.Positions.FirstOrDefault(t => t.ID == item).ID;
                                //brüt ücretin satırında olan quantity'i aldım. 
                                md.Quantity = offerpositions.FirstOrDefault(t => t.PositionID == item && t.PositionItemID == 1).Quantity;

                                md.HumanResourcesUnitPrice = CalculateHRUnitPrice(selectedOffer, item);
                                md.HrSum = md.HumanResourcesUnitPrice * md.Quantity.Value;
                                var NonHrUnit = CalculateNonHrUnitPrice(selectedOffer, allHrSum);
                                md.NonHumanResourcesUnitPrice = NonHrUnit * md.Quantity.Value;
                                md.MonthlyWorkingDay = 30; // Default olarak gidiyor.
                                md.MonthlyMissingDay = 0; //Default olarak gidiyor.
                                md.TermWorkingDay = (md.Quantity * md.MonthlyWorkingDay) - md.MonthlyMissingDay;
                                md.TermHumanResourcesUnitPrice = ((md.HrSum / (md.MonthlyWorkingDay * md.Quantity)) * md.TermWorkingDay);
                                md.TermInvoiceAmount = md.TermHumanResourcesUnitPrice + md.NonHumanResourcesUnitPrice;
                                md.IsActive = false;
                                md.IsDeleted = false;
                                md.RecordDate = DateTime.Now;
                                md.RecordUserID = OypaHelper.RecordEmployee();
                                //md.UpdateDate = null;
                                //md.UpdateUserID = null;
                                mdList.Add(md);
                            }
                        }
                        Db.MissingDay.AddRange(mdList);
                        Db.SaveChanges();
                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == false && t.IsDeleted == false && t.InvoiceRowID == selectedInvoice.ID).Sum(t => t.TermInvoiceAmount);


                    }
                    else if (checkInvoiceRowId.IsActive == false)
                    {
                        //Silinmemiş ve aktif olmayan listeyi getir.
                        //Eski kayıtları silip yeniden kayıt attım. O arada İK Brüt fiyatı değişebilir!!
                        var list = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        Db.MissingDay.RemoveRange(list);
                        Db.SaveChanges();
                        var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == selectedInvoice.OfferID);
                        TimeSpan res = selectedOffer.DateEnd.Value - selectedOffer.DateBegin.Value;
                        var totalDay = res.TotalDays;
                        int totalMonth = Convert.ToInt32((totalDay / 30));
                        //Teklifte var olan poziyonları distinct olarak çekiyorum, pozisyonları tekil olarak yazdırabilmek için
                        var posiontionsID = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).Select(t => t.PositionID).Distinct();
                        var offerpositions = Db.OfferPositions.Where(t => t.OfferID == selectedOffer.ID).ToList();
                        List<MissingDay> mdList = new List<MissingDay>();
                        if (posiontionsID != null && posiontionsID.Count() > 0)
                        {
                            // Burada kaldım!.
                            //var selectOffer = Db.Offer.FirstOrDefault(t=>t.)

                            var allHrSum = CalculateHrSum(selectedOffer, posiontionsID);
                            foreach (var item in posiontionsID)
                            {

                                MissingDay md = new MissingDay();

                                md.InvoiceRowID = selectedInvoice.ID;
                                md.PositionID = Db.Positions.FirstOrDefault(t => t.ID == item).ID;
                                //brüt ücretin satırında olan quantity'i aldım. 
                                md.Quantity = offerpositions.FirstOrDefault(t => t.PositionID == item && t.PositionItemID == 1).Quantity;


                                md.HumanResourcesUnitPrice = CalculateHRUnitPrice(selectedOffer, item);
                                md.HrSum = md.HumanResourcesUnitPrice * md.Quantity.Value;
                                var NonHrUnit = CalculateNonHrUnitPrice(selectedOffer, allHrSum);
                                md.NonHumanResourcesUnitPrice = NonHrUnit * md.Quantity.Value;
                                md.MonthlyWorkingDay = 30; // Default olarak gidiyor.
                                md.MonthlyMissingDay = 0; //Default olarak gidiyor.
                                md.TermWorkingDay = (md.Quantity * md.MonthlyWorkingDay) - md.MonthlyMissingDay;
                                md.TermHumanResourcesUnitPrice = ((md.HrSum / (md.MonthlyWorkingDay * md.Quantity)) * md.TermWorkingDay);
                                md.TermInvoiceAmount = md.TermHumanResourcesUnitPrice + md.NonHumanResourcesUnitPrice;
                                md.IsActive = false;
                                md.IsDeleted = false;
                                md.RecordDate = DateTime.Now;
                                md.RecordUserID = OypaHelper.RecordEmployee();
                                //md.UpdateDate = null;
                                //md.UpdateUserID = null;
                                mdList.Add(md);
                            }
                        }
                        Db.MissingDay.AddRange(mdList);
                        Db.SaveChanges();
                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == false && t.IsDeleted == false && t.InvoiceRowID == selectedInvoice.ID).Sum(t => t.TermInvoiceAmount);

                    }
                    else if (checkInvoiceRowId.IsActive == true) //Eğer Eksik gün kayıd var ise kayıtlı verileri getir.
                    {
                        //IsActive 'i true olan kayıtlar var kullanıcı eksik günde güncelleme yapabilir!
                        model.IsActive = true;
                        model.RowID = checkInvoiceRowId.ID;
                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == true && t.InvoiceRowID == selectedInvoice.ID).ToList();
                        model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == true && t.IsDeleted == false && t.InvoiceRowID == selectedInvoice.ID).Sum(t => t.TermInvoiceAmount);
                        model.Description = selectedInvoice.MissingDayDescription;
                        return PartialView("~/Views/Invoice/_PartialMissingDayReadOnly.cshtml", model);
                    }
                }
            }
            return PartialView("~/Views/Invoice/_PartialMissingDayReadOnly.cshtml", model);




        }

        public PartialViewResult CalculateMissingDay(CalculateMissingDayParamaters paramaters)
        {
            MissingDayControlModel model = new MissingDayControlModel();
            if (paramaters != null)
            {
                double? hrUnitPrice = !string.IsNullOrEmpty(paramaters.HRUnitPrice) ? Convert.ToDouble(paramaters.HRUnitPrice.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : 0;
                double? workingDay = !string.IsNullOrEmpty(paramaters.MonthlyWorkingDay) ? Convert.ToDouble(paramaters.MonthlyWorkingDay.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : 0;
                double? missingDay = !string.IsNullOrEmpty(paramaters.MonthlyMissingDay) ? Convert.ToDouble(paramaters.MonthlyMissingDay.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : 0;

                var selectedRow = Db.MissingDay.FirstOrDefault(t => t.ID == paramaters.RowID);
                var selectedPosition = Db.Positions.FirstOrDefault(t => t.ID == paramaters.PositionID);
                if (selectedRow != null && selectedPosition != null) //Parametreden gelen değer null değilse
                {
                    var mdList = Db.MissingDay.Where(t => t.InvoiceRowID == selectedRow.InvoiceRowID && t.IsActive == false && t.IsDeleted == false).ToList();
                    var invo = Db.InvoiceRow.FirstOrDefault(t => t.ID == selectedRow.InvoiceRowID);
                    var offer = Db.Offer.FirstOrDefault(t => t.ID == invo.OfferID);
                    model.MonthName = GetMonthName(invo.PaymentDate.Value);
                    selectedRow.HumanResourcesUnitPrice = hrUnitPrice;
                    Db.SaveChanges(); // İK Toplam Fiyatlarının toplamını doğru alabilmek için HrUnit price', dbye anında kaydediyorum.

                    double? allHrSum = 0;
                    foreach (var item in mdList)
                    {
                        allHrSum += item.Quantity * item.HumanResourcesUnitPrice;
                    }

                    selectedRow.HrSum = selectedRow.Quantity * hrUnitPrice; //ik toplam fiyatı da hesaplandı.
                    selectedRow.MonthlyWorkingDay = workingDay;
                    selectedRow.MonthlyMissingDay = missingDay;
                    selectedRow.TermWorkingDay = (selectedRow.Quantity * workingDay) - missingDay;
                    Db.SaveChanges();
                    var NonHrUnit = CalculateNonHrUnitPrice(offer, allHrSum.Value);
                    var posiontionsID = Db.OfferPositions.Where(t => t.OfferID == offer.ID).Select(t => t.PositionID).Distinct().ToList();
                    //İk Birim Fiyatı değiştiğinde ik dışı, ik dönem fiyatı, kd dönem fatura kolonları da etkileniyor. Bu yüzden bir ik birim fiyatı değişse bile tüm kayıtlarım bu 3 kolonunu güncellemeliyim.

                    foreach (var item in posiontionsID)
                    {
                        var selected = Db.MissingDay.FirstOrDefault(t => t.InvoiceRowID == selectedRow.InvoiceRowID && t.IsActive == false && t.IsDeleted == false && t.PositionID == item);

                        selected.NonHumanResourcesUnitPrice = NonHrUnit * selected.Quantity;

                        if (hrUnitPrice != 0 && workingDay != 0)
                        {
                            selected.TermHumanResourcesUnitPrice = ((selected.HrSum / (selected.MonthlyWorkingDay * selected.Quantity)) * selected.TermWorkingDay);
                        }
                        else
                        {
                            selected.TermHumanResourcesUnitPrice = 0;
                        }
                        selected.TermInvoiceAmount = selected.TermHumanResourcesUnitPrice + selected.NonHumanResourcesUnitPrice;
                    }
                    Db.SaveChanges();
                    //Guncelleme yapıldı.
                    model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == false && t.InvoiceRowID == selectedRow.InvoiceRowID).ToList();
                    model.TotalTermInvoiceAmount = Db.MissingDay.Where(t => t.IsActive == false && t.IsDeleted == false && t.InvoiceRowID == selectedRow.InvoiceRowID).Sum(t => t.TermInvoiceAmount);

                }
            }
            return PartialView("~/Views/Invoice/_PartialMissingDay.cshtml", model);
        }
        public PartialViewResult UpdateMissingDay(CalculateMissingDayParamaters paramaters)
        {
            MissingDayControlModel model = new MissingDayControlModel();
            if (paramaters != null)
            {
                if (paramaters.PositionID > 0)
                {
                    double? hrUnitPrice = !string.IsNullOrEmpty(paramaters.HRUnitPrice) ? Convert.ToDouble(paramaters.HRUnitPrice.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : 0;
                    double? workingDay = !string.IsNullOrEmpty(paramaters.MonthlyWorkingDay) ? Convert.ToDouble(paramaters.MonthlyWorkingDay.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : 0;
                    double? missingDay = !string.IsNullOrEmpty(paramaters.MonthlyMissingDay) ? Convert.ToDouble(paramaters.MonthlyMissingDay.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : 0;

                    var selectedRow = Db.MissingDay.FirstOrDefault(t => t.ID == paramaters.RowID);
                    var selectedPosition = Db.Positions.FirstOrDefault(t => t.ID == paramaters.PositionID);
                    if (selectedRow != null && selectedPosition != null)
                    {
                        var selectedInvoiceRow = Db.InvoiceRow.FirstOrDefault(t => t.ID == selectedRow.InvoiceRowID);
                        var mdList = Db.MissingDay.Where(t => t.InvoiceRowID == selectedRow.InvoiceRowID && t.IsActive == true && t.IsDeleted == false).ToList();
                        var offer = Db.Offer.FirstOrDefault(t => t.ID == selectedInvoiceRow.OfferID);

                        model.MonthName = GetMonthName(selectedInvoiceRow.PaymentDate.Value);
                        selectedRow.HumanResourcesUnitPrice = hrUnitPrice;
                        Db.SaveChanges(); // İK Toplam Fiyatlarının toplamını doğru alabilmek için HrUnit price', dbye anında kaydediyorum.


                        double? allHrSum = 0;
                        foreach (var item in mdList)
                        {
                            allHrSum += item.Quantity * item.HumanResourcesUnitPrice;
                        }
                        selectedRow.HrSum = selectedRow.Quantity * hrUnitPrice; //ik toplam fiyatı da hesaplandı.
                        selectedRow.MonthlyWorkingDay = workingDay;
                        selectedRow.MonthlyMissingDay = missingDay;
                        selectedRow.TermWorkingDay = (selectedRow.Quantity * workingDay) - missingDay;


                        var NonHrUnit = CalculateNonHrUnitPrice(offer, allHrSum.Value);
                        selectedRow.NonHumanResourcesUnitPrice = NonHrUnit * selectedRow.Quantity;
                        var posiontionsID = Db.OfferPositions.Where(t => t.OfferID == offer.ID).Select(t => t.PositionID).Distinct().ToList();
                        //İk Birim Fiyatı değiştiğinde ik dışı, ik dönem fiyatı, kd dönem fatura kolonları da etkileniyor. Bu yüzden bir ik birim fiyatı değişse bile tüm kayıtlarım bu 3 kolonunu güncellemeliyim.
                        foreach (var item in posiontionsID)
                        {
                            var selected = Db.MissingDay.FirstOrDefault(t => t.InvoiceRowID == selectedRow.InvoiceRowID && t.IsActive == true && t.IsDeleted == false && t.PositionID == item);

                            selected.NonHumanResourcesUnitPrice = NonHrUnit * selected.Quantity;

                            if (hrUnitPrice != 0 && workingDay != 0)
                            {
                                selected.TermHumanResourcesUnitPrice = ((selected.HrSum / (selected.MonthlyWorkingDay * selected.Quantity)) * selected.TermWorkingDay);

                            }
                            else
                            {
                                selected.TermHumanResourcesUnitPrice = 0;
                            }
                            selected.TermInvoiceAmount = selected.TermHumanResourcesUnitPrice + selected.NonHumanResourcesUnitPrice;
                        }
                        int s = Db.SaveChanges();
                        selectedInvoiceRow.ShortfallAmount = Db.MissingDay.Where(t => t.IsActive == true && t.IsDeleted == false && t.InvoiceRowID == selectedRow.InvoiceRowID).Sum(t => t.TermInvoiceAmount);
                        selectedInvoiceRow.MissingDayDescription = paramaters.Description;
                        Db.SaveChanges();
                        //Guncelleme yapıldı.

                        var invo = Db.InvoiceRow.Where(t => t.ID == selectedRow.InvoiceRowID).FirstOrDefault();

                        model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == true && t.InvoiceRowID == selectedRow.InvoiceRowID).ToList();
                        model.IsActive = true; //Bu Kaydedilmiş bir kayıt.
                        model.RowID = selectedRow.ID;
                        model.TotalTermInvoiceAmount = selectedInvoiceRow.ShortfallAmount;
                        model.Description = invo.MissingDayDescription;

                        OypaHelper.AddApplicationLog("Management", "UpdateMissingDay", "Invoice", selectedRow.InvoiceRowID.Value, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {offer.OfferNumber} nolu teklifin {invo.PaymentDate.Value.ToShortDateString()} tarihli faturasında yapılan eksik gün hesaplama işlemini güncelledi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    }
                }
                else
                {
                    var selectedRow = Db.MissingDay.FirstOrDefault(t => t.ID == paramaters.RowID);
                    var selectedInvoiceRow = Db.InvoiceRow.FirstOrDefault(t => t.ID == selectedRow.InvoiceRowID);
                    selectedInvoiceRow.MissingDayDescription = paramaters.Description;
                    Db.SaveChanges();
                    var invo = Db.InvoiceRow.Where(t => t.ID == selectedRow.InvoiceRowID).FirstOrDefault();

                    model.MdList = Db.MissingDay.Where(t => t.IsDeleted == false && t.IsActive == true && t.InvoiceRowID == selectedRow.InvoiceRowID).ToList();
                    model.IsActive = true; //Bu Kaydedilmiş bir kayıt.
                    model.RowID = selectedRow.ID;
                    model.TotalTermInvoiceAmount = selectedInvoiceRow.ShortfallAmount;
                    model.Description = invo.MissingDayDescription;
                }
            }
            return PartialView("~/Views/Invoice/_PartialUpdateMissingDay.cshtml", model);
        }
        public PartialViewResult DeleteMissingDay(int RowID)
        {
            InvoiceControlModel model = new InvoiceControlModel();
            var selectedInvoiceRow = Db.MissingDay.FirstOrDefault(t => t.ID == RowID);
            if (selectedInvoiceRow != null)
            {
                var findInvoice = Db.InvoiceRow.FirstOrDefault(t => t.ID == selectedInvoiceRow.InvoiceRowID);
                var DeletedMissingDay = Db.MissingDay.Where(t => t.InvoiceRowID == selectedInvoiceRow.InvoiceRowID && t.IsActive == true && t.IsDeleted == false).ToList(); // Aktif olan ve daha önceden silinmemiş olan aktif eksik gün kayıtlarını yakala.
                if (DeletedMissingDay != null && DeletedMissingDay.Count() > 0)
                {
                    foreach (var item in DeletedMissingDay)
                    {
                        item.IsActive = false; //ARtık aktif değil
                        item.IsDeleted = true; //Silindi.
                        item.UpdateDate = DateTime.Now;
                        item.UpdateUserID = OypaHelper.RecordEmployee();
                        Db.SaveChanges();
                    }
                }
                //Eksik gün hesaplaması silindi. faturadaki eksik güne dair kayıtları geri çektim.
                findInvoice.IsMissingDay = false;
                findInvoice.ShortfallAmount = 0;
                Db.SaveChanges();
                model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID).ToList();
                model.MonthlyInvoice = findInvoice.MonthlyInvoice.Value;
                int invoiceCount = model.InvoiceRows.Count;
                var paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
                var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Count();
                model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;
                model.OfferID = findInvoice.OfferID.Value;
                if (paidedInvoicesPrice != null)
                {
                    model.Remaining = (findInvoice.TotalPrice) - paidedInvoicesPrice;
                }
                else
                {
                    model.Remaining = findInvoice.TotalPrice;
                }

                OypaHelper.AddApplicationLog("Management", "DeleteMissingDay", "Invoice", findInvoice.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {findInvoice.Offer.OfferNumber} nolu teklifin {findInvoice.PaymentDate.Value.ToShortDateString()} tarihli faturasında yapılan eksik gün hesaplama işlemini sildi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }
            return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult SaveMissingDay( int RowID, string Description)
        {
            InvoiceControlModel model = new InvoiceControlModel();
            var selectedrow = Db.MissingDay.FirstOrDefault(t => t.ID == RowID);

            if (selectedrow != null)
            {
                var findInvoice = Db.InvoiceRow.FirstOrDefault(t => t.ID == selectedrow.InvoiceRowID);
                findInvoice.MissingDayDescription = Description;
                var datas = Db.MissingDay.Where(t => t.InvoiceRowID == selectedrow.InvoiceRowID && t.IsActive == false && t.IsDeleted == false).ToList(); //Silinmemiş ve henüz aktif edilmemiş kayıtları yakala ve aktif et.
                foreach (var item in datas)
                {
                    item.IsActive = true;
                    item.RecordDate = DateTime.Now;
                    item.RecordUserID = OypaHelper.RecordEmployee();
                    Db.SaveChanges();
                }
                findInvoice.IsMissingDay = true;
                findInvoice.ShortfallAmount = Db.MissingDay.Where(t => t.IsActive == true && t.IsDeleted == false && t.InvoiceRowID == findInvoice.ID).Sum(t => t.TermInvoiceAmount);
                Db.SaveChanges();
                model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID).ToList();
                model.MonthlyInvoice = findInvoice.MonthlyInvoice.Value;
                int invoiceCount = model.InvoiceRows.Count;
                var paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
                var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Count();
                model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;
                if (paidedInvoicesPrice != null)
                {
                    model.Remaining = (findInvoice.TotalPrice) - paidedInvoicesPrice;
                }
                else
                {
                    model.Remaining = findInvoice.TotalPrice;
                }

                model.TotalPaidedInvoiceTotal = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(i => i.PaidedPrice);
                model.OfferID = findInvoice.OfferID.Value;

                OypaHelper.AddApplicationLog("Management", "SaveMissingDay", "Invoice", findInvoice.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {findInvoice.Offer.OfferNumber} nolu teklifin {findInvoice.PaymentDate.Value.ToShortDateString()} tarihli faturasında eksik gün hesaplama işlemi gerçekleştirdi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            }
            return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);


        }

        public PartialViewResult RefreshInvoiceList(int? RowID)
        {
            InvoiceControlModel model = new InvoiceControlModel();
            var selectedrow = Db.MissingDay.FirstOrDefault(t => t.ID == RowID);

            if (selectedrow != null)
            {
                var findInvoice = Db.InvoiceRow.FirstOrDefault(t => t.ID == selectedrow.InvoiceRowID);
                model.InvoiceRows = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID).ToList();
                model.MonthlyInvoice = findInvoice.MonthlyInvoice.Value;
                int invoiceCount = model.InvoiceRows.Count;
                var paidedInvoicesPrice = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Sum(t => t.PaidedPrice);
                var paidedInvoicesCount = Db.InvoiceRow.Where(i => i.OfferID == findInvoice.OfferID && i.IsPaid == true).Count();
                model.RemainInvoiceCount = invoiceCount - paidedInvoicesCount;
                model.OfferID = findInvoice.OfferID.Value;
                if (paidedInvoicesPrice != null)
                {
                    model.Remaining = (findInvoice.TotalPrice) - paidedInvoicesPrice;
                }
                else
                {
                    model.Remaining = findInvoice.TotalPrice;
                }
            }

            return PartialView("~/Views/Invoice/_PartialInvoiceList.cshtml", model);
        }

        public double CalculateHRUnitPrice(Offer offer, int? positionID)
        {
            double? sumHRUnitPrice = 0;
            var offerPositions = Db.OfferPositions.Where(t => t.OfferID == offer.ID && t.PositionID == positionID).ToList();
            TimeSpan res = offer.DateEnd.Value - offer.DateBegin.Value;
            var totalDay = res.TotalDays;
            var month = Convert.ToInt32((totalDay / 30));

            //var month = (offer.DateEnd.Value.Month - offer.DateBegin.Value.Month) + (12 * (offer.DateEnd.Value.Year - offer.DateBegin.Value.Year));
            if (month == 0)
            {
                month = 1;
            }

            if (offerPositions != null && offerPositions.Count() > 0)
            {
                foreach (var item in offerPositions)
                {

                    if (item.PositionItemID != 12 && item.PositionItemID != 13 && item.PositionItemID != 14 && item.PositionItemID != 15 && item.PositionItemID != 16)
                    {
                        sumHRUnitPrice += item.UnitPrice.Value;
                    }
                }
            }

            return (sumHRUnitPrice.Value / month);
        }

        public double CalculateHrSum(Offer offer, IQueryable<int?> PositionsID)
        {
            //her pozisyon için hesaplanan ik toplam tutarların toplamı.
            double hrSum = 0;
            foreach (var item in PositionsID)
            {
                var quantity = Db.OfferPositions.FirstOrDefault(t => t.OfferID == offer.ID && t.PositionID == item && t.PositionItemID == 1).Quantity;
                var res = CalculateHRUnitPrice(offer, item);
                hrSum += res * quantity.Value; //İlgili Position'ın İk Toplamı
            }

            return hrSum; // Burada ise teklifte yer alan tüm positionların ik toplamı
        }

        public double CalculateNonHrUnitPrice(Offer offer, double hrSum) //İk Dışı Birim Fiyat
        {
            var OfferTotalNonKdv = offer.GenelToplam - offer.KdvToplam;
            TimeSpan res = offer.DateEnd.Value - offer.DateBegin.Value;
            var totalDay = res.TotalDays;
            var month = Convert.ToInt32((totalDay / 30));
 
            if (month == 0)
            {
                month = 1;
            }
            double result = OfferTotalNonKdv.Value / month;
            result = result - hrSum;
            int? totalQuantity = Db.OfferPositions.Where(t => t.OfferID == offer.ID && t.PositionItemID == 1).Sum(t => t.Quantity);
            return result / totalQuantity.Value; //İk dışı birim fiyat dönüldü. Modalın içerisinde position Quantity ile çarpılacak.
        }

        public string GetMonthName(DateTime date)
        {
            if (date.Month == 1)
            {
                return "Ocak";
            }
            else if (date.Month == 2)
            {
                return "Şubat";
            }
            else if (date.Month == 3)
            {
                return "Mart";
            }
            else if (date.Month == 4)
            {
                return "Nisan";
            }
            else if (date.Month == 5)
            {
                return "Mayıs";
            }
            else if (date.Month == 6)
            {
                return "Haziran";
            }
            else if (date.Month == 7)
            {
                return "Temmuz";
            }
            else if (date.Month == 8)
            {
                return "Ağustos";
            }
            else if (date.Month == 9)
            {
                return "Eylül";
            }
            else if (date.Month == 10)
            {
                return "Ekim";
            }
            else if (date.Month == 11)
            {
                return "Kasım";
            }
            else
            {
                return "Aralık";
            }
        }

        public ActionResult ExportToExcelInvoiceRow(int? offerID)
        {
            var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == offerID);
            if (selectedOffer != null)
            {
                if (selectedOffer.IsOneTime.Value)
                {
                    var gv = new GridView();
                    var AllOperatorList = Db.InvoiceRow.Where(t => t.OfferID == offerID).ToList();
                    var fat_tar = Db.InvoiceRow.FirstOrDefault(t => t.OfferID == offerID).PaymentDate.Value.ToShortDateString().ToString();
                    var firstList = Db.InvoiceRow.Where(t => t.OfferID == offerID).Select(x => new
                    {
                        x.PaymentDate,
                        x.PaidedPrice,
                        x.ShortfallAmount,
                        x.OvertTimeGeneralTotal,
                        x.IsPaid

                    });

                    var list = firstList.ToList().Select(t => new
                    {
                        FaturaTarihi = (t.PaymentDate != null ? t.PaymentDate.Value.ToShortDateString() : "-"),
                        FaturaKesilenTutar = (t.PaidedPrice != null ? (t.PaidedPrice.Value.ToString("N2") + " TL") : "0,00 TL"),
                        HizmetBedeli = (t.ShortfallAmount != null ? (t.ShortfallAmount.Value.ToString("N2") + " TL") : "0,00 TL"),
                        FazlaMesaiTutari = (t.OvertTimeGeneralTotal != null ? (t.OvertTimeGeneralTotal.Value.ToString("N2") + " TL") : "0,00 TL"),
                        FaturaDurumu = (t.IsPaid == true ? "Fatura Kesildi" : "Fatura Kesilmedi"),
                    }).ToList();

                    gv.DataSource = list;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader($"content-disposition", $"attachment; filename={DateTime.Now.ToShortDateString()}_{(selectedOffer.Customers.CustomerFullName != null ? selectedOffer.Customers.CustomerFullName : null)}_FaturaDetayı.xls");
                    Response.ContentType = "application/ms-excel";
                    Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
                    StringWriter objStringWriter = new StringWriter();
                    HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                    gv.RenderControl(objHtmlTextWriter);
                    Response.Output.Write(objStringWriter.ToString());
                    Response.Flush();
                    Response.End();

                    OypaHelper.AddApplicationLog("Management", "ExportToExcelInvoiceRow", "Invoice", selectedOffer.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer.OfferNumber} nolu teklifin fatura listesini excel formatında indirdi! ", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return View();
                }
                else
                {
                    var gv = new GridView();
                    var AllOperatorList = Db.InvoiceRow.Where(t => t.OfferID == offerID).ToList();
                    var fat_tar = Db.InvoiceRow.FirstOrDefault(t => t.OfferID == offerID).PaymentDate.Value.ToShortDateString().ToString();
                    var firstList = Db.InvoiceRow.Where(t => t.OfferID == offerID).Select(x => new
                    {
                        x.PaymentDate,
                        x.PaidedPrice,
                        x.IsPaid

                    });

                    var list = firstList.ToList().Select(t => new
                    {
                        FaturaTarihi = (t.PaymentDate != null ? t.PaymentDate.Value.ToShortDateString() : "-"),
                        FaturaKesilenTutar = (t.PaidedPrice != null ? (t.PaidedPrice.Value.ToString("N2") + " TL") : "0,00 TL"),
                        FaturaDurumu = (t.IsPaid == true ? "Fatura Kesildi" : "Fatura Kesilmedi"),
                    }).ToList();

                    gv.DataSource = list;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader($"content-disposition", $"attachment; filename={DateTime.Now.ToShortDateString()}_{(selectedOffer.Customers.CustomerFullName != null ? selectedOffer.Customers.CustomerFullName : null)}_FaturaDetayı.xls");
                    Response.ContentType = "application/ms-excel";
                    Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
                    StringWriter objStringWriter = new StringWriter();
                    HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                    gv.RenderControl(objHtmlTextWriter);
                    Response.Output.Write(objStringWriter.ToString());
                    Response.Flush();
                    Response.End();

                    OypaHelper.AddApplicationLog("Management", "ExportToExcelInvoiceRow", "Invoice", selectedOffer.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer.OfferNumber} nolu teklifin fatura listesini excel formatında indirdi! ", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    return View();
                }
            }
            else
            {
                ViewBag.Message = "Fatura detayı Excel'e aktarılamadı.";
                return View();
            }
        }





    }
}
