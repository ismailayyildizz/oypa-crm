﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class ConstantParamaterController : BaseController
    {
        // GET: ConstantParamater
        //Tab tab  sabit paramatre listeleri
        public ActionResult Index()
        {
            var model = new ConstantParameterListModel();

            var CompensationList = Db.ConstantParamaterDetails.Include(x => x.ConstantParameter).Where(x => x.ConstantParameter.Name.Contains("Kıdem Tazminatı") && x.IsActive == true).ToList();

            foreach (var item in CompensationList)
            {
                var md = new Compensation();
                md.Id = item.ID;
                md.ParameterName = item.ConstantParameter.Name;
                md.ParameterValue = item.ParamaterValue.ToString();
                md.PositionStatus = item.ConstantParameter.PositionStatus; //PositionStatus=0 ; Çalışan-emekli ayrımı yok.
                md.UsedDate = item.UsedDate;
                model.CompensationList.Add(md);
            }

            var GrossRateateList = Db.ConstantParamaterDetails.Where(x => x.ConstantParameter.Name.Contains("İK Brüt Oranı") && x.IsActive == true).ToList();

            foreach (var item in GrossRateateList)
            {
                var md = new GrossRateate();
                md.Id = item.ID;
                md.ParameterName = item.ConstantParameter.Name;
                md.ParameterValue = item.ParamaterValue.ToString();
                md.PositionStatus = item.ConstantParameter.PositionStatus; //PositionStatus=1 - Çalışan; PositionStatus=2- Emekli
                md.UsedDate = item.UsedDate;
                model.GrossRateateList.Add(md);
            }

            OypaHelper.AddApplicationLog("Management", "Index", "ConstantParamater", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, sabit değerler listeleme sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        //Sabit degerler güncelleme get action
        public ActionResult EditConstantParameter(int? id)
        {
            if (id != 0)
            {
                var constantDetail = Db.ConstantParamaterDetails.Include(x => x.ConstantParameter).Where(x => x.ID == id && x.IsActive == true).FirstOrDefault();

                if (constantDetail != null)
                {
                    var model = new ConstantParameterModel();
                    model.Id = constantDetail.ID;
                    model.ConstantParemeterId = constantDetail.ConstantParameter.Id;
                    model.ParameterName = constantDetail.ConstantParameter.Name;
                    model.ParameterValue = constantDetail.ParamaterValue.ToString().Replace(",", ".");
                    model.PositionStatus = constantDetail.ConstantParameter.PositionStatus;

                    if (constantDetail.UsedDate != null)
                    {
                        model.UsedDate = constantDetail.UsedDate.Value;
                        model.FormatDate = model.UsedDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }

                    OypaHelper.AddApplicationLog("Management", "EditConstantParameter", "ConstantParamater", id != null ? id.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {id} Id'li sabit değerin detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return View(model);
                }
            }

            OypaHelper.AddApplicationLog("Management", "EditConstantParameter", "ConstantParamater", id != null ? id.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {id} Id'li sabit değerin detay sayfasını görüntülemek istedi, bu Id'ye ait kayıt bulunamadı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }

        //Sabit degerler güncelleme post action
        [HttpPost]
        public ActionResult EditConstantParameter(ConstantParameterModel model)
        {
            if (model != null)
            {
                var compensation = Db.ConstantParamaterDetails.Where(x => x.ID == model.Id && x.IsActive == true).FirstOrDefault();

                if (compensation != null)
                {
                    //Tarih değişmişse yeni bir dönem için oran giriliyo
                    if (compensation.UsedDate != model.UsedDate)
                    {
                        //Eski dönemdeki kayıt siliniyo
                        compensation.IsActive = false;
                        compensation.UpdateDate = DateTime.Now;
                        compensation.UpdateUserId = OypaHelper.RecordEmployee();

                        //yerine yeni dönem tarih ve degeri giriliyo
                        var newCompensation = new ConstantParamaterDetails();

                        newCompensation.ParamaterValue = !string.IsNullOrEmpty(model.ParameterValue) ? Convert.ToDouble(model.ParameterValue.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                        newCompensation.ConstantParameterId = model.ConstantParemeterId;
                        newCompensation.RecordDate = DateTime.Now;
                        newCompensation.IsActive = true;
                        newCompensation.RecordUserId = OypaHelper.RecordEmployee();
                        newCompensation.UsedDate = model.UsedDate;

                        Db.ConstantParamaterDetails.Add(newCompensation);
                        Db.SaveChanges();

                        OypaHelper.AddApplicationLog("Management", "EditConstantParameter", "ConstantParamater", newCompensation.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {model.Id} Id'li sabit değeri silip, yeni dönem için {newCompensation.ID} Id'li sabit değeri ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    }
                    //YTarih değişmemişse o dönem için sadece oran değişiyo
                    else
                    {
                        compensation.ParamaterValue = !string.IsNullOrEmpty(model.ParameterValue) ? Convert.ToDouble(model.ParameterValue.Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                        compensation.UpdateDate = DateTime.Now;
                        compensation.UpdateUserId = OypaHelper.RecordEmployee();

                        OypaHelper.AddApplicationLog("Management", "EditConstantParameter", "ConstantParamater", model.Id, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.Id} Id'li sabit değerin güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    }

                    Db.SaveChanges();                   
                }
            }
            return RedirectToAction("Index", "ConstantParamater");
        }

        public ActionResult ArchiveList()
        {
            var model = new ConstantParameterListModel();

            var CompensationList = Db.ConstantParamaterDetails.Where(x => x.ConstantParameter.Name.Contains("Kıdem Tazminatı")).OrderByDescending(x => x.UsedDate).ToList();

            foreach (var item in CompensationList)
            {
                var md = new Compensation();
                md.Id = item.ID;
                md.ParameterName = item.ConstantParameter.Name;
                md.ParameterValue = item.ParamaterValue.ToString();
                md.PositionStatus = item.ConstantParameter.PositionStatus; //PositionStatus=0 ; Çalışan-emekli ayrımı yok.
                md.UsedDate = item.UsedDate;
                md.IsActive = item.IsActive;
                model.CompensationList.Add(md);
            }

            var GrossRateateList = Db.ConstantParamaterDetails.Where(x => x.ConstantParameter.Name.Contains("İK Brüt Oranı")).OrderByDescending(x => x.UsedDate).ToList();

            foreach (var item in GrossRateateList)
            {
                var md = new GrossRateate();
                md.Id = item.ID;
                md.ParameterName = item.ConstantParameter.Name;
                md.ParameterValue = item.ParamaterValue.ToString();
                md.PositionStatus = item.ConstantParameter.PositionStatus; //PositionStatus=1 - Çalışan; PositionStatus=2- Emekli
                md.UsedDate = item.UsedDate;
                md.IsActive = item.IsActive;
                model.GrossRateateList.Add(md);
            }

            OypaHelper.AddApplicationLog("Management", "ArchiveList", "ConstantParamater", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Sabit Değerler Arşivi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }
    }
}
