﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class RegionController : BaseController
    {
        // GET: Region
        public ActionResult Index()
        {
            var model = new List<RegionModel>();

            var list = Db.Region.Where(x => x.IsActive == true).ToList();
            if (list != null)
            {
                foreach (var item in list)
                {
                    var md = new RegionModel();
                    md.Id = item.ID;
                    md.Name = item.Name;
                    md.GroupMail = item.GroupMail;
                    model.Add(md);
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteRegion(int? id)
        {
            if (id != 0)
            {
                var region = Db.Region.Where(x => x.ID == id && x.IsActive == true).FirstOrDefault();
                if (region != null)
                {
                    region.IsActive = false;
                    var isSaved = Db.SaveChanges();
                    if (isSaved > 0)
                    {
                        OypaHelper.AddApplicationLog("Management", "DeleteRegion", "Region", id != null ? id.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {id} Id'li bölgeyi sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRegion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRegion(RegionModel model)
        {
            if (model != null)
            {
                var region = new Region();
                region.Name = model.Name;
                region.GroupMail = model.GroupMail;
                region.IsActive = true;

                Db.Region.Add(region);
                var isSaved = Db.SaveChanges();
                if (isSaved > 0)
                {
                    OypaHelper.AddApplicationLog("Management", "AddRegion", "Region", region.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, { region.Name} isimli yeni bir bölge ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return RedirectToAction("Index", "Region");
                }
            }

            return View();
        }

        public ActionResult EditRegion(int? id)
        {
            if (id != 0)
            {
                var region = Db.Region.Where(x => x.ID == id && x.IsActive == true).FirstOrDefault();

                if (region != null)
                {
                    var model = new RegionModel();
                    model.Id = region.ID;
                    model.Name = region.Name;
                    model.GroupMail = region.GroupMail;

                    return View(model);
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult EditRegion(RegionModel model)
        {
            if (model != null)
            {
                var region = Db.Region.Where(x => x.ID == model.Id && x.IsActive == true).FirstOrDefault();

                if (region != null)
                {
                    region.Name = model.Name;
                    region.GroupMail = model.GroupMail;
                    Db.SaveChanges();

                    OypaHelper.AddApplicationLog("Management", "EditRegion", "Region", region.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {region.ID} Id'li bölgeyi güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return RedirectToAction("Index", "Region");
                }
            }
            return View();
        }

    }
}