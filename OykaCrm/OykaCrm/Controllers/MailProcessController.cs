﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class MailProcessController : BaseController
    {
        // GET: MailProcess
        public ActionResult Index()
        {
            ViewBag.MailProcessList = Db.MailProcess.Where(t => t.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "MailProcess", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Mail Aksiyon Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }
        public ActionResult Add()
        {
            ViewBag.MailProcessTypeList = Db.MailProcessType.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();

            OypaHelper.AddApplicationLog("Management", "Add", "MailProcess", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Mail Aksiyon Ekleme sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }
        [HttpPost]
        public ActionResult Add(MailProcess model)
        {
            if (model != null)
            {
                ViewBag.MailProcessTypeList = Db.MailProcessType.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();
                var checkMail = Db.MailProcess.FirstOrDefault(t => t.Email == model.Email && t.MailProcessTypeId == model.MailProcessTypeId && t.IsActive == true);
                if (checkMail == null)
                {
                    model.IsActive = true;
                    model.RecordDate = DateTime.Now;
                    model.RecordUserId = OypaHelper.RecordEmployee();
                    Db.MailProcess.Add(model);
                    Db.SaveChanges();

                    OypaHelper.AddApplicationLog("Management", "Add", "MailProcess", model.Id, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.Id} Id'li yeni mail aksiyonu ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.EMessage = "Birimde kayıtlı olan bir mail adresi girildi";

                    OypaHelper.AddApplicationLog("Management", "Add", "MailProcess", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, yeni mail aksiyonu eklemek isterken sistemde var olan bir mail adresi girdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                }
            }
            else
            {
                ViewBag.EMessage = "Kaydetme işlemi gerçekleşmedi, Lütfen tekrar deneyiniz";

                OypaHelper.AddApplicationLog("Management", "Add", "MailProcess", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, yeni mail aksiyonu eklemek isterken modelin null gelmesi sonucu hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }

            return View();
        }

        public ActionResult Detail(int? mailProcessId, string IsExistMail)
        {
            if (IsExistMail != null)
            {
                ViewBag.EMessage2 = IsExistMail;
                var Id = Db.MailProcess.FirstOrDefault(t => t.IsActive == true && t.Id == mailProcessId);
                if (Id != null)
                {
                    ViewBag.Item = Id;
                    ViewBag.MailProcessTypeList = Db.MailProcessType.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();
                }

                OypaHelper.AddApplicationLog("Management", "Detail", "MailProcess", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {mailProcessId} Id'li mail aksiyonu detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View();
            }

            var checMailProcessId = Db.MailProcess.FirstOrDefault(t => t.IsActive == true && t.Id == mailProcessId);
            if (checMailProcessId != null)
            {
                ViewBag.Item = checMailProcessId;
                ViewBag.MailProcessTypeList = Db.MailProcessType.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();

                OypaHelper.AddApplicationLog("Management", "Detail", "MailProcess", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {mailProcessId} Id'li mail aksiyonu detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View();
            }
            else
            {
                ViewBag.EMessage = "Detaya Ulaşılamadı.";

                OypaHelper.AddApplicationLog("Management", "Detail", "MailProcess", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {mailProcessId} Id'li mail aksiyonu detayını görüntülemek isterken hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View();
            }
        }
        [HttpPost]
        public ActionResult Update(MailProcess model)
        {
            if (model != null)
            {
                var getMailProcess = Db.MailProcess.FirstOrDefault(t => t.IsActive == true && t.Id == model.Id);
                var checkEmail = Db.MailProcess.FirstOrDefault(t => t.IsActive == true && t.MailProcessTypeId == model.MailProcessTypeId && t.Email == model.Email);
                if (checkEmail == null)
                {
                    if (getMailProcess != null)
                    {
                        getMailProcess.DepartmentName = model.DepartmentName;
                        getMailProcess.Email = model.Email;
                        getMailProcess.FullName = model.FullName;
                        getMailProcess.MailProcessTypeId = model.MailProcessTypeId;
                        getMailProcess.UpdateDate = DateTime.Now;
                        getMailProcess.UpdateUserId = OypaHelper.RecordEmployee();

                        OypaHelper.AddApplicationLog("Management", "Update", "MailProcess", model.Id, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.Id} Id'li mail aksiyonunu güncelledi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        Db.SaveChanges();
                    }
                }
                else
                {
                    if (checkEmail.Id == model.Id)
                    {
                        getMailProcess.DepartmentName = model.DepartmentName;
                        getMailProcess.FullName = model.FullName;
                        getMailProcess.MailProcessTypeId = model.MailProcessTypeId;
                        getMailProcess.UpdateDate = DateTime.Now;
                        getMailProcess.UpdateUserId = OypaHelper.RecordEmployee();

                        OypaHelper.AddApplicationLog("Management", "Update", "MailProcess", model.Id, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.Id} Id'li mail aksiyonunu güncelledi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        Db.SaveChanges();
                    }
                    else
                    {
                        ViewBag.Message = "Süreç içerisinden aynı mail adresi tekrar kullanılamaz!";

                        OypaHelper.AddApplicationLog("Management", "Update", "MailProcess", model.Id, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.Id} Id'li mail aksiyonunu güncellemek isterken {ViewBag.Message} hatası ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return RedirectToAction("Detail", "MailProcess", new { mailProcessId = getMailProcess.Id, IsExistMail = "Süreç içerisinden aynı mail adresi tekrar kullanılamaz!" });
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public PartialViewResult Delete(int mailProcessId)
        {
            var checMailProcessId = Db.MailProcess.FirstOrDefault(t => t.IsActive == true && t.Id == mailProcessId);
            if (checMailProcessId != null)
            {
                checMailProcessId.IsActive = false;
                Db.SaveChanges();
                ViewBag.MailProcessList = Db.MailProcess.Where(t => t.IsActive == true).ToList();

                OypaHelper.AddApplicationLog("Management", "Delete", "MailProcess", checMailProcessId.Id, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {checMailProcessId.Id} Id'li mail aksiyonunu sildi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }

            return PartialView("~/Views/MailProcess/_PartialMailProcessList.cshtml", ViewBag.MailProcessList);
        }
    }
}