﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class DocumentCategoryController : BaseController
    {
        //Ayarlar -> DokümanKategorileri
        public ActionResult Index()
        {
            var list = Db.DocumentCategory.Where(i => i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "DocumentCategory", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Doküman Kategorileri Listeleme sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(list);
        }

        public PartialViewResult AddCategory(string CategoryName)
        {
            DocumentCategory dc = new DocumentCategory();

            dc.CategoryName = CategoryName;
            dc.IsActive = true;
            Db.DocumentCategory.Add(dc);
            Db.SaveChanges();

            var list = Db.DocumentCategory.Where(i => i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "AddCategory", "DocumentCategory", dc.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {CategoryName} adlı doküman kategorisi ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/DocumentCategory/_PartialDocumentCategories.cshtml", list);
        }

        public PartialViewResult RemoveCategory(string CategoryID)
        {
            int catID = Convert.ToInt32(CategoryID);
            var deletedCategory = Db.DocumentCategory.FirstOrDefault(i => i.ID == catID);

            if (deletedCategory != null)
            {
                deletedCategory.IsActive = false;
                Db.SaveChanges();
            }

            var list = Db.DocumentCategory.Where(i => i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "RemoveCategory", "DocumentCategory", catID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {catID} Id'li doküman kategorisini sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/DocumentCategory/_PartialDocumentCategories.cshtml", list);
        }


    }
}