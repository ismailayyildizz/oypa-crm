﻿
using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace OykaCrm.Controllers
{
    public class AuditController : BaseController
    {
        // GET: Audit=(Denetim) Ayarlar->Yetkilendirme
        public ActionResult Index()
        {
            IEnumerable<RoleGroup> rolegrouplist = Db.RoleGroup.Where(x => x.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Audit", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, rol grup listeleme sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(rolegrouplist);
        }

        public ActionResult New(AuditAddViewModel model)
        {
            IEnumerable<RoleLevel> levellist = Db.RoleLevel.ToList();

            OypaHelper.AddApplicationLog("Management", "New", "Audit", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, yeni rol grup ekleme sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(levellist);
        }

        [HttpPost]
        public ActionResult Add(AuditAddViewModel model)
        {
            RoleGroup rolegroup = new RoleGroup();
            rolegroup.GroupName = model.rolgroupname;
            rolegroup.IsActive = true;
            rolegroup.RoleLevel = model.RoleLevelNumber;
            Db.RoleGroup.Add(rolegroup);
            Db.SaveChanges();

            OypaHelper.AddApplicationLog("Management", "Add", "Audit", rolegroup.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {rolegroup.GroupName} adlı rol grubunu ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return RedirectToAction("Index");
        }

        public ActionResult Detail(int? ID)
        {
            AuditDetailViewModel model = new AuditDetailViewModel();
            model.RoleGroup = Db.RoleGroup.Where(i => i.ID == ID).FirstOrDefault();
            model.rolelevelname = model.RoleGroup.RoleLevel1.Name;
            model.RoleLevel = Db.RoleLevel.ToList();
            model.PermList = Db.RoleGroupPermissions.Where(i => i.RoleGroupID == ID).ToList();
            model.PermissionList = Db.Permission.Where(i => i.Action != null).ToList();

            OypaHelper.AddApplicationLog("Management", "Detail", "Audit", ID != null ? ID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Rol Grup belirleme detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        [HttpPost]
        public ActionResult AddGroupPermission(RoleGroupPermissions roleGroupp)
        {
            AuditAddViewModel model = new AuditAddViewModel();

            int? rolegrupid = 0;

            if (roleGroupp != null)
            {
                var singleRow = Db.Permission.FirstOrDefault(t => t.ID == roleGroupp.PermissionID);
                rolegrupid = roleGroupp.RoleGroupID;
                if (singleRow.Action == "CustomerFull")//Firma Tüm Yetkiler
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Customer" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "OfferFull") //Teklif Full Yetki
                {
                    var AllOfferPermission = Db.Permission.Where(t => t.Controller == "Offer" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllOfferPermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }                        
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }                    
                }
                else if(singleRow.Action=="ProjectFull")
                {
                    var AllProjectPermission = Db.Permission.Where(t => t.Controller == "Project" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllProjectPermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID =isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if(singleRow.Action== "ContractFull")
                {
                    var AllContractPermission = Db.Permission.Where(t => t.Controller == "Contract" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllContractPermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };


                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action== "InvoiceFull")
                {
                    var AllCustomerPermission = Db.Permission.Where(t => t.Controller == "Invoice" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllCustomerPermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };


                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }


                    }
                }
                else if (singleRow.Action== "ProductFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Product" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "DocumentFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Document" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "ReportFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Report" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "AuditFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Audit" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "OpportunityFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Opportunity" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action=="EmployeeFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Employee" && t.Description.Contains("Kullanıcı")&& t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();


                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "PersonalFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Employee" && t.Description.Contains("Personel")&& t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "RoleFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Role" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "RegionFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "Region" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "MailProcessFull")
                {
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "MailProcess" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "DocumentCategoryFull")
                {                   
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "DocumentCategory" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else if (singleRow.Action == "ConstantParamaterFull")
                {                    
                    var AllInvoicePermission = Db.Permission.Where(t => t.Controller == "ConstantParamater" && t.ID != roleGroupp.PermissionID).ToList();
                    foreach (var item in AllInvoicePermission)
                    {
                        var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                        var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                        var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                        var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                        var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                        var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == item.ID); //Bu yetki bu rolde var mı?
                        if (isexists == null)
                        {
                            var _groupp = new RoleGroupPermissions();
                            _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                            _groupp.PermissionID = item.ID;
                            _groupp.IsNavigate = isnavigate;
                            _groupp.IsSelect = isselect;
                            _groupp.IsInsert = isinsert;
                            _groupp.IsUpdate = isupdate;
                            _groupp.IsDelete = isdelete;

                            Db.RoleGroupPermissions.Add(_groupp);
                            Db.SaveChanges();

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == item.ID);
                            string permname = _permname.Controller + " - " + _permname.Action;
                            //rolegrupid = _groupp.ID;
                            OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                        }
                        else
                        {
                            RoleGroupPermissions self = new RoleGroupPermissions()
                            {
                                ID = isexists.ID,
                                PermissionID = isexists.PermissionID,
                                IsDelete = isexists.IsDelete,
                                IsInsert = isexists.IsInsert,
                                RoleGroupID = isexists.RoleGroupID,
                                IsNavigate = isexists.IsNavigate,
                                IsSelect = isexists.IsSelect,
                                IsUpdate = isexists.IsUpdate
                            };

                            isexists.RoleGroupID = roleGroupp.RoleGroupID;
                            isexists.PermissionID = isexists.PermissionID;
                            isexists.IsNavigate = isnavigate;
                            isexists.IsSelect = isselect;
                            isexists.IsInsert = isinsert;
                            isexists.IsUpdate = isupdate;
                            isexists.IsDelete = isdelete;

                            Db.SaveChanges();

                            rolegrupid = isexists.RoleGroupID;

                            string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                            var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                            string permname = _permname.Controller + " - " + _permname.Action;

                            var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                            OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                        }
                    }
                }
                else
                {

                    var isnavigate = Request.Form["PermNavigate"] == "1" ? true : false;
                    var isselect = Request.Form["PermSelect"] == "1" ? true : false;
                    var isinsert = Request.Form["PermInsert"] == "1" ? true : false;
                    var isupdate = Request.Form["PermUpdate"] == "1" ? true : false;
                    var isdelete = Request.Form["PermDelete"] == "1" ? true : false;

                    var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == roleGroupp.RoleGroupID && x.PermissionID == roleGroupp.PermissionID);

                    if (isexists == null)
                    {

                        var _groupp = new RoleGroupPermissions();
                        _groupp.RoleGroupID = roleGroupp.RoleGroupID;
                        _groupp.PermissionID = roleGroupp.PermissionID;
                        _groupp.IsNavigate = isnavigate;
                        _groupp.IsSelect = isselect;
                        _groupp.IsInsert = isinsert;
                        _groupp.IsUpdate = isupdate;
                        _groupp.IsDelete = isdelete;

                        Db.RoleGroupPermissions.Add(_groupp);
                        Db.SaveChanges();

                        rolegrupid = _groupp.RoleGroupID;

                        string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                        var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                        string permname = _permname.Controller + " - " + _permname.Action;

                        OypaHelper.AddApplicationLog("Management", "Insert", "RoleGroupPermissions", _groupp.ID, "RoleGroupPermissions", true, $"{_groupp.ID} ID li, {grupname} Grup Adına {permname} Adlı Yetki Grubu eklendi.", model.CurrentUser.ID, "", null);
                    }
                    else
                    {
                        RoleGroupPermissions self = new RoleGroupPermissions()
                        {
                            ID = isexists.ID,
                            PermissionID = isexists.PermissionID,
                            IsDelete = isexists.IsDelete,
                            IsInsert = isexists.IsInsert,
                            RoleGroupID = isexists.RoleGroupID,
                            IsNavigate = isexists.IsNavigate,
                            IsSelect = isexists.IsSelect,
                            IsUpdate = isexists.IsUpdate
                        };


                        isexists.RoleGroupID = roleGroupp.RoleGroupID;
                        isexists.PermissionID = roleGroupp.PermissionID;
                        isexists.IsNavigate = isnavigate;
                        isexists.IsSelect = isselect;
                        isexists.IsInsert = isinsert;
                        isexists.IsUpdate = isupdate;
                        isexists.IsDelete = isdelete;

                        Db.SaveChanges();

                        rolegrupid = isexists.RoleGroupID;

                        string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == roleGroupp.RoleGroupID)?.GroupName;
                        var _permname = Db.Permission.FirstOrDefault(x => x.ID == roleGroupp.PermissionID);
                        string permname = _permname.Controller + " - " + _permname.Action;

                        var isequal = OypaHelper.PublicInstancePropertiesEqual<RoleGroupPermissions>(self, isexists, OypaHelper.getIgnorelist());
                        OypaHelper.AddApplicationLog("Management", "Update", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions", true, $"{isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki güncellendi.", model.CurrentUser.ID, "", isequal);
                    }
                }
            }

            return RedirectToAction("Detail", "Audit", new { id = rolegrupid });
        }

        public ActionResult RemoveGrupPermission(int? id, int? GrupID)
        {
            AuditAddViewModel model = new AuditAddViewModel();

            if (id > 0 && GrupID > 0)
            {
                var isexists = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == GrupID && x.ID == id);

                if (isexists != null)
                {
                    try
                    {
                        string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == isexists.RoleGroupID)?.GroupName;
                        var _permname = Db.Permission.FirstOrDefault(x => x.ID == isexists.PermissionID);
                        string permname = _permname.Controller + " - " + _permname.Action;

                        OypaHelper.AddApplicationLog("Management", "Delete", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions Delete", true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki satırını sildi.", model.CurrentUser.ID, "", null);

                        Db.RoleGroupPermissions.Remove(isexists);
                        Db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        string grupname = Db.RoleGroup.FirstOrDefault(x => x.ID == isexists.RoleGroupID)?.GroupName;
                        var _permname = Db.Permission.FirstOrDefault(x => x.ID == isexists.PermissionID);
                        string permname = _permname.Controller + " - " + _permname.Action;

                        OypaHelper.AddApplicationLog("Management", "Delete", "RoleGroupPermissions", isexists.ID, "RoleGroupPermissions Delete", false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {isexists.ID} ID li, {grupname} Grubu {permname} Adlı Yetki satırını silerken {ex.Message} hatası ile karşılaştı", model.CurrentUser.ID, OypaHelper.UserIpAdress(), null);
                    }
                }
            }

            return RedirectToAction("Detail", "Audit", new { id = GrupID });
        }
    }
}