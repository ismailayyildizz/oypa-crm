﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class BaseController:Controller
    {
        public OypaDbEntities Db { get; private set; }
        public BaseController(OypaDbEntities db)
        {
            Db = db;
        }

        public BaseController()
        {
            Db = new OypaDbEntities();

            
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["profile"] == null && filterContext.RouteData.Values["controller"].ToString() != "Login")
            {
                filterContext.Result = new RedirectResult("/Login/Index");
                return;
            }

            string controller = filterContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RouteData.Values["action"].ToString();
            Employee employee = Session["profile"] as Employee;

            var permlist = Db.RoleGroupPermissions.FirstOrDefault(x => x.Permission.Controller == controller && x.Permission.Action == action && x.RoleGroupID == employee.RolGroupID); // role grubu yetkisi ilişkili tablosunda bu action ve controller e ait erişim yetkisi varmı

            if (Session["profile"] != null)
            {
                if (permlist==null)
                {
                    filterContext.Result = new RedirectResult("/Error/Index");
                    return;
                }
                else if (permlist.IsNavigate==false)
                {
                    filterContext.Result = new RedirectResult("/Error/Index");
                    return;
                }
                
            }
            else
            {
                filterContext.Result = new RedirectResult("/Login/Index");
                return;

            }

            base.OnActionExecuting(filterContext);

        }
    }
}