﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OykaCrm.Controllers
{
    public class ContractController : BaseController
    {
        public ActionResult Index()
        {
            ContractControlModel model = new ContractControlModel();
            model.Contracts = Db.Offer.Where(i => i.StatusLevel == 10 && i.IsActive == true).OrderByDescending(t => t.ID).ToList();

            if (model.Contracts != null && model.Contracts.Count > 0)
            {
                foreach (var item in model.Contracts)
                {
                    var existTemplate = Db.OfferContractTemplate.Where(i => i.ContractID == item.ID).FirstOrDefault();
                    if (existTemplate != null)
                    {
                        model.ContractTemplateList.Add(existTemplate);
                    }
                }
            }

            OypaHelper.AddApplicationLog("Management", "Index", "Contract", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Sözleşme Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }


        public ActionResult Detail(int? contractID)
        {
            ContractDetailModel model = new ContractDetailModel();
            model.ContractTemplates = Db.ContractTemplate.Where(i => i.IsActive == true).ToList();

            if (Db.Offer.Any(i => i.ID == contractID))
            {
                model.singleoffer = Db.Offer.Find(contractID);

                if (model.singleoffer.IsOneTime == false)// tek seferlik teklif ise direk detail'a git.
                {
                    OypaHelper.AddApplicationLog("Management", "Detail", "Contract", contractID != null ? contractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {contractID} Id'li tek seferlik teklif için Sözleşme Detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return RedirectToAction("Detail", "Invoice", new { ContractID = contractID });
                }
                else
                {
                    var oct = Db.OfferContractTemplate.Where(i => i.ContractID == contractID && i.IsActive == true).FirstOrDefault();

                    if (oct != null)
                    {
                        model.SingleOfferContract = oct;

                        if (oct.IsBilled == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "Detail", "Contract", contractID != null ? contractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {contractID} Id'li teklifin faturalandırılmış sözleşmesi için Fatura Detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return RedirectToAction("Detail", "Invoice", new { ContractID = contractID });
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "Detail", "Contract", contractID != null ? contractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {contractID} Id'li teklif için Sözleşme Detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return RedirectToAction("ShowContract", new { OfferContractID = model.SingleOfferContract.ID });
                        }
                    }
                }
            }
            else
            {
                ViewBag.Error = "Böyle bir teklif bulunmamaktadır.";

                OypaHelper.AddApplicationLog("Management", "Detail", "Contract", contractID != null ? contractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {contractID} Id'li teklif için Sözleşme Detay sayfasını görüntülemek isterken {ViewBag.Error} mesajı ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View();
            }

            OypaHelper.AddApplicationLog("Management", "Detail", "Contract", contractID != null ? contractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {contractID} Id'li teklif için Sözleşme Detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        public ActionResult Content(int? contractID, int? templateID) //sozlesme icin yeni sablon olustur
        {
            ContractDetailModel model = new ContractDetailModel();
            int MonthCount = 0; double? monthlyInvoice = 0;
            model.singletemplate = Db.ContractTemplate.FirstOrDefault(i => i.ID == templateID);
            model.OfferID = contractID != null ? contractID.Value : 0;

            var checkoffer = Db.Offer.FirstOrDefault(t => t.ID == contractID && t.IsActive == true);
            if (checkoffer != null && model.singletemplate != null)
            {
                if (checkoffer.DateBegin != null && checkoffer.DateEnd != null)
                {
                    TimeSpan res = checkoffer.DateEnd.Value - checkoffer.DateBegin.Value;
                    var totalDay = res.TotalDays;
                    var diffCount = Convert.ToInt32((totalDay / 30));

                    MonthCount = diffCount;
                }

                if (checkoffer.GenelToplam != 0 && MonthCount != 0)
                {
                    monthlyInvoice = checkoffer.GenelToplam / MonthCount;
                }

                if (templateID == 2) // ek-protokol 6 Seçilirse
                {
                    var html = Db.ContractTemplate.FirstOrDefault(t => t.ID == templateID).TemplateContent;
                    var customer = checkoffer.Customers.CustomerFullName;
                    var beginDate = checkoffer.DateBegin.Value.ToShortDateString();
                    var endDate = checkoffer.DateEnd.Value.ToShortDateString();
                    var opportunityName = checkoffer.OfferNumber + " - " + checkoffer.OfferName;
                    var customerAddress = checkoffer.Customers.Address;
                    var montlyInvoice = monthlyInvoice;

                    model.singletemplate.TemplateContent = OypaHelper.ReplaceContractTemplate(html, opportunityName, customer, customerAddress, beginDate.ToString(), endDate.ToString(), monthlyInvoice.ToString());
                }
                else
                {
                    var html = Db.ContractTemplate.FirstOrDefault(t => t.ID == templateID).TemplateContent;
                    var customer = checkoffer.Customers.CustomerFullName;
                    var beginDate = checkoffer.DateBegin.Value.ToShortDateString();
                    var endDate = checkoffer.DateEnd.Value.ToShortDateString();
                    var opportunityName = checkoffer.OfferNumber + " - " + checkoffer.OfferName;
                    var customerAddress = checkoffer.Customers.Address;

                    model.singletemplate.TemplateContent = OypaHelper.ReplaceContractTemplate(html, opportunityName, customer, customerAddress, beginDate.ToString(), endDate.ToString());
                }
            }

            OypaHelper.AddApplicationLog("Management", "Content", "Contract", contractID != null ? contractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {contractID} Id'li teklif için Sözleşme oluşturma sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        public ActionResult ShowContract(int? OfferContractID) //sozlesme icin var olan sablonu goruntule
        {
            ContractDetailModel model = new ContractDetailModel();
            model.SingleOfferContract = Db.OfferContractTemplate.FirstOrDefault(i => i.ID == OfferContractID && i.IsActive == true);
            if (model.SingleOfferContract != null)
            {
                model.OfferID = model.SingleOfferContract.ContractID != null ? model.SingleOfferContract.ContractID.Value : 0;

                OypaHelper.AddApplicationLog("Management", "ShowContract", "Contract", model.OfferID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {model.OfferID} Id'li teklif için var olan {OfferContractID} Id'li sözleşmeyi görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }
            else
            {
                ViewBag.ErrorMessage = "Böyle bir sözleşme bulunmamaktadır.";

                OypaHelper.AddApplicationLog("Management", "ShowContract", "Contract", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {OfferContractID} Id'li sözleşme detayını görüntülemek isterken {ViewBag.ErrorMessage} mesajı ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateContent(string cdata, int? ContractID)
        {
            var checkOfferContract = Db.OfferContractTemplate.Where(t => t.ContractID == ContractID && t.IsActive == true).FirstOrDefault();
            var checkOffer = Db.Offer.FirstOrDefault(t => t.ID == ContractID && t.IsActive == true);
            if (checkOfferContract != null)
            {
                var self = new OfferContractTemplate();
                self.ContractContent = checkOfferContract.ContractContent;
                self.UpdateDate = checkOfferContract.UpdateDate;
                self.UpdateUserID = checkOfferContract.UpdateUserID;

                checkOfferContract.ContractContent = cdata;
                checkOfferContract.UpdateDate = DateTime.Now;
                checkOfferContract.UpdateUserID = OypaHelper.RecordEmployee();

                var isequal = OypaHelper.PublicInstancePropertiesEqual<OfferContractTemplate>(self, checkOfferContract, OypaHelper.getIgnorelist());
                OypaHelper.AddApplicationLog("Management", "UpdateContent", "Contract", ContractID != null ? ContractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {ContractID} Id'li teklif için {checkOfferContract.ID} Id'li sözleşmeyi güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), isequal);
            }
            else
            {
                OfferContractTemplate oct = new OfferContractTemplate()
                {
                    ContractID = ContractID,
                    ContractContent = cdata,
                    IsActive = true,
                    RecordDate = DateTime.Now,
                    RecordUserID = OypaHelper.RecordEmployee()
                };
                Db.OfferContractTemplate.Add(oct);

                OypaHelper.AddApplicationLog("Management", "UpdateContent", "Contract", ContractID != null ? ContractID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; {ContractID} Id'li teklif için {oct.ID} Id'li sözleşmeyi oluşturdu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }

            Db.SaveChanges();

            return RedirectToAction("Detail", "Contract", new { contractID = ContractID });
        }

        public ActionResult ExportToExcelContracts()
        {
            var gv = new GridView();
            var AllOperatorList = Db.Offer.Where(i => i.StatusLevel == 10 && i.IsActive == true).ToList();

            var list = (from e in Db.Offer
                        where e.IsActive == true && e.StatusLevel == 10
                        select new
                        {
                            Proje_Adı = e.Projects != null ? e.Projects.ProjectName : "-",
                            Müşteri = e.Customers.CustomerFullName,
                            Tarih = e.RecordDate,
                            Toplam_Tutar = e.GenelToplam != null ? e.GenelToplam.Value + " TL" : "-"

                        }).ToList();

            gv.DataSource = list;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=SözleşmeListesi.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            OypaHelper.AddApplicationLog("Management", "ExportToExcelContracts", "Contract", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; sözleşmeleri excel olarak indirdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }

    }
}