﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class ProductController : BaseController
    {
        public ActionResult Index()
        {
            var products = Db.Products.Where(x => x.IsActive == true).OrderByDescending(t => t.UpdateDate).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Product", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Ürün Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(products);
        }

        public ActionResult Create()
        {
            ProductModel model = new ProductModel();
            model.ProductCategories = Db.ProductCategory.Where(x => x.IsActive == true).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ProductModel model)
        {
            Products product = new Products();
            product = Mapper.MapSingle(model, product);
            product.RecordDate = DateTime.Now;
            product.IsActive = true;

            Db.Products.Add(product);
            //Db.AddLog("TYH-WEB APP", "Insert", "Product", 9999, "", "", "", "", true, "", OypaHelper.RecordEmployee(), "", DateTime.Now);
            int success = Db.SaveChanges();

            if (success == 0)
            {
                TempData["Error"] = "Bir hata meydana çıktı!";
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }

            model.ProductCategories = Db.ProductCategory.Where(x => x.IsActive == true).ToList();
            return View(model);
        }

        public ActionResult Detail(int? ID)
        {
            Products product = Db.Products.Find(ID);

            if (product != null)
            {
                ProductModel model = new ProductModel();
                product.IsActive = false;
                model.ProductCategories = Db.ProductCategory.Where(x => x.IsActive == true).ToList();
                model.ProductCode = product.ProductCode;
                model.ProductName = product.ProductName;
                model.Unit = product.Unit;
                model.UnitPrice = product.UnitPrice;
                model.Currency = product.Currency;
                model.Depreciation = product.Depreciation;
                model.LogoID = product.LogoID;
                model.LogoCode = product.LogoCode;
                model.ID = product.ID;
                model.CategoryID = product.CategoryID;

                OypaHelper.AddApplicationLog("Management", "Detail", "Product", ID != null ? ID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {ID} Id'li ürün detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }

            OypaHelper.AddApplicationLog("Management", "Detail", "Product", ID != null ? ID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {ID} Id'li ürün detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }

        [HttpPost]
        public ActionResult Detail(ProductModel model)
        {
            if (ModelState.IsValid)
            {
                Products product = Db.Products.Find(model.ID);
                product.UpdateDate = DateTime.Now;
                product = Mapper.MapSingle(model, product);
                product.UpdateDate = DateTime.Now;

                Db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                //Db.AddLog("TYH-WEB APP", "Details", "ProductDetails", 9999, "", "", "", "", true, "", OypaHelper.RecordEmployee(), "", DateTime.Now);
                int successfully = Db.SaveChanges();

                //if (successfully == 0)
                //{
                //    TempData["Error"] = "Bir hata meydana çıktı!";
                //}
                //else
                //{
                return RedirectToAction("Index", "Product");
                // }
            }

            model.ProductCategories = Db.ProductCategory.Where(x => x.IsActive == true).ToList();
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Products e = Db.Products.Find(id);
            e.IsActive = false;
            Db.AddLog("TYH-WEB APP", "Delete", "ProductDelete", 9999, "", "", "", "", true, "", OypaHelper.RecordEmployee(), "", DateTime.Now);
            int successfully = Db.SaveChanges();

            if (successfully == 0)
            {
                TempData["Error"] = "Bir hata meydana çıktı!";
            }

            return RedirectToAction("Index", "Product");
        }

        public ActionResult UpdateProductFromLogo()
        {
            try
            {
                OypaService.Authentication auth = new OypaService.Authentication();
                auth.Username = "testUser";
                auth.Password = "testKullanici";
                auth.FirmNR = "124";
                auth.PeriodNR = "1";

                OypaService.LOGOTIGERSoapClient client = new OypaService.LOGOTIGERSoapClient();
                client.Open();
                List<Products> newProductList = new List<Products>();
                
                var logoProductList = client.GetItemCardList(auth);

                var ourProductlist = Db.Products.Where(t => t.IsActive == true).ToList();
                var AllCatProducts = logoProductList.Where(t => t.STGRPCODE == "10" || t.STGRPCODE == "11" || t.STGRPCODE == "12" || t.STGRPCODE == "13" || t.STGRPCODE == "14" || t.STGRPCODE == "15" || t.STGRPCODE == "16" || t.STGRPCODE == "17" || t.STGRPCODE == "18" || t.STGRPCODE == "19" || t.STGRPCODE == "20" || t.STGRPCODE == "21" || t.STGRPCODE == "22" || t.STGRPCODE == "23").OrderBy(t => t.STGRPCODE).ToList();

                if (AllCatProducts.Count() > 0 && AllCatProducts != null)
                {
                    if (AllCatProducts.Count() != ourProductlist.Count())
                    {
                        foreach (var item in AllCatProducts)
                        {
                            //Logodan gelen ürün bizde yoksa Db'ye kaydet.
                            if (ourProductlist.Any(t => t.ProductCode == item.CODE) == false)
                            {
                                Products p = new Products()
                                {
                                    CategoryID = Convert.ToInt32(item.STGRPCODE),
                                    ProductCode = item.CODE,
                                    ProductName = item.NAME,
                                    LogoCode = Convert.ToInt32(item.LOGICALREF),
                                    RecordDate = DateTime.Now,
                                    UnitPrice = double.Parse(item.LASTINPRICE, CultureInfo.InvariantCulture),
                                    Unit = item.PIECE,
                                    IsActive = true,
                                    UpdateDate = DateTime.Now,
                                    UpdateUserID = OypaHelper.RecordEmployee()

                                };
                                newProductList.Add(p);
                            }
                            else
                            {
                                var find = ourProductlist.FirstOrDefault(t => t.ProductCode == item.CODE && t.IsActive == true);
                                find.UnitPrice = double.Parse(item.LASTINPRICE, CultureInfo.InvariantCulture);
                            }


                            //else //Bizde varsa bak bakalım bir güncelleştirme yapılmış mı?
                            //{
                            //    var find = ourProductlist.FirstOrDefault(t => t.ProductCode == item.CODE && t.IsActive == true);

                            //    if ((find.ProductCode != item.CODE) || (find.CategoryID == Convert.ToInt32(item.STGRPCODE)) || (find.ProductName == item.NAME) || (find.LogoCode != Convert.ToInt32(item.LOGICALREF)) || (find.UnitPrice != Convert.ToDouble(item.LASTINPRICE)) || (find.Unit != item.PIECE))
                            //    {
                            //        

                            //        find.ProductCode = item.CODE;
                            //        find.CategoryID = Convert.ToInt32(item.STGRPCODE);
                            //        find.ProductName = item.NAME;
                            //        find.LogoCode = Convert.ToInt32(item.LOGICALREF);
                            //        find.UpdateDate = DateTime.Now;
                            //        find.UnitPrice = double.Parse(item.LASTINPRICE, CultureInfo.InvariantCulture);
                            //        find.Unit = item.PIECE;
                            //        find.UpdateUserID = OypaHelper.RecordEmployee();
                            //        Db.SaveChanges();
                            //    }
                            //}
                        }

                        Db.Products.AddRange(newProductList);
                        var res = Db.SaveChanges();
                        if (res > 0)
                        {
                            OypaHelper.AddApplicationLog("Management", "UpdateProductFromLogo", "Product", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, ürünleri güncellemek için logoya istekte bulundu ve {newProductList.Count()} adet ürün uygulamaya dahil edildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Success", message = $"Logo  tarafında yeni açılmış {newProductList.Count()} adet ürün uygulamaya dahil edildi." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "UpdateProductFromLogo", "Product", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, ürünleri güncellemek için logoya istekte bulundu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Success2", message = $"Ürün bilgileri güncel durumdadır." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "UpdateProductFromLogo", "Product", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, ürünleri güncellemek için logoya istekte bulundu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return Json(new { result = "Success", message = $"Ürün bilgileri güncel durumdadır." }, JsonRequestBehavior.AllowGet);
                    }
                    //Eğer Logodan silinmiş bir ürün var ise uygulama DB'sinden de silinecek!
                    //foreach (var item in ourProductlist)
                    //{
                    //    if (logoProductList.Any(t => t.CODE == item.ProductCode) == false)
                    //    {
                    //        item.IsActive = false;
                    //        Db.SaveChanges();
                    //    }
                    //}

                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "UpdateProductFromLogo", "Product", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, ürünleri güncellemek için logoya istekte bulundu fakat logodan verileri çekerken bir sorun oluştu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return Json(new { result = "NoDifference", message = $"Logodan güncel ürün bilgileri çekilemedi, lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                OypaHelper.AddApplicationLog("Management", "GetCustomerFromLogo", "Product", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, ürünleri güncellemek için logoya istekte bulundu fakat logodan verileri çekerken bir sorun oluştu. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                OypaHelper.SendErrorMail("TYHErrorUpdateProductFromLogo", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, ürünleri güncellemek için logoya istekte bulundu fakat logodan verileri çekerken bir sorun oluştu. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");
                return Json(new { result = "NoDifference", message = $"Logodan güncel ürün bilgileri getirilirken bir hata oluştu, lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
            }
           
        }



    }
}