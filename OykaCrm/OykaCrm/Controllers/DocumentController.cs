﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OykaCrm.Models;

namespace OykaCrm.Controllers
{
    public class DocumentController : BaseController
    {
        // GET: Document - Dokümanlar sekmesi
        public ActionResult Index()
        {
            DocumentModel model = new DocumentModel();
             model.documents = Db.Documents.Where(c => c.IsActive == true && c.CategoryID!=null ).OrderByDescending(c=>c.ID).ToList();
            model.documentCategories = Db.DocumentCategory.Where(c => c.IsActive == true ).ToList();
            model.employees = Db.Employee.Where(c => c.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Document", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Doküman Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }
        
        public ActionResult DownloadFile(string filePath)
        {
            string fullName = Server.MapPath("~/UploadData/" + filePath);

            OypaHelper.AddApplicationLog("Management", "DownloadFile", "Document", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {filePath} adlı dokümanı indirdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            byte[] fileBytes = GetFile(fullName);
            return File(
                fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filePath);
        }
      
        byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        [HttpPost]
        public JsonResult uploadfile()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                Guid g = Guid.NewGuid();
                var fileName = Path.GetFileName(g.ToString() + file.FileName);
                //var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/UploadData/"), fileName);
                file.SaveAs(path);
                Documents doc = new Documents();
                doc.DocumentName = Path.GetFileName(fileName);
                doc.FileName = Path.GetFileName(fileName);
                doc.FilePath = Path.Combine(path);
                doc.RecordDate = DateTime.Now;
                doc.Date = DateTime.Today;
                doc.RecordEmployee = (Session["profile"] as Employee).ID;
                doc.IsActive = true;
                doc.RecordIP = HttpContext.Request.UserHostAddress;
                Db.Documents.Add(doc);
                Db.SaveChanges();

                OypaHelper.AddApplicationLog("Management", "uploadfile", "Document", doc.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {doc.ID} Id'li dokümanı yükledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }
            
            return Json(new { hasError = true, message = "Dosya null geldi." });
        }

        //DökümanEkleme sayfası kaydetme işlemi
        public PartialViewResult RecordFile(int Category)
        {
            var doc = Db.Documents.OrderByDescending(c => c.ID).FirstOrDefault();
            doc.CategoryID = Category;
            Db.SaveChanges();
            DocumentModel model = new DocumentModel();
            model.documents = Db.Documents.Where(c => c.IsActive == true).ToList();
            model.documentCategories = Db.DocumentCategory.Where(c => c.IsActive == true).ToList();
            model.employees = Db.Employee.Where(c => c.IsActive == true).ToList();
            return PartialView("~/Views/Document/_PartialDocumentList.cshtml", model);
        }

        public PartialViewResult DeleteFile(int id)
        {
            var deletefile = Db.Documents.Where(c => c.ID == id).FirstOrDefault();
            deletefile.IsActive = false;
            Db.SaveChanges();
            DocumentModel model = new DocumentModel();
            model.documents = Db.Documents.Where(c => c.IsActive == true).ToList();
            model.documentCategories = Db.DocumentCategory.Where(c => c.IsActive == true).ToList();
            model.employees = Db.Employee.Where(c => c.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "DeleteFile", "Document", id, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {id} Id'li dokümanı sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Document/_PartialDocumentList.cshtml", model);
        }



    }
}