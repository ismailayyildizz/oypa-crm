﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class ScheduleController : Controller
    {
        // GET: Schedule
        //OypaDbEntities Db = new OypaDbEntities();
        public ActionResult Index()
        {
            return View();
        }


        #region RunScheduledTaskInformation
        //Her gece 02.30 da 194.100.100.21 sunucusu üzerinde  oluşturulmui windows schedule task ile bu action result tetiklenir.
        //Logoya yeni eklenen Proje, firma, ürün, dataları db'ye insert edilir.
        #endregion
        public ActionResult RunScheduledTask()
        {
            using (OypaDbEntities Db = new OypaDbEntities())
            {
                OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, true, $"Zamanlanmış görev tetiklendi!", 99, null, null);
                //İlgili firmalar neye göre çekilecek, netleştirilecek.
                OypaService.Authentication auth = new OypaService.Authentication();
                auth.Username = "testUser";
                auth.Password = "testKullanici";
                auth.FirmNR = "124";
                auth.PeriodNR = "1";

                OypaService.LOGOTIGERSoapClient client = new OypaService.LOGOTIGERSoapClient();
                client.Open();

                //CustomerList start.
                var list = client.GetClientCardList(auth);
                List<Customers> customerList = new List<Customers>();

                var logoCustomerList = list.Where(t => (t.CODE.Contains("1.120") == true) || (t.CODE.Contains("1.2.4002") == true) || (t.CODE.Contains("1.340") == true) || (t.CODE.Contains("1.159") == true) || (t.CODE.Contains("1.195") == true)).ToList();
                List<Customers> newCustomerList = new List<Customers>();

                var ourCustomer = Db.Customers.Where(t => t.IsActive == true).ToList();

                //CustomerList End

                //ProductList Start
                List<Products> newProductList = new List<Products>();
                var logoProductList = client.GetItemCardList(auth);
                var ourProductlist = Db.Products.Where(t => t.IsActive == true).ToList();
                var AllCatProducts = logoProductList.Where(t => t.STGRPCODE == "10" || t.STGRPCODE == "11" || t.STGRPCODE == "12" || t.STGRPCODE == "13" || t.STGRPCODE == "14" || t.STGRPCODE == "15" || t.STGRPCODE == "16" || t.STGRPCODE == "17" || t.STGRPCODE == "18" || t.STGRPCODE == "19" || t.STGRPCODE == "20" || t.STGRPCODE == "21" || t.STGRPCODE == "22" || t.STGRPCODE == "23").OrderBy(t => t.STGRPCODE).ToList();
                //ProductList End

                ////ProjectList Start
                var projectList = client.GetProjectCardList(auth).ToList();
                var getOurProjectList = Db.Projects.Where(t => t.IsActive == true).ToList();
                List<Projects> newProjectList = new List<Projects>();
                ////ProjectList End
                var successMessage = string.Empty;
                var errorMessage = string.Empty;

                if (logoCustomerList != null && logoCustomerList.Count() > 0)
                {
                    foreach (var item in logoCustomerList)
                    {
                        //if (ourCustomer.Any(t => t.AccountCode == item.CODE) == true)
                        //{
                        //    //Db de kayıtı olan bir ürün ise git bak bakalım değişen verisi var mı 
                        //    var selectedCustomer = ourCustomer.FirstOrDefault(t => t.IsActive == true && t.AccountCode == item.CODE);
                        //    selectedCustomer.AccountCode = item.CODE;
                        //    selectedCustomer.CustomerFullName = item.DEFINITION_;
                        //    selectedCustomer.RegionID = 1;
                        //    selectedCustomer.CategoryID = 1;
                        //    selectedCustomer.EMail = item.EMAILADDR;
                        //    selectedCustomer.Phone = item.TELNRS1;
                        //    selectedCustomer.Address = item.ADDR1;
                        //    selectedCustomer.CityID = null;
                        //    selectedCustomer.CountyID = null;
                        //    selectedCustomer.PostCode = item.POSTCODE;
                        //    selectedCustomer.TaxOffice = item.TAXOFFICE;
                        //    selectedCustomer.TaxNumber = item.TAXNR;
                        //    selectedCustomer.LogoCode = item.LOGICALREF;
                        //    selectedCustomer.FaxNumber = item.FAXNR;
                        //    selectedCustomer.LogoID = null;
                        //    selectedCustomer.Title = null;
                        //    selectedCustomer.IBAN = null;
                        //    selectedCustomer.OpeningBalance = null;
                        //    selectedCustomer.OpeningDate = null;
                        //    selectedCustomer.CurrencyType = null;
                        //    selectedCustomer.IsActive = true;
                        //    selectedCustomer.UpdateDate = DateTime.Now;
                        //    selectedCustomer.UpdateUserID = 99; //System Auto Schedule TaskRun
                        //    selectedCustomer.OpeningDate = DateTime.Now;


                        //    int s = Db.SaveChanges();


                        //}
                        //else
                        //{
                        //    //Yeni eklenmiş bir müşteri, db ye ekle!
                        //    Customers newCustomer = new Customers();
                        //    newCustomer.AccountCode = item.CODE;
                        //    newCustomer.CustomerFullName = item.DEFINITION_;
                        //    newCustomer.RegionID = 1;
                        //    newCustomer.CategoryID = 1;
                        //    newCustomer.EMail = item.EMAILADDR;
                        //    newCustomer.Phone = item.TELNRS1;
                        //    newCustomer.Address = item.ADDR1;
                        //    newCustomer.CityID = null;
                        //    newCustomer.CountyID = null;
                        //    newCustomer.PostCode = item.POSTCODE;
                        //    newCustomer.TaxOffice = item.TAXOFFICE;
                        //    newCustomer.TaxNumber = item.TAXNR;
                        //    newCustomer.LogoCode = item.LOGICALREF;
                        //    newCustomer.FaxNumber = item.FAXNR;
                        //    newCustomer.LogoID = null;
                        //    newCustomer.Title = null;
                        //    newCustomer.IBAN = null;
                        //    newCustomer.OpeningBalance = null;
                        //    newCustomer.OpeningDate = null;
                        //    newCustomer.CurrencyType = null;
                        //    newCustomer.IsActive = true;
                        //    newCustomer.UpdateDate = DateTime.Now;
                        //    newCustomer.UpdateUserID = 99;
                        //    newCustomer.OpeningDate = DateTime.Now;

                        //    newCustomerList.Add(newCustomer);

                        //}

                        if (ourCustomer.Any(t => t.AccountCode == item.CODE) == false)
                        {
                            //Yeni eklenmiş bir müşteri, db ye ekle!
                            Customers newCustomer = new Customers();
                            newCustomer.AccountCode = item.CODE;
                            newCustomer.CustomerFullName = item.DEFINITION_;
                            newCustomer.RegionID = 1;
                            newCustomer.CategoryID = 1;
                            newCustomer.EMail = item.EMAILADDR;
                            newCustomer.Phone = item.TELNRS1;
                            newCustomer.Address = item.ADDR1;
                            newCustomer.CityID = null;
                            newCustomer.CountyID = null;
                            newCustomer.PostCode = item.POSTCODE;
                            newCustomer.TaxOffice = item.TAXOFFICE;
                            newCustomer.TaxNumber = item.TAXNR;
                            newCustomer.LogoCode = item.LOGICALREF;
                            newCustomer.FaxNumber = item.FAXNR;
                            newCustomer.LogoID = null;
                            newCustomer.Title = null;
                            newCustomer.IBAN = null;
                            newCustomer.OpeningBalance = null;
                            newCustomer.OpeningDate = null;
                            newCustomer.CurrencyType = null;
                            newCustomer.IsActive = true;
                            newCustomer.UpdateDate = DateTime.Now;
                            newCustomer.UpdateUserID = 99;
                            newCustomer.OpeningDate = DateTime.Now;

                            newCustomerList.Add(newCustomer);
                        }

                    }
                    Db.Customers.AddRange(newCustomerList);
                    int save = Db.SaveChanges();


                    foreach (var item2 in ourCustomer)
                    {
                        if (logoCustomerList.Any(t => t.CODE == item2.AccountCode) == false)
                        {
                            item2.IsActive = false;
                            int s = Db.SaveChanges();
                        }
                    }

                    if (save > 0)
                    {
                        OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, true, $"Firma bilgileri güncellendi ve logo üzerinden yeni açılmış {newCustomerList.Count} adet firma uygulamaya dahil edilidi.", 99, null, null);
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, true, $"Firma bilgileri başarıyla güncellendi. Yeni eklenen firma bulunmamaktadır.", 99, null, null);
                    }

                    successMessage += "#Logodan Customer başarıyla çekildi, dbye kaydedildi.";
                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, false, $"logocustomerList null geldi, logodan customerler çekilemedi.", 99, null, null);

                    errorMessage = "#logodan customer null geldi, ";
                }

                if (AllCatProducts.Count() > 0 && AllCatProducts != null)
                {
                    foreach (var item in AllCatProducts)
                    {
                        //Logodan gelen ürün bizde yoksa Db'ye kaydet.
                        if (ourProductlist.Any(t => t.ProductCode == item.CODE) == false)
                        {
                            Products p = new Products()
                            {
                                CategoryID = Convert.ToInt32(item.STGRPCODE),
                                ProductCode = item.CODE,
                                ProductName = item.NAME,
                                LogoCode = Convert.ToInt32(item.LOGICALREF),
                                RecordDate = DateTime.Now,
                                UnitPrice = double.Parse(item.LASTINPRICE, CultureInfo.InvariantCulture),
                                Unit = item.PIECE,
                                IsActive = true,
                                UpdateDate = DateTime.Now,
                                UpdateUserID = 99

                            };
                            newProductList.Add(p);
                        }
                        else //Bizde varsa bak bakalım bir güncelleştirme yapılmış mı?
                        {
                            var find = ourProductlist.FirstOrDefault(t => t.ProductCode == item.CODE && t.IsActive == true);


                            //find.ProductCode = item.CODE;
                            //find.CategoryID = Convert.ToInt32(item.STGRPCODE);
                            //find.ProductName = item.NAME;
                            //find.LogoCode = Convert.ToInt32(item.LOGICALREF);
                            //find.UpdateDate = DateTime.Now;
                            find.UnitPrice = double.Parse(item.LASTINPRICE, CultureInfo.InvariantCulture);
                            //find.Unit = item.PIECE;
                            //find.UpdateUserID = 99;

                        }
                    }


                    //Eğer Logodan silinmiş bir ürün var ise uygulama DB'sinden de silinecek!
                    //foreach (var item in ourProductlist)
                    //{
                    //    if (logoProductList.Any(t => t.CODE == item.ProductCode) == false)
                    //    {
                    //        item.IsActive = false;
                    //        Db.SaveChanges();
                    //    }
                    //}
                    Db.Products.AddRange(newProductList);
                    var res = Db.SaveChanges();

                    if (res > 0)
                    {
                        OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProduct", "Schedule", 0, null, true, $"Logo tarafında yeni açılmış {newProductList.Count()} adet ürün uygulamaya dahil edilidi.", 99, null, null);
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProduct", "Schedule", 0, null, true, $"Ürünler güncellendi. Logoya yeni ürün eklenmemiştir.", 99, null, null);
                    }

                    successMessage += "#LogodanProductlar  başarıyla çekildi ve db ye kaydedildi,  ";
                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProduct", "Schedule", 0, null, false, $"Logodan AllCatProducts listi null geldi. Productlar çekilemedi.", 99, null, null);

                    errorMessage += "#LogodanProductlar  null geldi";
                }

                if (projectList.Count() > 0 && projectList != null)
                {
                    // projectList.RemoveRange(0, getOurProjectList.Count());
                    foreach (var item in projectList)
                    {
                        if (getOurProjectList.Any(t => t.Code == item.CODE) == false) //Logodan gelen proje bizde kayıtlı değilse
                        {
                            Projects p = new Projects()
                            {
                                Code = item.CODE,
                                ProjectName = item.NAME,
                                RecordDate = DateTime.Now,
                                IsActive = true,
                                UpdateDate = DateTime.Now,
                                UpdateUserId = 99

                            };
                            newProjectList.Add(p);

                        }
                        //else
                        //{
                        //    var find = getOurProjectList.FirstOrDefault(t => t.Code == item.CODE && t.IsActive == true);
                        //    find.Code = item.CODE;
                        //    find.ProjectName = item.NAME;
                        //    find.UpdateDate = DateTime.Now;
                        //    find.UpdateUserId = 99;
                        //    Db.SaveChanges();
                        //}
                    }
                    Db.Projects.AddRange(newProjectList);
                    int save = Db.SaveChanges();

                    if (save > 0)
                    {
                        OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProject", "Schedule", 0, null, true, $"Logo üzerinden yeni açılmış {newProjectList.Count} adet proje uygulamaya dahil edilidi.", 99, null, null);
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProject", "Schedule", 0, null, true, $"Proje bilgileri başarıyla güncellendi. Logoya yeni proje eklenmemişir.", 99, null, null);
                    }

                    successMessage += "#LogodanProjectlerlar  başarıyla çekildi ve db ye kaydedildi,  ";
                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProject", "Schedule", 0, null, false, $"Logodan ProjectList null geldi, projeler çekilemedi.", 99, null, null);
                    //return Json(new { result = "NoDifference", message = $"Logodan güncel proje bilgileri çekilemedi, lütfen tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
                    errorMessage += "#LogodaProjectler null geldi,";
                }


                var logoExpenseCenterList = client.GetExpenseCenterList(auth);
                List<ExpenseCenter> newExpenseList = new List<ExpenseCenter>();
                var ourExpenseList = Db.ExpenseCenter.ToList();
                if (logoExpenseCenterList != null && logoExpenseCenterList.Count() > 0)
                {
                    if (logoExpenseCenterList.Count() != ourExpenseList.Count())
                    {
                        foreach (var item in logoExpenseCenterList)
                        {
                            if (ourExpenseList.Any(t => t.LogicalRef == item.LOGICALREF) == false)
                            {
                                ExpenseCenter data = new ExpenseCenter()
                                {
                                    Code = item.CODE,
                                    CyphCode = item.CYPHCODE,
                                    LogicalRef = item.LOGICALREF,
                                    Name = item.NAME,
                                    SpeCode = item.SPECODE,
                                    IsActive = true,
                                    RecordDate = DateTime.Now,
                                    RecordUserId = 99
                                };
                                newExpenseList.Add(data);

                            }


                        }
                        Db.ExpenseCenter.AddRange(newExpenseList);
                        int save = Db.SaveChanges();
                        if (save > 0)
                        {
                            OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, true, $"Masraf merkezi kayıtları güncellendi ve logo üzerinden yeni açılmış {newExpenseList.Count} adet masraf merkezi uygulamaya dahil edildi.", 99, null, null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, true, $"Masraf merkezi bilgileri başarıyla güncellendi. Yeni eklenen masraf merkezi bulunmamaktadır.", 99, null, null);
                        }

                        successMessage += "#Logodan masraf merkezi başarıyla çekildi, dbye kaydedildi.";
                    }
                    successMessage += "Masraf merkezleri güncel durumda";

                }
                else
                {

                    OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskCustomer", "Schedule", 0, null, false, $"LogoExpenseCenterList null geldi, logodan masraf merkezleri çekilemedi.", 99, null, null);

                    errorMessage = "#logodan masraf merkezi null geldi, ";

                }



                //return Json(new { result = "Fail", message = $"Logodan güncel firma bilgileri çekilemedi, lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                var fullmessage = successMessage + errorMessage;

                var checkSchedule = Db.ApplicationLog.Where(t => t.ModelName == "Schedule" && t.RecordUser == 99).OrderByDescending(t => t.RecordDate).Take(3).ToList();
                var scheduleMessage = "Zamanlanmış görevin son çalışma zamanı ";

                if (checkSchedule.Count() > 0)
                {
                    foreach (var item in checkSchedule)
                    {
                        if (item.RecordDate.Value.ToShortDateString() == DateTime.Now.ToShortDateString())
                        {

                            scheduleMessage += item.Description + "/ " + item.RecordDate + " ## ";
                        }
                        else
                        {
                            scheduleMessage += "Görev zamanlayıcısı bugün çalışmadı";
                        }
                    }
                }
                else
                {
                    scheduleMessage += "Log tablosundan Schedule Task ile ilgili kayıt gelmedi.";
                }


                fullmessage += scheduleMessage;
                //var mailResult = OypaHelper.SendMail("ScheduleTaskTest", fullmessage, "tahsin.tiryaki@sibertechno.com", null);

                //OypaHelper.AddApplicationLog("Management", "RuncScheduleTaskProduct", "Schedule", 0, null, true, "", 99, null, null);

               
              
                Process[] processlist = Process.GetProcesses();
                foreach (Process theprocess in processlist)
                {
                    String ProcessUserSID = theprocess.StartInfo.EnvironmentVariables["USERNAME"];
                    String CurrentUser = System.Environment.UserName;
                    if (theprocess.ProcessName.ToLower().ToString() == "chrome" && ProcessUserSID == CurrentUser)
                    {
                        theprocess.Kill();
                    }
                }
            }

            return View();
        }


    }
}
