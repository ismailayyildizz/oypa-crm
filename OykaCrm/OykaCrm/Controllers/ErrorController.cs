﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            OypaHelper.AddApplicationLog("Management", "Index", "Error", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Yetkiniz Yok uyarısı ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }
        public ActionResult NotFound()
        {
            OypaHelper.AddApplicationLog("Management", "NotFound", "Error", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, NotFound sayfası ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }
    }
}