﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class EmployeeController : BaseController
    {
        // GET: Employee - Ayarlar->Kullanıcı İşlemleri
        public ActionResult Index()
        {
            List<Employee> employees = Db.Employee.Where(x => x.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Employee", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Kullanıcı Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(employees);
        }

        public ActionResult New()
        {
            EmployeeAddViewModel model = new EmployeeAddViewModel();

            model.RoleGroup = Db.RoleGroup.Where(x => x.IsActive == true).ToList();
            model.Region = Db.Region.Where(x => x.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "New", "Employee", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Yeni Kullanıcı Oluşturma sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        [HttpPost]
        public ActionResult New(EmployeeAddViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Employee e = Db.Employee.Where(x => x.EMail == model.EMail && x.IsActive == true).FirstOrDefault();

                    if (e == null)
                    {
                        Employee employee1 = new Employee();
                        employee1.FullName = model.FullName;
                        employee1.RegionID = model.RegionID;
                        employee1.RolGroupID = model.RoleGroupID;
                        employee1.EMail = model.EMail;
                        employee1.Password = model.Password;
                        employee1.ManagerID = null;
                        employee1.IsActive = true;
                        employee1.RecordDate = DateTime.Now;
                        employee1.RecordEmployeeId = OypaHelper.RecordEmployee();
                        Db.Employee.Add(employee1);
                        int successfully = Db.SaveChanges();

                        if (successfully == 0)
                        {
                            TempData["Error"] = "Bir hata meydana çıktı!";

                            OypaHelper.AddApplicationLog("Management", "New", "Employee", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {employee1.FullName} adlı yeni bir kullanıcı eklerken hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "New", "Employee", employee1.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {employee1.FullName} adlı kullanıcıyı ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return RedirectToAction("Index", "Employee");
                        }
                    }
                    else
                    {
                        TempData["Error"] = "Bu E-Posta kullanılmış!";

                        OypaHelper.AddApplicationLog("Management", "New", "Employee", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, daha önce kullanılmış mail adresi ile ( {model.EMail} ) yeni bir kullanıcı eklemek istedi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    }
                    model.RoleGroup = Db.RoleGroup.Where(x => x.IsActive == true).ToList();
                    model.Region = Db.Region.Where(x => x.IsActive == true).ToList();
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                OypaHelper.SendErrorMail("TYHErrorNewEmployee", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.FullName} adlı yeni bir kullanıcı eklerken hata ile karşılaştı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");


            }
            return RedirectToAction("Index", "Employee");


        }

        public ActionResult Detail(int? ID)
        {
            Employee e = Db.Employee.Find(ID);

            if (e != null)
            {
                EmployeeAddViewModel model = new EmployeeAddViewModel();
                model.ID = e.ID;
                model.FullName = e.FullName;
                model.RegionID = e.RegionID;
                model.RoleGroupID = e.RolGroupID;
                model.EMail = e.EMail;
                model.ManagerID = e.ManagerID;
                model.IsActive = e.IsActive;
                model.RecordDate = e.RecordDate;
                model.RecordEmployeeId = e.RecordEmployeeId;
                model.RoleGroup = Db.RoleGroup.Where(x => x.IsActive == true).ToList();
                model.Region = Db.Region.Where(x => x.IsActive == true).ToList();
                model.Password = e.Password;

                OypaHelper.AddApplicationLog("Management", "Detail", "Employee", ID != null ? ID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {ID} Id'li kullanıcının detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }

            OypaHelper.AddApplicationLog("Management", "Detail", "Employee", ID != null ? ID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {ID} Id'li kullanıcının detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }

        [HttpPost]
        public ActionResult Detail(EmployeeAddViewModel model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    Employee employee = Db.Employee.Find(model.ID);
                    employee.FullName = model.FullName;
                    employee.RegionID = model.RegionID;
                    employee.RolGroupID = model.RoleGroupID;
                    employee.EMail = model.EMail;
                    employee.ManagerID = model.ManagerID;
                    employee.IsActive = model.IsActive;
                    employee.RecordDate = model.RecordDate;
                    employee.RecordEmployeeId = model.RecordEmployeeId;
                    employee.Password = model.Password;
                    Db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                    int successfully = Db.SaveChanges();

                    OypaHelper.AddApplicationLog("Management", "Detail", "Employee", model.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.ID} Id'li kullanıcıyı güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return RedirectToAction("Index", "Employee");
                }


            }
            catch (Exception ex)
            {
                OypaHelper.SendErrorMail("TYHErrorEmployeeDetail", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.ID} Id'li kullanıcıyı güncellerken bir hata ile karşılatı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");

            }
            model.RoleGroup = Db.RoleGroup.Where(x => x.IsActive == true).ToList();
            model.Region = Db.Region.Where(x => x.IsActive == true).ToList();
            return View(model);

        }

        public ActionResult Delete(int? id)
        {
            Employee e = Db.Employee.Find(id);
            if (e != null)
            {
                e.IsActive = false;
                int successfully = Db.SaveChanges();

                if (successfully > 0)
                {
                    OypaHelper.AddApplicationLog("Management", "Delete", "Employee", id != null ? id.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {id} Id'li kullanıcıyı sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }

            OypaHelper.AddApplicationLog("Management", "Delete", "Employee", id != null ? id.Value : 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {id} Id'li kullanıcıyı silmek isterken bir hatayla karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Titles()
        {
            TitleControlModel model = new TitleControlModel();
            model.PositionList = Db.Positions.Where(i => i.IsActive == true).ToList();
            model.StatusList = Db.PositionStatus.Where(i => i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "Titles", "Employee", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Pozisyon Tanımları listesini görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        public ActionResult NewTitle()
        {
            TitleControlModel model = new TitleControlModel();
            //model.PositionList = Db.Positions.Where(i => i.IsActive == true).ToList();
            model.StatusList = Db.PositionStatus.Where(i => i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "NewTitle", "Employee", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, yeni pozisyon tanımı ekleme sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        [HttpPost]
        public ActionResult NewTitle(string titleName, int? statusID)
        {
            if (!string.IsNullOrEmpty(titleName) && !string.IsNullOrWhiteSpace(titleName))
            {
                var checkPositions = Db.Positions.FirstOrDefault(t => t.PositionName == titleName.Trim() && t.StatusID == statusID && t.IsActive == true);
                if (checkPositions == null)
                {
                    Positions p = new Positions()
                    {
                        PositionName = titleName,
                        StatusID = statusID,
                        IsActive = true
                    };

                    Db.Positions.Add(p);
                    Db.SaveChanges();

                    OypaHelper.AddApplicationLog("Management", "NewTitle", "Employee", p.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {p.PositionName} adlı yeni pozisyon tanımı ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return RedirectToAction("Titles");
                }
                else
                {
                    TitleControlModel model = new TitleControlModel();

                    model.StatusList = Db.PositionStatus.Where(i => i.IsActive == true).ToList();
                    ViewBag.ErrorMessage = "Pozisyon sistemde tanımlı!";

                    OypaHelper.AddApplicationLog("Management", "NewTitle", "Employee", checkPositions.ID, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {titleName} adlı sistemde var olan pozisyon tanımını eklemek istedi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return View(model);
                }
            }
            else
            {
                TitleControlModel model = new TitleControlModel();

                model.StatusList = Db.PositionStatus.Where(i => i.IsActive == true).ToList();
                ViewBag.ErrorMessage2 = "Pozisyon adı boş geçilemez!";

                OypaHelper.AddApplicationLog("Management", "NewTitle", "Employee", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, yeni pozisyon tanımı eklemek isterken pozisyon adını boş bıraktığı için hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }

        }

        public ActionResult DeleteTitle(int? titleID)
        {
            var deletedTitle = Db.Positions.Find(titleID);
            if (deletedTitle != null)
            {
                deletedTitle.IsActive = false;
                Db.SaveChanges();

                OypaHelper.AddApplicationLog("Management", "DeleteTitle", "Employee", titleID != null ? titleID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {deletedTitle.PositionName} adlı pozisyon tanımını sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }

            return RedirectToAction("Titles");
        }


    }
}