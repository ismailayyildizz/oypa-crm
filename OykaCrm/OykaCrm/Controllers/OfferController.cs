﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.qrcode;
using iTextSharp.tool.xml;
using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OykaCrm.Controllers
{
    public class OfferController : BaseController
    {

        // GET: Offer -Teklifler
        [HttpPost]
        public ActionResult Index(string filtertext)
        {

            List<Offer> offerlist = null;
            OypaHelper helper = new OypaHelper();
            var regionId = helper.EmployeeRegionID();
            if (!(regionId == 1 || regionId == 8 || regionId == 9 || regionId == 11 || regionId == 12))
            {

                regionId = 0;
            }
            else
            {
                var userId = OypaHelper.RecordEmployee();
                if(userId == 1048)
                {
                    regionId = 0;
                }
            }
            if (filtertext != null)
            {
                offerlist = Db.Offer.Where(i => i.Customers.CustomerFullName.Contains(filtertext) && i.IsActive == true).ToList();
                if (regionId > 0)
                {
                    offerlist = offerlist.Where(x => x.RegionID == regionId).ToList();
                }
                OypaHelper.AddApplicationLog("Management", "Index", "Offer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {filtertext} kelimesi ile teklif listesinde arama yaptı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return View(offerlist);
            }
            else
            {
                offerlist = Db.Offer.Where(t => t.IsActive == true).ToList();
                if (regionId > 0)
                {
                    offerlist = offerlist.Where(x => x.RegionID == regionId).ToList();
                }
                OypaHelper.AddApplicationLog("Management", "Index", "Offer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı teklif listesini görüntüledi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return View(offerlist);
            }

        }

        //Tüm teklifler
        public ActionResult Index(bool? MessageType, string Message)
        {
            if (MessageType != null)
            {
                if (MessageType.Value == true)
                {

                    ViewBag.SMessage = Message;
                }
                else
                {
                    ViewBag.EMessage = Message;
                }
            }
            OypaHelper helper = new OypaHelper();
            var regionId = helper.EmployeeRegionID();
            if(!(regionId == 1 || regionId == 8 || regionId == 9 || regionId == 11 || regionId == 12))
            {
                regionId = 0;
            }
            else
            {
                var userId = OypaHelper.RecordEmployee();
                if (userId == 1048)
                {
                    regionId = 0;
                }
            }
            var roleGroup = OypaHelper.EmployeeRoleGroup();
            List<Offer> offerlist = new List<Offer>();
            //if (/*roleGroup==2 || roleGroup==21 || roleGroup==24 || roleGroup==1*/ roleGroup == 18 || roleGroup == 22) // İk birimi
            //{
            //    offerlist = Db.Offer.OrderByDescending(t => t.OpportunityID).Where(t => t.StatusLevel == 2 || t.StatusLevel == 5 || t.StatusLevel == 6 || t.StatusLevel == 7).ToList();
            //    //List<Offer> offerlist = Db.Offer.OrderByDescending(t => t.OpportunityID).ToList();
            //}
            //else if (roleGroup == 19) // SAtın alma uzm.
            //{
            //    offerlist = Db.Offer.OrderByDescending(t => t.OpportunityID).Where(t => t.StatusLevel == 2 || t.StatusLevel == 3 || t.StatusLevel == 4 || t.StatusLevel == 7).ToList();
            //}
            //else if (roleGroup == 17) // satın alma müd
            //{
            //    offerlist = Db.Offer.OrderByDescending(t => t.OpportunityID).Where(t => t.StatusLevel == 2 || t.StatusLevel == 3 || t.StatusLevel == 4 || t.StatusLevel == 7 || t.StatusLevel == 19 || t.StatusLevel == 20).ToList();
            //}
            //else
            //{
            offerlist = Db.Offer.Where(t => t.IsActive == true).OrderByDescending(t => t.OpportunityID).ToList();
            if(regionId > 0)
            {
                offerlist = offerlist.Where(x => x.RegionID == regionId).ToList();
            }
            OypaHelper.AddApplicationLog("Management", "Index", "Offer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı teklif listesini görüntüledi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            //}
            return View(offerlist);
        }

        public ActionResult Create()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult Create(OfferPostModel postmodel)
        //{
        //    if (postmodel != null)
        //    {

        //        OFFER offer = new OFFER();// teklif header ekle
        //        offer.NET_TOTAL = postmodel.geneltoplam;
        //        offer.NET_KDV = postmodel.toplamkdv;
        //        offer.CUSTOMER_NAME = postmodel.musteriadi;
        //        offer.BASKET_TOTAL = postmodel.aratoplam;
        //        offer.SAVE_DATE = DateTime.Now;
        //        offer.VADE_DATE = DateTime.Now;
        //        offer.CUSTOMER_ID = 4;
        //        db.OFFER.Add(offer);
        //        db.SaveChanges();

        //        List<OFFER_ROW> multirow = new List<OFFER_ROW>();
        //        for (int i = 0; i < postmodel.urunadi.Count; i++)
        //        {
        //            var urunadi = postmodel.urunadi[i];
        //            //postmodel.urunmiktar[i] = postmodel.urunmiktar[i].Replace(",", "");
        //            var miktar = postmodel.urunmiktar[i];
        //            var fiyat = postmodel.urunfiyat[i];
        //            Double _miktar = Convert.ToDouble(miktar, CultureInfo.InvariantCulture);
        //            Double _fiyat = Convert.ToDouble(fiyat, CultureInfo.InvariantCulture);

        //            OFFER_ROW temp_row = new OFFER_ROW();
        //            temp_row.OFFER_ID = offer.ID;
        //            temp_row.PRODUCT_NAME = urunadi;
        //            temp_row.UNIT_PRICE = _fiyat;
        //            temp_row.QUANTITY = _miktar;
        //            multirow.Add(temp_row);
        //        }
        //        db.OFFER_ROW.AddRange(multirow);
        //        db.SaveChanges();
        //    }
        //    return View();
        //}



        public ActionResult DetailPop()
        {

            return View();

        }

        public ActionResult New()
        {

            OfferNewViewModel model = new OfferNewViewModel();
            OypaHelper helper = new OypaHelper();

            model.CustomerList = Db.Customers.ToList();
            model.GetOfferNumber = helper.GetUpdOfferNumber();

            return View(model);


        }

        [HttpPost]
        public ActionResult Add(OfferAddPostModel formmodel)
        {


            Offer NewOffer = new Offer();
            NewOffer.CustomerID = Convert.ToInt32(formmodel.Customer);
            NewOffer.DateBegin = formmodel.duzenlemetarihi;
            NewOffer.DateEnd = formmodel.vadetarihi;
            NewOffer.EmployeeID = OypaHelper.RecordEmployee();
            NewOffer.RecordEmployeeID = OypaHelper.RecordEmployee();
            NewOffer.IsActive = true;
            NewOffer.RecordDate = DateTime.Now;
            NewOffer.OfferNumber = formmodel.OfferNumber;
            NewOffer.StatusLevel = Convert.ToInt32(formmodel.select);
            Db.Offer.Add(NewOffer);
            Db.SaveChanges();

            return RedirectToAction("Detail", "Offer", new { id = NewOffer.ID });

        }

        [HttpPost]
        public ActionResult Edit(OfferAddPostModel formmodel)
        {

            if (formmodel != null)
            {

                var offerheader = Db.Offer.FirstOrDefault(x => x.ID == 1); // formmodelden gelen id

                Offer self = new Offer()  // bu modelin eski hali değişikliğe uğramamış hali.
                {
                    CustomerID = offerheader.CustomerID,
                    ID = offerheader.ID
                };

                OypaHelper helper = new OypaHelper();
                Offer NewOffer = new Offer();
                NewOffer.CustomerID = Convert.ToInt32(formmodel.Customer);
                NewOffer.DateBegin = formmodel.duzenlemetarihi;
                NewOffer.DateEnd = formmodel.vadetarihi;
                NewOffer.EmployeeID = OypaHelper.RecordEmployee();
                NewOffer.RecordEmployeeID = OypaHelper.RecordEmployee();
                NewOffer.IsActive = true;
                NewOffer.RecordDate = DateTime.Now;
                NewOffer.OfferNumber = formmodel.OfferNumber;
                NewOffer.StatusLevel = Convert.ToInt32(formmodel.select);
                Db.Offer.Add(NewOffer);
                Db.SaveChanges();


                // NewOffer modelin değişmiş yeni hali.

                var isequal = OypaHelper.PublicInstancePropertiesEqual<Offer>(self, NewOffer, OypaHelper.getIgnorelist()); //değişen listesini al

                // eğer rowlarda bu dizi dolu geliyorsa rowlşarda değişiklik var demektir. öncelikle eski hali history tablosuna ekleyip yeni değerleri offer e savechanges et.

                //Offer headerde ilk kayıtta versionprefix = "V" versionnumber = 1 versiondigit = 0 ata
                // offer edit edildiğinde satırlarda bir değişiklik var ise versiyondigit i 1 arttır. eğer versiyon digit 10 a ulaşmışsa versiyon number i 1 arttır digit kısmını 0 ver. örnek V.1.9 iken  yeni versiyon V.2.0 olmalı 


                OypaHelper.AddApplicationLog("", "", "", 1, "", true, "", 1, "", isequal); // değişen listeli loga gönder.


            }

            return RedirectToAction("Detail", "Offer", new { id = 1 }); // 1 yerine offerID koy

        }

        public ActionResult Detail(int? id)
        {
            OfferDetailViewModel model = new OfferDetailViewModel();
            model.singleoffer = Db.Offer.FirstOrDefault(i => i.ID == id && i.IsActive == true);
            return View(model);
        }

        [HttpGet]
        public ActionResult Detail(int? OpportunityID, int? AutoOpportunity)
        {
            OfferControlModel model = new OfferControlModel();

            OypaHelper helper = new OypaHelper();

            if (OpportunityID.HasValue && AutoOpportunity.HasValue)
            {
                model.singleopportuniy = Db.Opportunity.FirstOrDefault(i => i.ID == OpportunityID);

                if (model.singleopportuniy != null)
                {
                    var checkoffer = Db.Offer.Where(i => i.OpportunityID == OpportunityID && i.IsActive == true).FirstOrDefault();
                    if (checkoffer != null)
                    {
                        model.RoleGroup = OypaHelper.EmployeeRoleGroup();
                        model.singleoffer = checkoffer;
                        model.offerproducts = Db.OfferProducts.Where(i => i.OfferID == checkoffer.ID).ToList();
                        model.offerpositions = Db.OfferPositions.Where(i => i.OfferID == checkoffer.ID).OrderBy(i => i.PositionItemID).ToList();
                        model.DocumentsList = Db.Documents.Where(t => t.OpportunityID == OpportunityID && t.IsActive == true).ToList();
                        model.SingleOfferContract = Db.OfferContract.FirstOrDefault(t => t.IsActive == true && t.OfferID == checkoffer.ID);
                        model.OfferStatusList = Db.OfferStatus.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();
                        model.ProjectList = Db.Projects.Where(t => t.IsActive == true).ToList();
                        model.OfferContentTemplateList = Db.OfferContentTemplate.Where(t => t.IsActive == true).ToList();

                        model.Employee = Db.Employee.Where(t => t.ID == checkoffer.EmployeeID).FirstOrDefault();
                        //Position Id ye göre distinc yaptım
                        model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                        model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                        model.PositionStatuses = Db.PositionStatus.Where(i => i.IsActive == true).ToList();


                        var notes = Db.Notes.Where(x => x.OfferId == checkoffer.ID && x.IsActive == true).ToList();
                        if (notes != null)
                        {
                            model.Notes = notes;
                        }

                        model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == checkoffer.ID).ToList();
                        model.offerequipments = Db.OfferEquipment.Where(i => i.OfferID == checkoffer.ID).ToList();

                        //Tek seferlik Teklif için kullanıclacak Liste (İK)
                        model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == checkoffer.ID).ToList();
                        model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                        model.IsOneTimeOfferOtherExpenseItems = Db.IsOneTimeOfferOtherExpenseItems.Where(t => t.IsActive == true).ToList();
                        model.IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == checkoffer.ID).ToList();

                        model.CustomerContactList=Db.CustomerContact.Where(x => x.CustomerID == checkoffer.CustomerID && x.IsActive==true).ToList();
                        calculateFooter(checkoffer.ID);
                        if (model.singleoffer.TotalHrProfit != null) // ik
                        {
                            model.singleoffer.BrutToplam += model.singleoffer.TotalHrProfit;
                        }
                        if (model.singleoffer.SumMachineExpenseProfit != null) //Makine demirbaş
                        {
                            model.singleoffer.TotalMachineExpense += model.singleoffer.SumMachineExpenseProfit;
                        }
                        if (model.singleoffer.SumAraToplamProfit != null) //malzeme stok karlılık oranı toplamı
                        {
                            model.singleoffer.AraToplam += model.singleoffer.SumAraToplamProfit;
                        }
                        if (model.singleoffer.SumOtherExpenseProfit != null) //diğer harcamalar karlılık oranı toplamı
                        {
                            model.singleoffer.TotalOtherExpense += model.singleoffer.SumOtherExpenseProfit;
                        }
                        OypaHelper.AddApplicationLog("Management", "Detail", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    }
                    else
                    {
                        Offer singleofferx = new Offer();

                        singleofferx.CustomerID = model.singleopportuniy.CustomerID.Value;
                        singleofferx.DateBegin = model.singleopportuniy.DateBegin;
                        singleofferx.DateEnd = model.singleopportuniy.DateEnd;
                        singleofferx.IsOneTime = model.singleopportuniy.IsOneTime;
                        singleofferx.OpportunityID = model.singleopportuniy.ID;
                        singleofferx.OfferNumber = helper.GetUpdOfferNumber();
                        singleofferx.RecordDate = DateTime.Now;
                        singleofferx.RecordEmployeeID = OypaHelper.RecordEmployee();
                        singleofferx.Customers = Db.Customers.FirstOrDefault(x => x.ID == singleofferx.CustomerID);
                        singleofferx.IsActive = true;
                        singleofferx.ProjectID = model.singleopportuniy.ProjectID;
                        singleofferx.EmployeeID = model.singleopportuniy.EmployeeID;
                        singleofferx.StatusLevel = 1;
                        singleofferx.OfferName = model.singleopportuniy.OpportunityName;
                        singleofferx.PurchasingManagerCheck = 0;//Bekliyor.
                        singleofferx.HumanResourcesCheck = 0;//Bekliyor.
                        singleofferx.PurchasingSpecialistCheck = 0;//Bekliyor.
                        singleofferx.RegionalDirectorCheck = 0;//Bekliyor.
                        //Teklif, teklif hazırlık durumunda.
                        singleofferx.IsRevision = false;

                        singleofferx.VersiyonPrefix = "V";

                        singleofferx.RegionID = model.singleopportuniy.RegionID;

                        Db.Offer.Add(singleofferx);
                        Db.SaveChanges();
                        model.RoleGroup = OypaHelper.EmployeeRoleGroup();
                        model.singleoffer = singleofferx;

                        model.Employee = Db.Employee.Where(t => t.ID == singleofferx.EmployeeID).FirstOrDefault();
                        model.offerproducts = Db.OfferProducts.Where(i => i.OfferID == singleofferx.ID).ToList();
                        model.offerpositions = Db.OfferPositions.Where(i => i.OfferID == singleofferx.ID).OrderBy(x => x.PositionItemID).ToList();
                        model.DocumentsList = Db.Documents.Where(t => t.OpportunityID == OpportunityID).ToList();
                        model.SingleOfferContract = Db.OfferContract.FirstOrDefault(t => t.IsActive == true && t.OfferID == singleofferx.ID);
                        model.OfferStatusList = Db.OfferStatus.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();
                        model.ProjectList = Db.Projects.Where(t => t.IsActive == true).ToList();
                        model.OfferContentTemplateList = Db.OfferContentTemplate.Where(t => t.IsActive == true).ToList();

                        model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == singleofferx.ID).ToList();
                        model.offerequipments = Db.OfferEquipment.Where(i => i.OfferID == singleofferx.ID).ToList();

                        //Tek seferlik Teklif için kullanıclacak Liste (İK)
                        model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == singleofferx.ID).ToList();
                        model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                        model.IsOneTimeOfferOtherExpenseItems = Db.IsOneTimeOfferOtherExpenseItems.Where(t => t.IsActive == true).ToList();
                        model.IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == singleofferx.ID).ToList();

                        //Position Id ye göre distonc yaptım
                        model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                        model.PositionStatuses = Db.PositionStatus.Where(i => i.IsActive == true).ToList();
                        model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                        calculateFooter(singleofferx.ID);
                        if (model.singleoffer.TotalHrProfit != null)
                        {
                            model.singleoffer.BrutToplam += model.singleoffer.TotalHrProfit;
                        }

                        OypaHelper.AddApplicationLog("Management", "Detail", "Offer", singleofferx.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleopportuniy.OpportunityName} isimli fırsatı teklife dönüştürdü.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    }
                    return View(model);

                }
                else
                {
                    ViewBag.ErrorMessage = "Böyle bir teklif bulunmamaktadır!";

                }



            }
            else
            {
                ViewBag.ErrorMessage = "Böyle bir teklif bulunmamaktadır!";
            }
            return View();

        }

        public ActionResult OfferDetailPreview(int? OpportunityID, int? AutoOpportunity)
        {
            OfferControlModel model = new OfferControlModel();

            var checkoffer = Db.Offer.Where(i => i.OpportunityID == OpportunityID && i.IsActive == true).FirstOrDefault();
            if (checkoffer != null)
            {
                model.singleoffer = checkoffer;
                model.offerproducts = Db.OfferProducts.Where(i => i.OfferID == checkoffer.ID).ToList();
                model.offerpositions = Db.OfferPositions.Where(i => i.OfferID == checkoffer.ID).OrderBy(i => i.PositionItemID).ToList();
                model.DocumentsList = Db.Documents.Where(t => t.OpportunityID == OpportunityID).ToList();
                model.SingleOfferContract = Db.OfferContract.FirstOrDefault(t => t.IsActive == true && t.OfferID == checkoffer.ID);
                model.OfferStatusList = Db.OfferStatus.Where(t => t.IsActive == true).OrderBy(t => t.SortBy).ToList();
                model.ProjectList = Db.Projects.Where(t => t.IsActive == true).ToList();
                model.OfferContentTemplateList = Db.OfferContentTemplate.Where(t => t.IsActive == true).ToList();

                //Position Id ye göre distinc yaptım
                model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                model.PositionStatuses = Db.PositionStatus.Where(i => i.IsActive == true).ToList();
                model.RoleGroup = OypaHelper.EmployeeRoleGroup();

                var notes = Db.Notes.Where(x => x.OfferId == checkoffer.ID && x.IsActive == true).ToList();
                if (notes != null)
                {
                    model.Notes = notes;
                }


                model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == checkoffer.ID).ToList();
                model.offerequipments = Db.OfferEquipment.Where(i => i.OfferID == checkoffer.ID).ToList();

                //Tek seferlik Teklif için kullanıclacak Liste (İK)
                model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == checkoffer.ID).ToList();
                model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                model.IsOneTimeOfferOtherExpenseItems = Db.IsOneTimeOfferOtherExpenseItems.Where(t => t.IsActive == true).ToList();
                model.IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == checkoffer.ID).ToList();
                calculateFooter(checkoffer.ID);
                if (model.singleoffer.TotalHrProfit != null) // ik
                {
                    model.singleoffer.BrutToplam += model.singleoffer.TotalHrProfit;
                }
                if (model.singleoffer.SumMachineExpenseProfit != null) //Makine demirbaş
                {
                    model.singleoffer.TotalMachineExpense += model.singleoffer.SumMachineExpenseProfit;
                }
                if (model.singleoffer.SumAraToplamProfit != null) //malzeme stok karlılık oranı toplamı
                {
                    model.singleoffer.AraToplam += model.singleoffer.SumAraToplamProfit;
                }
                if (model.singleoffer.SumOtherExpenseProfit != null) //diğer harcamalar karlılık oranı toplamı
                {
                    model.singleoffer.TotalOtherExpense += model.singleoffer.SumOtherExpenseProfit;
                }
                OypaHelper.AddApplicationLog("Management", "OfferDetailPreview", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }
            else
            {
                ViewBag.ErrorMessage = "Böyle bir teklif bulunmamaktadır!";
            }
            return View();

        }

        //Not ekleme
        [HttpPost]
        public PartialViewResult SaveNote(AddNoteModels models)
        {
            var opportunityId = 0;

            var model = new OfferControlModel();

            if (models.OfferId != 0 && models.Notes != null)
            {
                var newNote = new Notes();
                newNote.Note = models.Notes;
                newNote.UserId = OypaHelper.RecordEmployee();
                newNote.RecardDate = DateTime.Now;
                newNote.OfferId = models.OfferId;
                newNote.IsActive = true;

                opportunityId = Db.Offer.Find(models.OfferId).OpportunityID.Value;

                Db.Notes.Add(newNote);
                Db.SaveChanges();
            }
            var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == models.OfferId);
            var notes = Db.Notes.Where(x => x.OfferId == models.OfferId && x.IsActive == true).ToList();
            if (notes != null)
            {
                model.Notes = notes;
            }

            OypaHelper.AddApplicationLog("Management", "SaveNote", "Offer", models.OfferId, "Notes", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer?.OfferNumber} nolu teklife not ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Offer/_PartialNoteListView.cshtml", model);


        }

        [HttpPost]
        public ActionResult PreviewNote(int id)
        {
            if (id != 0)
            {
                var note = Db.Notes.Where(x => x.Id == id && x.IsActive == true).FirstOrDefault();
                if (note != null)
                {
                    return Json(new { Note = note.Note }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteNote(int id)
        {
            if (id != 0)
            {
                var note = Db.Notes.Where(x => x.Id == id && x.IsActive == true).FirstOrDefault();
                if (note != null)
                {
                    note.IsActive = false;
                    note.UpdateDate = DateTime.Now;
                    var isSaved = Db.SaveChanges();
                    var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == note.OfferId);
                    OypaHelper.AddApplicationLog("Management", "DeleteNote", "Offer", note.Id, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer.OfferNumber} nolu teklife yer alan {note.Id} id li notu sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    if (isSaved > 0)
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    OypaHelper.AddApplicationLog("Management", "DeleteNote", "Offer", note.Id, "Offer", false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer.OfferNumber} nolu teklife yer alan {note.Id} id li notu silerken hata ile karşılaştı", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    return Json(false, JsonRequestBehavior.AllowGet);

                }
            }
            OypaHelper.AddApplicationLog("Management", "DeleteNote", "Offer", 0, "Offer", false, $"  {id} id li not bulunamadı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        //kar oranı ekle
        public PartialViewResult AddProfitRate(int? offerid, string profitRate)
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerid && t.IsActive == true).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.AraToplamProfitRate = profitrate;
                Db.SaveChanges();
            }
            calculateFooter(checkoffer.ID);
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid && x.IsActive == true);
            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == offerid).ToList();
            OypaHelper.AddApplicationLog("Management", "AddProfitRate", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin malzeme stok maliyetinin karlılık oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("_PartialOfferRows", model);
        }
        //insan kaynakları kar oranı ekleme
        public PartialViewResult AddProfitRateHumanResources(int? offerid, string profitRate)
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerid).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.BrutToplamProfitRate = profitrate;
                Db.SaveChanges();
                calculateFooter(checkoffer.ID);

                OypaHelper.AddApplicationLog("Management", "AddProfitRateHumanResources", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin İk maliyetinin karlılık oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                if (checkoffer.IsOneTime == true)
                {

                    model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid);
                    model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == offerid).OrderBy(x => x.PositionItemID).ToList();
                    return PartialView("_PartialOfferPositions", model);
                }
                else
                {
                    model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid);
                    model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == offerid).ToList();
                    return PartialView("_PartialOfferPosIsOneTime", model);
                }


            }

            return PartialView("_PartialOfferPositions", model);

        }

        public PartialViewResult AddProfitRateForOther(int? offerid, string profitRate) //diger giderler icin karlilik orani ekleme 
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerid).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.ProfitRateForOthers = profitrate;
                Db.SaveChanges();

                calculateFooter(checkoffer.ID);

                OypaHelper.AddApplicationLog("Management", "AddProfitRateForOther", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin diğer maliyetlerinin karlılık oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);


                if (checkoffer.IsOneTime == true)
                {
                    model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid);
                    model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == offerid).OrderBy(x => x.PositionItemID).ToList();
                    model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == checkoffer.ID).ToList();
                    return PartialView("_PartialOfferOthers", model);
                }
                else
                {
                    model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid);
                    model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == offerid).ToList();
                    model.IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(i => i.OfferID == checkoffer.ID).ToList();
                    model.IsOneTimeOfferOtherExpenseItems = Db.IsOneTimeOfferOtherExpenseItems.Where(t => t.IsActive == true).ToList();

                    return PartialView("_PartialIsOneTimeOfferOthers", model);
                }
            }
            return PartialView("_PartialOfferOthers", model);
        }

        public PartialViewResult AddProfitRateForMachine(int? offerid, string profitRate) //makine/demirbas icin karlilik orani ekleme
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerid).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.ProfitRateForMachine = profitrate;
                Db.SaveChanges();
            }
            calculateFooter(checkoffer.ID);
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid);
            model.offerequipments = Db.OfferEquipment.Where(i => i.OfferID == checkoffer.ID).ToList();

            OypaHelper.AddApplicationLog("Management", "AddProfitRateForMachine", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin makine demirbaş maliyetinin karlılık oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("_PartialOfferRowForMachine", model);

        }

        public PartialViewResult AddManagementExpenseRateForHR(int? offerID, string profitRate) //ik icin isletme gideri orani ekleme 
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerID).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.ManagementExpenseRateHR = profitrate;
                Db.SaveChanges();
            }
            calculateFooter(checkoffer.ID);
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
            model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == offerID).OrderBy(x => x.PositionItemID).ToList();

            OypaHelper.AddApplicationLog("Management", "AddManagementExpenseRateForHR", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin İk maliyetinin işletme gideri oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("_PartialOfferPositions", model);
        }

        public PartialViewResult AddManagementExpenseRateForProduct(int? offerID, string profitRate) //malzeme/stok icin isletme gideri orani ekleme 
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerID).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.ManagementExpenseRateProduct = profitrate;
                Db.SaveChanges();
            }
            calculateFooter(checkoffer.ID);
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == offerID).ToList();
            OypaHelper.AddApplicationLog("Management", "AddManagementExpenseRateForProduct", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin malzeme stok maliyetinin işletme gideri oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("_PartialOfferRows", model);
        }

        public PartialViewResult AddManagementExpenseRateForOther(int? offerID, string profitRate) //diger giderler icin isletme gideri orani ekleme 
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerID).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.ManagementExpenseRateOther = profitrate;
                Db.SaveChanges();
            }
            calculateFooter(checkoffer.ID);
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
            model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == offerID).OrderBy(x => x.PositionItemID).ToList();
            model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == checkoffer.ID).ToList();
            OypaHelper.AddApplicationLog("Management", "AddManagementExpenseRateForOther", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin diğer maliyetlerinin işletme gideri oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("_PartialOfferOthers", model);
        }

        public PartialViewResult AddManagementExpenseRateForMachine(int? offerID, string profitRate) //makine/demirbas icin isletme gideri orani ekleme 
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerID).FirstOrDefault();
            double? profitrate = !string.IsNullOrEmpty(profitRate) ? Convert.ToDouble(profitRate.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
            if (checkoffer != null)
            {
                checkoffer.ManagementExpenseRateMD = profitrate;
                Db.SaveChanges();
            }
            calculateFooter(checkoffer.ID);
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
            model.offerequipments = Db.OfferEquipment.Where(i => i.OfferID == checkoffer.ID).ToList();

            OypaHelper.AddApplicationLog("Management", "AddManagementExpenseRateForMachine", "Offer", checkoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklifin makine demirbaş maliyetinin işletme gideri oranını % {(profitrate != null ? profitrate : 0)} olarak belirledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("_PartialOfferRowForMachine", model);
        }

        public ActionResult AddOfferOthersRow(int? offerID, string description, string price)
        {
            OfferControlModel model = new OfferControlModel();
            var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerID).FirstOrDefault();
            if (checkoffer != null)
            {
                OfferOtherExpenses otherExpenses = new OfferOtherExpenses();
                otherExpenses.Price = Convert.ToDouble(price);
                otherExpenses.Description = description;
                otherExpenses.IsActive = true;
                otherExpenses.OfferID = checkoffer.ID;
                otherExpenses.RecordDate = DateTime.Now;
                otherExpenses.RecordUserID = OypaHelper.RecordEmployee();
                Db.OfferOtherExpenses.Add(otherExpenses);

                if (checkoffer.TotalOtherExpense == null)
                {
                    checkoffer.TotalOtherExpense = 0;
                }
                checkoffer.TotalOtherExpense += otherExpenses.Price;
                Db.SaveChanges();
                calculateFooter(checkoffer.ID);
                model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == checkoffer.ID).ToList();

                OypaHelper.AddApplicationLog("Management", "AddOfferOthersRow", "Offer", checkoffer.ID, "OfferOtherExpenses", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklife {description} isimli diğer maliyet kalemini ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return PartialView("_PartialOfferOthers", model);
            }
            else
            {
                OypaHelper.AddApplicationLog("Management", "AddOfferOthersRow", "Offer", 0, "OfferOtherExpenses", false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {offerID} id li teklife {description} isimli diğer maliyet kalemini eklerken hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return Json(new
                {
                    failed = true,
                    message = "Satır eklenemedi, Lütfen tekrar deneyiniz.."
                });
            }

        }

        public ActionResult RemoveOfferOthersRow(int? rowID)
        {
            OfferControlModel model = new OfferControlModel();
            var row = Db.OfferOtherExpenses.Find(rowID);

            if (row != null)
            {
                var checkoffer = Db.Offer.Where(t => t.IsActive == true && t.ID == row.OfferID).FirstOrDefault();
                //checkoffer.GenelToplam -= row.Price;
                checkoffer.TotalOtherExpense -= row.Price;
                row.IsActive = false;
                Db.SaveChanges();
                calculateFooter(checkoffer.ID);
                model.OtherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == row.OfferID).ToList();

                OypaHelper.AddApplicationLog("Management", "RemoveOfferOthersRow", "Offer", checkoffer.ID, "OfferOtherExpenses", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkoffer.OfferNumber} nolu teklife {row.Description} isimli diğer maliyet kalemini sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return PartialView("_PartialOfferOthers", model);
            }
            else
            {
                OypaHelper.AddApplicationLog("Management", "RemoveOfferOthersRow", "Offer", 0, "OfferOtherExpenses", false, $"{rowID} id li diğer malitet kalemi bulunamdı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return Json(new
                {
                    failed = true,
                    message = "Satır silinemedi, Lütfen tekrar deneyiniz.."
                });
            }

        }

        [HttpPost]
        public ActionResult AddDocument(HttpPostedFileBase[] uploadFiles, int? offerID)
        {
            //int offerid = Convert.ToInt32(offerID);
            var selectedOffer = Db.Offer.Where(t => t.ID == offerID).FirstOrDefault();
            if (uploadFiles != null && uploadFiles.Count() > 0)
            {
                foreach (var uploadFile in uploadFiles)
                {
                    if (uploadFile != null)
                    {
                        Documents doc = new Documents();
                        Guid g = Guid.NewGuid();
                        var fileName = Path.GetFileName(g.ToString() + uploadFile.FileName);
                        //Dosya belirtilen yola kaydediliyor.
                        var filePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadData/");
                        uploadFile.SaveAs(Path.Combine(filePath, fileName));
                        doc.CategoryID = 1;
                        doc.DocumentName = Path.GetFileName(fileName);
                        doc.FileName = Path.GetFileName(fileName);
                        doc.FilePath = Path.Combine(filePath, fileName);
                        doc.OpportunityID = selectedOffer.OpportunityID;
                        doc.OfferID = selectedOffer.ID;
                        doc.Date = DateTime.Now;
                        doc.RecordDate = DateTime.Now;
                        doc.TypeID = null;
                        doc.OfferID = null;
                        doc.RecordEmployee = OypaHelper.RecordEmployee();
                        doc.RecordIP = HttpContext.Request.UserHostAddress;
                        doc.IsActive = true;
                        doc.ProjectID = selectedOffer.ProjectID;
                        Db.Documents.Add(doc);
                        Db.SaveChanges();

                        OypaHelper.AddApplicationLog("Management", "AddDocument", "Offer", selectedOffer.ID, "OfferOtherExpenses", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer.OfferNumber} nolu teklife {fileName} isimli dokümanı ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    }
                }

            }
            return RedirectToAction("Detail", new { OpportunityID = selectedOffer.OpportunityID, AutoOpportunity = 1 });
        }
        //malzeme stok sorgulama
        public PartialViewResult SearchProductPartial(string id, string catid)
        {
            OfferControlModel model = new OfferControlModel();

            int CatID = Convert.ToInt32(catid);
            model.productlist = Db.Products.Where(x => x.ProductName.Contains(id) && x.CategoryID == CatID).ToList();

            OypaHelper.AddApplicationLog("Management", "SearchProductPartial", "Offer", CatID, "Products", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı{CatID} id li kategoride  {id} stok kodlu ürünü sorguladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("_PartialProductSearch", model);
        }

        //Makina/demirbaş ürün sorgulama
        public PartialViewResult SearchProductPartialForMachine(string id, string catid)
        {
            OfferControlModel model = new OfferControlModel();

            int CatID = Convert.ToInt32(catid);
            model.productlist = Db.Products.Where(x => x.ProductName.Contains(id) && x.CategoryID == CatID).ToList();

            OypaHelper.AddApplicationLog("Management", "SearchProductPartialForMachine", "Offer", CatID, "Products", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı{CatID} id li kategoride  {id} stok kodlu ürünü sorguladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("_PartialProductSearchForMachine", model);
        }

        [HttpPost]
        public PartialViewResult AddProduct(int? id, int? OfferID)
        {
            OfferControlModel model = new OfferControlModel();



            var products = Db.Products.FirstOrDefault(x => x.ID == id);
            var IsExistOfferProduct = Db.OfferProducts.Where(i => i.ProductID == id && i.OfferID == OfferID).FirstOrDefault();
            if (IsExistOfferProduct != null)
            {
                IsExistOfferProduct.Quantity = IsExistOfferProduct.Quantity + 1;
                Db.SaveChanges();
            }
            else
            {
                OfferProducts offerproduct = new OfferProducts()
                {
                    ProductID = products.ID,
                    Currency = products.Currency,
                    Unit = products.Unit,
                    UnitPrice = products?.UnitPrice ?? 0,
                    CostPrice = 0,
                    OfferID = OfferID,
                    Quantity = 1
                };

                Db.OfferProducts.Add(offerproduct);
                Db.SaveChanges();
            };


            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == OfferID).ToList();


            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == OfferID);
            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == OfferID).ToList();

            OypaHelper.AddApplicationLog("Management", "AddProduct", "Offer", model.singleoffer.ID, "OfferProducts", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı{model.singleoffer.OfferNumber} nolu teklifin malzeme stok maliyetine {products.ProductName} isimli ürünü ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);


            return PartialView("_PartialOfferRows", model);

        }

        public PartialViewResult AddProductForMachine(int? id, int? OfferID)
        {
            OfferControlModel model = new OfferControlModel();

            var products = Db.Products.FirstOrDefault(x => x.ID == id);
            var IsExistOfferEquipment = Db.OfferEquipment.Where(i => i.ProductID == id && i.OfferID == OfferID).FirstOrDefault();
            if (IsExistOfferEquipment != null)
            {
                IsExistOfferEquipment.Quantity = IsExistOfferEquipment.Quantity + 1;
                Db.SaveChanges();
            }
            else
            {
                OfferEquipment offerEquipment = new OfferEquipment()
                {
                    ProductID = products.ID,
                    Currency = products.Currency,
                    Unit = products.Unit,
                    UnitPrice = products?.UnitPrice ?? 0,
                    CostPrice = 0,
                    OfferID = OfferID,
                    Quantity = 1

                    //TODO : amortisman ve aylik amortisman degerleri sonradan atilacak(Depreciation)
                };

                Db.OfferEquipment.Add(offerEquipment);
                Db.SaveChanges();
            };

            //calculateFooter(1);  // burası dinamik olacak offer header id gelecek 
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == OfferID);
            model.offerequipments = Db.OfferEquipment.Where(x => x.OfferID == OfferID).ToList();

            OypaHelper.AddApplicationLog("Management", "AddProductForMachine", "Offer", model.singleoffer.ID, "OfferEquipment", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı{model.singleoffer.OfferNumber} nolu teklifin makine demirbaş maliyetine {products.ProductName} isimli ürünü ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("_PartialOfferRowForMachine", model);

        }

        public PartialViewResult RemoveOfferRow(int? rowid)
        {
            OfferControlModel model = new OfferControlModel();
            int? offerID = 1;
            string pname = string.Empty;
            using (OypaDbEntities db = new OypaDbEntities())
            {
                var row = db.OfferProducts.FirstOrDefault(x => x.ID == rowid);

                if (row != null)
                {
                    pname = row.Products.ProductName;
                    offerID = row.OfferID;

                    db.OfferProducts.Remove(row);
                    db.SaveChanges();
                }


                calculateFooter(offerID);
            }


            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == offerID).ToList();
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);

            OypaHelper.AddApplicationLog("Management", "RemoveOfferRow", "Offer", model.singleoffer.ID, "OfferProducts", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı{model.singleoffer.OfferNumber} nolu teklifin malzeme stok maliyetinde yer alan {pname} isimli ürünü sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);


            return PartialView("_PartialOfferRows", model);


        }

        public PartialViewResult RemoveOfferRowForMachine(int? rowid)
        {
            OfferControlModel model = new OfferControlModel();
            int? offerID = 1;
            string pname = string.Empty;
            using (OypaDbEntities db = new OypaDbEntities())
            {
                var row = db.OfferEquipment.FirstOrDefault(x => x.ID == rowid);

                if (row != null)
                {
                    pname = row.Products.ProductName;
                    offerID = row.OfferID;

                    db.OfferEquipment.Remove(row);
                    db.SaveChanges();
                }

                calculateFooter(offerID);
            }

            model.offerequipments = Db.OfferEquipment.Where(x => x.OfferID == offerID).ToList();
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);


            OypaHelper.AddApplicationLog("Management", "RemoveOfferRow", "Offer", model.singleoffer.ID, "OfferProducts", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı{model.singleoffer.OfferNumber} nolu teklifin makine demirbaş maliyetinde yer alan {pname} isimli ürünü sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("_PartialOfferRowForMachine", model);


        }

        public PartialViewResult RemoveOfferPositionsRow(int? rowid, int? OfferID)
        {
            OfferControlModel model = new OfferControlModel();


            var selectedOffer = Db.Offer.FirstOrDefault(t => t.ID == OfferID);
            if (selectedOffer != null)
            {
                if (selectedOffer.IsOneTime == true) //Süreli teklif
                {
                    var row = Db.OfferPositions.FirstOrDefault(x => x.ID == rowid);
                    var positionName = row.Positions.PositionName;
                    var status = row.Positions.PositionStatus.StatusName;
                    if (row != null)
                    {

                        var rowsdelete = Db.OfferPositions.Where(x => x.OfferID == row.OfferID && x.PositionID == row.PositionID).ToList();

                        Db.OfferPositions.RemoveRange(rowsdelete);
                        Db.SaveChanges();

                        var HrProfiItems = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == selectedOffer.ID).Select(t => t.PositionItemID).ToList();
                        HrCalculateTotalProfit(HrProfiItems, selectedOffer.ManagementExpenseRateHR, selectedOffer.BrutToplamProfitRate, selectedOffer.ID);
                        calculateFooter(OfferID);

                        //model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == OfferID).ToList();
                        model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == OfferID).OrderBy(t => t.PositionItemID).ToList();
                        model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == OfferID);

                        model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                        model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();

                        OypaHelper.AddApplicationLog("Management", "RemoveOfferPositionsRow", "Offer", selectedOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin ik maliyetinde yer alan pozisyonu {positionName}-{status} olan personeli sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return PartialView("_PartialOfferPositions", model);
                    }
                }
                else //Tekseferlik teklif 
                {
                    var row = Db.IsOneTimeOfferPositions.FirstOrDefault(x => x.ID == rowid);
                    if (row != null)
                    {
                        var positionName = row.Positions.PositionName;
                        var status = row.Positions.PositionStatus.StatusName;
                        var rowsdelete = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == row.OfferID && x.PositionID == row.PositionID).ToList();

                        Db.IsOneTimeOfferPositions.RemoveRange(rowsdelete);
                        Db.SaveChanges();

                        calculateFooter(OfferID);

                        //model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == OfferID).ToList();
                        model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == OfferID).ToList();
                        model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == OfferID);

                        //Toplu hesaplama için eklenmiştir.
                        //model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                        //model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                        OypaHelper.AddApplicationLog("Management", "RemoveOfferPositionsRow", "Offer", selectedOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin ik maliyetinde yer alan pozisyonu {positionName}-{status} olan personeli sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        return PartialView("_PartialOfferPosIsOneTime", model);
                    }
                }


            }
            return PartialView("_PartialOfferPositions", model);
        }
        //Eklenen personel hesaplama işlemi
        //Personelin brut maaşına göre tüm ik kalemlerinin hesaplandığı metottur.
        [AllowAnonymous]
        public PartialViewResult CalculateBrutToNet(int? rowid, string brut, int? quantity)
        {
            OfferControlModel model = new OfferControlModel();
            var OfferID = 0;

            if (rowid > 0 && !string.IsNullOrEmpty(brut))
            {

                using (OypaDbEntities db = new OypaDbEntities())
                {
                    var offerposition = db.OfferPositions.FirstOrDefault(x => x.ID == rowid);
                    var offer = db.Offer.FirstOrDefault(x => x.ID == offerposition.OfferID);
                    OfferID = offer.ID;
                    var position = db.Positions.FirstOrDefault(x => x.ID == offerposition.PositionID);
                    var positionitem = db.PositionItems.FirstOrDefault(x => x.ID == offerposition.PositionItemID);

                    var oldBrut = db.OfferPositions.Where(t => t.OfferID == OfferID && t.PositionID == offerposition.PositionID && t.PositionItemID == 1).FirstOrDefault().UnitPrice;

                    var oldQuantity = db.OfferPositions.Where(t => t.OfferID == OfferID && t.PositionID == offerposition.PositionID && t.PositionItemID == 1).FirstOrDefault().Quantity;

                    var positonGvOrani = db.OfferPositions.Where(t => t.OfferID == OfferID && t.PositionID == offerposition.PositionID && t.PositionItemID == 15).FirstOrDefault().Value;



                    //double brutUcret = Convert.ToDouble(brut.Replace(".", ","));
                    double? brutUcret = !string.IsNullOrEmpty(brut) ? Convert.ToDouble(brut.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;


                    int status = position.StatusID.Value;

                    SalaryManager sm = new SalaryManager(status, brutUcret.Value, positonGvOrani);


                    var salaryModel = sm.GetSalary();
                    if (offerposition != null && salaryModel != null)
                    {

                        if (oldQuantity != quantity || oldBrut != brutUcret)
                        {
                            var opsiyonelItemsValues = db.OfferPositions.Where(t => t.OfferID == OfferID && t.PositionID == offerposition.PositionID).ToList();
                            CalculateAllPrice cap = new CalculateAllPrice();
                            foreach (var item in opsiyonelItemsValues)
                            {
                                if (item.PositionItemID == 4)
                                {
                                    cap.YillikIzin = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 5)
                                {
                                    cap.KidemTaz = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 15)
                                {
                                    cap.GvOrani = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 7)
                                {
                                    cap.BayramMesai = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 6)
                                {
                                    if (item.PositionOptions != null)
                                    {

                                        cap.IhbarTaz = item.PositionOptions.OptionName;
                                    }
                                    else
                                    {
                                        cap.IhbarTaz = null;
                                    }
                                }
                                else if (item.PositionItemID == 9)
                                {
                                    cap.Yol = item.Value.ToString();
                                    if (item.PositionOptions != null)
                                    {
                                        cap.RadioYol = item.PositionOptions.OptionName;
                                    }
                                }
                                else if (item.PositionItemID == 10)
                                {
                                    cap.Yemek = item.Value.ToString();
                                    if (item.PositionOptions != null)
                                    {
                                        cap.RadioYemek = item.PositionOptions.OptionName;
                                    }
                                }
                                else if (item.PositionItemID == 11)
                                {
                                    cap.SaglikGideri = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 17)
                                {
                                    cap.IsgGideri = item.Value.ToString();
                                }
                                // Diğer tabının içerisine taşınacak!!
                                //else if (item.PositionItemID == 18) 
                                //{
                                //    cap.PoliceGideri = item.Value.ToString();
                                //}
                                //else if (item.PositionItemID == 19)
                                //{
                                //    cap.IhaleBelgeMasrafi = item.Value.ToString();
                                //}
                                else if (item.PositionItemID == 20)
                                {
                                    cap.EgitimBelgeGideri = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 21)
                                {
                                    cap.FazlaMesai = item.Value.ToString();
                                    if (item.PositionOptions != null)
                                    {

                                        cap.radioFazlaMesai = item.PositionOptions.OptionName;
                                    }
                                    else
                                    {
                                        cap.radioFazlaMesai = null;
                                    }

                                }
                                else if (item.PositionItemID == 22)
                                {
                                    cap.SaglikSigortasi = item.Value.ToString();
                                }
                                else if (item.PositionItemID == 23)
                                {
                                    cap.BireyselEmeklilik = item.Value.ToString();
                                }
                            }
                            cap.rowID = offerposition.ID;
                            cap.OfferID = offer.ID;
                            //CalculateOpsiyonel(cap, brutUcret.ToString(), offerposition, offer, position, quantity);
                            CalculateAllPersonelPrice(cap, brutUcret.ToString(), offerposition, offer, position, quantity);

                        }
                        var HrProfiItems = db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == offer.ID).Select(t => t.PositionItemID).ToList();
                        HrCalculateTotalProfit(HrProfiItems, offer.ManagementExpenseRateHR, offer.BrutToplamProfitRate, offer.ID);
                        calculateFooter(OfferID);


                        OypaHelper.AddApplicationLog("Management", "CalculateBrutToNet", "Offer", offer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {offer.OfferNumber} nolu teklifin İk maliyetinde yer alan pozisyonu {position.PositionName} - {position.PositionStatus.StatusName} olan personelin ücret detaylarını hesapladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    }

                }
            }

            model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == OfferID).OrderBy(x => x.PositionItemID).ToList();
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == OfferID);

            return PartialView("_PartialOfferPositions", model);
        }

        public PartialViewResult SearchPositionsPartial(string id, int? offerid)
        {
            OfferControlModel model = new OfferControlModel();

            model.positions = Db.Positions.Where(x => x.PositionName.Contains(id) && x.IsActive == true).ToList();
            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerid);


            return PartialView("_PartialPositionSearch", model);
        }


        public ActionResult AddPosition(string titleName, string statusID, int offerID)
        {
            OfferControlModel model = new OfferControlModel();
            var id = Convert.ToInt32(statusID);
            var checkPositions = Db.Positions.FirstOrDefault(t => t.PositionName == titleName.Trim() && t.StatusID == id && t.IsActive == true);

            if (checkPositions == null)
            {
                Positions p = new Positions()
                {
                    PositionName = titleName,
                    StatusID = id,
                    IsActive = true
                };

                Db.Positions.Add(p);
                Db.SaveChanges();
                model.positions = Db.Positions.Include(x => x.PositionStatus).Where(x => x.IsActive == true).ToList();
                model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
                //model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == offerID).ToList();
                //model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == offerID).ToList();

                OypaHelper.AddApplicationLog("Management", "AddPosition", "Offer", model.singleoffer.ID, "Positions", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı uygulamaya pozisyonu {p.PositionName} - {p.PositionStatus.StatusName} olan yeni bir personel ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return PartialView("_PartialPositionSearch", model);
            }
            else
            {
                return Json(new { result = "Error" }, JsonRequestBehavior.AllowGet);
            }


            //Db.Configuration.LazyLoadingEnabled = false;



        }

        //İK personel ekleme

        public ActionResult AddPositionsOffer(int? id, int? offerid)
        {

            OfferControlModel model = new OfferControlModel();

            using (OypaDbEntities db = new OypaDbEntities())
            {
                var offer = db.Offer.FirstOrDefault(x => x.ID == offerid);
                model.singleoffer = offer;
                if (model.singleoffer.IsOneTime == true) // süreli teklif ise
                {

                    var position = db.Positions.FirstOrDefault(x => x.ID == id);
                    var PositionsItem = db.PositionItems.Where(i => i.ParentID == 0 && i.IsActive == true).ToList();

                    var checkPosition = db.OfferPositions.FirstOrDefault(t => t.OfferID == offerid && t.PositionID == id);
                    if (checkPosition != null)
                    {
                        return Json(new { result = "Fail", message = "Daha önce eklenmiş pozisyon!" });
                    }

                    List<OfferPositions> offerposlist = new List<OfferPositions>();

                    foreach (var item in PositionsItem)
                    {
                        OfferPositions row = new OfferPositions()
                        {
                            OfferID = offerid,
                            PositionItemID = item.ID,
                            Currency = "TRL",
                            Quantity = 1,
                            PositionID = id,
                            CostPrice = 0,
                            UnitPrice = 0,
                            Value = 0
                        };
                        offerposlist.Add(row);
                    }

                    db.OfferPositions.AddRange(offerposlist);
                    db.SaveChanges();

                    model.offerpositions = Db.OfferPositions.Where(x => x.OfferID == offerid).ToList();

                    model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                    model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();

                    //calculateFooter(1);  // burası dinamik olacak offer header id gelecek


                    OypaHelper.AddApplicationLog("Management", "AddPositionsOffer", "Offer", model.singleoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin ik maliyetine poziyonu {position.PositionName}-{position.PositionStatus.StatusName} olan bir personel ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);



                    return PartialView("_PartialOfferPositions", model);


                }
                else // tek seferlik teklif ise
                {
                    var position = db.Positions.FirstOrDefault(x => x.ID == id); //Hangi meslek grubu
                    var PositionsItem = db.IsOneTimePositionItems.Where(t => t.IsActive == true).ToList();

                    var checkPosition = Db.IsOneTimeOfferPositions.FirstOrDefault(t => t.OfferID == offerid && t.PositionID == id);
                    if (checkPosition != null)
                    {

                        return Json(new { result = "Fail", message = "Daha önce eklenmiş pozisyon!" });
                    }

                    List<IsOneTimeOfferPositions> IsOneTimeofferposlist = new List<IsOneTimeOfferPositions>();

                    foreach (var item in PositionsItem)
                    {
                        IsOneTimeOfferPositions row = new IsOneTimeOfferPositions()
                        {
                            OfferID = model.singleoffer.ID,
                            PositionItemID = item.ID,
                            Currency = "TRL",
                            Quantity = 1,
                            PositionID = id,
                            WorkingHour = 0,
                            OvertimePay = 0,
                            Price = 0


                        };
                        IsOneTimeofferposlist.Add(row);
                    }
                    db.IsOneTimeOfferPositions.AddRange(IsOneTimeofferposlist);
                    db.SaveChanges();



                    //calculateFooter(model.singleoffer.ID);  // burası dinamik olacak offer header id gelecek

                    model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == offerid).ToList();

                    //model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                    model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();

                    OypaHelper.AddApplicationLog("Management", "AddPositionsOffer", "Offer", model.singleoffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin ik maliyetine poziyonu {position.PositionName}-{position.PositionStatus.StatusName} olan bir personel ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return PartialView("_PartialOfferPosIsOneTime", model);
                }

            };



        }


        public PartialViewResult UpdateOfferRow(int? rowid, string productName, int? quantity, string unit, string unitPrice, string costPrice)
        {
            OfferControlModel model = new OfferControlModel();

            int? offerID = 1;
            var pname = string.Empty;
            using (OypaDbEntities db = new OypaDbEntities())
            {

                var row = db.OfferProducts.FirstOrDefault(x => x.ID == rowid);

                if (row != null)
                {
                    offerID = row.Offer.ID;



                    row.Products.ProductName = productName;
                    pname = productName;
                    row.Quantity = quantity;


                    if (string.IsNullOrEmpty(unitPrice))
                    {
                        unitPrice = "0";
                    }
                    if (string.IsNullOrEmpty(costPrice))
                    {
                        costPrice = "0";
                    }
                    if (quantity == null)
                    {
                        quantity = 1;
                    }
                    var unitpricecast = Convert.ToDouble(unitPrice);
                    var costpricecast = Convert.ToDouble(costPrice);
                    row.UnitPrice = unitpricecast;
                    row.CostPrice = costpricecast;

                    db.SaveChanges();
                }


                calculateFooter(row.OfferID);



            }


            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == offerID).ToList();

            OypaHelper.AddApplicationLog("Management", "UpdateOfferRow", "Offer", model.singleoffer.ID, "OfferProducts", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin malzeme stok maliyetinde yer alan {pname} isimli ürünün değerlerini güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("_PartialOfferRows", model);

        }

        public PartialViewResult CalculateIsOneTimeOfferPosition(int RowID, int Quantity, string WorkingHour, string OvertimePay)
        {
            OfferControlModel model = new OfferControlModel();
            var checkOfferPos = Db.IsOneTimeOfferPositions.FirstOrDefault(t => t.ID == RowID);
            var offer = Db.Offer.FirstOrDefault(t => t.ID == checkOfferPos.OfferID);
            if (WorkingHour != null && OvertimePay != null && RowID > 0)
            {


                double? workingHour = !string.IsNullOrEmpty(WorkingHour) ? Convert.ToDouble(WorkingHour.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;


                double? overtimePay = !string.IsNullOrEmpty(OvertimePay) ? Convert.ToDouble(OvertimePay.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;



                var ToplamMesaiUcreti = Db.IsOneTimeOfferPositions.FirstOrDefault(t => t.OfferID == offer.ID && t.PositionID == checkOfferPos.PositionID && t.PositionItemID == 1);
                if (ToplamMesaiUcreti != null)
                {
                    ToplamMesaiUcreti.Quantity = Quantity;
                    ToplamMesaiUcreti.Price = (workingHour * overtimePay) * Quantity;
                    ToplamMesaiUcreti.WorkingHour = workingHour;
                    ToplamMesaiUcreti.OvertimePay = overtimePay;


                }


                var isverenMaliyeti = Db.IsOneTimeOfferPositions.FirstOrDefault(t => t.OfferID == offer.ID && t.PositionID == checkOfferPos.PositionID && t.PositionItemID == 2);
                if (isverenMaliyeti != null)
                {
                    isverenMaliyeti.Quantity = Quantity;
                    var res = (((workingHour * overtimePay) * Quantity) * 0.225);
                    isverenMaliyeti.Price = ((workingHour * overtimePay) * Quantity) + res;
                    isverenMaliyeti.WorkingHour = workingHour;
                    isverenMaliyeti.OvertimePay = overtimePay;


                }
                Db.SaveChanges();
                calculateFooter(offer.ID);
                model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == offer.ID).ToList();
                model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                model.IsOneTimeOfferOtherExpenseItems = Db.IsOneTimeOfferOtherExpenseItems.Where(t => t.IsActive == true).ToList();
                model.IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == offer.ID).ToList();

                OypaHelper.AddApplicationLog("Management", "CalculateIsOneTimeOfferPosition", "Offer", offer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {offer.OfferNumber} nolu teklifin İk maliyetinde yer alan pozisyonu {checkOfferPos.Positions.PositionName} olan personelin ücret detaylarını hesapladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            }


            return PartialView("~/Views/Offer/_PartialOfferPosIsOneTime.cshtml", model);
        }

        public PartialViewResult CalculateIsOneTimeOtherExpense(int OfferID, string FoodExpense, string RoadExpense)
        {
            OfferControlModel model = new OfferControlModel();

            var checkOffer = Db.Offer.FirstOrDefault(t => t.ID == OfferID);
            if (checkOffer != null)
            {
                double? foodExpense = !string.IsNullOrEmpty(FoodExpense) ? Convert.ToDouble(FoodExpense.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                double? roadExpense = !string.IsNullOrEmpty(RoadExpense) ? Convert.ToDouble(RoadExpense.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                var checkRoadExpense = Db.IsOneTimeOtherExpense.FirstOrDefault(t => t.OfferID == checkOffer.ID && t.OtherExpenseItemID == 1);//yol
                var checkFoodExpense = Db.IsOneTimeOtherExpense.FirstOrDefault(t => t.OfferID == checkOffer.ID && t.OtherExpenseItemID == 2);//yemek

                if (checkRoadExpense == null)
                {
                    IsOneTimeOtherExpense Offerex = new IsOneTimeOtherExpense()
                    {
                        OfferID = checkOffer.ID,
                        OtherExpenseItemID = 1,
                        Price = roadExpense
                    };
                    Db.IsOneTimeOtherExpense.Add(Offerex);
                    Db.SaveChanges();
                }
                else
                {
                    checkRoadExpense.Price = roadExpense;
                    Db.SaveChanges();
                }

                if (checkFoodExpense == null)
                {
                    IsOneTimeOtherExpense Offerex = new IsOneTimeOtherExpense()
                    {
                        OfferID = checkOffer.ID,
                        OtherExpenseItemID = 2,
                        Price = foodExpense
                    };
                    Db.IsOneTimeOtherExpense.Add(Offerex);
                    Db.SaveChanges();
                }
                else
                {
                    checkFoodExpense.Price = foodExpense;
                    Db.SaveChanges();
                }

            }
            calculateFooter(checkOffer.ID);
            model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == checkOffer.ID).ToList();
            model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
            model.IsOneTimeOfferOtherExpenseItems = Db.IsOneTimeOfferOtherExpenseItems.Where(t => t.IsActive == true).ToList();
            model.IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == checkOffer.ID).ToList();

            OypaHelper.AddApplicationLog("Management", "CalculateIsOneTimeOtherExpense", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkOffer.OfferNumber} nolu teklifin diğer maliyetlerini hesapladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Offer/_PartialIsOneTimeOfferOthers.cshtml", model);
        }
        public PartialViewResult IsOneTimeCalculateOpsiyonel(string FoodExpense, string RoadExpense, int RowID)
        {
            OfferControlModel model = new OfferControlModel();
            var offePositions = Db.IsOneTimeOfferPositions.FirstOrDefault(t => t.ID == RowID);
            if (!string.IsNullOrEmpty(FoodExpense) && !string.IsNullOrEmpty(RoadExpense))
            {
                var quantity = offePositions.Quantity;
                double? foodExpense = !string.IsNullOrEmpty(FoodExpense) ? Convert.ToDouble(FoodExpense.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                double? roadExpense = !string.IsNullOrEmpty(RoadExpense) ? Convert.ToDouble(RoadExpense.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                IsOneTimeCalculate cal = new IsOneTimeCalculate()
                {
                    RowID = RowID,
                    Quantity = quantity.Value,
                    OveritmePay = null,
                    WorkingHour = null,
                    FoodExpense = foodExpense.ToString(),
                    RoadExpense = roadExpense.ToString(),

                };
                // IsOneTimeOfferCalculateIK(cal);

            }
            calculateFooter(offePositions.OfferID);
            model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == offePositions.OfferID).ToList();
            model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();

            OypaHelper.AddApplicationLog("Management", "IsOneTimeCalculateOpsiyonel", "Offer", offePositions.OfferID.Value, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {offePositions.OfferID} id li teklifin opsiyonel maliyetlerini hesapladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
            return PartialView("~/Views/Offer/_PartialOfferPosIsOneTime.cshtml", model);
        }



        public PartialViewResult UpdateOfferRowForMachine(int? rowid, int? quantity, string unit, string unitPrice, string costPrice, /*string depreciation,*/ string monthCount)
        {
            OfferControlModel model = new OfferControlModel();

            int? offerID = 1;
            var pname = string.Empty;
            using (OypaDbEntities db = new OypaDbEntities())
            {

                var row = db.OfferEquipment.FirstOrDefault(x => x.ID == rowid);

                if (row != null)
                {
                    offerID = row.Offer.ID;
                    pname = row.Products.ProductName;

                    row.Quantity = quantity;
                    if (string.IsNullOrWhiteSpace(unitPrice))
                    {
                        unitPrice = "0";
                    }
                    if (string.IsNullOrWhiteSpace(costPrice))
                    {
                        costPrice = "0";
                    }
                    if (quantity == null)
                    {
                        quantity = 1;
                    }
                    var unitpricecast = Convert.ToDouble(unitPrice);
                    var costpricecast = Convert.ToDouble(costPrice);
                    row.UnitPrice = unitpricecast;
                    row.CostPrice = costpricecast;

                    row.Depreciation = row.CostPrice * row.Quantity;
                    //if (depreciation != "")
                    //{
                    //    row.Depreciation = Convert.ToDouble(depreciation);
                    //}
                    //else
                    //{
                    //    row.Depreciation = 0;
                    //}
                    if (string.IsNullOrWhiteSpace(monthCount))
                    {
                        monthCount = "1";
                    }

                    row.MonthCount = Convert.ToInt32(monthCount);
                    if (row.MonthCount == 0)
                    {
                        row.MonthCount = 1;
                    }
                    row.MonthlyDepreciation = (row.Depreciation / row.MonthCount);
                    row.Unit = unit;
                    row.MonthCount = row.MonthCount;
                    db.SaveChanges();
                }

                calculateFooter(row.OfferID);

            }


            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerID);
            model.offerequipments = Db.OfferEquipment.Where(x => x.OfferID == offerID).ToList();

            OypaHelper.AddApplicationLog("Management", "UpdateOfferRowForMachine", "Offer", model.singleoffer.ID, "OfferEquipment", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.singleoffer.OfferNumber} nolu teklifin makine demirbaş maliyetinde yer alan {pname} isimli ürünün değerlerini güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("_PartialOfferRowForMachine", model);

        }

        public void calculateFooter(int? offerID)
        {
            using (OypaDbEntities db = new OypaDbEntities())
            {
                var OfferRows = db.OfferProducts.Where(x => x.OfferID == offerID).ToList();
                var offerOtherRows = db.OfferOtherExpenses.Where(i => i.OfferID == offerID && i.IsActive == true).ToList();
                var offerEquipments = db.OfferEquipment.Where(i => i.OfferID == offerID).ToList();

                var offer = db.Offer.FirstOrDefault(x => x.ID == offerID);

                if (offer.IsOneTime == true) //True ise süreli teklif
                {
                    offer.AraToplam = OfferRows.Sum(x => (x.CostPrice * x.Quantity)); // Malzeme Stok 
                    double? sumAraToplamProfitPrice = 0;
                    if (offer.AraToplamProfitRate != null)
                    {
                        sumAraToplamProfitPrice += offer.AraToplamProfitRate;
                    }
                    if (offer.ManagementExpenseRateProduct != null)
                    {
                        sumAraToplamProfitPrice += offer.ManagementExpenseRateProduct;
                    }
                    if (offer.AraToplamProfitRate == null && offer.ManagementExpenseRateProduct == null)
                    {
                        offer.SumAraToplamProfit = 0;
                    }
                    offer.SumAraToplamProfit = (offer.AraToplam * (sumAraToplamProfitPrice / 100));


                    // diger giderler icin toplam ve oranlar ile hesaplamalar..
                    offer.TotalOtherExpense = offerOtherRows.Sum(i => i.Price);
                    double? sumOtherExpenseProfitPrice = 0;
                    if (offer.ManagementExpenseRateOther != null)
                    {
                        sumOtherExpenseProfitPrice += offer.ManagementExpenseRateOther;
                    }
                    if (offer.ProfitRateForOthers != null)
                    {
                        sumOtherExpenseProfitPrice += offer.ProfitRateForOthers;
                    }
                    if (offer.ManagementExpenseRateOther == null && offer.ProfitRateForOthers == null)
                    {
                        offer.SumOtherExpenseProfit = 0;
                    }
                    offer.SumOtherExpenseProfit = (offer.TotalOtherExpense * (sumOtherExpenseProfitPrice / 100));

                    // makine/demirbas icin toplam ve oranlar ile hesaplamalar..
                    offer.TotalMachineExpense = db.OfferEquipment.Where(i => i.OfferID == offer.ID).Sum(i => (i.CostPrice * i.Quantity));
                    offer.TotalMonthlyDepreciation = offerEquipments.Sum(i => i.MonthlyDepreciation);
                    double? sumMachineExpenseProfitPrice = 0;
                    if (offer.TotalMachineExpense == null)
                    {
                        offer.TotalMachineExpense = 0;
                    }
                    if (offer.ManagementExpenseRateMD != null) // makine/demirbas icin isletme gideri orani 
                    {
                        sumMachineExpenseProfitPrice += offer.ManagementExpenseRateMD;
                    }
                    if (offer.ProfitRateForMachine != null)  // makine/demirbas icin karlilik orani 
                    {
                        sumMachineExpenseProfitPrice += offer.ProfitRateForMachine;
                    }
                    if (offer.ManagementExpenseRateMD == null && offer.ProfitRateForMachine == null)
                    {
                        offer.SumMachineExpenseProfit = 0;
                    }

                    if (offer.TotalHrProfit == null)
                    {
                        offer.TotalHrProfit = 0;
                    }

                    offer.SumMachineExpenseProfit = (offer.TotalMachineExpense * (sumMachineExpenseProfitPrice / 100));
                    //Aylik Amortisman fiyatınada karlılık oranı eklenecek!
                    offer.TotalMonthlyDepreciation += (offer.TotalMonthlyDepreciation * (sumMachineExpenseProfitPrice / 100));
                    // amortisman toplam ucreti

                    var offerpos = db.OfferPositions.Where(t => t.OfferID == offerID).ToList();
                    var toplamBrut = offerpos.Where(t => t.PositionItemID == 1).Sum(t => t.CostPrice);
                    var toplamIsveren = offerpos.Where(t => t.PositionItemID == 2).Sum(t => t.CostPrice);
                    var toplamIssizlik = offerpos.Where(t => t.PositionItemID == 3).Sum(t => t.CostPrice);
                    var toplamYol = offerpos.Where(t => t.PositionItemID == 9).Sum(t => t.CostPrice);
                    var toplamYemek = offerpos.Where(t => t.PositionItemID == 10).Sum(t => t.CostPrice);
                    var toplamIhbar = offerpos.Where(t => t.PositionItemID == 6).Sum(t => t.CostPrice);
                    var toplamKidem = offerpos.Where(t => t.PositionItemID == 5).Sum(t => t.CostPrice);
                    var toplamIzin = offerpos.Where(t => t.PositionItemID == 4).Sum(t => t.CostPrice);
                    var toplambayram = offerpos.Where(t => t.PositionItemID == 7).Sum(t => t.CostPrice);
                    var toplamSaglikGideri = offerpos.Where(t => t.PositionItemID == 11).Sum(t => t.CostPrice);
                    var toplamIsgGideri = offerpos.Where(t => t.PositionItemID == 17).Sum(t => t.CostPrice);
                    var toplamEgitimBelgeGideri = offerpos.Where(t => t.PositionItemID == 20).Sum(t => t.CostPrice);
                    var toplamFazlaMesai = offerpos.Where(t => t.PositionItemID == 21).Sum(t => t.CostPrice);
                    var toplamSaglikSigortasi = offerpos.Where(t => t.PositionItemID == 22).Sum(t => t.CostPrice);
                    var toplamBireyselEmeklilik = offerpos.Where(t => t.PositionItemID == 23).Sum(t => t.CostPrice);

                    var ToplamIK = toplamBrut + toplamIsveren + toplamIssizlik + toplamYol + toplamYemek + toplamIhbar + toplamKidem + toplamIzin + toplambayram + toplamSaglikGideri + toplamIsgGideri + toplamEgitimBelgeGideri + toplamFazlaMesai + toplamSaglikSigortasi + toplamBireyselEmeklilik;
                    offer.BrutToplam = ToplamIK; //Toplam ik maliyetini Offer tablosundaki Brüt alanına yazdım.

                    double sumBrutProfitPrice = 0;

              

                    var diffCount = 1;
                    if (offer.DateBegin != null && offer.DateEnd != null)
                    {
  
                        TimeSpan res = offer.DateEnd.Value - offer.DateBegin.Value;
                        var totalDay = res.TotalDays;
                        diffCount = Convert.ToInt32((totalDay / 30));
                    }
                    offer.MonthlyUnitPrice = offer.BrutToplam + offer.TotalHrProfit + offer.TotalOtherExpense + offer.SumOtherExpenseProfit + offer.AraToplam + offer.SumAraToplamProfit + offer.TotalMonthlyDepreciation;


                   
                    offer.GenelToplam = offer.MonthlyUnitPrice * diffCount;
                    var offerDate = offer.DateBegin;
                    var date = new DateTime(2024, 01, 01);
                    if(offerDate < date)
                    {
                        offer.KdvToplam = (offer.GenelToplam * 18) / 100;
                    }
                    else
                    {
                        offer.KdvToplam = (offer.GenelToplam * 20) / 100;
                    }

                    offer.GenelToplam += offer.KdvToplam;
                    //İK maliyet Hesaplama End
                    db.SaveChanges();
                }
                else // Tek seferlik teklif
                {
                    offer.AraToplam = OfferRows.Sum(x => (x.CostPrice * x.Quantity));
                    double? sumAraToplamProfitRate = 0;
                    if (offer.AraToplamProfitRate != null)
                    {
                        

                        sumAraToplamProfitRate = offer.AraToplamProfitRate;
                    }
                    else
                    {
                        sumAraToplamProfitRate = 0;
                    }
                    offer.SumAraToplamProfit = (offer.AraToplam * (sumAraToplamProfitRate / 100));


                    var SumIsOneTimeOfferPositions = db.IsOneTimeOfferPositions.Where(t => t.OfferID == offerID && t.PositionItemID == 2).Sum(t => t.Price);
                    offer.BrutToplam = SumIsOneTimeOfferPositions; // IK toplamı eklendi.
                    double? sumBrutProfitRate = 0;
                    if (offer.BrutToplamProfitRate != null) // Eğer İK'da karlılık oranı varsa
                    {
                        
                        sumBrutProfitRate = offer.BrutToplamProfitRate;
                    }
                    else
                    {
                        sumBrutProfitRate = 0;
                    }
                    if (offer.BrutToplam == null)
                    {
                        offer.BrutToplam = 0;
                    }

                    offer.TotalHrProfit = (offer.BrutToplam * (sumBrutProfitRate / 100));
                    var sumOtherExpense = db.IsOneTimeOtherExpense.Where(t => t.OfferID == offer.ID).Sum(t => t.Price);

                    offer.TotalOtherExpense = sumOtherExpense; // Diğer harcamalar eklendi.
                    if (offer.TotalOtherExpense == null)
                    {
                        offer.TotalOtherExpense = 0;
                    }

                    double? sumOtherProfitRate = 0;

                    if (offer.ProfitRateForOthers != null)
                    {
                        sumOtherProfitRate = offer.ProfitRateForOthers;
                        
                    }
                    else
                    {
                        sumOtherProfitRate = 0;
                    }
                    offer.SumOtherExpenseProfit = (offer.TotalOtherExpense * (sumOtherProfitRate / 100));

                    offer.GenelToplam = offer.BrutToplam + offer.TotalHrProfit + offer.AraToplam + offer.SumAraToplamProfit + offer.TotalOtherExpense + offer.SumOtherExpenseProfit;

                    var offerDate = offer.DateBegin;
                    var date = new DateTime(2024, 01, 01);
                    if (offerDate < date)
                    {
                        offer.KdvToplam = (offer.GenelToplam * 18) / 100;
                    }
                    else
                    {
                        offer.KdvToplam = (offer.GenelToplam * 20) / 100;
                    }

                    offer.MonthlyUnitPrice = offer.GenelToplam;
                    offer.GenelToplam += offer.KdvToplam;
                    //Tek seferlik teklifte kdv hesaplanmadı

                    db.SaveChanges();

                }


            }
        }



        public PartialViewResult RefreshOfferFooter(int? offerID)
        {
            OfferControlModel model = new OfferControlModel();

            using (OypaDbEntities db = new OypaDbEntities())
            {
                var offer = db.Offer.FirstOrDefault(x => x.ID == offerID);
                if (offer != null)
                {
                    if (offer.IsOneTime == true)
                    {
                        model.singleoffer = offer;
                        if (model.singleoffer.TotalHrProfit != null) //İk karlılık oranı toplamı
                        {
                            model.singleoffer.BrutToplam += model.singleoffer.TotalHrProfit.Value;
                        }
                        if (model.singleoffer.SumMachineExpenseProfit != null) //Makina demirbaş karlılık oranı toplamı
                        {
                            model.singleoffer.TotalMachineExpense += model.singleoffer.SumMachineExpenseProfit;
                        }
                        if (model.singleoffer.SumAraToplamProfit != null) //malzeme stok karlılık oranı toplamı
                        {
                            model.singleoffer.AraToplam += model.singleoffer.SumAraToplamProfit;
                        }
                        if (model.singleoffer.SumOtherExpenseProfit != null) //diğer harcamalar karlılık oranı toplamı
                        {
                            model.singleoffer.TotalOtherExpense += model.singleoffer.SumOtherExpenseProfit;
                        }
                        model.offerproducts = db.OfferProducts.Where(x => x.OfferID == offer.ID).ToList();
                        return PartialView("_PartialOfferFooter", model);
                    }
                    else
                    {
                        //Tek seferlik teklif Maliyet analizi kalemleri farklı olucak.
                        model.singleoffer = offer;
                        if (model.singleoffer.TotalHrProfit != null) //İk karlılık oranı toplamı
                        {
                            model.singleoffer.BrutToplam += model.singleoffer.TotalHrProfit.Value;
                        }

                        if (model.singleoffer.SumAraToplamProfit != null) //malzeme stok karlılık oranı toplamı
                        {
                            model.singleoffer.AraToplam += model.singleoffer.SumAraToplamProfit;
                        }
                        if (model.singleoffer.SumOtherExpenseProfit != null) //diğer harcamalar karlılık oranı toplamı
                        {
                            model.singleoffer.TotalOtherExpense += model.singleoffer.SumOtherExpenseProfit;
                        }



                        model.offerproducts = db.OfferProducts.Where(x => x.OfferID == offer.ID).ToList();
                        return PartialView("_PartialIsOneTimeOfferFooter", model);
                    }


                }

                return PartialView("_PartialOfferFooter", model);

            }

        }

        public PartialViewResult TemplateOfferContent(string tempid)
        {
            var TemplateID = Convert.ToInt32(tempid);
            OfferContentModel model = new OfferContentModel();
            model.singletemplate = Db.OfferContentTemplate.FirstOrDefault(i => i.ID == TemplateID);





            return PartialView("_PartialTemplateContent", model);
        }

        //public ActionResult Content(int? OfferID)
        //{
        //    OfferContentModel model = new OfferContentModel();
        //    model.singletemplate = Db.OfferContentTemplate.FirstOrDefault(i => i.ID == 4);
        //    model.OfferID = OfferID.Value;
        //    return View(model);
        //}


        // Seçilen Şablona göre template oluşturuluyor.
        //Şablon seçimi oluştur butonu
        public ActionResult Content(int? OfferID, int? SelectTemplate)
        {
            OfferContentModel model = new OfferContentModel();

            var checkoffer = Db.Offer.FirstOrDefault(t => t.ID == OfferID && t.IsActive == true);
            model.singletemplate = Db.OfferContentTemplate.FirstOrDefault(i => i.ID == SelectTemplate);
            if (checkoffer != null && model.singletemplate != null)
            {

                var html = Db.OfferContentTemplate.FirstOrDefault(t => t.ID == SelectTemplate).TemplateContent;
                var customer = checkoffer.Customers.CustomerFullName;
                var customerAddress = checkoffer.Customers.Address;
                var offerName = checkoffer.OfferName;
                var year = DateTime.Now.Year.ToString();
                var date = DateTime.Now.ToShortDateString();

                html = html.Replace("#Year#", year);
                html = html.Replace("#Customer#", customer);
                html = html.Replace("#CustomerAddress#", customerAddress);
                html = html.Replace("#OfferName#", offerName);
                html = html.Replace("#Date#", date);
                //html = html.Replace("#monthlyPrice#", checkoffer.MonthlyUnitPrice.Value.ToString("N2"));


                //if (SelectTemplate == 3) //Detaylı Fiyat Listesi Şablonu Seçilirse
                //{
                //    //var html = Db.OfferContentTemplate.FirstOrDefault(t => t.ID == SelectTemplate);
                //    //var price = (checkoffer.GenelToplam - checkoffer.KdvToplam).Value.ToString("N2");
                //    //var kdv = checkoffer.KdvToplam.Value.ToString("N2");
                //    //var totalPrice = checkoffer.GenelToplam.Value.ToString("N2");

                //    //model.singletemplate.TemplateContent = OypaHelper.ReplaceOfferContract(html.TemplateContent, customer, customerAddress, year, offerName, 3);
                //    //Şablon oluşturulduktan sonra teklifte bir fiyat değişikliği olabilme ihtimaline karşı, teklifin üst yazısı ck editörde açılırken fiyat, kdv ve genel toplam alanları replace edilmemişir.
                //    //Bu alanlar Teklif üst yazısı müşteriye gönderilirken replace edilecektir.
                //}
                //else if (SelectTemplate == 1) // Temel Şablon Seçilirse
                //{
                //    //var html = Db.OfferContentTemplate.FirstOrDefault(t => t.ID == SelectTemplate);
                //    //var customer = checkoffer.Customers.CustomerFullName;
                //    //var customerAddress = checkoffer.Customers.Address;
                //    //model.singletemplate.TemplateContent = OypaHelper.ReplaceContract(html.TemplateContent, customer, customerAddress);
                //}


                model.singletemplate.TemplateContent = html;
                model.SingleOffer = checkoffer;
                model.OfferID = OfferID.Value;


            }
            return View(model);
        }


        #region ShowContractInformation
            //teklifin teklif üst yazı şablonunu gösterir.
            //Teklif müşteriye teklif üst yazı formu olmadan gönderilemez.
        #endregion
        public ActionResult ShowContract(int? OfferContractID)
        {
            OfferContentModel model = new OfferContentModel();
            model.SingleOfferContract = Db.OfferContract.FirstOrDefault(i => i.ID == OfferContractID && i.IsActive == true);
            if (model.SingleOfferContract != null)
            {
                model.OfferID = model.SingleOfferContract.OfferID.Value;
                model.SingleOffer = Db.Offer.FirstOrDefault(t => t.ID == model.OfferID);

                return View(model);
            }
            else
            {
                ViewBag.ErrorMessage = "Böyle bir şablon bulunmamaktadır";
                return View();
            }
        }


        #region UpdateContent
        //teklifin teklif üst yazı şablonunu güncellenir.
  
        #endregion

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateContent(string cdata, int offerID, int offerContentTemplateID)
        {
            
            var checkOfferContract = Db.OfferContract.Where(t => t.OfferID == offerID && t.IsActive == true).FirstOrDefault();
            var checkOffer = Db.Offer.FirstOrDefault(t => t.ID == offerID && t.IsActive == true);
            if (checkOfferContract != null)
            {

                checkOfferContract.ContractContent = cdata;
                checkOfferContract.UpdateDate = DateTime.Now;
                checkOfferContract.UpdateUserID = OypaHelper.RecordEmployee();
                checkOfferContract.OfferContentTemplateID = offerContentTemplateID;
            }
            else
            {
                Guid g = Guid.NewGuid();
                OfferContract of = new OfferContract()
                {
                    OfferID = offerID,
                    ContractContent = cdata,
                    IsActive = true,
                    RecordDate = DateTime.Now,
                    RecordUserID = OypaHelper.RecordEmployee(),
                    Guid = g.ToString(),
                    OfferContentTemplateID = offerContentTemplateID
                };
                Db.OfferContract.Add(of);
            }

            Db.SaveChanges();
            return RedirectToAction("Detail", "Offer", new { OpportunityID = checkOffer.OpportunityID, AutoOpportunity = 1 });

        }

        #region SaveOfferInformation
        //Teklif ilk oluştuğu anda durumu ilk kayıt olmaktadır.
        //İlgili personel teklif içerisine maliyet kalemlerini ekledikten sonra teklif ilgili bölge müdürüne onay gönderilir.
        //Bölge müdürü onay maili Teklifi kaydını oluşturan kullanıcının bağlı olduğu bölgenin grup mail adresine gönderilir.
        //Bölge müdürü teklife onay verdiği anda teklif içerisinde İk ve satın alma kalemleri var ise ilgili birimlere teklifi kontrol etmeleri için bilgilendirme maili atılmaktadır.,
        //Birimlerin mail adresinin yönetimi sol menude ayarlar içersinde yer alan mail aksiyonları sekmesinden yönetilmektedir.
        //Status level teklif durumuna(OfferStatus tablosu) karşılık gelmektedir.
        
        #endregion

        public ActionResult SaveOffer(int? offerID, int? StatusLevel)
        {
         

            try
            {
             
                var checkOffer = Db.Offer.FirstOrDefault(t => t.IsActive == true && t.ID == offerID);
                var checkStatus = Db.OfferStatus.FirstOrDefault(t => t.IsActive == true && t.StatusLevel == StatusLevel);
                if (checkOffer != null && checkStatus != null)
                {
                    string mailContentTitle = Db.Employee.FirstOrDefault(t => t.ID == checkOffer.Opportunity.RecordEmployeeID).FullName;
                    var Id = checkOffer.RecordEmployeeID;
                    var empMail = Db.Employee.FirstOrDefault(t => t.IsActive == true && t.ID == Id);
                    var purchasingMailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 3 && t.IsActive == true).ToList();
                    var hrmMailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 4 && t.IsActive == true).ToList();
                    var operationSupporMailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 5 && t.IsActive == true).ToList();
                    var coordinatorMailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 7 && t.IsActive == true).ToList();
                    var directorMailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 8 && t.IsActive == true).ToList();

                    if (checkStatus.StatusLevel == 13) //  bm onay ise 
                    {
                        checkOffer.StatusLevel = StatusLevel;
                        checkOffer.RegionalDirectorCheck = 1;
                        checkOffer.RegionalDirectorID = OypaHelper.RecordEmployee();
                        int Save = Db.SaveChanges();
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        //if (Save > 0)
                        //{
                        var checkOfferProducts = Db.OfferProducts.Where(t => t.OfferID == offerID).ToList();
                        if (checkOffer.IsOneTime.Value)
                        {

                            var checkOfferEquipments = Db.OfferEquipment.Where(t => t.OfferID == offerID).ToList();
                            var checkOfferOtherExpense = Db.OfferOtherExpenses.Where(t => t.OfferID == offerID && t.IsActive == true).ToList();
                            var checkOfferPositions = Db.OfferPositions.Where(t => t.OfferID == offerID).ToList();
                            if (checkOffer.HumanResourcesCheck != 1) //Onaylanmadıysa
                            {
                                checkOffer.HumanResourcesCheck = 0;
                                Db.SaveChanges();
                            }
                            if (checkOfferPositions != null && checkOfferPositions.Count() > 0) //İK'ya mail gidecek.
                            {
                                //Onay bekliyor yada reddedildiyse mail gönder
                                if (checkOffer.HumanResourcesCheck != 1)
                                {
                                    if (hrmMailList != null && hrmMailList.Count() > 0)
                                    {
                                        foreach (var item in hrmMailList)
                                        {
                                            var mailresult = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                  "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin İK maliyetlerini kontrol ediniz..",
                                  $"Sn. İnsan Kaynakları ilgilisi", $"{item.Email}", //"gbaranoglu@oypa.com.tr"
                                  "TYH - Teklif Durum Değişikliği",
                                  "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                            if (mailresult == true)
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{checkOffer.OfferNumber} nolu teklifin İk maliyetinin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }
                                            else
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{checkOffer.OfferNumber} nolu teklifin İk maliyetinin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);


                                            }

                                        }
                                    }
                                    else
                                    {
                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"Mail aksiyonları sekmesinde İk biriminin bilgilendirilebilmesi için mail adresi tanımlanmamıştır!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                        return Json(new { result = "HrMailError" }, JsonRequestBehavior.AllowGet);
                                    }
                                }


                            }

                            if (checkOffer.PurchasingManagerCheck != 1) //Onaylanmadıysa
                            {
                                checkOffer.PurchasingManagerCheck = 0;
                                Db.SaveChanges();
                            }

                            if (checkOffer.PurchasingSpecialistCheck != 1) //Onaylanmadıysa
                            {
                                checkOffer.PurchasingSpecialistCheck = 0;
                                Db.SaveChanges();
                            }
                            if (checkOfferEquipments.Count() > 0 || checkOfferOtherExpense.Count() > 0 || checkOfferProducts.Count() > 0)
                            {
                                //Onay bekliyor yada reddedildiyse mail gönder
                                if (checkOffer.PurchasingManagerCheck != 1)
                                {
                                    if (purchasingMailList != null && purchasingMailList.Count() > 0)
                                    {
                                        foreach (var item in purchasingMailList)
                                        {
                                            var mailresult = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.", "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin Satın alma maliyetlerini kontrol ediniz..", $"Sn. Satın Alma İlgilisi", $"{item.Email}", //"gbulur@oypa.com.tr"
                                            "TYH - Teklif Durum Değişikliği", "http://194.100.100.21/", null, "CreatedOpportunity.html");

                                            if (mailresult == true)
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{checkOffer.OfferNumber} nolu teklifin malzeme stok, makine demirbaş ve diğer  maliyetlerin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }
                                            else
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{checkOffer.OfferNumber} nolu teklifin malzeme stok, makine demirbaş ve diğer  maliyetlerin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }

                                        }

                                    }
                                    else
                                    {
                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"Mail aksiyonları sekmesinde Satın alma biriminin bilgilendirilebilmesi için mail adresi tanımlanmamıştır!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                        return Json(new { result = "PurchasingMailError" }, JsonRequestBehavior.AllowGet);
                                    }
                                }



                            }
                        }
                        else
                        {
                            var checkIsOneTimeOtherExpense = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == offerID).ToList();
                            var checkIsOneTimeOfferPositions = Db.IsOneTimeOfferPositions.Where(t => t.OfferID == offerID).ToList();
                            if (checkOffer.HumanResourcesCheck != 1) //Onaylanmadıysa
                            {
                                checkOffer.HumanResourcesCheck = 0;
                                Db.SaveChanges();
                            }

                            if (checkIsOneTimeOfferPositions != null && checkIsOneTimeOfferPositions.Count() > 0) //İK'ya mail gidecek.
                            {
                                if (checkOffer.HumanResourcesCheck != 1)
                                {

                                    if (hrmMailList != null && hrmMailList.Count() > 0)
                                    {
                                        foreach (var item in hrmMailList)
                                        {
                                            var mailresult = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                  "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin İK maliyetlerini kontrol ediniz..",
                                  $"Sn. İnsan Kaynakları İlgilisi", $"{item.Email}", //"gbaranoglu@oypa.com.tr"
                                  "TYH - Teklif Durum Değişikliği",
                                  "http://194.100.100.21/", null, "CreatedOpportunity.html");

                                            if (mailresult == true)
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{checkOffer.OfferNumber} nolu teklifin İk maliyetinin kontrol edilmesi için {item.FullName} isimli personele bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }
                                            else
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{checkOffer.OfferNumber} nolu teklifin İk maliyetinin kontrol edilmesi için{item.FullName} isimli personele bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }


                                        }
                                    }
                                    else
                                    {
                                        return Json(new { result = "HrMailError" }, JsonRequestBehavior.AllowGet);
                                    }
                                }


                            }

                            if (checkOffer.PurchasingManagerCheck != 1) //Onaylanmadıysa
                            {
                                checkOffer.PurchasingManagerCheck = 0;
                                Db.SaveChanges();
                            }

                            if (checkOffer.PurchasingSpecialistCheck != 1) //Onaylanmadıysa
                            {
                                checkOffer.PurchasingSpecialistCheck = 0;
                                Db.SaveChanges();
                            }
                            if (checkIsOneTimeOtherExpense.Count() > 0 || checkOfferProducts.Count() > 0)
                            {
                                if (checkOffer.PurchasingManagerCheck != 1)
                                {

                                    if (purchasingMailList != null && purchasingMailList.Count() > 0)
                                    {
                                        foreach (var item in purchasingMailList)
                                        {
                                            var mailresult = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.", "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin Satın alma maliyetlerini kontrol ediniz..", $"Sn. Satın Alma İlgilisi", $"{item.Email}", //"gbulur@oypa.com.tr"
                                            "TYH - Teklif Durum Değişikliği", "http://194.100.100.21/", null, "CreatedOpportunity.html");

                                            if (mailresult == true)
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{checkOffer.OfferNumber} nolu teklifin malzeme stok ve diğer maliyetlerinin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }
                                            else
                                            {
                                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{checkOffer.OfferNumber} nolu teklifin malzeme stok ve diğer maliyetlerinin kontrol edilmesi için {item.Email} adresine  bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"Mail aksiyonları sekmesinde Satın alma biriminin bilgilendirilebilmesi için mail adresi tanımlanmamıştır!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                        return Json(new { result = "PurchasingMailError" }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                        }

                        //}

                    }
                    else if (checkStatus.StatusLevel == 2) //teklif hazırlık
                    {
                        checkOffer.StatusLevel = StatusLevel; // teklifin surecı guncellendi.



                        if (checkOffer.PurchasingManagerCheck != 1) //Onaylanmadıysa
                        {
                            checkOffer.PurchasingManagerCheck = 0;

                        }

                        if (checkOffer.PurchasingSpecialistCheck != 1) //Onaylanmadıysa
                        {
                            checkOffer.PurchasingSpecialistCheck = 0;

                        }
                        if (checkOffer.HumanResourcesCheck != 1) //Onaylanmadıysa
                        {
                            checkOffer.HumanResourcesCheck = 0;
                        }
                        if (checkOffer.RegionalDirectorCheck != 1) //Onaylanmadıysa
                        {
                            checkOffer.RegionalDirectorCheck = 0;
                        }
                        Db.SaveChanges();
                        var regionGroupMail = string.Empty;
                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }

                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        //Önceden teklif hazırlık sürecinde geldiğinde teklifi oluşturan kişinin mailine bilgi maili gidiyordu. Şimdi teklifi bölge müdürleri değilde ilgili bölgede çalışan personeller oluşturduğu için mailller bölge müdürlüklerinin grup maillerine gönderilecektir.
                        //var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                        //           "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                        //           $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{empMail.EMail}", //"gsahin@oypa.com.tr"
                        //           "TYH - Teklif Durum Değişikliği",
                        //           "http://194.100.100.21/", null, "CreatedOpportunity.html");

                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                               "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                               $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}", //"gsahin@oypa.com.tr"
                               "TYH - Teklif Durum Değişikliği",
                               "http://194.100.100.21/", null, "CreatedOpportunity.html");


                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }



                    }
                    else if (checkStatus.StatusLevel == 14) //Bölge müdürü Red
                    {
                        checkOffer.StatusLevel = StatusLevel; // teklifin surecı guncellendi.
                        checkOffer.RegionalDirectorCheck = 2; //Bölge müdürü Red
                        checkOffer.RegionalDirectorID = OypaHelper.RecordEmployee();
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        Db.SaveChanges();
                    }
                    else if (checkStatus.StatusLevel == 1) //İlk kayıt
                    {
                        checkOffer.StatusLevel = StatusLevel;
                        Db.SaveChanges();
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    }
                    else if (checkStatus.StatusLevel == 3) //Satın alma onay
                    {
                        if (checkOffer.RegionalDirectorCheck != 1)
                        {
                            return Json(new { result = "RegionalDirectorCheckError" }, JsonRequestBehavior.AllowGet);

                        }

                        checkOffer.StatusLevel = StatusLevel; // teklifin surecı guncellendi.
                        checkOffer.PurchasingSpecialistCheck = 1; //Satın alma Uzm. Onay
                        checkOffer.PurchasingSpecialistID = OypaHelper.RecordEmployee();

                        Db.SaveChanges();
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        var PurchnaseManagerMailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 6 && t.IsActive == true).ToList();
                        if (PurchnaseManagerMailList != null && PurchnaseManagerMailList.Count() > 0)
                        {
                            foreach (var item in PurchnaseManagerMailList)
                            {

                                //Satın alma uzmanı teklifi satın alma onaya getirdi ve satın alma müdürüne bir mail gitti.
                                var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu satın alma uzmanı tarafından {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. Satın Alma İlgilisi", $"{item.Email}", //"gsahin@oypa.com.tr"
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");

                                if (mailres == true)
                                {
                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{checkOffer.OfferNumber} nolu teklifin malzeme stok, makine demirbaş ve diğer  maliyetlerin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                }
                                else
                                {
                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{checkOffer.OfferNumber} nolu teklifin malzeme stok, makine demirbaş ve diğer  maliyetlerin kontrol edilmesi için {item.Email} adresine bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                }
                            }
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"Mail aksiyonları sekmesinde Satın alma müdürünün bilgilendirilebilmesi için mail adresi tanımlanmamıştır!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            return Json(new { result = "PurchasingManagerMailError" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (checkStatus.StatusLevel == 4) //Satın alma Red
                    {
                        if (checkOffer.RegionalDirectorCheck != 1)
                        {
                            return Json(new { result = "RegionalDirectorCheckError" }, JsonRequestBehavior.AllowGet);

                        }

                        //History Tablolarıma kayıt atmama gerek yok. Direk Teklif durumunu set ediyorum.
                        checkOffer.StatusLevel = StatusLevel;
                        checkOffer.PurchasingSpecialistCheck = 2; //Satın alma Uzmanı Red
                        checkOffer.PurchasingSpecialistID = OypaHelper.RecordEmployee();
                        Db.SaveChanges();
                        var regionGroupMail = string.Empty;
                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }
                        //Satın alma uzmanı teklifin satın alma red 'e çekti, süreç satın alma müdürüne gitmeden direk teklifi oluşturan bölge müdürüne mesaj gidiyor.
                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}", //"gsahin@oypa.com.tr"
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");

                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }
                    }
                    else if (checkStatus.StatusLevel == 5) // İK Onay
                    {
                        if (checkOffer.RegionalDirectorCheck != 1)
                        {
                            return Json(new { result = "RegionalDirectorCheckError" }, JsonRequestBehavior.AllowGet);

                        }
                        if (checkOffer.IsOneTime == true) //Süreli Teklif
                        {

                            var checkOfferPositions = Db.OfferPositions.Where(t => t.OfferID == offerID).ToList();
                            var checkOfferHRProfitItems = Db.OfferHRProfitItems.Where(t => t.OfferID == offerID).ToList();
                            if (checkOfferPositions != null)
                            {
                                List<OfferPositionsHistory> HistoryListPosition = new List<OfferPositionsHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // history'de bu offer'a ait hiç kayıt yok ise
                                    {
                                        //Eski verileri silindi.
                                        var oldValue = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionNumber == 1 && t.VersionDigit == 0).ToList();

                                        //oldValue= oldValue.Select(t => { t.IsActive = false; return t; }).ToList();
                                        Db.OfferPositionsHistory.RemoveRange(oldValue);
                                        Db.SaveChanges();
                                        foreach (var item in checkOfferPositions)
                                        {
                                            //İK için girilen personelleri ve ücretleri history tabloma kaydettim.
                                            OfferPositionsHistory positionsHistory = new OfferPositionsHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = 0;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.PositionOptionID = item.PositionOptionID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.CostPrice = item.CostPrice;
                                            positionsHistory.UnitPrice = item.UnitPrice;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Value = item.Value;
                                            //positionsHistory.IsActive = true;

                                            HistoryListPosition.Add(positionsHistory);

                                        }
                                        Db.OfferPositionsHistory.AddRange(HistoryListPosition);
                                        Db.SaveChanges();



                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve İK fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        Db.OfferPositionsHistory.RemoveRange(positionsHistoryList);
                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkOfferPositions)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            OfferPositionsHistory positionsHistory = new OfferPositionsHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = checkOffer.VersionDigit;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.PositionOptionID = item.PositionOptionID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.CostPrice = item.CostPrice;
                                            positionsHistory.UnitPrice = item.UnitPrice;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Value = item.Value;

                                            HistoryListPosition.Add(positionsHistory);
                                        }

                                        Db.OfferPositionsHistory.AddRange(HistoryListPosition);
                                        int save = Db.SaveChanges();


                                    }



                                }
                                else // Revizyon düzenlemesi
                                {

                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;
                                        //Bir revizyon talebinde de birden fazla kez değerler değişebilir o yüzden eskileri silip nihai sonucu yazdım tabloma.
                                        var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        Db.OfferPositionsHistory.RemoveRange(positionsHistoryList);
                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkOfferPositions)
                                        {
                                            //Bu bir revizyon talebi. personeli, personel ucretini ve versiyon'u güncelledim. .
                                            OfferPositionsHistory positionsHistory = new OfferPositionsHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = NewVersionDigit;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.PositionOptionID = item.PositionOptionID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.CostPrice = item.CostPrice;
                                            positionsHistory.UnitPrice = item.UnitPrice;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Value = item.Value;

                                            HistoryListPosition.Add(positionsHistory);
                                        }
                                        Db.OfferPositionsHistory.AddRange(HistoryListPosition);
                                        Db.SaveChanges();

                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;

                                        //var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        //Db.OfferPositionsHistory.RemoveRange(positionsHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkOfferPositions)
                                        {
                                            //Yeni version  için tabloma kayıt attım.
                                            OfferPositionsHistory positionsHistory = new OfferPositionsHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            //positionsHistory.VersionDigit = checkRevision.VersionDigit;
                                            positionsHistory.VersionDigit = NewVersionDigit;

                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.PositionOptionID = item.PositionOptionID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.CostPrice = item.CostPrice;
                                            positionsHistory.UnitPrice = item.UnitPrice;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Value = item.Value;

                                            HistoryListPosition.Add(positionsHistory);


                                        }
                                        Db.OfferPositionsHistory.AddRange(HistoryListPosition);
                                        Db.SaveChanges();

                                    }


                                }
                                checkOffer.StatusLevel = StatusLevel; // teklifin surecı guncellendi.
                                checkOffer.HumanResourcesCheck = 1; //IK Onayladı.
                                checkOffer.RecordHumanResourcesID = OypaHelper.RecordEmployee();

                                Db.SaveChanges();
                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);


                            }

                            //Add HRProfitItemHistory
                            if (checkOfferHRProfitItems != null)
                            {
                                List<OfferHRProfitItemHistory> HRHistoryListProfitItem = new List<OfferHRProfitItemHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // history'de bu offer'a ait hiç kayıt yok ise
                                    {
                                        var oldValue = Db.OfferHRProfitItemHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionNumber == 1 && t.VersionDigit == 0).ToList();
                                        Db.OfferHRProfitItemHistory.RemoveRange(oldValue);
                                        Db.SaveChanges();
                                        foreach (var item in checkOfferHRProfitItems)
                                        {
                                            //İK için girilen personelleri ve ücretleri history tabloma kaydettim.
                                            OfferHRProfitItemHistory hrProfitItemHistory = new OfferHRProfitItemHistory();
                                            hrProfitItemHistory.OfferID = item.OfferID;
                                            hrProfitItemHistory.VersionNumber = 1;
                                            hrProfitItemHistory.VersionDigit = 0;
                                            hrProfitItemHistory.PositionItemID = item.PositionItemID;
                                            hrProfitItemHistory.RecordDate = DateTime.Now;

                                            //positionsHistory.OfferID = item.OfferID;
                                            //positionsHistory.VersionNumber = 1;
                                            //positionsHistory.VersionDigit = 0;
                                            //positionsHistory.PositionID = item.PositionID;
                                            //positionsHistory.PositionItemID = item.PositionItemID;
                                            //positionsHistory.PositionOptionID = item.PositionOptionID;
                                            //positionsHistory.Quantity = item.Quantity;
                                            //positionsHistory.CostPrice = item.CostPrice;
                                            //positionsHistory.UnitPrice = item.UnitPrice;
                                            //positionsHistory.Currency = item.Currency;
                                            //positionsHistory.RecordDate = DateTime.Now;
                                            //positionsHistory.Value = item.Value;
                                            //HistoryListPosition.Add(positionsHistory);
                                            HRHistoryListProfitItem.Add(hrProfitItemHistory);

                                        }
                                        //Db.OfferPositionsHistory.AddRange(HistoryListPosition);
                                        Db.OfferHRProfitItemHistory.AddRange(HRHistoryListProfitItem);
                                        Db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve İK fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        //var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        var positionsHRProfitItemHistoryList = Db.OfferHRProfitItemHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki kar kalemlerini yakaladım
                                        Db.OfferHRProfitItemHistory.RemoveRange(positionsHRProfitItemHistoryList);

                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkOfferHRProfitItems)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            //OfferPositionsHistory positionsHistory = new OfferPositionsHistory();
                                            //positionsHistory.OfferID = item.OfferID;
                                            //positionsHistory.VersionNumber = 1;
                                            //positionsHistory.VersionDigit = checkOffer.VersionDigit;
                                            //positionsHistory.PositionID = item.PositionID;
                                            //positionsHistory.PositionItemID = item.PositionItemID;
                                            //positionsHistory.PositionOptionID = item.PositionOptionID;
                                            //positionsHistory.Quantity = item.Quantity;
                                            //positionsHistory.CostPrice = item.CostPrice;
                                            //positionsHistory.UnitPrice = item.UnitPrice;
                                            //positionsHistory.Currency = item.Currency;
                                            //positionsHistory.RecordDate = DateTime.Now;
                                            //positionsHistory.Value = item.Value;
                                            //HistoryListPosition.Add(positionsHistory);



                                            OfferHRProfitItemHistory hrProfitItemHistory = new OfferHRProfitItemHistory();
                                            hrProfitItemHistory.OfferID = item.OfferID;
                                            hrProfitItemHistory.VersionNumber = 1;
                                            hrProfitItemHistory.VersionDigit = checkOffer.VersionDigit;
                                            hrProfitItemHistory.PositionItemID = item.PositionItemID;
                                            hrProfitItemHistory.RecordDate = DateTime.Now;


                                            HRHistoryListProfitItem.Add(hrProfitItemHistory);
                                        }

                                        //Db.OfferPositionsHistory.AddRange(HistoryListPosition);
                                        Db.OfferHRProfitItemHistory.AddRange(HRHistoryListProfitItem);
                                        int save = Db.SaveChanges();

                                    }



                                }
                                else // Revizyon düzenlemesi
                                {

                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;
                                        //Bir revizyon talebinde de birden fazla kez değerler değişebilir o yüzden eskileri silip nihai sonucu yazdım tabloma.
                                        //var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        var positionsHRProfitItemHistoryList = Db.OfferHRProfitItemHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki karlılık kalemlerini yakaladım


                                        Db.OfferHRProfitItemHistory.RemoveRange(positionsHRProfitItemHistoryList);
                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkOfferHRProfitItems)
                                        {
                                            //Bu bir revizyon talebi. personelkarlılık kalemlerini ve versiyon'u güncelledim. .



                                            OfferHRProfitItemHistory hrProfitItemHistory = new OfferHRProfitItemHistory();
                                            hrProfitItemHistory.OfferID = item.OfferID;
                                            hrProfitItemHistory.VersionNumber = 1;
                                            hrProfitItemHistory.VersionDigit = NewVersionDigit;
                                            hrProfitItemHistory.PositionItemID = item.PositionItemID;
                                            hrProfitItemHistory.RecordDate = DateTime.Now;


                                            HRHistoryListProfitItem.Add(hrProfitItemHistory);
                                        }
                                        Db.OfferHRProfitItemHistory.AddRange(HRHistoryListProfitItem);
                                        Db.SaveChanges();
                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;

                                        //var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        //Db.OfferPositionsHistory.RemoveRange(positionsHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkOfferHRProfitItems)
                                        {
                                            OfferHRProfitItemHistory hrProfitItemHistory = new OfferHRProfitItemHistory();
                                            hrProfitItemHistory.OfferID = item.OfferID;
                                            hrProfitItemHistory.VersionNumber = 1;
                                            hrProfitItemHistory.VersionDigit = NewVersionDigit;
                                            hrProfitItemHistory.PositionItemID = item.PositionItemID;
                                            hrProfitItemHistory.RecordDate = DateTime.Now;

                                            HRHistoryListProfitItem.Add(hrProfitItemHistory);


                                        }
                                        Db.OfferHRProfitItemHistory.AddRange(HRHistoryListProfitItem);
                                        Db.SaveChanges();
                                    }


                                }
                                checkOffer.StatusLevel = StatusLevel; // teklifin surecı guncellendi.
                                checkOffer.HumanResourcesCheck = 1; //IK Onayladı.
                                checkOffer.RecordHumanResourcesID = OypaHelper.RecordEmployee();


                                Db.SaveChanges();

                            }

                        }
                        else //Tek seferlik Teklif
                        {
                            //var checkRevision = Db.IsOneTimeOfferPositonHistory.ToList().LastOrDefault(t => t.OfferID == offerID);
                            var checkIsOneTimeOfferPositions = Db.IsOneTimeOfferPositions.Where(t => t.OfferID == offerID).ToList();
                            if (checkIsOneTimeOfferPositions != null)
                            {
                                List<IsOneTimeOfferPositonHistory> HistoryListIsOneTimePosition = new List<IsOneTimeOfferPositonHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // history'de bu offer'a ait hiç kayıt yok ise
                                    {
                                        var positionsHistoryList = Db.IsOneTimeOfferPositonHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionNumber == 1 && t.VersionDigit == 0).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        Db.IsOneTimeOfferPositonHistory.RemoveRange(positionsHistoryList);
                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkIsOneTimeOfferPositions)
                                        {
                                            //İK için girilen personelleri ve ücretleri history tabloma kaydettim.
                                            IsOneTimeOfferPositonHistory positionsHistory = new IsOneTimeOfferPositonHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = 0;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.WorkingHour = item.WorkingHour;
                                            positionsHistory.OvertimePay = item.OvertimePay;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Price = item.Price;

                                            HistoryListIsOneTimePosition.Add(positionsHistory);

                                        }
                                        Db.IsOneTimeOfferPositonHistory.AddRange(HistoryListIsOneTimePosition);
                                        Db.SaveChanges();

                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve İK fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var positionsHistoryList = Db.IsOneTimeOfferPositonHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        Db.IsOneTimeOfferPositonHistory.RemoveRange(positionsHistoryList);
                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkIsOneTimeOfferPositions)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            IsOneTimeOfferPositonHistory positionsHistory = new IsOneTimeOfferPositonHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = checkOffer.VersionDigit;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.WorkingHour = item.WorkingHour;
                                            positionsHistory.OvertimePay = item.OvertimePay;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Price = item.Price;

                                            HistoryListIsOneTimePosition.Add(positionsHistory);

                                        }
                                        Db.IsOneTimeOfferPositonHistory.AddRange(HistoryListIsOneTimePosition);
                                        Db.SaveChanges();


                                    }



                                }
                                else // Revizyon düzenlemesi
                                {

                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;

                                        var positionsHistoryList = Db.IsOneTimeOfferPositonHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        Db.IsOneTimeOfferPositonHistory.RemoveRange(positionsHistoryList);
                                        int s = Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.

                                        foreach (var item in checkIsOneTimeOfferPositions)
                                        {
                                            //Bu bir revizyon talebi. personeli, personel ucretini ve versiyon'u güncelledim. .
                                            IsOneTimeOfferPositonHistory positionsHistory = new IsOneTimeOfferPositonHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = NewVersionDigit;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.WorkingHour = item.WorkingHour;
                                            positionsHistory.OvertimePay = item.OvertimePay;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Price = item.Price;

                                            HistoryListIsOneTimePosition.Add(positionsHistory);

                                        }
                                        Db.IsOneTimeOfferPositonHistory.AddRange(HistoryListIsOneTimePosition);
                                        Db.SaveChanges();


                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;

                                        //var positionsHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki personelleri yakaladım
                                        //Db.OfferPositionsHistory.RemoveRange(positionsHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş personelleri history tablomdan temizledim.
                                        foreach (var item in checkIsOneTimeOfferPositions)
                                        {
                                            //Yeni version  için tabloma kayıt attım.
                                            IsOneTimeOfferPositonHistory positionsHistory = new IsOneTimeOfferPositonHistory();
                                            positionsHistory.OfferID = item.OfferID;
                                            positionsHistory.VersionNumber = 1;
                                            positionsHistory.VersionDigit = NewVersionDigit;
                                            positionsHistory.PositionID = item.PositionID;
                                            positionsHistory.PositionItemID = item.PositionItemID;
                                            positionsHistory.Quantity = item.Quantity;
                                            positionsHistory.Currency = item.Currency;
                                            positionsHistory.WorkingHour = item.WorkingHour;
                                            positionsHistory.OvertimePay = item.OvertimePay;
                                            positionsHistory.RecordDate = DateTime.Now;
                                            positionsHistory.Price = item.Price;

                                            HistoryListIsOneTimePosition.Add(positionsHistory);

                                        }
                                        Db.IsOneTimeOfferPositonHistory.AddRange(HistoryListIsOneTimePosition);
                                        Db.SaveChanges();

                                    }


                                }
                                checkOffer.StatusLevel = StatusLevel; // teklifin surecı guncellendi.
                                checkOffer.HumanResourcesCheck = 1; //IK Onay
                                checkOffer.RecordHumanResourcesID = OypaHelper.RecordEmployee();
                                Db.SaveChanges();
                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            }
                        }
                        //Fırsat teklife dönüştü, Teklifin durumu   değiştirdiğinde fırsatı oluşturan bölge mudurune mail gidecek.
                        var regionGroupMail = string.Empty;
                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }

                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}",
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }

                    }
                    else if (checkStatus.StatusLevel == 6)//IK Red
                    {
                        if (checkOffer.RegionalDirectorCheck != 1)
                        {
                            return Json(new { result = "RegionalDirectorCheckError" }, JsonRequestBehavior.AllowGet);

                        }

                        //History Tablolarıma kayıt atmama gerek yok. Direk Teklif durumunu set ediyorum.
                        checkOffer.StatusLevel = StatusLevel;
                        checkOffer.HumanResourcesCheck = 2; //IK Red
                        checkOffer.RecordHumanResourcesID = OypaHelper.RecordEmployee();
                        Db.SaveChanges();
                        var regionGroupMail = string.Empty;
                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        //Fırsat teklife dönüştü, Teklifin durumu   değiştirdiğinde fırsatı oluşturan bölge mudurune mail gidecek.
                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}", //"gsahin@oypa.com.tr"
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }
                    }
                    else if (checkStatus.StatusLevel == 7) //Fiyat revizyon
                    {
                        //Teklifte revizyon istedi!
                        if (checkOffer.VersionNumber != null)
                        {
                            checkOffer.StatusLevel = StatusLevel;
                            checkOffer.IsRevision = true;
                            int Save = Db.SaveChanges();
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            //if (Save > 0)
                            //{
                            var checkOfferProducts = Db.OfferProducts.Where(t => t.OfferID == offerID).ToList();
                            checkOffer.RegionalDirectorCheck = 0;//BM Onayı tekrardan bekliyora çekildi.
                            if (checkOffer.IsOneTime.Value)
                            {


                                var checkOfferEquipments = Db.OfferEquipment.Where(t => t.OfferID == offerID).ToList();
                                var checkOfferOtherExpense = Db.OfferOtherExpenses.Where(t => t.OfferID == offerID && t.IsActive == true).ToList();
                                var checkOfferPositions = Db.OfferPositions.Where(t => t.OfferID == offerID).ToList();
                                if (checkOfferPositions != null && checkOfferPositions.Count() > 0) //İK'ya mail gidecek.
                                {
                                    checkOffer.HumanResourcesCheck = 0; //Ik Onayı tekrardan bekliyora çekildi.
                                    if (hrmMailList != null && hrmMailList.Count() > 0)
                                    {
                                        foreach (var item in hrmMailList)
                                        {
                                            OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                 "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin İK maliyetlerini kontrol ediniz..",
                                 $"Sn. İnsan Kaynakları ilgilisi", $"{item.Email}", //"gbaranoglu@oypa.com.tr"
                                 "TYH - Teklif Durum Değişikliği",
                                 "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                        }
                                    }
                                    else
                                    {
                                        return Json(new { result = "HrMailError" }, JsonRequestBehavior.AllowGet);
                                    }

                                }
                                if (checkOfferEquipments.Count() > 0 || checkOfferOtherExpense.Count() > 0 || checkOfferProducts.Count() > 0)
                                {
                                    checkOffer.PurchasingSpecialistCheck = 0;
                                    checkOffer.PurchasingManagerCheck = 0;
                                    if (purchasingMailList != null && purchasingMailList.Count() > 0)
                                    {
                                        foreach (var item in purchasingMailList)
                                        {
                                            OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin Satın alma maliyetlerini kontrol ediniz..",
                                $"Sn. Satın Alma İlgilisi", $"{item.Email}", //"gbulur@oypa.com.tr"
                                "TYH - Teklif Durum Değişikliği",
                                "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                        }

                                    }
                                    else
                                    {
                                        return Json(new { result = "PurchasingMailError" }, JsonRequestBehavior.AllowGet);
                                    }

                                }
                            }
                            else
                            {
                                var checkIsOneTimeOtherExpense = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == offerID).ToList();
                                var checkIsOneTimeOfferPositions = Db.IsOneTimeOfferPositions.Where(t => t.OfferID == offerID).ToList();

                                if (checkIsOneTimeOfferPositions != null && checkIsOneTimeOfferPositions.Count() > 0) //İK'ya mail gidecek.
                                {
                                    checkOffer.HumanResourcesCheck = 0;
                                    if (hrmMailList != null && hrmMailList.Count() > 0)
                                    {
                                        foreach (var item in hrmMailList)
                                        {
                                            OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                 "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin İK maliyetlerini kontrol ediniz..",
                                 $"Sn. İnsan Kaynakları ilgilisi", $"{item.Email}", //"gbaranoglu@oypa.com.tr"
                                 "TYH - Teklif Durum Değişikliği",
                                 "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                        }
                                    }
                                    else
                                    {
                                        return Json(new { result = "HrMailError" }, JsonRequestBehavior.AllowGet);
                                    }

                                }
                                if (checkIsOneTimeOtherExpense.Count() > 0 || checkOfferProducts.Count() > 0)
                                {
                                    checkOffer.PurchasingManagerCheck = 0;
                                    checkOffer.PurchasingSpecialistCheck = 0;
                                    if (purchasingMailList != null && purchasingMailList.Count() > 0)
                                    {
                                        foreach (var item in purchasingMailList)
                                        {
                                            OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin Satın alma maliyetlerini kontrol ediniz..",
                                $"Sn. Satın Alma İlgilisi", $"{item.Email}", //"gbulur@oypa.com.tr"
                                "TYH - Teklif Durum Değişikliği",
                                "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                        }

                                    }
                                    else
                                    {
                                        return Json(new { result = "PurchasingMailError" }, JsonRequestBehavior.AllowGet);
                                    }


                                }

                            }

                            Db.SaveChanges();
                            //}
                        }
                        else
                        {

                            return Json(new { result = "Failed" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else if (checkStatus.StatusLevel == 8)
                    {
                        checkOffer.StatusLevel = StatusLevel;
                        Db.SaveChanges();
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    }
                    else if (checkStatus.StatusLevel == 9)
                    {
                        return Json(new { result = "NotUpdated" }, JsonRequestBehavior.AllowGet);
                    }
                    else if (checkStatus.StatusLevel == 10)
                    {
                        if (checkOffer.StatusLevel == 9)
                        {
                            checkOffer.StatusLevel = 10;
                            Db.SaveChanges();
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            if (operationSupporMailList != null && operationSupporMailList.Count() > 0)
                            {
                                foreach (var item in operationSupporMailList)
                                {
                                    //Mail'in Sn. kısmına birim adı yazılacak! Birimin adını öğren.
                                    var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                         "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifin sözleşme aşamasını tamamlayınız..",
                                         $"Sn. {item.FullName}", $"{item.Email}",
                                         "TYH - Teklif Durum Değişikliği",
                                         "http://194.100.100.21/", null, "CreatedOpportunity.html");


                                    if (mailres == true)
                                    {
                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                    }
                                    else
                                    {
                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                                    }
                                }
                                return Json(new { result = "OfferUpdated" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { result = "OperationMailError" }, JsonRequestBehavior.AllowGet);
                            }


                        }
                        else
                        {
                            //müşteriye gönderilmeyen bir teklif sözleşme hazırlığa çekilemez.
                            return Json(new { result = "OfferUpdateError" }, JsonRequestBehavior.AllowGet);
                        }



                    }
                    else if (checkStatus.StatusLevel == 11) // Satın alma müd onay
                    {
                        if (checkOffer.RegionalDirectorCheck != 1)
                        {
                            return Json(new { result = "RegionalDirectorCheckError" }, JsonRequestBehavior.AllowGet);

                        }
                        //if (checkOffer.PurchasingSpecialistCheck != 1)
                        //{
                        //    return Json(new { result = "PurchasingManagerFail" }, JsonRequestBehavior.AllowGet);
                        //}
                        var offerHistory = Db.OfferHistory.FirstOrDefault(t => t.OfferID == checkOffer.ID && t.VersionDigit == checkOffer.VersionDigit);
                        if (checkOffer.IsOneTime == true)
                        {


                            var checkOfferProducts = Db.OfferProducts.Where(t => t.OfferID == offerID).ToList();

                            var checkOfferEquipments = Db.OfferEquipment.Where(t => t.OfferID == offerID).ToList();
                            var checkOfferOtherExpense = Db.OfferOtherExpenses.Where(t => t.OfferID == offerID && t.IsActive == true).ToList();
                            //Satın alma Malzeme, Makine Demirbaş ve diğer harcamaları kontorl edecek Birim!
                            if (checkOfferProducts != null)
                            {
                                //Teklif revizyon numaraları her zaman offer revizyon numaralarını referans almalıdır.


                                List<OfferProductsHistory> HistoryListPrd = new List<OfferProductsHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // bu teklif müşteriye gönderilmemiş bu yüzden version 1.0
                                    {
                                        var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == 0 && t.VersionNumber == 1).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Satın alma için girilen urunleri ve fiyatları history tabloma kaydettim.
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            productHistory.VersionDigit = 0;
                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;

                                            //Db.OfferProductsHistory.Add(productHistory);
                                            HistoryListPrd.Add(productHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve satın alma fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            productHistory.VersionDigit = checkOffer.VersionDigit;
                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            HistoryListPrd.Add(productHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();

                                    }



                                }
                                else // Revizyon düzenlemesi
                                {

                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;
                                        var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Bu bir revizyon talebi. Ürünleri, fiyatları ve versiyon'u güncelledim. .
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            productHistory.VersionDigit = NewVersionDigit;
                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            HistoryListPrd.Add(productHistory);
                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);

                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;
                                        //var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        //Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            //productHistory.VersionDigit = checkRevision.VersionDigit;
                                            productHistory.VersionDigit = NewVersionDigit; // Teklifin versionu 1.2 olsa bile OfferProductHistory hala v1.0 da olabilir. Çünkü her telif revizyonunda OfferProductHistory'de revizyon yapılmak zorunluluğu yok!!

                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.
                                            HistoryListPrd.Add(productHistory);
                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();
                                    }
                                }

                            }
                            if (checkOfferEquipments != null)
                            {

                                List<OfferEquipmentHistory> HistoryListEquipment = new List<OfferEquipmentHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // bu teklif müşteriye gönderilmemiş bu yüzden version 1.0
                                    {
                                        var equHistoryList = Db.OfferEquipmentHistory.Where(t => t.OfferID == offerID && t.VersionNumber == 1 && t.VersionDigit == 0).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferEquipmentHistory.RemoveRange(equHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkOfferEquipments)
                                        {
                                            //Satın alma için girilen urunleri ve fiyatları history tabloma kaydettim.
                                            OfferEquipmentHistory equipmentHistory = new OfferEquipmentHistory();
                                            equipmentHistory.OfferID = item.OfferID;
                                            equipmentHistory.VersionNumber = 1;
                                            equipmentHistory.VersionDigit = 0;
                                            equipmentHistory.ProductID = item.ProductID;
                                            equipmentHistory.Quantity = item.Quantity;
                                            equipmentHistory.Unit = item.Unit;
                                            equipmentHistory.CostPrice = item.CostPrice;
                                            equipmentHistory.UnitPrice = item.UnitPrice;
                                            equipmentHistory.Currency = item.Currency;
                                            equipmentHistory.Depreciation = item.Depreciation;
                                            equipmentHistory.MonthlyDepreciation = item.MonthlyDepreciation;
                                            equipmentHistory.RecordDate = DateTime.Now;
                                            equipmentHistory.MonthCount = item.MonthCount;

                                            HistoryListEquipment.Add(equipmentHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferEquipmentHistory.AddRange(HistoryListEquipment);
                                        //Db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve satın alma fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var equHistoryList = Db.OfferEquipmentHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferEquipmentHistory.RemoveRange(equHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferEquipments)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            OfferEquipmentHistory equipmentHistory = new OfferEquipmentHistory();
                                            equipmentHistory.OfferID = item.OfferID;
                                            equipmentHistory.VersionNumber = 1;
                                            equipmentHistory.VersionDigit = checkOffer.VersionDigit;
                                            equipmentHistory.ProductID = item.ProductID;
                                            equipmentHistory.Quantity = item.Quantity;
                                            equipmentHistory.Unit = item.Unit;
                                            equipmentHistory.CostPrice = item.CostPrice;
                                            equipmentHistory.UnitPrice = item.UnitPrice;
                                            equipmentHistory.Currency = item.Currency;
                                            equipmentHistory.Depreciation = item.Depreciation;
                                            equipmentHistory.MonthlyDepreciation = item.MonthlyDepreciation;
                                            equipmentHistory.RecordDate = DateTime.Now;
                                            equipmentHistory.MonthCount = item.MonthCount;

                                            HistoryListEquipment.Add(equipmentHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferEquipmentHistory.AddRange(HistoryListEquipment);
                                        //Db.SaveChanges();

                                    }



                                }
                                else // Revizyon düzenlemesi
                                {
                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;
                                        var equHistoryList = Db.OfferEquipmentHistory.Where(t => t.OfferID == offerID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferEquipmentHistory.RemoveRange(equHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferEquipments)
                                        {
                                            OfferEquipmentHistory equipmentHistory = new OfferEquipmentHistory();
                                            equipmentHistory.OfferID = item.OfferID;
                                            equipmentHistory.VersionNumber = 1;
                                            equipmentHistory.VersionDigit = NewVersionDigit;
                                            equipmentHistory.ProductID = item.ProductID;
                                            equipmentHistory.Quantity = item.Quantity;
                                            equipmentHistory.Unit = item.Unit;
                                            equipmentHistory.CostPrice = item.CostPrice;
                                            equipmentHistory.UnitPrice = item.UnitPrice;
                                            equipmentHistory.Currency = item.Currency;
                                            equipmentHistory.Depreciation = item.Depreciation;
                                            equipmentHistory.MonthlyDepreciation = item.MonthlyDepreciation;
                                            equipmentHistory.RecordDate = DateTime.Now;
                                            equipmentHistory.MonthCount = item.MonthCount;

                                            HistoryListEquipment.Add(equipmentHistory);


                                        }
                                        Db.OfferEquipmentHistory.AddRange(HistoryListEquipment);
                                        //Db.SaveChanges();
                                        //Teklifte bir revizyon yapıldı ve teklifin revizyon versiyonuda güncellendi.
                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;
                                        //var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        //Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferEquipments)
                                        {
                                            OfferEquipmentHistory equipmentHistory = new OfferEquipmentHistory();
                                            equipmentHistory.OfferID = item.OfferID;
                                            equipmentHistory.VersionNumber = 1;
                                            equipmentHistory.VersionDigit = NewVersionDigit;
                                            equipmentHistory.ProductID = item.ProductID;
                                            equipmentHistory.Quantity = item.Quantity;
                                            equipmentHistory.Unit = item.Unit;
                                            equipmentHistory.CostPrice = item.CostPrice;
                                            equipmentHistory.UnitPrice = item.UnitPrice;
                                            equipmentHistory.Currency = item.Currency;
                                            equipmentHistory.Depreciation = item.Depreciation;
                                            equipmentHistory.MonthlyDepreciation = item.MonthlyDepreciation;
                                            equipmentHistory.RecordDate = DateTime.Now;
                                            equipmentHistory.MonthCount = item.MonthCount;

                                            HistoryListEquipment.Add(equipmentHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferEquipmentHistory.AddRange(HistoryListEquipment);
                                        //Db.SaveChanges();
                                    }
                                }

                            }
                            if (checkOfferOtherExpense != null)
                            {

                                List<OfferOtherExpenseHistory> HistoryListOtherExpense = new List<OfferOtherExpenseHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) //  
                                    {
                                        var otherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionNumber == 1 && t.VersionDigit == 0).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferOtherExpenseHistory.RemoveRange(otherExpenseHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkOfferOtherExpense)
                                        {
                                            //Satın alma için girilen urunleri ve fiyatları history tabloma kaydettim.
                                            OfferOtherExpenseHistory otherExpenseHistory = new OfferOtherExpenseHistory();
                                            otherExpenseHistory.OfferID = item.OfferID;
                                            otherExpenseHistory.VersionNumber = 1;
                                            otherExpenseHistory.VersionDigit = 0;
                                            otherExpenseHistory.Price = item.Price;
                                            otherExpenseHistory.Description = item.Description;
                                            otherExpenseHistory.RecordUserId = item.RecordUserID;
                                            otherExpenseHistory.RecordDate = DateTime.Now;


                                            HistoryListOtherExpense.Add(otherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferOtherExpenseHistory.AddRange(HistoryListOtherExpense);
                                        //Db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve satın alma fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var otherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferOtherExpenseHistory.RemoveRange(otherExpenseHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferOtherExpense)
                                        {
                                            OfferOtherExpenseHistory otherExpenseHistory = new OfferOtherExpenseHistory();
                                            otherExpenseHistory.OfferID = item.OfferID;
                                            otherExpenseHistory.VersionNumber = 1;
                                            otherExpenseHistory.VersionDigit = checkOffer.VersionDigit;
                                            otherExpenseHistory.Price = item.Price;
                                            otherExpenseHistory.Description = item.Description;
                                            otherExpenseHistory.RecordUserId = item.RecordUserID;
                                            otherExpenseHistory.RecordDate = DateTime.Now;


                                            HistoryListOtherExpense.Add(otherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferOtherExpenseHistory.AddRange(HistoryListOtherExpense);
                                        //Db.SaveChanges();

                                    }



                                }
                                else // Revizyon düzenlemesi
                                {

                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;
                                        var otherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferOtherExpenseHistory.RemoveRange(otherExpenseHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkOfferOtherExpense)
                                        {
                                            OfferOtherExpenseHistory otherExpenseHistory = new OfferOtherExpenseHistory();
                                            otherExpenseHistory.OfferID = item.OfferID;
                                            otherExpenseHistory.VersionNumber = 1;
                                            otherExpenseHistory.VersionDigit = NewVersionDigit;
                                            otherExpenseHistory.Price = item.Price;
                                            otherExpenseHistory.Description = item.Description;
                                            otherExpenseHistory.RecordUserId = item.RecordUserID;
                                            otherExpenseHistory.RecordDate = DateTime.Now;


                                            HistoryListOtherExpense.Add(otherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferOtherExpenseHistory.AddRange(HistoryListOtherExpense);
                                        //Db.SaveChanges();

                                        //Teklifte bir revizyon yapıldı ve teklifin revizyon versiyonuda güncellendi.
                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;
                                        //var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        //Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferOtherExpense)
                                        {
                                            OfferOtherExpenseHistory otherExpenseHistory = new OfferOtherExpenseHistory();
                                            otherExpenseHistory.OfferID = item.OfferID;
                                            otherExpenseHistory.VersionNumber = 1;
                                            otherExpenseHistory.VersionDigit = NewVersionDigit;
                                            otherExpenseHistory.Price = item.Price;
                                            otherExpenseHistory.Description = item.Description;
                                            otherExpenseHistory.RecordUserId = item.RecordUserID;
                                            otherExpenseHistory.RecordDate = DateTime.Now;


                                            HistoryListOtherExpense.Add(otherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferOtherExpenseHistory.AddRange(HistoryListOtherExpense);
                                        //Db.SaveChanges();

                                    }
                                }

                            }





                        }
                        else
                        {
                            //Tek seferlik teklfin detayında satın almanın onaylayacağı tablar=> Malzeme stok ve diğer harcamalar
                            var checkOfferProducts = Db.OfferProducts.Where(t => t.OfferID == offerID).ToList();

                            if (checkOfferProducts != null)
                            {
                                //var checkRevision = Db.OfferProductsHistory.ToList().LastOrDefault(t => t.OfferID == offerID);

                                List<OfferProductsHistory> HistoryListPrd = new List<OfferProductsHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // V1.0
                                    {
                                        var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == 0 && t.VersionNumber == 1).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Satın alma için girilen urunleri ve fiyatları history tabloma kaydettim.
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            productHistory.VersionDigit = 0;
                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            //Db.OfferProductsHistory.Add(productHistory);
                                            HistoryListPrd.Add(productHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve satın alma fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkOffer.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            productHistory.VersionDigit = checkOffer.VersionDigit;
                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            HistoryListPrd.Add(productHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();

                                    }



                                }
                                else // Revizyon düzenlemesi
                                {
                                    //var NewVersionDigit = checkRevision.VersionDigit + 1;
                                    //if (checkOffer.VersionDigit + 1 == NewVersionDigit)
                                    //{
                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;

                                        var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Bu bir revizyon talebi. Ürünleri, fiyatları ve versiyon'u güncelledim. .
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            productHistory.VersionDigit = NewVersionDigit;
                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            HistoryListPrd.Add(productHistory);
                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();
                                        //Teklifte bir revizyon yapıldı ve teklifin revizyon versiyonuda güncellendi.
                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;
                                        //var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        //Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkOfferProducts)
                                        {
                                            //Yeniden version 1.0 için tabloma kayıt attım.
                                            OfferProductsHistory productHistory = new OfferProductsHistory();
                                            productHistory.OfferID = item.OfferID;
                                            productHistory.VersionNumber = 1;
                                            //productHistory.VersionDigit = checkRevision.VersionDigit;
                                            productHistory.VersionDigit = NewVersionDigit; // Teklifin versionu 1.2 olsa bile OfferProductHistory hala v1.0 da olabilir. Çünkü her telif revizyonunda OfferProductHistory'de revizyon yapılmak zorunluluğu yok!!

                                            productHistory.ProductID = item.ProductID;
                                            productHistory.Quantity = item.Quantity;
                                            productHistory.Unit = item.Unit;
                                            productHistory.CostPrice = item.CostPrice;
                                            productHistory.UnitPrice = item.UnitPrice;
                                            productHistory.Currency = item.Currency;
                                            productHistory.RecordDate = DateTime.Now;
                                            productHistory.Description = item.Description;
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.
                                            HistoryListPrd.Add(productHistory);
                                        }
                                        Db.OfferProductsHistory.AddRange(HistoryListPrd);
                                        //Db.SaveChanges();
                                    }
                                }

                            }
                            var checkIsOneTimeOtherExpense = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == offerID).ToList();
                            if (checkIsOneTimeOtherExpense != null)
                            {
                                //var checkRevision = Db.IsOneTimeOtherExpenseHistory.ToList().LastOrDefault(t => t.OfferID == offerID);
                                List<IsOneTimeOtherExpenseHistory> HistoryListIsOneTimeOtherExpense = new List<IsOneTimeOtherExpenseHistory>();
                                if (checkOffer.IsRevision == false)// ilk (V1.0) düzenlemesi, Bu bir revizyon düzenlemesi değil.
                                {
                                    if (checkOffer.VersionDigit == null) // v 1.0
                                    {
                                        var otherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == 0 && t.VersionNumber == 1).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferOtherExpenseHistory.RemoveRange(otherExpenseHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkIsOneTimeOtherExpense)
                                        {
                                            //Satın alma için girilen urunleri ve fiyatları history tabloma kaydettim.
                                            IsOneTimeOtherExpenseHistory isOneTimeOtherExpenseHistory = new IsOneTimeOtherExpenseHistory();
                                            isOneTimeOtherExpenseHistory.OfferID = item.OfferID;
                                            isOneTimeOtherExpenseHistory.VersionNumber = 1;
                                            isOneTimeOtherExpenseHistory.VersionDigit = 0;
                                            isOneTimeOtherExpenseHistory.OtherExpenseItemID = item.OtherExpenseItemID;
                                            isOneTimeOtherExpenseHistory.Price = item.Price;
                                            isOneTimeOtherExpenseHistory.RecordDate = DateTime.Now;



                                            HistoryListIsOneTimeOtherExpense.Add(isOneTimeOtherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.IsOneTimeOtherExpenseHistory.AddRange(HistoryListIsOneTimeOtherExpense);
                                        //Db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Bir revizyon talebi yok ve satın alma fiyatları 2.kez değiştirmek istiyor. yani Version hala 1.0
                                        var otherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == 0).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferOtherExpenseHistory.RemoveRange(otherExpenseHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkIsOneTimeOtherExpense)
                                        {
                                            IsOneTimeOtherExpenseHistory isOneTimeOtherExpenseHistory = new IsOneTimeOtherExpenseHistory();
                                            isOneTimeOtherExpenseHistory.OfferID = item.OfferID;
                                            isOneTimeOtherExpenseHistory.VersionNumber = 1;
                                            isOneTimeOtherExpenseHistory.VersionDigit = checkOffer.VersionDigit;
                                            isOneTimeOtherExpenseHistory.OtherExpenseItemID = item.OtherExpenseItemID;
                                            isOneTimeOtherExpenseHistory.Price = item.Price;
                                            isOneTimeOtherExpenseHistory.RecordDate = DateTime.Now;



                                            HistoryListIsOneTimeOtherExpense.Add(isOneTimeOtherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.IsOneTimeOtherExpenseHistory.AddRange(HistoryListIsOneTimeOtherExpense);
                                        //Db.SaveChanges();

                                    }



                                }
                                else // Revizyon düzenlemesi
                                {
                                    //var NewVersionDigit = checkRevision.VersionDigit + 1;
                                    //if (checkOffer.VersionDigit + 1 == NewVersionDigit)
                                    //{
                                    if (checkOffer.VersionDigit != null)
                                    {
                                        int? NewVersionDigit = checkOffer.VersionDigit + 1;
                                        var otherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == NewVersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        Db.OfferOtherExpenseHistory.RemoveRange(otherExpenseHistoryList);
                                        Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.
                                        foreach (var item in checkIsOneTimeOtherExpense)
                                        {


                                            IsOneTimeOtherExpenseHistory isOneTimeOtherExpenseHistory = new IsOneTimeOtherExpenseHistory();
                                            isOneTimeOtherExpenseHistory.OfferID = item.OfferID;
                                            isOneTimeOtherExpenseHistory.VersionNumber = 1;
                                            isOneTimeOtherExpenseHistory.VersionDigit = NewVersionDigit;
                                            isOneTimeOtherExpenseHistory.OtherExpenseItemID = item.OtherExpenseItemID;
                                            isOneTimeOtherExpenseHistory.Price = item.Price;
                                            isOneTimeOtherExpenseHistory.RecordDate = DateTime.Now;



                                            HistoryListIsOneTimeOtherExpense.Add(isOneTimeOtherExpenseHistory);


                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.IsOneTimeOtherExpenseHistory.AddRange(HistoryListIsOneTimeOtherExpense);
                                        //Db.SaveChanges();

                                        //Teklifte bir revizyon yapıldı ve teklifin revizyon versiyonuda güncellendi.
                                    }
                                    else
                                    {
                                        int? NewVersionDigit = 0;
                                        //var prdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == checkRevision.VersionDigit).ToList(); // teklifin son versiyonundaki ürünleri yakaladım
                                        //Db.OfferProductsHistory.RemoveRange(prdHistoryList);
                                        //Db.SaveChanges();//ilk girilmiş ürünleri history tablomdan temizledim.

                                        foreach (var item in checkIsOneTimeOtherExpense)
                                        {

                                            IsOneTimeOtherExpenseHistory isOneTimeOtherExpenseHistory = new IsOneTimeOtherExpenseHistory();
                                            isOneTimeOtherExpenseHistory.OfferID = item.OfferID;
                                            isOneTimeOtherExpenseHistory.VersionNumber = 1;
                                            isOneTimeOtherExpenseHistory.VersionDigit = NewVersionDigit;
                                            isOneTimeOtherExpenseHistory.OtherExpenseItemID = item.OtherExpenseItemID;
                                            isOneTimeOtherExpenseHistory.Price = item.Price;
                                            isOneTimeOtherExpenseHistory.RecordDate = DateTime.Now;



                                            HistoryListIsOneTimeOtherExpense.Add(isOneTimeOtherExpenseHistory);
                                            /*checkOffer.StatusLevel = StatusLevel;*/ // teklifin surecı guncellendi.

                                        }
                                        Db.IsOneTimeOtherExpenseHistory.AddRange(HistoryListIsOneTimeOtherExpense);
                                        //Db.SaveChanges();

                                    }
                                }

                            }



                        }



                        checkOffer.StatusLevel = StatusLevel;
                        checkOffer.PurchasingManagerCheck = 1; //Satın alma müd. onay
                        checkOffer.RecordPurchasingManagerID = OypaHelper.RecordEmployee();
                        Db.SaveChanges();
                        var regionGroupMail = string.Empty;
                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklifin durumu {checkOffer.OfferStatus.StatusName} olarak güncellendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        //Fırsat teklife dönüştü, Teklifin durumu   değiştirdiğinde fırsatı oluşturan bölge mudurune mail gidecek.
                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}", //"gsahin@oypa.com.tr"
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }
                    }
                    else if (checkStatus.StatusLevel == 12) // Satın alma müd red
                    {

                        if (checkOffer.RegionalDirectorCheck != 1)
                        {
                            return Json(new { result = "RegionalDirectorCheckError" }, JsonRequestBehavior.AllowGet);

                        }

                        checkOffer.StatusLevel = StatusLevel;
                        checkOffer.PurchasingManagerCheck = 2; //Satın alma müd. onay
                        checkOffer.RecordPurchasingManagerID = OypaHelper.RecordEmployee();
                        Db.SaveChanges();
                        var regionGroupMail = string.Empty;
                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }

                        //Fırsat teklife dönüştü, Teklifin durumu   değiştirdiğinde fırsatı oluşturan bölge mudurune mail gidecek.
                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}", //"gsahin@oypa.com.tr"
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }
                    }

                    else if (checkStatus.StatusLevel == 15) // Koordinatör Onayına Gitmesi
                    {

                        checkOffer.StatusLevel = StatusLevel;
                        checkOffer.RecordPurchasingManagerID = OypaHelper.RecordEmployee();
                        checkOffer.CoordinatorCheck = 1;

                        Db.SaveChanges();

                        var regionGroupMail = string.Empty;

                        if (checkOffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkOffer.Employee.Region.GroupMail;
                        }
                        var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi.",
                                  "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                  $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{regionGroupMail}", //"gsahin@oypa.com.tr"
                                  "TYH - Teklif Durum Değişikliği",
                                  "http://194.100.100.21/", null, "CreatedOpportunity.html");
                        if (mailres == true)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{regionGroupMail} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        }
                        if (directorMailList != null && directorMailList.Count > 0)
                        {
                            foreach (var item in directorMailList)
                            {
                                mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi. Koordinasyon onayı bekliyor.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{item.Email}", //"gsahin@oypa.com.tr"
                                   "TYH - Teklif Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                if (mailres == true)
                                {
                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                }
                                else
                                {
                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                                }
                            }
                        }
                        //Koordinatör get

                    }
                    if(checkStatus.StatusLevel != 15)
                    {
                        if(checkOffer.RegionalDirectorCheck == 1)
                        {
                            //insan kaynakları check

                            var checkOfferPositions = Db.OfferPositions.Where(t => t.OfferID == offerID).ToList();

                            if(checkOfferPositions.Count() > 0)
                            {
                                if(checkOffer.HumanResourcesCheck == 1)
                                {
                                    var checkOfferProducts = Db.OfferProducts.Where(t => t.OfferID == offerID).ToList();

                                    var checkOfferEquipments = Db.OfferEquipment.Where(t => t.OfferID == offerID).ToList();
                                    var checkOfferOtherExpense = Db.OfferOtherExpenses.Where(t => t.OfferID == offerID && t.IsActive == true).ToList();

                                    if(checkOfferProducts.Count() > 0 || checkOfferEquipments.Count() > 0 || checkOfferOtherExpense.Count() > 0)
                                    {
                                        if(checkOffer.PurchasingManagerCheck == 1 && checkOffer.PurchasingSpecialistCheck == 1)
                                        {
                                            if(coordinatorMailList != null && coordinatorMailList.Count > 0)
                                            {
                                                foreach (var item in coordinatorMailList)
                                                {
                                                    var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi. Koordinasyon onayı bekliyor.",
                                                       "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                                       $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{item.Email}", //"gsahin@oypa.com.tr"
                                                       "TYH - Teklif Durum Değişikliği",
                                                       "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                                    if (mailres == true)
                                                    {
                                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                                    }
                                                    else
                                                    {
                                                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                                                    }
                                                }
                                            }
                                          
                                        }
                                    }
                                    else
                                    {
                                        if (coordinatorMailList != null && coordinatorMailList.Count > 0)
                                        {
                                            foreach (var item in coordinatorMailList)
                                            {
                                                var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi. Koordinasyon onayı bekliyor.",
                                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{item.Email}", //"gsahin@oypa.com.tr"
                                                   "TYH - Teklif Durum Değişikliği",
                                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                                if (mailres == true)
                                                {
                                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                                }
                                                else
                                                {
                                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var checkOfferProducts = Db.OfferProducts.Where(t => t.OfferID == offerID).ToList();

                                var checkOfferEquipments = Db.OfferEquipment.Where(t => t.OfferID == offerID).ToList();
                                var checkOfferOtherExpense = Db.OfferOtherExpenses.Where(t => t.OfferID == offerID && t.IsActive == true).ToList();

                                if (checkOfferProducts.Count() > 0 || checkOfferEquipments.Count() > 0 || checkOfferOtherExpense.Count() > 0)
                                {
                                    if (checkOffer.PurchasingManagerCheck == 1 && checkOffer.PurchasingSpecialistCheck == 1)
                                    {
                                        if (coordinatorMailList != null && coordinatorMailList.Count > 0)
                                        {
                                            foreach (var item in coordinatorMailList)
                                            {
                                                var mailres = OypaHelper.FillinEmailContent($"{checkOffer.OfferNumber + "-" + checkOffer.OfferName} isimli teklifin durumu {checkOffer.OfferStatus.StatusName} olarak değiştirildi. Koordinasyon onayı bekliyor.",
                                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak teklifi kontrol ediniz..",
                                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", $"{item.Email}", //"gsahin@oypa.com.tr"
                                                   "TYH - Teklif Durum Değişikliği",
                                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                                if (mailres == true)
                                                {
                                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", true, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                                }
                                                else
                                                {
                                                    OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkOffer.ID, "Offer", false, $"{item.Email} adresine teklif durum değişikliği bilgilendirme maili gönderilirken bir hata ile karşılaşıldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      
                    }
                   
                }
                //return RedirectToAction("Index", "Offer");
                return Json(new { result = "OfferUpdated" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex )
            {
                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", offerID.Value, "Offer", false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {offerID.Value} id'li teklifi süreci güncellenirken hata ile karşılaştı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace:{ex.StackTrace}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                OypaHelper.SendErrorMail("TYHErrorSaveOffer", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {offerID.Value} id'li teklifi süreci güncellenirken hata ile karşılaştı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace:{ex.StackTrace}");
                return Json(new { result = "GeneralError" }, JsonRequestBehavior.AllowGet);
             
            }
           

        }

        #region SendOfferInformation 
        //Teklifin müşteriye gönderildiği metottur. teklif hangi firmaya hazırlandı ise o firma için tanımlanan yetkili bilgilerine teklif pdf formatında mail atılır.
        //Teklifin müşteriye gönderilmesi iiçn teklif üst yazısının eklenmesi ve teklifin tüm birimler(Bölge Müdürü, İK, Satın alma) tarafından onaylanması gerekmektedir.
        #endregion
       
        public JsonResult SendOffer(SendOfferModel model)
        {

            var checkoffer = Db.Offer.FirstOrDefault(t => t.ID == model.offerID && t.IsActive == true);

            var htmltoPdf = string.Empty;
            try
            {
                if (checkoffer != null)
                {
                 

                    //Öncelikle IK, satın alma uzm. ve satın alma müd. onayladı mı ?
                    if (checkoffer.IsOneTime.Value)
                    {
                        var offerproducts = Db.OfferProducts.Where(i => i.OfferID == checkoffer.ID).ToList();
                        var offerpositions = Db.OfferPositions.Where(i => i.OfferID == checkoffer.ID).OrderBy(i => i.PositionItemID).ToList();
                        var otherExpenses = Db.OfferOtherExpenses.Where(i => i.IsActive == true && i.OfferID == checkoffer.ID).ToList();
                        var offerequipments = Db.OfferEquipment.Where(i => i.OfferID == checkoffer.ID).ToList();
                        if (offerpositions != null && offerpositions.Count() > 0)
                        {
                            
                            if (checkoffer.HumanResourcesCheck != 1) //Teklifin gönderilmesi için ik birimi onaylaması gerekmektedir.
                            {
                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif ik biriminden onay almadan müşteriye gönderilemez.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                return Json(new { success = "OfferConfirmError" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (offerproducts.Count() > 0 || otherExpenses.Count() > 0 || offerequipments.Count() > 0)
                        {
                            //Teklif gönderilirken satın alma tarafında müdür onayı teklifi göndermek için yeterli, uzman onayı zorunluluğu bulunmamaktadır.
                            if (/*checkoffer.PurchasingSpecialistCheck!=1 ||*/ checkoffer.PurchasingManagerCheck != 1)
                            {
                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif satın alma uzmanından onay almadan müşteriye gönderilemez.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                return Json(new { success = "OfferConfirmError" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (checkoffer.RegionalDirectorCheck != 1) //Teklifin gönderilmesi için Bölge müdürünün onaylaması gerekmektedir.
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif bölge müdürü tarafından onay almadan müşteriye gönderilemez.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            return Json(new { success = "OfferConfirmError" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        var IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(x => x.OfferID == checkoffer.ID).ToList();
                        var offerproducts = Db.OfferProducts.Where(i => i.OfferID == checkoffer.ID).ToList();
                        var IsOneTimeOfferOtherExpenseList = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == checkoffer.ID).ToList();

                        if (IsOneTimeOfferPosList != null && IsOneTimeOfferPosList.Count() > 0)
                        {
                            if (checkoffer.HumanResourcesCheck != 1)
                            {
                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif ik biriminden onay almadan müşteriye gönderilemez.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                return Json(new { success = "OfferConfirmError" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (offerproducts.Count() > 0 || IsOneTimeOfferOtherExpenseList.Count() > 0)
                        {
                            //Teklif gönderilirken satın alma tarafında müdür onayı teklifi göndermek için yeterli, uzman onayı zorunluluğu bulunmamaktadır.
                            if (checkoffer.PurchasingManagerCheck != 1)
                            {
                                OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif satın alma uzmanından onay almadan müşteriye gönderilemez.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                return Json(new { success = "OfferConfirmError" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (checkoffer.RegionalDirectorCheck != 1)
                        {
                            OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif bölge müdürü tarafından onay almadan müşteriye gönderilemez.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            return Json(new { success = "OfferConfirmError" }, JsonRequestBehavior.AllowGet);
                        }

                    }

                    var html = Db.OfferContract.FirstOrDefault(t => t.OfferID == model.offerID);//Teklif için özelleştiriilmiş Teklif üst yazısı
                    if (html == null) //Teklif için bir üst yazı oluşturulmadı.
                    {
                        OypaHelper.AddApplicationLog("Management", "SaveOffer", "Offer", checkoffer.ID, "Offer", true, $"{checkoffer.OfferNumber} nolu teklif için üst yazı şablonu oluşturulmadan müşteriye gönderilemez!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        return Json(new { success = "AddContract" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (html.OfferContentTemplateID == 3 || html.OfferContentTemplateID == 6) //Erdemir ve isdemir şablonları, burada fiyat belirtilmiyor
                        {
                            htmltoPdf = html.ContractContent;
                        }
                        else if (html.OfferContentTemplateID == 4 || html.OfferContentTemplateID == 5 || html.OfferContentTemplateID == 7) // İlaçlama, standart, tek seferlik
                        {
                            htmltoPdf = html.ContractContent;
                            htmltoPdf = htmltoPdf.Replace("#monthlyPrice#", checkoffer.MonthlyUnitPrice.Value.ToString("N2"));
            

                        }

                    }

                    Guid g = Guid.NewGuid();
                    var checkOfferContract = Db.OfferContract.FirstOrDefault(t => t.OfferID == model.offerID);
                    checkOfferContract.Guid = g.ToString();
                    using (MemoryStream stream = new System.IO.MemoryStream())
                    {
                        System.IO.FileStream fs = new FileStream(Server.MapPath("~/pdf/") + "\\" + g.ToString() + "ustyazi.pdf", FileMode.Create);
                        string filepath = g.ToString() + "ustyazi.pdf";
                        StringReader sr = new StringReader(htmltoPdf);
 
                        Document pdfDoc = new Document(PageSize.A4, 25, 25, 90, 140);
                        
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, fs);
                        writer.PageEvent = new HeaderandFooter();
                        pdfDoc.Open();

                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                        pdfDoc.Close();

                        var customerMailAddress = Db.CustomerContact.Where(t => t.IsActive == true && t.CustomerID == checkoffer.CustomerID).ToList(); ;


                        bool mailResult = false;
                        var mailresultCount = 0;
                        var errormail = string.Empty;
                        var sendedEmailAddress = string.Empty;

                        string regionGroupMail = null;
                        string TYHSupportGroupMail = null;
                        if (checkoffer.Employee.Region.GroupMail != null)
                        {
                            regionGroupMail = checkoffer.Employee.Region.GroupMail;
                        }
                        var tyhMail = Db.Region.Where(t => t.IsActive == true && t.Name == "TESİS YÖNETİM DİREKTÖRLÜĞÜ-OPERASYON DESTEK").FirstOrDefault();
                        if (tyhMail != null)
                        {
                            TYHSupportGroupMail = tyhMail.GroupMail;
                        }
                        if (customerMailAddress != null && customerMailAddress.Count() > 0)
                        {
                            foreach (var item in customerMailAddress)
                            {
                                if (model.CustomerEmailId.Contains(item.ID.ToString()))
                                {
                                    string mailContentTitle = "Sn." + item.FullName;
                                    mailResult = OypaHelper.FillinEmailContentWithMailGroup("Tarafınıza sunduğumuz teklife ait üst yazımız ektedir.", "Saygılarımızla..", mailContentTitle, item.Email, "Oyak Pazarlama Teklif Üst Yazısı Hk.", null, filepath, "CustomerMail.html", regionGroupMail, TYHSupportGroupMail); //"gsahin@oypa.com.tr"
                                    if (mailResult)
                                    {
                                        sendedEmailAddress += " " + item.Email;
                                        mailresultCount++;
                                        OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", checkoffer.ID, "Offer", true, $" {checkoffer.OfferNumber} nolu teklif için müşteri için tanımlanan {item.Email} mail adresine teklif üst yazısı başarıyla gönderildi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                    }
                                    else
                                    {
                                        errormail += item.Email + ", ";
                                        OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", checkoffer.ID, "Offer", false, $" {checkoffer.OfferNumber} nolu teklif için müşteri için tanımlanan {item.Email} mail adresine teklif üst yazısı gönderilirken bir hata oluştu.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                                    }
                                }
                               
                            }
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", checkoffer.ID, "Offer", false, $" {checkoffer.OfferNumber} nolu teklif için {checkoffer.Customers.CustomerFullName} isimli firmanın yetkili bilgileri sistem üzerinde tanımlı olmadığı için teklif üst yazı şablonu gönderilemedi!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                            return Json(new { success = "MissingEmail" }, JsonRequestBehavior.AllowGet);
                        }


                        if (mailresultCount == model.CustomerEmailId.Count())
                        {
                            checkoffer.IsRevision = false;
                            checkoffer.StatusLevel = 9; //Teklif gönderildiği anda  teklifin durumu otomatik olarak müşteri onay bekliyor olarak güncelleniyor.
                            if (checkoffer.VersionDigit == null)
                            {
                                checkoffer.VersiyonPrefix = "V";
                                checkoffer.VersionNumber = 1;
                                checkoffer.VersionDigit = 0;

                                //Gönderilen teklif history tablosuna da kaydedilmektedir.
                                OfferHistory history = new OfferHistory();
                                history.OfferID = checkoffer.ID;
                                history.OpportunityID = checkoffer.OpportunityID;
                                history.OfferNumber = checkoffer.OfferNumber;
                                history.VersiyonPrefix = checkoffer.VersiyonPrefix;
                                history.VersionNumber = checkoffer.VersionNumber;
                                history.VersionDigit = checkoffer.VersionDigit;
                                history.OfferName = checkoffer.OfferName;
                                history.DateBegin = checkoffer.DateBegin;
                                history.DateEnd = checkoffer.DateEnd;
                                history.StatusLevel = checkoffer.StatusLevel;
                                history.IsRevision = checkoffer.IsRevision;
                                history.CustomerID = checkoffer.CustomerID;
                                history.EmployeeID = checkoffer.EmployeeID;
                                history.RegionID = checkoffer.RegionID;
                                history.EnvironmentID = checkoffer.EnvironmentID;
                                history.ProjectID = checkoffer.ProjectID;
                                history.AraToplam = checkoffer.AraToplam;
                                history.AraToplamProfitRate = checkoffer.AraToplamProfitRate;
                                history.KdvToplam = checkoffer.KdvToplam;
                                history.BrutToplam = checkoffer.BrutToplam;
                                history.BrutToplamProfitRate = checkoffer.BrutToplamProfitRate;
                                history.GenelToplam = checkoffer.GenelToplam;
                                history.Description = checkoffer.Description;
                                history.RecordDate = DateTime.Now;
                                history.RecordEmployeeID = checkoffer.RecordEmployeeID;
                                history.RecordIP = checkoffer.RecordIP;
                                history.IsActive = checkoffer.IsActive;
                                history.IsOneTime = checkoffer.IsOneTime;
                                history.ManagementExpenseRateHR = checkoffer.ManagementExpenseRateHR;
                                history.ManagementExpenseRateProduct = checkoffer.ManagementExpenseRateProduct;
                                history.ManagementExpenseRateMD = checkoffer.ManagementExpenseRateMD;
                                history.ManagementExpenseRateOther = checkoffer.ManagementExpenseRateOther;
                                history.TotalOtherExpense = checkoffer.TotalOtherExpense;
                                history.ProfitRateForOthers = checkoffer.ProfitRateForOthers;
                                history.ProfitRateForMachine = checkoffer.ProfitRateForMachine;
                                history.TotalMonthlyDepreciation = checkoffer.TotalMonthlyDepreciation;
                                history.TotalHrProfit = checkoffer.TotalHrProfit;
                                history.SumAraToplamProfit = checkoffer.SumAraToplamProfit;
                                history.SumMachineExpenseProfit = checkoffer.SumMachineExpenseProfit;
                                history.SumOtherExpenseProfit = checkoffer.SumOtherExpenseProfit;
                                history.MonthlyUnitPrice = checkoffer.MonthlyUnitPrice;
                                history.PurchasingManagerCheck = checkoffer.PurchasingManagerCheck;
                                history.HumanResourcesCheck = checkoffer.HumanResourcesCheck;
                                history.RecordPurchasingManagerID = checkoffer.RecordPurchasingManagerID;
                                history.RecordHumanResourcesID = checkoffer.RecordHumanResourcesID;
                                history.PurchasingSpecialistCheck = checkoffer.PurchasingSpecialistCheck;
                                history.PurchasingSpecialistID = checkoffer.PurchasingSpecialistID;

                                Db.OfferHistory.Add(history);
                                Db.SaveChanges();


                            }
                            else
                            {
                                //Versiyon numaarı 1 arttırılarak yeni bir history kaydı atılır.
                                checkoffer.VersionDigit += 1;
                                OfferHistory history = new OfferHistory();
                                history.OfferID = checkoffer.ID;
                                history.OpportunityID = checkoffer.OpportunityID;
                                history.OfferNumber = checkoffer.OfferNumber;
                                history.VersiyonPrefix = checkoffer.VersiyonPrefix;
                                history.VersionNumber = checkoffer.VersionNumber;
                                history.VersionDigit = checkoffer.VersionDigit;
                                history.OfferName = checkoffer.OfferName;
                                history.DateBegin = checkoffer.DateBegin;
                                history.DateEnd = checkoffer.DateEnd;
                                history.StatusLevel = checkoffer.StatusLevel;
                                history.IsRevision = checkoffer.IsRevision;
                                history.CustomerID = checkoffer.CustomerID;
                                history.EmployeeID = checkoffer.EmployeeID;
                                history.RegionID = checkoffer.RegionID;
                                history.EnvironmentID = checkoffer.EnvironmentID;
                                history.ProjectID = checkoffer.ProjectID;
                                history.AraToplam = checkoffer.AraToplam;
                                history.AraToplamProfitRate = checkoffer.AraToplamProfitRate;
                                history.KdvToplam = checkoffer.KdvToplam;
                                history.BrutToplam = checkoffer.BrutToplam;
                                history.BrutToplamProfitRate = checkoffer.BrutToplamProfitRate;
                                history.GenelToplam = checkoffer.GenelToplam;
                                history.Description = checkoffer.Description;
                                history.RecordDate = DateTime.Now;
                                history.RecordEmployeeID = checkoffer.RecordEmployeeID;
                                history.RecordIP = checkoffer.RecordIP;
                                history.IsActive = checkoffer.IsActive;
                                history.IsOneTime = checkoffer.IsOneTime;
                                history.ManagementExpenseRateHR = checkoffer.ManagementExpenseRateHR;
                                history.ManagementExpenseRateProduct = checkoffer.ManagementExpenseRateProduct;
                                history.ManagementExpenseRateMD = checkoffer.ManagementExpenseRateMD;
                                history.ManagementExpenseRateOther = checkoffer.ManagementExpenseRateOther;
                                history.TotalOtherExpense = checkoffer.TotalOtherExpense;
                                history.ProfitRateForOthers = checkoffer.ProfitRateForOthers;
                                history.ProfitRateForMachine = checkoffer.ProfitRateForMachine;
                                history.TotalMonthlyDepreciation = checkoffer.TotalMonthlyDepreciation;
                                history.TotalHrProfit = checkoffer.TotalHrProfit;
                                history.SumAraToplamProfit = checkoffer.SumAraToplamProfit;
                                history.SumMachineExpenseProfit = checkoffer.SumMachineExpenseProfit;
                                history.SumOtherExpenseProfit = checkoffer.SumOtherExpenseProfit;
                                history.MonthlyUnitPrice = checkoffer.MonthlyUnitPrice;
                                history.PurchasingManagerCheck = checkoffer.PurchasingManagerCheck;
                                history.HumanResourcesCheck = checkoffer.HumanResourcesCheck;
                                history.RecordPurchasingManagerID = checkoffer.RecordPurchasingManagerID;
                                history.RecordHumanResourcesID = checkoffer.RecordHumanResourcesID;
                                history.PurchasingSpecialistCheck = checkoffer.PurchasingSpecialistCheck;
                                history.PurchasingSpecialistID = checkoffer.PurchasingSpecialistID;
                                Db.OfferHistory.Add(history);
                                Db.SaveChanges();
                            }

                            return Json(new { success = "OK", message = sendedEmailAddress }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            OypaHelper.SendErrorMail("TYHError-SendOffer", $" {checkoffer.OfferNumber} nolu teklif için müşteri için tanımlanan {errormail} mail adreslerine teklif üst yazısı gönderilirken bir hata oluştu.");
                            return Json(new { success = "NotOK", message = $"{errormail} adreslerine mail gönderilemedi!,Lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                        }


                    }
                }


                return Json(new { success = "NotOK", message = "Böyle bir teklif bulunamadı! " }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", checkoffer.ID, "Offer", false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {checkoffer.OfferNumber} nolu teklifi müşteriye gönderirken hata ile karşılaştı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace:{ex.StackTrace}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                OypaHelper.SendErrorMail("TYHErrorSendOffer", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {checkoffer.OfferNumber} nolu teklifi müşteriye gönderirken hata ile karşılaştı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace:{ex.StackTrace}");
                return Json(new { success = "NotOK", message = "Teklif müşteriye gönderilirken bir hata oluştu, Lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);

            }





        }

        public PartialViewResult GetOfferPositionItemsValue(int? rowID)
        {
            OfferControlModel model = new OfferControlModel();
            if (rowID > 0)
            {
                using (OypaDbEntities db = new OypaDbEntities())
                {
                    var offerposition = db.OfferPositions.FirstOrDefault(x => x.ID == rowID);
                    var offer = db.Offer.FirstOrDefault(x => x.ID == offerposition.OfferID);
                    model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offer.ID);
                    model.offerpositions = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == offerposition.PositionID && t.PositionItems.ParentID == 0).ToList();
                    //var position = db.Positions.FirstOrDefault(x => x.ID == offerposition.PositionID);
                    //var positionitem = db.PositionItems.FirstOrDefault(x => x.ID == offerposition.PositionItemID);
                    model.PositionItems = db.PositionItems.Where(t => t.ParentID == 0).ToList();
                    model.PositonsOptions = db.PositionOptions.ToList();
                    model.KidemTazValue = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 2 && t.IsActive == true).ParamaterValue;

                }
            }
            return PartialView("~/Views/Offer/_PartialOpsiyonelHesaplama.cshtml", model);
        }

        public PartialViewResult GetAllCalculateItems(int? offerID)
        {
            OfferControlModel model = new OfferControlModel();
            //Position Id ye göre distinc yaptım
            model.offerpositions = Db.OfferPositions.Where(t => t.OfferID == offerID).ToList();
            model.OfferPositionID = model.offerpositions.Select(t => t.PositionID).Distinct().ToList();
            model.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
            model.PositionStatuses = Db.PositionStatus.Where(i => i.IsActive == true).ToList();
            model.RowID = Db.OfferPositions.Where(t => t.OfferID == offerID).FirstOrDefault().ID;
            model.KidemTazValue = Db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 2 && t.IsActive == true).ParamaterValue;
            return PartialView("~/Views/Offer/_PartialAllCalculateItems.cshtml", model);

        }


        #region CalculateAllPriceInformation

        //İK tabı içerisine eklenen tüm personeller için  CalculateAllPersonelPrice metodunu tetikler.
        #endregion
        public PartialViewResult CalculateAllPrice(int[] positions, CalculateAllPrice model)
        {
            OfferControlModel offerModel = new OfferControlModel();
            if (positions.Count() > 0 && positions != null && model != null)
            {

                using (OypaDbEntities db = new OypaDbEntities())
                {
                    foreach (var item in positions)
                    {
                        var offerposition = db.OfferPositions.FirstOrDefault(x => x.OfferID == model.OfferID && x.PositionID == item); //seçilen personel tipi  hangi teklifte olduğu yakalandı.
                                                                                                                                       //var brutucret2 = db.OfferPositions.FirstOrDefault(x => x.OfferID == model.OfferID && x.PositionID == item && x.PositionItemID == 1).UnitPrice;//brut ücret
                        var brutucret = model.Brut;
                        model.rowID = offerposition.ID;
                        var offer = db.Offer.FirstOrDefault(x => x.ID == model.OfferID);

                        var position = db.Positions.FirstOrDefault(x => x.ID == item); //Hangi positon olduğunu bulduk.
                        var quantity = db.OfferPositions.FirstOrDefault((x => x.OfferID == model.OfferID && x.PositionID == item && x.PositionItemID == 1)).Quantity; // quantity'yi aldık.
                        CalculateAllPersonelPrice(model, brutucret.ToString(), offerposition, offer, position, quantity);


                    }
                }
            }
            offerModel.offerpositions = Db.OfferPositions.Where(x => x.OfferID == model.OfferID).OrderBy(x => x.PositionItemID).ToList();
            offerModel.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == model.OfferID);
            var HrProfiItems = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == model.OfferID).Select(t => t.PositionItemID).ToList();
            HrCalculateTotalProfit(HrProfiItems, offerModel.singleoffer.ManagementExpenseRateHR, offerModel.singleoffer.BrutToplamProfitRate, offerModel.singleoffer.ID);
            calculateFooter(model.OfferID);//İK Maliyet hesaplama

            return PartialView("_PartialOfferPositions", offerModel);
        }

        public PartialViewResult CalculateOpsiyonelItemsValue(CalculateAllPrice model)
        {
            OfferControlModel offerModel = new OfferControlModel();
            if (model != null)
            {
                var offerposition = Db.OfferPositions.FirstOrDefault(x => x.ID == model.rowID);
                var offer = Db.Offer.FirstOrDefault(x => x.ID == offerposition.OfferID);
                var Offerid = offer.ID;
                var quantity = Db.OfferPositions.FirstOrDefault((x => x.OfferID == Offerid && x.PositionID == offerposition.PositionID && x.PositionItemID == 1)).Quantity; // quantity'yi aldık.
                var position = Db.Positions.FirstOrDefault(x => x.ID == offerposition.PositionID);
                var positionitem = Db.PositionItems.FirstOrDefault(x => x.ID == offerposition.PositionItemID);


                //CalculateOpsiyonel(model, model.Brut, offerposition, offer, position, quantity);
                CalculateAllPersonelPrice(model, model.Brut, offerposition, offer, position, quantity);
                var HrProfiItems = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == offer.ID).Select(t => t.PositionItemID).ToList();

                HrCalculateTotalProfit(HrProfiItems, offer.ManagementExpenseRateHR, offer.BrutToplamProfitRate, offer.ID);


                calculateFooter(model.OfferID);//İK Maliyet hesaplama
                offerModel.offerpositions = Db.OfferPositions.Where(x => x.OfferID == model.OfferID).OrderBy(x => x.PositionItemID).ToList();
                offerModel.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == model.OfferID);

                //offerModel.OfferPositionID = offerModel.offerpositions.Select(t => t.PositionID).Distinct().ToList();
                //offerModel.PositionItems = Db.PositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();
                //offerModel.PositionStatuses = Db.PositionStatus.Where(i => i.IsActive == true).ToList();

            }
            return PartialView("_PartialOfferPositions", offerModel);
            //return PartialView();
        }

        #region CalculateAllPersonelPriceInformation
        //İk tabında personele ait tüm hesaplamaların yapıldığı metodtur.
        //Positon Options personle için opsiyonel hesaplamaların yazıldığı tablodur.
        //Offer position her personel için hesaplanması gereken kalemlerin hesaplanıp yazıldığı tablodur.
        //Her brüt ücret değişikliğinde bu metad tetiklenmektedir.
        #endregion
        public void CalculateAllPersonelPrice(CalculateAllPrice model, string brut, OfferPositions offerposition, Offer offer, Positions position, int? quantity)
        {
            if (model != null)
            {
                if (model.rowID > 0)
                {
                    using (OypaDbEntities db = new OypaDbEntities())
                    {
                     

                        double? brutUcret = !string.IsNullOrEmpty(brut) ? Convert.ToDouble(brut.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                        double? gvOrani;
                        var gvorani = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 15);
                        if (!string.IsNullOrWhiteSpace(model.GvOrani) && model.GvOrani != null)
                        {


                            model.GvOrani = model.GvOrani.Replace(".", ",");
                            gvOrani = Convert.ToDouble(model.GvOrani);
                            gvorani.Value = gvOrani;
                            db.SaveChanges();
                        }
                        else
                        {
                            gvOrani = 0;
                            gvorani.Value = 0;
                            db.SaveChanges();
                        }

                        int status = position.StatusID.Value;

                        SalaryManager sm = new SalaryManager(status, brutUcret.Value, gvOrani);


                        var salaryModel = sm.GetSalary();

                        List<OfferPositions> offpositionlist = new List<OfferPositions>();

                        var brutmaas = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 1); // tabloda bu kayıt varmı
                        if (brutmaas != null)
                        {
                            brutmaas.CostPrice = salaryModel.BrutUcret * quantity; //eleman sayısı * brutücret
                            brutmaas.UnitPrice = salaryModel.BrutUcret;
                            brutmaas.Quantity = quantity;
                            int s = db.SaveChanges();

                        }
                        var isnetucret = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 12); // tabloda bu kayıt varmı
                        if (isnetucret == null)
                        {
                            OfferPositions netucretitem = new OfferPositions();
                            netucretitem.OfferID = offer.ID;
                            netucretitem.PositionID = position.ID;
                            netucretitem.PositionItemID = 12;
                            netucretitem.Quantity = quantity;
                            netucretitem.CostPrice = salaryModel.NetUcret * quantity; //Maliyet(CostPrice): UnitPrice*quantity
                            netucretitem.UnitPrice = salaryModel.NetUcret;
                            netucretitem.Currency = "TRL";

                            offpositionlist.Add(netucretitem);
                        }
                        else
                        {

                            isnetucret.Quantity = quantity;
                            isnetucret.CostPrice = salaryModel.NetUcret * quantity;
                            isnetucret.UnitPrice = salaryModel.NetUcret;
                            db.SaveChanges();
                        }


                        var isiscisskitem = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 13); // tabloda bu kayıt varmı
                        if (isiscisskitem == null)
                        {
                            OfferPositions iscisskitem = new OfferPositions();
                            iscisskitem.OfferID = offer.ID;
                            iscisskitem.PositionID = position.ID;
                            iscisskitem.PositionItemID = 13;
                            iscisskitem.Quantity = quantity;
                            iscisskitem.CostPrice = salaryModel.IsciSSKKesintisi * quantity;
                            iscisskitem.UnitPrice = salaryModel.IsciSSKKesintisi;
                            iscisskitem.Currency = "TRL";

                            offpositionlist.Add(iscisskitem);
                        }
                        else
                        {
                            isiscisskitem.Quantity = quantity;
                            isiscisskitem.CostPrice = salaryModel.IsciSSKKesintisi * quantity;
                            isiscisskitem.UnitPrice = salaryModel.IsciSSKKesintisi;
                            db.SaveChanges();
                        }


                        var isisciissizlikitem = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 14); // tabloda bu kayıt varmı
                        if (isisciissizlikitem == null)
                        {
                            OfferPositions isciissizlikitem = new OfferPositions();
                            isciissizlikitem.OfferID = offer.ID;
                            isciissizlikitem.PositionID = position.ID;
                            isciissizlikitem.PositionItemID = 14;
                            isciissizlikitem.Quantity = quantity;
                            isciissizlikitem.CostPrice = salaryModel.IsciIssizlikKesintisi * quantity;
                            isciissizlikitem.UnitPrice = salaryModel.IsciIssizlikKesintisi;
                            isciissizlikitem.Currency = "TRL";

                            offpositionlist.Add(isciissizlikitem);

                        }
                        else
                        {
                            isisciissizlikitem.Quantity = quantity;
                            isisciissizlikitem.CostPrice = salaryModel.IsciIssizlikKesintisi * quantity;
                            isisciissizlikitem.UnitPrice = salaryModel.IsciIssizlikKesintisi;
                            db.SaveChanges();
                        }




                        var isiscigvitem = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 15); // tabloda bu kayıt varmı
                        if (isiscigvitem == null)
                        {
                            OfferPositions iscigvitem = new OfferPositions();
                            iscigvitem.OfferID = offer.ID;
                            iscigvitem.PositionID = position.ID;
                            iscigvitem.PositionItemID = 15;
                            iscigvitem.Quantity = quantity;
                            iscigvitem.CostPrice = salaryModel.GelirVergisi * quantity;
                            iscigvitem.UnitPrice = salaryModel.GelirVergisi;
                            iscigvitem.Currency = "TRL";

                            offpositionlist.Add(iscigvitem);

                        }
                        else
                        {
                            isiscigvitem.Quantity = quantity;
                            isiscigvitem.CostPrice = salaryModel.GelirVergisi * quantity;
                            isiscigvitem.UnitPrice = salaryModel.GelirVergisi;
                            db.SaveChanges();
                        }


                        var isiscidvitem = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 16); // tabloda bu kayıt varmı
                        if (isiscidvitem == null)
                        {
                            OfferPositions iscidvitem = new OfferPositions();
                            iscidvitem.OfferID = offer.ID;
                            iscidvitem.PositionID = position.ID;
                            iscidvitem.PositionItemID = 16;
                            iscidvitem.Quantity = quantity;
                            iscidvitem.CostPrice = salaryModel.IsciDamgaVergisi * quantity;
                            iscidvitem.UnitPrice = salaryModel.IsciDamgaVergisi;
                            iscidvitem.Currency = "TRL";

                            offpositionlist.Add(iscidvitem);

                        }
                        else
                        {
                            isiscidvitem.Quantity = quantity;
                            isiscidvitem.CostPrice = salaryModel.IsciDamgaVergisi * quantity;
                            isiscidvitem.UnitPrice = salaryModel.IsciDamgaVergisi;
                            db.SaveChanges();
                        }


                        //Yeni Eklenen Maliyet kalemleri
                        //İşçi GV İstisna Tutarı Yıllık Ort
                        var isisciGvIstisnaTutari = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 24); // tabloda bu kayıt varmı
                        //Değer oypa tarafından iletildi.
                        double defaultValue = 717.35;
                        if (isisciGvIstisnaTutari == null)
                        {
                            OfferPositions isciGvIstisnaTutari = new OfferPositions();
                            isciGvIstisnaTutari.OfferID = offer.ID;
                            isciGvIstisnaTutari.PositionID = position.ID;
                            isciGvIstisnaTutari.PositionItemID = 24;
                            isciGvIstisnaTutari.Quantity = quantity;
                            isciGvIstisnaTutari.CostPrice = defaultValue * quantity;
                            isciGvIstisnaTutari.UnitPrice = defaultValue;
                            isciGvIstisnaTutari.Currency = "TRL";

                            offpositionlist.Add(isciGvIstisnaTutari);

                        }
                        else
                        {
                            isisciGvIstisnaTutari.Quantity = quantity;
                            isisciGvIstisnaTutari.CostPrice = defaultValue * quantity;
                            isisciGvIstisnaTutari.UnitPrice = defaultValue;
                            db.SaveChanges();
                        }

                        var isisciDvIstisnaTutari = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 25); // tabloda bu kayıt varmı
                        //Değer oypa tarafından iletildi.
                        double defaultValue2 = 37.98;
                        if (isisciDvIstisnaTutari == null)
                        {
                            OfferPositions isciDvIstisnaTutari = new OfferPositions();
                            isciDvIstisnaTutari.OfferID = offer.ID;
                            isciDvIstisnaTutari.PositionID = position.ID;
                            isciDvIstisnaTutari.PositionItemID = 25;
                            isciDvIstisnaTutari.Quantity = quantity;
                            isciDvIstisnaTutari.CostPrice = defaultValue2 * quantity;
                            isciDvIstisnaTutari.UnitPrice = defaultValue2;
                            isciDvIstisnaTutari.Currency = "TRL";

                            offpositionlist.Add(isciDvIstisnaTutari);

                        }
                        else
                        {
                            isisciDvIstisnaTutari.Quantity = quantity;
                            isisciDvIstisnaTutari.CostPrice = defaultValue2 * quantity;
                            isisciDvIstisnaTutari.UnitPrice = defaultValue2;
                            db.SaveChanges();
                        }


                        double? sgkIsverenPayiUnitPriceForBordro = 0;
                        double? sgkIsverenPayiCostPriceForBordro = 0;
                        var sgkisverenpayiitem = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 2); // tabloda bu kayıt varmı
                        if (sgkisverenpayiitem == null)
                        {
                            OfferPositions sgkisverenitem = new OfferPositions();
                            sgkisverenitem.OfferID = offer.ID;
                            sgkisverenitem.PositionID = position.ID;
                            sgkisverenitem.PositionItemID = 2;
                            sgkisverenitem.Quantity = quantity;
                            sgkisverenitem.CostPrice = salaryModel.SgkIsverenPayi * quantity;
                            sgkisverenitem.UnitPrice = salaryModel.SgkIsverenPayi;
                            sgkisverenitem.Currency = "TRL";

                            //Bordro hesaplamaları için
                            sgkIsverenPayiUnitPriceForBordro = sgkisverenitem.UnitPrice;
                            sgkIsverenPayiCostPriceForBordro = sgkisverenitem.CostPrice;
                            offpositionlist.Add(sgkisverenitem);
                        }
                        else
                        {
                            sgkisverenpayiitem.Quantity = quantity;
                            sgkisverenpayiitem.CostPrice = salaryModel.SgkIsverenPayi * quantity;
                            sgkisverenpayiitem.UnitPrice = salaryModel.SgkIsverenPayi;
                            db.SaveChanges();

                            ////Bordro hesaplamaları için
                            //sgkIsverenPayiUnitPriceForBordro = sgkisverenpayiitem.UnitPrice;
                            //sgkIsverenPayiCostPriceForBordro = sgkisverenpayiitem.CostPrice;
                        }

                        double? sgkIssizlikIsverenPayiUnitPriceForBordro = 0;
                        double? sgkIssizlikIsverenPayiCostPriceForBordro = 0;

                        var sgkissizlikisverenpayiitem = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 3); // tabloda bu kayıt varmı
                        if (sgkissizlikisverenpayiitem == null)
                        {
                            OfferPositions sgkissizlikisverenitem = new OfferPositions();
                            sgkissizlikisverenitem.OfferID = offer.ID;
                            sgkissizlikisverenitem.PositionID = position.ID;
                            sgkissizlikisverenitem.PositionItemID = 3;
                            sgkissizlikisverenitem.Quantity = quantity;
                            sgkissizlikisverenitem.CostPrice = salaryModel.SgkIssizlikIsverenPayi * quantity;
                            sgkissizlikisverenitem.UnitPrice = salaryModel.SgkIssizlikIsverenPayi;
                            sgkissizlikisverenitem.Currency = "TRL";

                            offpositionlist.Add(sgkissizlikisverenitem);

                            sgkIssizlikIsverenPayiUnitPriceForBordro = sgkissizlikisverenitem.UnitPrice;
                            sgkIssizlikIsverenPayiCostPriceForBordro = sgkissizlikisverenitem.CostPrice;


                        }
                        else
                        {
                            sgkissizlikisverenpayiitem.Quantity = quantity;
                            sgkissizlikisverenpayiitem.CostPrice = salaryModel.SgkIssizlikIsverenPayi * quantity;
                            sgkissizlikisverenpayiitem.UnitPrice = salaryModel.SgkIssizlikIsverenPayi;
                            db.SaveChanges();
                        }

                        if (model.YillikIzin != null) //Yıllık izin
                        {
                            var yillikIzin = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 4); // tabloda bu kayıt varmı




                            if (model.YillikIzin == "14")
                            {

                                yillikIzin.Quantity = quantity;
                                yillikIzin.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * 16) / 12) * quantity);
                                yillikIzin.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * 16) / 12);
                                yillikIzin.Value = Convert.ToDouble(model.YillikIzin);


                            }
                            else if (model.YillikIzin == "20")
                            {
                                yillikIzin.Quantity = quantity;
                                yillikIzin.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * 23) / 12) * quantity);
                                yillikIzin.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * 23) / 12);
                                yillikIzin.Value = Convert.ToDouble(model.YillikIzin);

                            }
                            else if (model.YillikIzin == "26")
                            {
                                yillikIzin.Quantity = quantity;
                                yillikIzin.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * 30) / 12) * quantity);
                                yillikIzin.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * 30) / 12);
                                yillikIzin.Value = Convert.ToDouble(model.YillikIzin);
                            }
                            else if (model.YillikIzin == "yillikIzinCancel")
                            {
                                yillikIzin.Quantity = quantity;
                                yillikIzin.CostPrice = 0;
                                yillikIzin.UnitPrice = 0;
                                yillikIzin.Value = 0;
                            }
                            db.SaveChanges();


                        }

                        if (model.RadioYol != null)
                        {
                            if (model.Yol != null)
                            {

                                double? RoadbrutUcret = !string.IsNullOrEmpty(model.Yol) ? Convert.ToDouble(model.Yol.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                                var RoadExpense = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 9);

                                if (RoadExpense != null)
                                {

                                    if (model.RadioYol == "bordro") //positons option bordro
                                    {

                                        if (RoadExpense.PositionOptionID == null)
                                        {
                                            if (status == 1) //position status çalışan ise hesaplamayı çalışan oranına göre yap
                                            {

                                                //Eski Versiyon, belirlenen İK Brüt oranına göre hesaplama yapılıyordu
                                                //var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                                //if (brutOran != null)
                                                //{

                                                //    PositionOptions po = new PositionOptions();
                                                //    po.PositionItemID = 9;
                                                //    po.OptionName = "bordro";
                                                //    db.PositionOptions.Add(po);
                                                //    db.SaveChanges();
                                                //    RoadExpense.Quantity = quantity;
                                                //    RoadExpense.CostPrice = ((RoadbrutUcret * brutOran) * quantity);
                                                //    RoadExpense.UnitPrice = (RoadbrutUcret * brutOran);
                                                //    RoadExpense.Value = RoadbrutUcret;
                                                //    RoadExpense.PositionOptionID = po.ID;
                                                //}
                                                var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                                if (brutOran != null)
                                                {
                                                    PositionOptions po = new PositionOptions();
                                                    po.PositionItemID = 9;
                                                    po.OptionName = "bordro";
                                                    db.PositionOptions.Add(po);
                                                    db.SaveChanges();
                                                    RoadExpense.Quantity = quantity;
                                                    RoadExpense.UnitPrice = RoadbrutUcret * brutOran;
                                                    RoadExpense.CostPrice = (RoadExpense.UnitPrice * quantity);
                                                    RoadExpense.Value = RoadbrutUcret;
                                                    RoadExpense.PositionOptionID = po.ID;

                                                }

                                                if (sgkisverenpayiitem == null)
                                                {

                                                    //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                    sgkIsverenPayiUnitPriceForBordro += (RoadExpense.UnitPrice.Value * 0.205);
                                                    sgkIsverenPayiCostPriceForBordro += (RoadExpense.UnitPrice.Value * 0.205) * quantity;
                                                }
                                                else
                                                {
                                                    //
                                                    sgkisverenpayiitem.UnitPrice += (RoadExpense.UnitPrice.Value * 0.205);
                                                    sgkisverenpayiitem.CostPrice += (RoadExpense.UnitPrice.Value * 0.205) * quantity;
                                                }

                                                if (sgkissizlikisverenpayiitem == null)
                                                {
                                                    //Bordro olduğu için Sgk İşsizlik ekleme yapılacak
                                                    sgkIssizlikIsverenPayiUnitPriceForBordro += (RoadExpense.UnitPrice.Value * 0.02);
                                                    sgkIssizlikIsverenPayiCostPriceForBordro += (RoadExpense.UnitPrice.Value * 0.02) * quantity;
                                                }
                                                else
                                                {
                                                    sgkissizlikisverenpayiitem.UnitPrice += (RoadExpense.UnitPrice.Value * 0.02);
                                                    sgkissizlikisverenpayiitem.CostPrice += (RoadExpense.UnitPrice.Value * 0.02) * quantity;
                                                }


                                            }
                                            else
                                            {
                                                //position status emekli ise hesaplamayı emekli oranına göre yap
                                                var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 4 && t.IsActive == true).ParamaterValue;
                                                if (brutOran != null)
                                                {

                                                    PositionOptions po = new PositionOptions();
                                                    po.PositionItemID = 9;
                                                    po.OptionName = "bordro";
                                                    db.PositionOptions.Add(po);
                                                    db.SaveChanges();
                                                    RoadExpense.Quantity = quantity;
                                                    RoadExpense.UnitPrice = RoadbrutUcret * brutOran;
                                                    RoadExpense.CostPrice = RoadExpense.UnitPrice * quantity;
                                                    RoadExpense.Value = RoadbrutUcret;
                                                    RoadExpense.PositionOptionID = po.ID;


                                                }


                                                if (sgkisverenpayiitem == null)
                                                {
                                                    //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                    sgkIsverenPayiUnitPriceForBordro += (RoadExpense.UnitPrice.Value * 0.245);
                                                    sgkIsverenPayiCostPriceForBordro += (RoadExpense.UnitPrice.Value * 0.245) * quantity;
                                                }
                                                else
                                                {
                                                    //
                                                    sgkisverenpayiitem.UnitPrice += (RoadExpense.UnitPrice.Value * 0.245);
                                                    sgkisverenpayiitem.CostPrice += (RoadExpense.UnitPrice.Value * 0.245) * quantity;
                                                }




                                            }



                                        }
                                        else
                                        {
                                            if (status == 1) //position status çalışan ise hesaplamayı çalışan oranına göre yap
                                            {
                                                //Eski hesaplama
                                                //var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                                //if (brutOran != null)
                                                //{

                                                //    RoadExpense.Quantity = quantity;
                                                //    RoadExpense.CostPrice = ((RoadbrutUcret * brutOran) * quantity);
                                                //    RoadExpense.UnitPrice = (RoadbrutUcret * brutOran);
                                                //    RoadExpense.Value = RoadbrutUcret;
                                                //    RoadExpense.PositionOptions.OptionName = model.RadioYol;
                                                //}

                                                var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                                if (brutOran != null)
                                                {

                                                    RoadExpense.Quantity = quantity;
                                                    RoadExpense.UnitPrice = RoadbrutUcret * brutOran;
                                                    RoadExpense.CostPrice = (RoadExpense.UnitPrice * quantity);
                                                    RoadExpense.Value = RoadbrutUcret;
                                                    RoadExpense.PositionOptions.OptionName = model.RadioYol;
                                                }



                                                if (sgkisverenpayiitem == null)
                                                {

                                                    //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                    sgkIsverenPayiUnitPriceForBordro += (RoadExpense.UnitPrice.Value * 0.205);
                                                    sgkIsverenPayiCostPriceForBordro += (RoadExpense.UnitPrice.Value * 0.205) * quantity;
                                                }
                                                else
                                                {
                                                    //
                                                    sgkisverenpayiitem.UnitPrice += (RoadExpense.UnitPrice.Value * 0.205);
                                                    sgkisverenpayiitem.CostPrice += (RoadExpense.UnitPrice.Value * 0.205) * quantity;
                                                }

                                                if (sgkissizlikisverenpayiitem == null)
                                                {
                                                    //Bordro olduğu için Sgk İşsizlik ekleme yapılacak
                                                    sgkIssizlikIsverenPayiUnitPriceForBordro += (RoadExpense.UnitPrice.Value * 0.02);
                                                    sgkIssizlikIsverenPayiCostPriceForBordro += (RoadExpense.UnitPrice.Value * 0.02) * quantity;
                                                }
                                                else
                                                {
                                                    sgkissizlikisverenpayiitem.UnitPrice += (RoadExpense.UnitPrice.Value * 0.02);
                                                    sgkissizlikisverenpayiitem.CostPrice += (RoadExpense.UnitPrice.Value * 0.02) * quantity;
                                                }


                                            }
                                            else
                                            {
                                                //position status emekli ise hesaplamayı emekli oranına göre yap
                                                var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 4 && t.IsActive == true).ParamaterValue;
                                                if (brutOran != null)
                                                {
                                                    RoadExpense.Quantity = quantity;
                                                    RoadExpense.UnitPrice = RoadbrutUcret * brutOran;
                                                    RoadExpense.CostPrice = RoadExpense.UnitPrice * quantity;
                                                    RoadExpense.Value = RoadbrutUcret;
                                                    RoadExpense.PositionOptions.OptionName = model.RadioYol;

                                                    if (sgkisverenpayiitem == null)
                                                    {
                                                        //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                        sgkIsverenPayiUnitPriceForBordro += (RoadExpense.UnitPrice.Value * 0.245);
                                                        sgkIsverenPayiCostPriceForBordro += (RoadExpense.UnitPrice.Value * 0.245) * quantity;
                                                    }
                                                    else
                                                    {
                                                        //
                                                        sgkisverenpayiitem.UnitPrice += (RoadExpense.UnitPrice.Value * 0.245);
                                                        sgkisverenpayiitem.CostPrice += (RoadExpense.UnitPrice.Value * 0.245) * quantity;
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else //kart ise direk net ücreti yaz quantity ile çarp
                                    {
                                        if (RoadExpense.PositionOptionID == null)
                                        {
                                            //Position durumuna bakmaya gerek yok kart ise direk net ücret * quantity
                                            PositionOptions po = new PositionOptions();
                                            po.PositionItemID = 9;
                                            po.OptionName = "kart";
                                            db.PositionOptions.Add(po);
                                            db.SaveChanges();
                                            RoadExpense.Quantity = quantity;
                                            RoadExpense.CostPrice = (RoadbrutUcret * quantity);
                                            RoadExpense.UnitPrice = RoadbrutUcret;
                                            RoadExpense.Value = RoadbrutUcret;
                                            RoadExpense.PositionOptionID = po.ID;

                                        }
                                        else
                                        {

                                            RoadExpense.Quantity = quantity;
                                            RoadExpense.CostPrice = (RoadbrutUcret * quantity);
                                            RoadExpense.UnitPrice = RoadbrutUcret;
                                            RoadExpense.Value = RoadbrutUcret;
                                            RoadExpense.PositionOptions.OptionName = model.RadioYol;

                                        }
                                    }
                                }
                                db.SaveChanges();
                            }
                        }
                        if (model.RadioYemek != null)
                        {
                            if (model.Yemek != null)
                            {
                                double? foodSalaryValue = !string.IsNullOrEmpty(model.Yemek) ? Convert.ToDouble(model.Yemek.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                                var FoodSalary = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 10);
                                if (model.RadioYemek == "bordro")
                                {

                                    if (FoodSalary.PositionOptionID == null)
                                    {
                                        if (status == 1) //position status çalışan ise hesaplamayı çalışan oranına göre yap
                                        {
                                            //Eski Hesaplama Yöntemi
                                            //var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                            //if (brutOran != null)
                                            //{
                                            //    PositionOptions po = new PositionOptions();
                                            //    po.PositionItemID = 10;
                                            //    po.OptionName = "bordro";
                                            //    db.PositionOptions.Add(po);
                                            //    db.SaveChanges();

                                            //    FoodSalary.Quantity = quantity;
                                            //    FoodSalary.CostPrice = ((foodSalaryValue * brutOran) * quantity);
                                            //    FoodSalary.UnitPrice = (foodSalaryValue * brutOran);
                                            //    FoodSalary.Value = foodSalaryValue;
                                            //    FoodSalary.PositionOptionID = po.ID;
                                            //}
                                            var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                            if (brutOran != null)
                                            {


                                                PositionOptions po = new PositionOptions();
                                                po.PositionItemID = 10;
                                                po.OptionName = "bordro";
                                                db.PositionOptions.Add(po);
                                                db.SaveChanges();

                                                FoodSalary.Quantity = quantity;
                                                FoodSalary.UnitPrice = foodSalaryValue * brutOran;
                                                FoodSalary.CostPrice = FoodSalary.UnitPrice * quantity;
                                                FoodSalary.Value = foodSalaryValue;
                                                FoodSalary.PositionOptionID = po.ID;
                                            }
                                            if (sgkisverenpayiitem == null)
                                            {

                                                //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                sgkIsverenPayiUnitPriceForBordro += (FoodSalary.UnitPrice.Value * 0.205);
                                                sgkIsverenPayiCostPriceForBordro += (FoodSalary.UnitPrice.Value * 0.205) * quantity;
                                            }
                                            else
                                            {
                                                //
                                                sgkisverenpayiitem.UnitPrice += (FoodSalary.UnitPrice.Value * 0.205);
                                                sgkisverenpayiitem.CostPrice += (FoodSalary.UnitPrice.Value * 0.205) * quantity;
                                            }

                                            if (sgkissizlikisverenpayiitem == null)
                                            {
                                                //Bordro olduğu için Sgk İşsizlik ekleme yapılacak
                                                sgkIssizlikIsverenPayiUnitPriceForBordro += (FoodSalary.UnitPrice.Value * 0.02);
                                                sgkIssizlikIsverenPayiCostPriceForBordro += (FoodSalary.UnitPrice.Value * 0.02) * quantity;
                                            }
                                            else
                                            {
                                                sgkissizlikisverenpayiitem.UnitPrice += (FoodSalary.UnitPrice.Value * 0.02);
                                                sgkissizlikisverenpayiitem.CostPrice += (FoodSalary.UnitPrice.Value * 0.02) * quantity;
                                            }




                                        }
                                        else if (status == 2)
                                        {
                                            var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 4 && t.IsActive == true).ParamaterValue;
                                            if (brutOran != null)
                                            {

                                                PositionOptions po = new PositionOptions();
                                                po.PositionItemID = 10;
                                                po.OptionName = "bordro";
                                                db.PositionOptions.Add(po);
                                                db.SaveChanges();

                                                FoodSalary.Quantity = quantity;
                                                FoodSalary.UnitPrice = foodSalaryValue * brutOran;
                                                FoodSalary.CostPrice = (FoodSalary.UnitPrice * quantity);
                                                FoodSalary.Value = foodSalaryValue;
                                                FoodSalary.PositionOptionID = po.ID;
                                            }

                                            if (sgkisverenpayiitem == null)
                                            {

                                                //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                sgkIsverenPayiUnitPriceForBordro += (FoodSalary.UnitPrice.Value * 0.245);
                                                sgkIsverenPayiCostPriceForBordro += (FoodSalary.UnitPrice.Value * 0.245) * quantity;
                                            }
                                            else
                                            {
                                                //
                                                sgkisverenpayiitem.UnitPrice += (FoodSalary.UnitPrice.Value * 0.245);
                                                sgkisverenpayiitem.CostPrice += (FoodSalary.UnitPrice.Value * 0.245) * quantity;
                                            }


                                            //}
                                        }



                                    }
                                    else
                                    {
                                        if (status == 1) //position status çalışan ise hesaplamayı çalışan oranına göre yap
                                        {

                                            //var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                            //if (brutOran != null)
                                            //{

                                            //    FoodSalary.Quantity = quantity;
                                            //    FoodSalary.CostPrice = ((foodSalaryValue * brutOran) * quantity);
                                            //    FoodSalary.UnitPrice = (foodSalaryValue * brutOran);
                                            //    FoodSalary.Value = foodSalaryValue;
                                            //    FoodSalary.PositionOptions.OptionName = model.RadioYemek;
                                            //}


                                            var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 3 && t.IsActive == true).ParamaterValue;
                                            if (brutOran != null)
                                            {
                                                FoodSalary.Quantity = quantity;
                                                FoodSalary.UnitPrice = foodSalaryValue * brutOran;
                                                FoodSalary.CostPrice = (FoodSalary.UnitPrice * quantity);
                                                FoodSalary.Value = foodSalaryValue;
                                                FoodSalary.PositionOptions.OptionName = model.RadioYemek;
                                            }
                                            if (sgkisverenpayiitem == null)
                                            {

                                                //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                sgkIsverenPayiUnitPriceForBordro += (FoodSalary.UnitPrice.Value * 0.205);
                                                sgkIsverenPayiCostPriceForBordro += (FoodSalary.UnitPrice.Value * 0.205) * quantity;
                                            }
                                            else
                                            {
                                                //
                                                sgkisverenpayiitem.UnitPrice += (FoodSalary.UnitPrice.Value * 0.205);
                                                sgkisverenpayiitem.CostPrice += (FoodSalary.UnitPrice.Value * 0.205) * quantity;
                                            }

                                            if (sgkissizlikisverenpayiitem == null)
                                            {
                                                //Bordro olduğu için Sgk İşsizlik ekleme yapılacak
                                                sgkIssizlikIsverenPayiUnitPriceForBordro += (FoodSalary.UnitPrice.Value * 0.02);
                                                sgkIssizlikIsverenPayiCostPriceForBordro += (FoodSalary.UnitPrice.Value * 0.02) * quantity;
                                            }
                                            else
                                            {
                                                sgkissizlikisverenpayiitem.UnitPrice += (FoodSalary.UnitPrice.Value * 0.02);
                                                sgkissizlikisverenpayiitem.CostPrice += (FoodSalary.UnitPrice.Value * 0.02) * quantity;
                                            }

                                        }
                                        else if (status == 2)
                                        {
                                            //var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 4 && t.IsActive == true).ParamaterValue;
                                            //if (brutOran != null)
                                            //{

                                            //    FoodSalary.Quantity = quantity;
                                            //    FoodSalary.CostPrice = ((foodSalaryValue * brutOran) * quantity);
                                            //    FoodSalary.UnitPrice = (foodSalaryValue * brutOran);
                                            //    FoodSalary.Value = foodSalaryValue;
                                            //    FoodSalary.PositionOptions.OptionName = model.RadioYemek;
                                            //}


                                            var brutOran = db.ConstantParamaterDetails.FirstOrDefault(t => t.ConstantParameterId == 4 && t.IsActive == true).ParamaterValue;
                                            if (brutOran != null)
                                            {
                                                FoodSalary.Quantity = quantity;
                                                FoodSalary.UnitPrice = foodSalaryValue * brutOran;
                                                FoodSalary.CostPrice = (FoodSalary.UnitPrice * quantity);
                                                FoodSalary.Value = foodSalaryValue;
                                                FoodSalary.PositionOptions.OptionName = model.RadioYemek;
                                            }
                                            if (sgkisverenpayiitem == null)
                                            {
                                                //Bordro olduğu için Sgk İşverene ekleme yapılacak
                                                sgkIsverenPayiUnitPriceForBordro += (FoodSalary.UnitPrice.Value * 0.245);
                                                sgkIsverenPayiCostPriceForBordro += (FoodSalary.UnitPrice.Value * 0.245) * quantity;
                                            }
                                            else
                                            {
                                                //
                                                sgkisverenpayiitem.UnitPrice += (FoodSalary.UnitPrice.Value * 0.245);
                                                sgkisverenpayiitem.CostPrice += (FoodSalary.UnitPrice.Value * 0.245) * quantity;
                                            }
                                        }



                                    }
                                    //db.SaveChanges();
                                }
                                else
                                {
                                    if (FoodSalary.PositionOptionID == null)
                                    {
                                        PositionOptions po = new PositionOptions();
                                        po.PositionItemID = 10;
                                        po.OptionName = "kart";
                                        db.PositionOptions.Add(po);
                                        db.SaveChanges();
                                        FoodSalary.Quantity = quantity;
                                        FoodSalary.CostPrice = (foodSalaryValue * quantity);
                                        FoodSalary.UnitPrice = foodSalaryValue;
                                        FoodSalary.Value = foodSalaryValue;
                                        FoodSalary.PositionOptionID = po.ID;
                                    }
                                    else
                                    {
                                        FoodSalary.Quantity = quantity;
                                        FoodSalary.CostPrice = (foodSalaryValue * quantity);
                                        FoodSalary.UnitPrice = foodSalaryValue;
                                        FoodSalary.Value = foodSalaryValue;
                                        FoodSalary.PositionOptions.OptionName = model.RadioYemek;


                                    }

                                    //db.SaveChanges();
                                }
                                db.SaveChanges();
                            }
                        }



                        //İhbar tazmınatı hesapla radio butonu  şeçilmiş ise hesaplanacak.

                        if (model.BayramMesai != null)
                        {
                            double? bayramMesaiGun = !string.IsNullOrEmpty(model.BayramMesai) ? Convert.ToDouble(model.BayramMesai.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;


                            var bayramMesai = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 7); // tabloda bu kayıt varmı
                            if (bayramMesai != null)
                            {

                                bayramMesai.Quantity = quantity;
                                bayramMesai.CostPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * bayramMesaiGun) / 12) * quantity;// 1 aya düşen bayram mesaisi
                                bayramMesai.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 30) * bayramMesaiGun) / 12);
                                bayramMesai.Value = bayramMesaiGun;





                            }


                        }
                        if (model.SaglikGideri != null) //Hesaplama istenmedi
                        {

                            double? saglikgideriValue = !string.IsNullOrEmpty(model.SaglikGideri) ? Convert.ToDouble(model.SaglikGideri.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                            var saglikGideri = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 11); // tabloda bu kayıt varmı
                            if (saglikGideri != null)
                            {

                                saglikGideri.Quantity = quantity;
                                saglikGideri.CostPrice = saglikgideriValue * quantity;
                                saglikGideri.UnitPrice = saglikgideriValue;
                                saglikGideri.Value = saglikgideriValue;

                            }

                        }
                        if (model.IsgGideri != null)
                        {

                            double? isgGideri = !string.IsNullOrEmpty(model.IsgGideri) ? Convert.ToDouble(model.IsgGideri.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                            var IsgGideri = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 17);
                            if (IsgGideri != null)
                            {
                                IsgGideri.Quantity = quantity;
                                IsgGideri.CostPrice = isgGideri * quantity;
                                IsgGideri.UnitPrice = isgGideri;
                                IsgGideri.Value = isgGideri;
                            }
                        }
                        if (model.PoliceGideri != null)
                        {
                            double? PoliceGideritxt = !string.IsNullOrEmpty(model.PoliceGideri) ? Convert.ToDouble(model.PoliceGideri.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                            var PoliceGideri = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 18);
                            if (PoliceGideri != null)
                            {
                                PoliceGideri.Quantity = quantity;
                                PoliceGideri.CostPrice = PoliceGideritxt * quantity;
                                PoliceGideri.UnitPrice = PoliceGideritxt;
                                PoliceGideri.Value = PoliceGideritxt;
                            }

                        }

                        if (model.radioFazlaMesai != null) // Radio buttondan bir option seçildi mi
                        {
                            if (model.FazlaMesai != null) //fazla mesai  saati girildi mi
                            {
                                double? fazlaMesaiTxt = !string.IsNullOrEmpty(model.FazlaMesai) ? Convert.ToDouble(model.FazlaMesai.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                                var FazlaMesai = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 21);

                                if (FazlaMesai != null)
                                {
                                    if (model.radioFazlaMesai == "1,5") //Fazla mesai çarpanı 1,5 ise
                                    {
                                        if (FazlaMesai.PositionOptionID == null)
                                        {
                                            PositionOptions po = new PositionOptions();
                                            po.PositionItemID = 21;
                                            po.OptionName = "1,5";
                                            db.PositionOptions.Add(po);
                                            db.SaveChanges();
                                            FazlaMesai.Quantity = quantity;
                                            FazlaMesai.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 1.5) * quantity);
                                            FazlaMesai.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 1.5);
                                            FazlaMesai.Value = fazlaMesaiTxt;
                                            FazlaMesai.PositionOptionID = po.ID;


                                        }
                                        else
                                        {
                                            FazlaMesai.Quantity = quantity;
                                            FazlaMesai.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 1.5) * quantity);
                                            FazlaMesai.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 1.5);
                                            FazlaMesai.Value = fazlaMesaiTxt;
                                            FazlaMesai.PositionOptions.OptionName = model.radioFazlaMesai;
                                        }



                                    }
                                    else
                                    {
                                        if (FazlaMesai.PositionOptionID == null)
                                        {
                                            PositionOptions po = new PositionOptions();
                                            po.PositionItemID = 21;
                                            po.OptionName = "2";
                                            db.PositionOptions.Add(po);
                                            db.SaveChanges();
                                            FazlaMesai.Quantity = quantity;
                                            FazlaMesai.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 2) * quantity);
                                            FazlaMesai.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 2);
                                            FazlaMesai.Value = fazlaMesaiTxt;
                                            FazlaMesai.PositionOptionID = po.ID;


                                        }
                                        else
                                        {
                                            FazlaMesai.Quantity = quantity;
                                            FazlaMesai.CostPrice = (((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 2) * quantity);
                                            FazlaMesai.UnitPrice = ((((salaryModel.BrutUcret + salaryModel.SgkIsverenPayi + salaryModel.SgkIssizlikIsverenPayi) / 225) * fazlaMesaiTxt) * 2);
                                            FazlaMesai.Value = fazlaMesaiTxt;
                                            FazlaMesai.PositionOptions.OptionName = model.radioFazlaMesai;
                                        }


                                    }


                                }
                            }
                        }
                        if (model.IhaleBelgeMasrafi != null)
                        {
                            double? IhaleMasrafiValue = !string.IsNullOrEmpty(model.IhaleBelgeMasrafi) ? Convert.ToDouble(model.IhaleBelgeMasrafi.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                            var IhaleMasrafi = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 19);
                            if (IhaleMasrafi != null)
                            {
                                IhaleMasrafi.Quantity = quantity;
                                IhaleMasrafi.CostPrice = IhaleMasrafiValue * quantity;
                                IhaleMasrafi.UnitPrice = IhaleMasrafiValue;
                                IhaleMasrafi.Value = IhaleMasrafiValue;
                            }
                        }
                        if (model.EgitimBelgeGideri != null)
                        {
                            double? EgitimGideriValue = !string.IsNullOrEmpty(model.EgitimBelgeGideri) ? Convert.ToDouble(model.EgitimBelgeGideri.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                            var EgitimGideri = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 20);
                            if (EgitimGideri != null)
                            {
                                EgitimGideri.Quantity = quantity;
                                EgitimGideri.CostPrice = EgitimGideriValue * quantity;
                                EgitimGideri.UnitPrice = EgitimGideriValue;
                                EgitimGideri.Value = EgitimGideriValue;

                            }


                        }
                        if (model.SaglikSigortasi != null)
                        {
                            double? SaglikSigortasiValue = !string.IsNullOrEmpty(model.SaglikSigortasi) ? Convert.ToDouble(model.SaglikSigortasi.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                            var SaglikSigortasi = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 22);
                            if (SaglikSigortasi != null)
                            {

                                SaglikSigortasi.Quantity = quantity;
                                SaglikSigortasi.CostPrice = SaglikSigortasiValue * quantity;
                                SaglikSigortasi.UnitPrice = SaglikSigortasiValue;
                                SaglikSigortasi.Value = SaglikSigortasiValue;
                                db.SaveChanges();

                            }


                        }
                        if (model.BireyselEmeklilik != null)
                        {
                            double? BireyselEmeklilikValue = !string.IsNullOrEmpty(model.BireyselEmeklilik) ? Convert.ToDouble(model.BireyselEmeklilik.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;

                            var BireyselEmeklilik = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 23);
                            if (BireyselEmeklilik != null)
                            {
                                BireyselEmeklilik.Quantity = quantity;
                                BireyselEmeklilik.CostPrice = BireyselEmeklilikValue * quantity;
                                BireyselEmeklilik.UnitPrice = BireyselEmeklilikValue;
                                BireyselEmeklilik.Value = BireyselEmeklilikValue;
                                db.SaveChanges();
                            }
                        }


                        if (model.KidemTaz != null)
                        {
                            var kidemTazminat = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 5);
                            double? kidemTazminatTxt = !string.IsNullOrEmpty(model.KidemTaz) ? Convert.ToDouble(model.KidemTaz.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)null;
                            if (kidemTazminatTxt > 0)
                            {


                                var yolUcret = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 9).FirstOrDefault();
                                var yemekUcret = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 10).FirstOrDefault();
                                var saglikSigortasi = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 22).FirstOrDefault();

                                var bireyselEmeklilikk = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 23).FirstOrDefault();

                                var toplam = brutUcret + yolUcret.UnitPrice + yemekUcret.UnitPrice + saglikSigortasi.UnitPrice + bireyselEmeklilikk.UnitPrice;

                                if (kidemTazminatTxt > toplam)
                                {
                                    kidemTazminat.UnitPrice = ((toplam / 12));
                                    kidemTazminat.CostPrice = ((toplam / 12) * quantity);

                                }
                                else if (kidemTazminatTxt <= toplam)
                                {
                                    var kidemTazTavan = db.ConstantParamaterDetails.FirstOrDefault(t => t.IsActive == true && t.ConstantParameterId == 2);
                                    if (kidemTazTavan != null)
                                    {
                                        kidemTazminat.UnitPrice = (kidemTazTavan.ParamaterValue.Value / 12);
                                        kidemTazminat.CostPrice = (kidemTazTavan.ParamaterValue.Value / 12) * quantity;
                                    }

                                }
                                kidemTazminat.Quantity = quantity;
                                kidemTazminat.Value = Convert.ToDouble(model.KidemTaz); // txtden girilmiş kıdem taz. yazıcaz, seneye bu kıdez taz. değişebilir faka geçmişe yönelik bu personele kıdem taz. ne girilmiş billnmesi için Value'ye txt den gelen değeri yazıyorum.
                                                                                        //db.SaveChanges();
                            }
                            else
                            {
                                kidemTazminat.Quantity = quantity;
                                kidemTazminat.CostPrice = 0;
                                kidemTazminat.UnitPrice = 0;
                                kidemTazminat.Value = 0;
                            }


                        }

                        if (model.IhbarTaz != null)
                        {

                            var ihbarTazminat = db.OfferPositions.FirstOrDefault(x => x.OfferID == offer.ID && x.PositionID == position.ID && x.PositionItemID == 6);
                            if (ihbarTazminat != null)
                            {
                                if (model.IhbarTaz == "ihbarHesaplaCancel")
                                {
                                    ihbarTazminat.Quantity = quantity;
                                    ihbarTazminat.CostPrice = 0;
                                    ihbarTazminat.UnitPrice = 0;
                                    ihbarTazminat.Value = 0;
                                    ihbarTazminat.PositionOptionID = null;
                                    db.SaveChanges();
                                }
                                else
                                {

                                    var yolUcret = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 9).FirstOrDefault();
                                    var yemekUcret = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 10).FirstOrDefault();
                                    var saglikSigortasi = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 22).FirstOrDefault();

                                    var bireyselEmeklilikk = db.OfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == position.ID && t.PositionItemID == 23).FirstOrDefault();

                                    var toplam = brutUcret + yolUcret.UnitPrice + yemekUcret.UnitPrice + saglikSigortasi.UnitPrice + bireyselEmeklilikk.UnitPrice;

                            
                                    TimeSpan res2 = offer.DateEnd.Value - offer.DateBegin.Value;
                                    var totalDay2 = res2.TotalDays;
                                    double resYear = Convert.ToDouble((totalDay2 / 360));

                               

                                    TimeSpan res = offer.DateEnd.Value - offer.DateBegin.Value;
                                    var totalDay = res.TotalDays;
                                    int resMount = Convert.ToInt32((totalDay / 30));

                                    if (ihbarTazminat.PositionOptionID == null) //Hesapla radio buttonu seçilirse, daha sonra kayıtları getirirken seçildiğini anlamak için positionOptions toplasına kayıt atıyorum.
                                    {

                                        PositionOptions po = new PositionOptions();
                                        po.PositionItemID = 6;
                                        po.OptionName = "ihbarHesapla";
                                        db.PositionOptions.Add(po);
                                        db.SaveChanges();
                                        if (resMount >= 12 || resYear >= 1) //Teklif süresi 1 yıldan fazla ise 12 ye bölünecek Değil ise süreye bölünecek
                                        {
                                            ihbarTazminat.Quantity = quantity;
                                            ihbarTazminat.CostPrice = ((((toplam / 30) * 28) / 12) * quantity);
                                            ihbarTazminat.UnitPrice = (((toplam / 30) * 28) / 12);
                                            ihbarTazminat.Value = (((toplam / 30) * 28) / 12);
                                            ihbarTazminat.PositionOptionID = po.ID;
                                            //db.SaveChanges();
                                        }
                                        else if (resMount == 0)// ResMount 0 ise 1 e böl
                                        {
                                            ihbarTazminat.Quantity = quantity;
                                            ihbarTazminat.CostPrice = ((((toplam / 30) * 28) / 1) * quantity);
                                            ihbarTazminat.UnitPrice = (((toplam / 30) * 28) / 1);
                                            ihbarTazminat.Value = (((toplam / 30) * 28) / 1);
                                            ihbarTazminat.PositionOptionID = po.ID;
                                            //db.SaveChanges();
                                        }
                                        else
                                        {
                                            ihbarTazminat.Quantity = quantity;
                                            ihbarTazminat.CostPrice = ((((toplam / 30) * 28) / resMount) * quantity);
                                            //Sözleşme 1 yıldan az ise Ay sayısına bölüyorum. Sözleşme süresini Teklifin Date begin , Date end tarihleri farkına bakarak belirliyorum.
                                            ihbarTazminat.UnitPrice = (((toplam / 30) * 28) / resMount);
                                            ihbarTazminat.Value = (((toplam / 30) * 28) / resMount);
                                            ihbarTazminat.PositionOptionID = po.ID;
                                            //db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (resMount >= 12 || resYear >= 1) //Teklif süresi 1 yıldan fazla ise 12 ye bölünecek Değil ise süreye bölünecek
                                        {
                                            ihbarTazminat.Quantity = quantity;
                                            ihbarTazminat.CostPrice = ((((toplam / 30) * 28) / 12) * quantity);
                                            ihbarTazminat.UnitPrice = (((toplam / 30) * 28) / 12);
                                            ihbarTazminat.Value = (((toplam / 30) * 28) / 12);

                                        }
                                        else if (resMount == 0)
                                        {

                                            ihbarTazminat.Quantity = quantity;
                                            ihbarTazminat.CostPrice = ((((toplam / 30) * 28) / 1) * quantity);
                                            ihbarTazminat.UnitPrice = (((toplam / 30) * 28) / 1);
                                            ihbarTazminat.Value = (((toplam / 30) * 28) / 1);




                                        }
                                        else
                                        {
                                            ihbarTazminat.Quantity = quantity;
                                            ihbarTazminat.CostPrice = ((((toplam / 30) * 28) / resMount) * quantity);
                                            //Sözleşme 1 yıldan az ise Ay sayısına bölüyorum. Sözleşme süresini Teklifin Date begin , Date end tarihleri farkına bakarak belirliyorum.
                                            ihbarTazminat.UnitPrice = (((toplam / 30) * 28) / resMount);
                                            ihbarTazminat.Value = (((toplam / 30) * 28) / resMount);

                                        }

                                    }
                                }

                            }
                        }
                        //SGk işveren vs null ise SGK işverene eklenecek tutarları ayrı bir değişkende topla sonrada offPositionlistte ilgili kaydı güncelle (IDsi 2-3 olan). Null değilse zaten direk ekle.

                        if (offpositionlist.Count() > 0 && offpositionlist != null)
                        {
                            var sgkIsverenUpdate = offpositionlist.FirstOrDefault(t => t.PositionItemID == 2);
                            var sgkIssizlikIsverenUpdate = offpositionlist.FirstOrDefault(t => t.PositionItemID == 3);

                            if (sgkIsverenUpdate != null)
                            {
                                sgkIsverenUpdate.UnitPrice = sgkIsverenPayiUnitPriceForBordro;
                                sgkIsverenUpdate.CostPrice = sgkIsverenPayiCostPriceForBordro;
                            }
                            if (sgkIssizlikIsverenUpdate != null)
                            {
                                sgkIssizlikIsverenUpdate.UnitPrice = sgkIssizlikIsverenPayiUnitPriceForBordro;
                                sgkIssizlikIsverenUpdate.CostPrice = sgkIssizlikIsverenPayiCostPriceForBordro;
                            }


                        }

                        db.OfferPositions.AddRange(offpositionlist);
                        db.SaveChanges();
                        int IsSave = db.SaveChanges();
                    }
                }
            }
        }

        public PartialViewResult RefreshOfferView(int OfferID)
        {
            OfferControlModel model = new OfferControlModel();


            var offer = Db.Offer.FirstOrDefault(t => t.ID == OfferID);
            if (offer != null)
            {
                //model.OpportunityNumber = helper.GetOpportunityNumber();
                model.singleoffer = offer;
                return PartialView("~/Views/Offer/_PartialRefreshOfferView.cshtml", model);
            }
            return PartialView("~/Views/Offer/_PartialRefreshOfferView.cshtml", model);
        }

        #region CreateReOfferInfomation
        //Var olan bir teklif tarihleri değiştirilerek yeniden oluşturulmak yani Teklif yenilemek için kullanılan metottur.
        //Teklifin başlangıç ve bitiş tarihleri verildikten sonra teklif tüm maliyet kalemleriyle birlikte yeni bir teklif olarak oluşturulur.
        //Teklif süreci ilgili birimlerden onay bekliyor sürecinde kalır.
        #endregion
        public JsonResult CreateReOffer(string BeginDate, string EndDate, int OfferID, string OppName)
        {
            OypaHelper helper = new OypaHelper();
            var oppNumber = helper.GetOpportunityNumber();
            var offNumber = helper.GetUpdOfferNumber();
            var beginDate = Convert.ToDateTime(BeginDate);
            DateTime endDate;
            if (!string.IsNullOrEmpty(EndDate)) // Eğer teklif tek seferlik ise EndDate null gelecek!
            {
                endDate = Convert.ToDateTime(EndDate);

            }
            else
            {
                endDate = beginDate; //// Eğer teklif tek seferlik ise EndDate null gelecek ve bitiş tarihine başlangışi tarihini atıcam!

            }


            var checkOffer = Db.Offer.FirstOrDefault(t => t.ID == OfferID);
            if (checkOffer != null)
            {
                //Önce klonlanmak istenek teklifin fırsatını yakala
                var findOpportunity = Db.Opportunity.Where(t => t.ID == checkOffer.OpportunityID);
                //var oppParam = Db.ParamCount.FirstOrDefault(t => t.Id == 3); //Son param count yakalandı.
                //var oppParamCount = oppParam.ParamCount1;

                Opportunity newOpp = new Opportunity()
                {
                    OpportunityNumber = oppNumber,
                    OpportunityName = OppName,
                    DateBegin = beginDate,
                    DateEnd = endDate,
                    StatusLevel = 3,//Direktor Onay
                    MainContent = checkOffer.Opportunity.MainContent,
                    ReasonDescription = checkOffer.Opportunity.ReasonDescription,
                    CustomerID = checkOffer.Opportunity.CustomerID,
                    EmployeeID = checkOffer.Opportunity.EmployeeID,
                    RegionID = checkOffer.Opportunity.RegionID,
                    RecordDate = DateTime.Now,
                    RecordEmployeeID = OypaHelper.RecordEmployee(),
                    RecordIP = OypaHelper.UserIpAdress(),
                    IsActive = true,
                    ProjectID = checkOffer.Opportunity.ProjectID,
                    IsOneTime = checkOffer.Opportunity.IsOneTime
                };
                Db.Opportunity.Add(newOpp);
                Db.SaveChanges();
                //Teklif klonlamada ilk aşama Yeni Opportunity oluşturuldu.

                //2. aşama teklif olustur.
                Offer newOffer = new Offer()
                {
                    OpportunityID = newOpp.ID,
                    OfferNumber = offNumber,
                    VersiyonPrefix = "V",
                    VersionNumber = null,
                    VersionDigit = null,
                    OfferName = newOpp.OpportunityName,
                    DateBegin = newOpp.DateBegin,
                    DateEnd = newOpp.DateEnd,
                    StatusLevel = 1, //Teklif süreci: İlk Kayıt
                    IsRevision = false,
                    CustomerID = newOpp.CustomerID,
                    EmployeeID = newOpp.EmployeeID,
                    RegionID = newOpp.RegionID,
                    EnvironmentID = newOpp.EnvironmentID,
                    ProjectID = newOpp.ProjectID,
                    AraToplam = checkOffer.AraToplam,//Bu kayıtlar teklifin içerisinde olduğu için tekliften çekiyorum dataları
                    AraToplamProfitRate = checkOffer.AraToplamProfitRate,
                    KdvToplam = checkOffer.KdvToplam,
                    BrutToplam = checkOffer.BrutToplam,
                    BrutToplamProfitRate = checkOffer.BrutToplamProfitRate,
                    GenelToplam = checkOffer.GenelToplam,
                    Description = checkOffer.Description,
                    RecordDate = DateTime.Now,
                    RecordEmployeeID = OypaHelper.RecordEmployee(),
                    RecordIP = OypaHelper.UserIpAdress(),
                    IsOneTime = checkOffer.IsOneTime,
                    IsActive = checkOffer.IsActive,
                    ManagementExpenseRateHR = checkOffer.ManagementExpenseRateHR,
                    ManagementExpenseRateProduct = checkOffer.ManagementExpenseRateProduct,
                    ManagementExpenseRateMD = checkOffer.ManagementExpenseRateMD,
                    ManagementExpenseRateOther = checkOffer.ManagementExpenseRateOther,
                    TotalOtherExpense = checkOffer.TotalOtherExpense,
                    TotalMachineExpense = checkOffer.TotalMachineExpense,
                    ProfitRateForOthers = checkOffer.ProfitRateForOthers,
                    ProfitRateForMachine = checkOffer.ProfitRateForMachine,
                    TotalMonthlyDepreciation = checkOffer.TotalMonthlyDepreciation,
                    TotalHrProfit = checkOffer.TotalHrProfit,
                    SumAraToplamProfit = checkOffer.SumAraToplamProfit,
                    SumMachineExpenseProfit = checkOffer.SumMachineExpenseProfit,
                    SumOtherExpenseProfit = checkOffer.SumOtherExpenseProfit,
                    MonthlyUnitPrice = checkOffer.MonthlyUnitPrice,
                    PurchasingManagerCheck = 0,
                    HumanResourcesCheck = 0,
                    PurchasingSpecialistCheck = 0,
                    RecordPurchasingManagerID = OypaHelper.RecordEmployee(),
                    RecordHumanResourcesID = OypaHelper.RecordEmployee(),
                    PurchasingSpecialistID = OypaHelper.RecordEmployee(),
                };
                Db.Offer.Add(newOffer);
                Db.SaveChanges(); //2. aşama teklif ekleme tamamlandı.

                //3.Aşama OfferProduct
                var offerProducts = Db.OfferProducts.Where(t => t.OfferID == checkOffer.ID).ToList(); //Eski teklifin productları

                foreach (var item in offerProducts) //Şimdi eski teklifte olan productları yeni teklife ekleyeceğim.
                {
                    OfferProducts product = new OfferProducts()
                    {
                        OfferID = newOffer.ID,
                        ProductID = item.ProductID,
                        Quantity = item.Quantity,
                        Unit = item.Unit,
                        CostPrice = item.CostPrice,
                        UnitPrice = item.UnitPrice,
                        Currency = item.Currency
                    };
                    Db.OfferProducts.Add(product);
                    Db.SaveChanges();   //3. Aşama Product ekleme tamamlandı

                }

                //4. Aşama OfferPositons
                if (checkOffer.IsOneTime == true) // Süreli Teklif
                {
                    var offerPositions = Db.OfferPositions.Where(t => t.OfferID == checkOffer.ID).ToList(); //Eski teklifin positionsları

                    foreach (var item in offerPositions) //Şimdi eski teklifte olan positionsları yeni teklife ekleyeceğim.
                    {
                        OfferPositions positions = new OfferPositions();

                        positions.OfferID = newOffer.ID;
                        positions.PositionItemID = item.PositionItemID;
                        if (item.PositionOptionID != null) //demekki ücret detaylarında bir opsiyon belirtilmiş. (Örn: Yemek bordrodan olsun )
                        {
                            PositionOptions po = new PositionOptions()
                            {
                                PositionItemID = item.PositionItemID,
                                OptionName = item.PositionOptions.OptionName,
                                OptionParameter = null,
                                OptionUnit = null,
                            };
                            Db.PositionOptions.Add(po);
                            Db.SaveChanges();
                            positions.PositionOptionID = po.ID;
                            //offer positons'a ait bir position options eklendi.

                        }
                        else
                        {
                            positions.PositionOptionID = null;
                        }
                        positions.PositionID = item.PositionID;
                        positions.Quantity = item.Quantity;
                        positions.CostPrice = item.CostPrice;
                        positions.UnitPrice = item.UnitPrice;
                        positions.Currency = item.Currency;
                        positions.Value = item.Value;

                        Db.OfferPositions.Add(positions);
                        Db.SaveChanges();  //4. Aşama Positons ekleme tamamlandı

                    }

                    //Karlılık kalemlerini eklendi.

                    var offerHRProfitItem = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == checkOffer.ID).ToList();
                    if (offerHRProfitItem != null && offerHRProfitItem.Count() > 0)
                    {
                        foreach (var item in offerHRProfitItem)
                        {
                            OfferHRProfitItems profitItem = new OfferHRProfitItems();
                            profitItem.OfferID = newOffer.ID;
                            profitItem.PositionItemID = item.PositionItemID;
                            profitItem.RecordDate = DateTime.Now;
                            profitItem.RecordUserID = OypaHelper.RecordEmployee();
                            profitItem.IsActive = true;

                            Db.OfferHRProfitItems.Add(profitItem);
                            Db.SaveChanges();
                        }
                    }

                }
                else // Tek seferlik ise
                {
                    var offerPositions = Db.IsOneTimeOfferPositions.Where(t => t.OfferID == checkOffer.ID).ToList(); //Eski teklifin positionsları

                    foreach (var item in offerPositions) //Şimdi eski teklifte olan positionsları yeni teklife ekleyeceğim.
                    {
                        IsOneTimeOfferPositions positions = new IsOneTimeOfferPositions();

                        positions.OfferID = newOffer.ID;
                        positions.PositionID = item.PositionID;
                        positions.Quantity = item.Quantity;
                        positions.Price = item.Price;
                        positions.Currency = item.Currency;
                        positions.WorkingHour = item.WorkingHour;
                        positions.OvertimePay = item.OvertimePay;
                        positions.PositionItemID = item.PositionItemID;
                        Db.IsOneTimeOfferPositions.Add(positions);
                        Db.SaveChanges();
                    }
                }



                //5. Aşama OfferOtherExpense
                if (checkOffer.IsOneTime == true) // Süreli Teklif
                {

                    var offerOtherExpenses = Db.OfferOtherExpenses.Where(t => t.OfferID == checkOffer.ID && t.IsActive == true).ToList(); //Eski teklifin diğer kalemleri
                    foreach (var item in offerOtherExpenses)
                    {
                        OfferOtherExpenses offerOther = new OfferOtherExpenses()
                        {
                            OfferID = newOffer.ID,
                            IsActive = item.IsActive,
                            Price = item.Price,
                            Description = item.Description,
                            RecordUserID = OypaHelper.RecordEmployee(),
                            RecordDate = DateTime.Now,
                            UpdateUserID = null,
                            UpdateDate = null
                        };
                        Db.OfferOtherExpenses.Add(offerOther);
                        Db.SaveChanges();
                        //Other Expenses eklendi.
                    }
                }
                else
                {
                    var offerOtherExpenses = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == checkOffer.ID).ToList();
                    foreach (var item in offerOtherExpenses)
                    {
                        IsOneTimeOtherExpense otherExpense = new IsOneTimeOtherExpense()
                        {
                            OfferID = newOffer.ID,
                            OtherExpenseItemID = item.OtherExpenseItemID,
                            Price = item.Price
                        };
                        Db.IsOneTimeOtherExpense.Add(otherExpense);
                        Db.SaveChanges();

                    }
                }
                //6. Aşama OfferEquipment
                var offerEquipments = Db.OfferEquipment.Where(t => t.OfferID == checkOffer.ID).ToList(); //Eski teklifin makine demirbaşları
                foreach (var item in offerEquipments)
                {
                    OfferEquipment offerEquipment = new OfferEquipment()
                    {
                        OfferID = newOffer.ID,
                        ProductID = item.ProductID,
                        Quantity = item.Quantity,
                        Unit = item.Unit,
                        CostPrice = item.CostPrice,
                        UnitPrice = item.UnitPrice,
                        Currency = item.Currency,
                        Depreciation = item.Depreciation,
                        MonthlyDepreciation = item.MonthlyDepreciation,
                        MonthCount = item.MonthCount,


                    };
                    Db.OfferEquipment.Add(offerEquipment);
                    Db.SaveChanges();
                    //Offer Equipment eklendi.
                }

                //7. Aşama Opportunity Document (Offer)

                var offerDocuments = Db.Documents.Where(t => t.OpportunityID == checkOffer.OpportunityID).ToList(); //Eski teklifin makine demirbaşları
                foreach (var item in offerDocuments)
                {
                    Documents doc = new Documents()
                    {
                        CategoryID = item.CategoryID,
                        TypeID = item.TypeID,
                        OpportunityID = newOpp.ID,
                        OfferID = item.OfferID,
                        DocumentName = item.DocumentName,
                        FileName = item.FileName,
                        FilePath = item.FilePath,
                        Date = item.Date,
                        RecordDate = DateTime.Now,
                        RecordEmployee = OypaHelper.RecordEmployee(),
                        RecordIP = OypaHelper.UserIpAdress(),
                        IsActive = item.IsActive,

                    };
                    Db.Documents.Add(doc);
                    Db.SaveChanges();
                    //Offer Documents eklendi.
                }

                //8. Aşama OfferNotes (Offer)
                var offerNotes = Db.Notes.Where(t => t.OfferId == checkOffer.ID).ToList();
                foreach (var item in offerNotes)
                {
                    Notes notes = new Notes()
                    {
                        Note = item.Note,
                        UserId = OypaHelper.RecordEmployee(),
                        RecardDate = DateTime.Now,
                        UpdateDate = null,
                        IsActive = item.IsActive,
                        OfferId = newOffer.ID
                    };
                    Db.Notes.Add(notes);
                    Db.SaveChanges();

                }


                OypaHelper.AddApplicationLog("Management", "SendOffer", "Offer", checkOffer.ID, "Offer", true, $" {OypaHelper.RecordEmployee()} isimli kullanıcı tarafından {checkOffer.OfferNumber} nolu teklif yenilendi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return Json(new { oppID = newOpp.ID }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { oppID = -1 }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetIsOneTimeOfferPositionItemsValue(int? rowID)
        {
            OfferControlModel model = new OfferControlModel();
            if (rowID > 0)
            {
                using (OypaDbEntities db = new OypaDbEntities())
                {
                    var offerposition = db.IsOneTimeOfferPositions.FirstOrDefault(x => x.ID == rowID);
                    var offer = db.Offer.FirstOrDefault(x => x.ID == offerposition.OfferID);
                    model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offer.ID);
                    //model.IsOneTimeOfferPosList = db.IsOneTimeOfferPositions.Where(t => t.OfferID == offerposition.OfferID && t.PositionID == offerposition.PositionID).ToList();
                    model.IsOneTimeOfferPosList = Db.IsOneTimeOfferPositions.Where(t => t.OfferID == offer.ID && t.PositionID == offerposition.PositionID).ToList();
                    model.IsOneTimePositionItems = Db.IsOneTimePositionItems.Where(t => t.IsActive == true && t.ParentID == 0).ToList();

                }
            }
            return PartialView("~/Views/Offer/_Partial_IsOneTimeOpsiyonelHesaplama.cshtml", model);
        }
        //Teklif arşivini görüntüler
        public ActionResult OfferArchive()
        {
            var offerList = Db.Offer.Where(t => t.VersionNumber != null && t.VersionDigit != null && t.IsActive == true).OrderByDescending(t => t.ID).ToList();
            return View(offerList);
        }
        //Teklif arşiv detayını görüntüler
        public ActionResult OfferArchiveDetail(int? offerID)
        {
            var selectedOffer = Db.OfferHistory.Where(t => t.OfferID == offerID).ToList();
            if (selectedOffer != null && selectedOffer.Count() > 0)
            {
                OypaHelper.AddApplicationLog("Management", "OfferArchiveDetail", "Offer", selectedOffer.FirstOrDefault().ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selectedOffer.FirstOrDefault().OfferNumber} nolu teklifin geçmişini görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return View(selectedOffer);

            }
            return View();
        }

        #region HrCalculateProfitModalPreViewInformation
        //İnsan kaynakları tabı içerisinde yer alan maliyete dahil edilmiş karlılık kalemlerini gösterir.
        #endregion
        public PartialViewResult HrCalculateProfitModalPreView(int offerId)
        {

            HRCalculateProfitModel model = new HRCalculateProfitModel();
            model.SingleOffer = Db.Offer.FirstOrDefault(t => t.IsActive == true && t.ID == offerId);
            model.PositionItemList = Db.PositionItems.Where(t => t.IsActive == true && t.IsHrProfitItem == true).ToList();
            model.SelectedHrProfitItemList = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == offerId).ToList();

            return PartialView("~/Views/Offer/_PartialHRProfitCalculate.cshtml", model);
        }

        public PartialViewResult HrCalculateProfitModalHistory(int offerId, int versionDigit)
        {

            HRCalculateProfitModel model = new HRCalculateProfitModel();
            model.SingleOfferHistory = Db.OfferHistory.FirstOrDefault(t => t.OfferID == offerId && t.VersionDigit == versionDigit);
            model.PositionItemList = Db.PositionItems.Where(t => t.IsActive == true && t.IsHrProfitItem == true).ToList();
            model.SelectedHrProfitItemListHistory = Db.OfferHRProfitItemHistory.Where(t => t.OfferID == offerId && versionDigit == versionDigit).ToList();

            return PartialView("~/Views/Offer/_PartialHRProfitCalculateHistory.cshtml", model);
        }


        public ActionResult HRCalculateProfit(List<int?> items, string managementExpenseTxt, string profitRateTxt, int offerId)
        {
            HRCalculateProfitModel model = new HRCalculateProfitModel();

            var checkOffer = Db.Offer.FirstOrDefault(t => t.IsActive == true & t.ID == offerId);
            if (offerId > 0 && (items == null && managementExpenseTxt == null && profitRateTxt == null))
            {
                if (checkOffer.TotalHrProfit != null)
                {

                    OypaHelper.AddApplicationLog("Management", "AddPositionsOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkOffer.OfferNumber} nolu teklifin karlılık oranını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                    return Json(new { result = "Success", message = checkOffer.TotalHrProfit.Value.ToString("N2") }, JsonRequestBehavior.AllowGet);
                }
            }
            double? managementExpense = !string.IsNullOrEmpty(managementExpenseTxt) ? Convert.ToDouble(managementExpenseTxt.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)0;
            double? profitRate = !string.IsNullOrEmpty(profitRateTxt) ? Convert.ToDouble(profitRateTxt.Replace(".", "").Replace(",", "."), CultureInfo.InvariantCulture) : (double?)0;



            var result = HrCalculateTotalProfit(items, managementExpense, profitRate, offerId);
            if (result == true)
            {
                model.SingleOffer = Db.Offer.FirstOrDefault(t => t.IsActive == true && t.ID == offerId);
                model.PositionItemList = Db.PositionItems.Where(t => t.IsActive == true && t.IsHrProfitItem == true).ToList();
                model.SelectedHrProfitItemList = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == offerId).ToList();

                OypaHelper.AddApplicationLog("Management", "AddPositionsOffer", "Offer", checkOffer.ID, "Offer", true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkOffer.OfferNumber} nolu teklifin karlılık oranı için {profitRate} işletme gideri için {managementExpense} değerlerini girerek toplam kar tutarını hesapladı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return Json(new { result = "Success", message = model.SingleOffer.TotalHrProfit.Value.ToString("N2") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                OypaHelper.AddApplicationLog("Management", "AddPositionsOffer", "Offer", checkOffer.ID, "Offer", false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {checkOffer.OfferNumber} nolu teklifin karlılık oranı için {profitRateTxt} işletme gideri için {managementExpenseTxt} değerlerini girerek toplam kar tutarını hesaplarken hatayla karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                return Json(new { result = "Fail", message = "İşlem gerçekleşmedi,tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
            }



        }


        #region HrCalculateTotalProfitInformation
        //items=> Karlılık oranı hesaplanacak itemId
        //managementExpense=> işletme gideri
        //profitRate => karlılık oranı

        //Girilen işletme gideri ve karlılık oranı değerlerine toplayıp seçilen kalemlerin toplam tutarlarına yüzdelik olarak ekleme işlemi yapar.

        //çıkan sonucu ik maliyet analiz tutarına eklenir.
        #endregion
        public bool HrCalculateTotalProfit(List<int?> items, double? managementExpense, double? profitRate, int offerId)
        {
            List<OfferHRProfitItems> HrProfitItemList = new List<OfferHRProfitItems>();
            var checkOffer = Db.Offer.FirstOrDefault(t => t.IsActive == true & t.ID == offerId);
            if (checkOffer != null)
            {
                checkOffer.ManagementExpenseRateHR = managementExpense;
                checkOffer.BrutToplamProfitRate = profitRate;
                double? TotalHrProfit = 0;
                var offerHrProfitItemList = Db.OfferHRProfitItems.Where(t => t.IsActive == true && t.OfferID == checkOffer.ID).ToList();
                Db.SaveChanges();
                if (items != null && items.Count() > 0)
                {
                    foreach (var item in items)
                    {
                        if (item != null)
                        {

                            var checkItem = Db.OfferHRProfitItems.FirstOrDefault(t => t.IsActive == true && t.PositionItemID == item && t.OfferID == checkOffer.ID); //Bu kalem Karlılık oranı hesaplamak içn eklendi mi


                            if (checkItem == null)
                            {
                                OfferHRProfitItems HrProfitItem = new OfferHRProfitItems
                                {
                                    IsActive = true,
                                    OfferID = checkOffer.ID,
                                    PositionItemID = item,
                                    RecordDate = DateTime.Now,
                                    RecordUserID = OypaHelper.RecordEmployee()
                                };
                                HrProfitItemList.Add(HrProfitItem);
                            }


                            //var data = Db.OfferPositions.FirstOrDefault(t => t.OfferID == offerId && t.PositionItemID == item);
                            var data = Db.OfferPositions.Where(t => t.PositionItemID == item && t.OfferID == checkOffer.ID).Sum(t => t.CostPrice);
                            if (data != null)
                            {
                                TotalHrProfit += data.Value;
                            }
                        }
                    }
                    checkOffer.TotalHrProfit = (TotalHrProfit * (managementExpense + profitRate) / 100);
                    Db.OfferHRProfitItems.AddRange(HrProfitItemList);
                    int save = Db.SaveChanges();


                    if (offerHrProfitItemList.Count() > 0 && offerHrProfitItemList != null)
                    {
                        foreach (var item in offerHrProfitItemList)
                        {
                            if (items.Contains(item.PositionItemID.Value) == false)
                            {
                                item.IsActive = false;
                                item.UpdateDate = DateTime.Now;
                                item.UpdateUserID = OypaHelper.RecordEmployee();
                                Db.SaveChanges();
                            }
                        }
                    }

                    return true;

                }
                else
                {
                    checkOffer.TotalHrProfit = 0;
                    if (offerHrProfitItemList != null && offerHrProfitItemList.Count() > 0)
                    {
                        foreach (var item in offerHrProfitItemList)
                        {
                            item.IsActive = false;
                            Db.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        public ActionResult OfferArchiveDetailSub(int offerID, int versionDigit)
        {
            OfferArchiveControlModel model = new OfferArchiveControlModel();
            var selectedOffer = Db.OfferHistory.FirstOrDefault(t => t.OfferID == offerID && t.VersionDigit == versionDigit);
            if (selectedOffer != null)
            {
                model.PrdHistoryList = Db.OfferProductsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == selectedOffer.VersionDigit).ToList();
                model.SingleOfferHistory = selectedOffer;
                if (selectedOffer.IsOneTime.Value)
                {
                    model.IsOneTime = true;
                    //Süreli teklif
                    model.PositionHistoryList = Db.OfferPositionsHistory.Where(t => t.OfferID == offerID && t.VersionDigit == selectedOffer.VersionDigit).OrderBy(t => t.PositionItemID).ToList();
                    model.OtherExpenseHistoryList = Db.OfferOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == selectedOffer.VersionDigit).ToList();
                    model.EquipmentHistoryList = Db.OfferEquipmentHistory.Where(t => t.OfferID == offerID && t.VersionDigit == selectedOffer.VersionDigit).ToList();

                }
                else
                {
                    model.IsOneTime = false;
                    //Tek seferlik teklif
                    model.IsOneTimePositionHistoryList = Db.IsOneTimeOfferPositonHistory.Where(t => t.OfferID == offerID && t.VersionDigit == selectedOffer.VersionDigit).ToList();
                    model.IsOneTimeOtherExpenseHistoryList = Db.IsOneTimeOtherExpenseHistory.Where(t => t.OfferID == offerID && t.VersionDigit == selectedOffer.VersionDigit).ToList();

                }
            }
            return View(model);
        }

        public ActionResult RequestNewProduct(string newProductName, string newBrand, string newModel, string description)
        {
            if (!string.IsNullOrWhiteSpace(newProductName) && !string.IsNullOrWhiteSpace(newBrand) && !string.IsNullOrWhiteSpace(description))
            {
                int empID = OypaHelper.RecordEmployee();
                var checkUser = Db.Employee.FirstOrDefault(t => t.ID == empID && t.IsActive == true);
                var mailres = OypaHelper.FillinEmailContentForNewProduct($"{checkUser.FullName} isimli kullanıcı logo üzerinde yeni bir ürün kaydı açılması için talepte bulundu. Talep edilen ürün bilgileri aşağıdaki gibidir.",
                            $"Ürün Adı: {newProductName}",
                            $"Marka: {newBrand}", $"Model: {newModel}", $"Açıklama: {description}",
                            $"Sn. Satın Alma ilgilisi", "satinalma@oypa.com.tr",//satinalma@oypa.com.tr
                            "Logo - Yeni Ürün Kayıt Talebi",
                            "http://194.100.100.21/", null, "RequestNewProduct.html");
                if (mailres)
                {

                    return Json(new { result = "Success", message = "Talebiniz satın alma birimine mail olarak iletildi" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { result = "FailMail", message = "Mail gönderilemedi, lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { result = "Error", message = "Ürün adı, marka ve açıklama  boş geçilemez!" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowProductDescription(int rowid)
        {
            ProductDescriptionModel model = new ProductDescriptionModel();

            if (rowid > 0)
            {
                var getProduct = Db.OfferProducts.Where(t => t.ID == rowid).FirstOrDefault();
                if (getProduct != null)
                {
                    model.Product = getProduct;
                    model.Offer = Db.Offer.Where(t => t.IsActive == true && t.ID == getProduct.OfferID).FirstOrDefault();
                    return PartialView("~/Views/Offer/_PartialProductDescription.cshtml", model);

                }


            }
            ViewBag.ErrorMessage = "Sistemde kayıtlı olmayan bir ürün.";
            return PartialView("~/Views/Offer/_PartialProductDescription.cshtml");
        }

        public ActionResult AddProductDescription(int rowid, string description)
        {


            if (rowid > 0)
            {
                if (!string.IsNullOrEmpty(description) && !string.IsNullOrWhiteSpace(description))
                {
                    var getProduct = Db.OfferProducts.Where(t => t.ID == rowid).FirstOrDefault();
                    if (getProduct != null)
                    {
                        getProduct.Description = description;
                        Db.SaveChanges();
                        return Json(new { result = "Success", message = "Açıklama başarıyla kaydedildi." }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json(new { result = "Error", message = "Sistemde kayıtlı olmayan bir ürün." }, JsonRequestBehavior.AllowGet);

                    }

                }
                else
                {
                    return Json(new { result = "Error", message = "Açıklama alanı boş geçilemez!" }, JsonRequestBehavior.AllowGet);

                }


            }
            else
            {
                return Json(new { result = "Error", message = "Sistemde kayıtlı olmayan bir ürün." }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult UpdateProductDescription(int rowid, string description)
        {

            if (rowid > 0)
            {
                if (!string.IsNullOrEmpty(description) && !string.IsNullOrWhiteSpace(description))
                {
                    var getProduct = Db.OfferProducts.Where(t => t.ID == rowid).FirstOrDefault();
                    if (getProduct != null)
                    {
                        getProduct.Description = description;
                        Db.SaveChanges();
                        return Json(new { result = "Success", message = "Açıklama başarıyla güncellendi." }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json(new { result = "Error", message = "Sistemde kayıtlı olmayan bir ürün." }, JsonRequestBehavior.AllowGet);

                    }

                }
                else
                {
                    return Json(new { result = "Error", message = "Açıklama alanı boş geçilemez!" }, JsonRequestBehavior.AllowGet);

                }


            }
            else
            {
                return Json(new { result = "Error", message = "Sistemde kayıtlı olmayan bir ürün." }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult DeleteProductDescription(int rowid)
        {

            if (rowid > 0)
            {

                var getProduct = Db.OfferProducts.Where(t => t.ID == rowid).FirstOrDefault();
                if (getProduct != null)
                {
                    getProduct.Description = null;
                    Db.SaveChanges();
                    return Json(new { result = "Success", message = "Açıklama başarıyla silindi." }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { result = "Error", message = "Sistemde kayıtlı olmayan bir ürün." }, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                return Json(new { result = "Error", message = "Sistemde kayıtlı olmayan bir ürün." }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult RefreshOfferProducts(int rowid)
        {
            OfferControlModel model = new OfferControlModel();



            var offerproduct = Db.OfferProducts.Where(t => t.ID == rowid).FirstOrDefault();



            model.offerproducts = Db.OfferProducts.Where(x => x.OfferID == offerproduct.OfferID).ToList();


            model.singleoffer = Db.Offer.FirstOrDefault(x => x.ID == offerproduct.OfferID && x.IsActive == true);


            return PartialView("_PartialOfferRows", model);
        }
        public ActionResult RemoveOffer(int offerId)
        {



            var offer = Db.Offer.Where(t => t.IsActive == true && t.ID == offerId && t.IsActive == true).FirstOrDefault();
            if (offer != null)
            {
                offer.IsActive = false;
                var offerDocuments = Db.Documents.Where(t => t.OpportunityID == offer.OpportunityID).ToList();
                if (offerDocuments != null && offerDocuments.Count() > 0)
                {
                    foreach (var item in offerDocuments)
                    {
                        item.IsActive = false;
                    }
                }

                Db.SaveChanges();
                return Json(new { result = "Success", message = "Teklif Başarıyla Silindi." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { result = "Error", message = "Teklif Bulunamadı." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(int? offerId, int? typeId)
        {
            //Type 1 süreli teklif malzeme stok
            //Type 2 süreli teklif diğer maliyetler
            //Type 3 süreli teklif Makinedemirbaş
            //Type 4 süreli teklif İK
            //Type 5 tek seferlik teklif İK
            //Type 6 tek seferlik teklif diğer maliyetler

            var checkOffer = Db.Offer.FirstOrDefault(t => t.ID == offerId && t.IsActive == true);
            var offerNumber = string.Empty;
            if (checkOffer != null)
            {
                offerNumber = checkOffer.OfferNumber;
            }
            var gv = new GridView();
            if (typeId == 1) //Malzeme stok
            {
                var AllOperatorList = Db.OfferProducts.Where(x => x.OfferID == offerId).ToList();

                var list = (from e in AllOperatorList
                            select new
                            {
                                ÜrünAdı = e.Products.ProductName,
                                Miktar = e.Quantity.ToString(),
                                Birim = e.Unit,
                                MaliyetBirimFiyat = e.UnitPrice.ToString(),
                                TeklifBirimFiyat = e.CostPrice.ToString(),
                                TeklifToplamFiyat = (e.CostPrice * e.Quantity).ToString()
                            }).ToList();
                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", $"attachment; filename={offerNumber}_MalzemeStokMaliyeti.xls");
            }
            else if (typeId == 2) //Süreli diğer
            {
                var offerOtherExpense = Db.OfferOtherExpenses.Where(t => t.OfferID == offerId && t.IsActive == true).ToList();
                var list = (from e in offerOtherExpense
                            select new
                            {
                                Açıklama = e.Description,
                                Tutar = e.Price.ToString(),

                            }).ToList();
                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", $"attachment; filename={offerNumber}_DigerMaliyetler.xls");
            }
            else if (typeId == 3)
            {
                var offerEquipment = Db.OfferEquipment.Where(t => t.OfferID == offerId).ToList();
                var list = (from e in offerEquipment
                            select new
                            {
                                ÜrünAdı = e.Products.ProductName,
                                Birim = e.Unit,
                                Miktar = e.Quantity,
                                MaliyetBirimFiyat = e.UnitPrice.ToString(),
                                TeklifBirimFiyat = e.CostPrice.ToString(),
                                AmortismanToplamFiyatı = (e.CostPrice * e.Quantity).ToString(),
                                AmortismanSüresi_Ay = e.MonthCount,
                                AylıkAmortismanFiyatı = ((e.CostPrice * e.Quantity) / e.MonthCount).ToString(),


                            }).ToList();

                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();

                Response.Buffer = true;
                Response.AddHeader("content-disposition", $"attachment; filename={offerNumber}MakineDemirbasMaliyeti.xls");
            }
            else if (typeId == 4) //Süreli
            {
                var offerPosition = Db.OfferPositions.Where(t => t.OfferID == offerId).OrderBy(t => t.PositionID).ThenBy(t => t.PositionItemID).ToList();
                if (offerPosition != null)
                {
                    var list = (from e in offerPosition
                                select new
                                {
                                    Pozisyon = e.Positions.PositionName,
                                    Durum = e.Positions.PositionStatus.StatusName,
                                    Miktar = offerPosition.Where(t => t.PositionItemID == 1 && t.PositionID == e.PositionID).FirstOrDefault().Quantity,
                                    MaliyetKalemi = e.PositionItems.ItemName.ToString(),
                                    Deger = e.Value != null ? e.Value.ToString() : "-",
                                    BirimFiyat = e.UnitPrice.ToString(),
                                    ToplamFiyat = e.CostPrice.ToString(),
                                    Detay = e.PositionOptionID != null ? e.PositionOptions.OptionName : "-",


                                }).ToList();
                    gv.DataSource = list;
                    gv.DataBind();
                    Response.ClearContent();

                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", $"attachment; filename={offerNumber}_IKMaliyeti.xls");
                }



            }
            else if (typeId == 5) //Tek seferlik
            {
                var offerPosition = Db.IsOneTimeOfferPositions.Where(t => t.OfferID == offerId).OrderBy(t => t.PositionID).ThenBy(t => t.PositionItemID).ToList();
                //var quantity = Db.OfferPositions.Where(t => t.OfferID == offerId && t.PositionItemID == 1).FirstOrDefault().Quantity;

                var list = (from e in offerPosition
                            select new
                            {
                                Pozisyon = e.Positions.PositionName,
                                Durum = e.Positions.PositionStatus.StatusName,
                                Miktar = e.Quantity,
                                MesaiSaati = e.WorkingHour,
                                MesaiÜcreti = e.OvertimePay,
                                MaliyetKalemi = e.IsOneTimePositionItems.ItemName,
                                Tutar = e.Price



                            }).ToList();

                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", $"attachment; filename={offerNumber}_IKMaliyeti.xls");
            }
            else if (typeId == 6) //Tekseferlik Diğer
            {
                var IsOneTimeOther = Db.IsOneTimeOtherExpense.Where(t => t.OfferID == offerId).ToList();
                //var quantity = Db.OfferPositions.Where(t => t.OfferID == offerId && t.PositionItemID == 1).FirstOrDefault().Quantity;

                var list = (from e in IsOneTimeOther
                            select new
                            {
                                Açıklama = e.IsOneTimeOfferOtherExpenseItems.ItemName,
                                Fiyat = e.Price,

                            }).ToList();

                gv.DataSource = list;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", $"attachment; filename={offerNumber}_DigerMaliyetler.xls");
            }

            //gv.DataBind();
            //Response.ClearContent();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", $"attachment; filename={offerName}_MalzemeStokListesi.xls");
            Response.ContentType = "application/ms-excel";
            //Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());


            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            return View();
        }



        #region HeaderandFooterInformation
        //aşağıdaki class teklif üst yazısını pdf'ye çevirirken oyak ile ilgili görselleri pdf'ye alt ve üst bilgi ekleme işlemini gerçekleştirmektedir.
        // alt bilgi ve üst bilgi alanları jpeg formatındadır.
        //local'de sunucu üzerindenki görsele erişim sağlanamadığı için localde çalıştırırken path olarak local ortama bakan yoluaktif edip, canlı ortama bakan yolu yorum satırını alınmalıdır.
        #endregion
        class HeaderandFooter : PdfPageEventHelper
        {
            public override void OnEndPage(PdfWriter writer, Document document)
            {
                PdfContentByte cb = writer.DirectContent;
                //Header img location
                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("http://194.100.100.21/images/header.PNG");  //Üst bilgi görselinin canlı ortamdaki yolu
                // iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance((HostingEnvironment.MapPath("~/images/header.PNG")));  //Üst bilgi görselinin local ortamdaki yolu


                img.SetAbsolutePosition(35, 760); //header
                iTextSharp.text.Image img2 = iTextSharp.text.Image.GetInstance("http://194.100.100.21/images/footer8.PNG");  //Alt bilgi görselinin canlı ortamdaki yolu
                //iTextSharp.text.Image img2 = iTextSharp.text.Image.GetInstance((HostingEnvironment.MapPath("~/images/footer8.PNG")));  //Alt bilgi görselinin local ortamdaki yolu

                img2.SetAbsolutePosition(35, 50);//Footer 
                img2.ScaleAbsolute(500, 85);
                img.ScaleAbsolute(165, 50);
                cb.AddImage(img);
                cb.AddImage(img2);



            }
        }
    }
}