﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;

namespace OykaCrm.Controllers
{

    public class CustomerController : BaseController
    {
        // private string jsonFile = @"C:\Users\emine\source\repos\oypa-crm\OykaCrm\OykaCrm\json1.json";

        // GET: Customer - CRM
        public ActionResult Index()
        {
            CustomerControlModel model = new CustomerControlModel();
            model.Customers = Db.Customers.Where(i => i.IsActive == true).OrderByDescending(t => t.UpdateDate).ToList();
            model.CityList = Db.Cities.ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Customer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Firma Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        //CityId'ye göre DropDownListFor'u County'lerle doldurma

        public PartialViewResult AddCity(int cityId)
        {
            var searchCounty = Db.County.Where(x => x.sehir == cityId).ToList();
            return PartialView("~/Views/Customer/_PartialCounty.cshtml", searchCounty);
        }

        public ActionResult Create()
        {
            CustomerControlModel model = new CustomerControlModel();
            model.Currencies = Db.Currency.ToList();
            model.CityList = Db.Cities.ToList();
            model.Counties = Db.County.ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CustomerControlModel model)
        {
            Customers customer = new Customers();

            if (model != null)
            {
                customer.IsActive = true;
                customer.OpeningBalance = model.OpeningBalance;
                customer.OpeningDate = model.OpeningDate;
                customer.Phone = model.Phone;
                customer.TaxNumber = model.TaxNumber;
                customer.TaxOffice = model.TaxOffice;
                customer.CustomerFullName = model.CustomerFullName;
                customer.IBAN = model.IBAN;
                customer.FaxNumber = model.FaxNumber;
                customer.EMail = model.EMail;
                customer.Address = model.Address;
                customer.CurrencyType = model.CurrencyType;
                customer.CountyID = model.CountyId;
                customer.CityID = model.CityID;
                customer.RegionID = 1;
                customer.CategoryID = 1;

                Db.Customers.Add(customer);
                Db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Detail(int? customerID)
        {
            CustomerControlModel model = new CustomerControlModel();
            //AddCounties();
            var selectedCustomer = Db.Customers.FirstOrDefault(i => i.ID == customerID);

            if (selectedCustomer != null)
            {
                model.SingleCustomer = selectedCustomer;

                model.CityList = Db.Cities.ToList();
                if (selectedCustomer.CityID != 0 && selectedCustomer.CityID != null)
                {
                    //model.CityName = Db.Cities.FirstOrDefault(i => i.id == selectedCustomer.CityID).sehir;
                    model.CityID = selectedCustomer.CityID;
                    model.Counties = Db.County.Where(t => t.sehir == model.SingleCustomer.CityID).ToList();
                }

                if (selectedCustomer.CountyID != 0 && selectedCustomer.CountyID != null)
                {
                    //model.CountyName = Db.County.FirstOrDefault(i => i.id == selectedCustomer.CountyID).ilce;
                    model.CountyId = Db.County.FirstOrDefault(i => i.id == selectedCustomer.CountyID).id;
                }
                else
                {
                    model.CityName = "-";
                    model.CountyName = "-";
                }

                if (selectedCustomer.OpeningDate != null)
                {
                    model.OpeningDate = selectedCustomer.OpeningDate.Value;

                    model.FormatDate = model.OpeningDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                }
            }

            model.i = 1;
            model.CustomerContacts = Db.CustomerContact.Where(i => i.IsActive == true && i.CustomerID == customerID).ToList();

            OypaHelper.AddApplicationLog("Management", "Detail", "Customer", customerID != null ? customerID.Value : 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {customerID} Id'li firma detay sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }
        //Müşteriye yetkili kişi ekleme
        public ActionResult AddContact(string yetikiliadi, string yetkilieposta, string yetkilitelefon, string yetkilinotlar, string CustomerID)
        {
            CustomerControlModel model = new CustomerControlModel();
            int CustID = Convert.ToInt32(CustomerID);

            var selectedCustomer = Db.Customers.FirstOrDefault(i => i.ID == CustID);
            if (string.IsNullOrWhiteSpace(yetikiliadi) || string.IsNullOrWhiteSpace(yetkilieposta))
            {
                return Json(new { result = "Error", message = "Yetkili adı veya E-posta boş geçilemez" }, JsonRequestBehavior.AllowGet);
            }

            if (selectedCustomer != null)
            {
                var checkEmail = Db.CustomerContact.Where(t => t.CustomerID == CustID && t.IsActive == true && t.Email == yetkilieposta).FirstOrDefault();
                if(checkEmail!=null)
                {
                    return Json(new { result = "Error", message = "Kayıtlı olan bir E-posta girildi!" }, JsonRequestBehavior.AllowGet);
                }

                var AddCustomerContact = new CustomerContact();
                AddCustomerContact.IsActive = true;
                AddCustomerContact.RecordDate = DateTime.Now;
                AddCustomerContact.CustomerID = CustID;
                AddCustomerContact.Email = yetkilieposta;
                AddCustomerContact.Phone = yetkilitelefon;
                AddCustomerContact.Notes = yetkilinotlar;
                AddCustomerContact.FullName = yetikiliadi;

                Db.CustomerContact.Add(AddCustomerContact);

                int IsSave = Db.SaveChanges();
                if (IsSave > 0)
                {
                    TempData["Message"] = "Projeye personel işlemi başarı ile tamamlandı";
                }

            }

            model.CustomerContacts = Db.CustomerContact.Where(i => i.CustomerID == CustID && i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "AddContact", "Customer", CustID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {CustID} Id'li firmaya yetkili kişi ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Customer/_PartialMusteriler.cshtml", model);
        }
        //müşteriden yetkili kişi kaldırma
        public PartialViewResult RemoveContact(string CustomerID, string ContactID)
        {
            CustomerControlModel model = new CustomerControlModel();
            int CustID = Convert.ToInt32(CustomerID);
            var _RemoveRowID = Convert.ToInt32(ContactID);
            CustomerContact contact = Db.CustomerContact.FirstOrDefault(i => i.ID == _RemoveRowID && i.CustomerID == CustID);

            if (contact != null)
            {
                contact.IsActive = false;
                Db.SaveChanges();
            }

            model.CustomerContacts = Db.CustomerContact.Where(i => i.CustomerID == CustID && i.IsActive == true).ToList();

            OypaHelper.AddApplicationLog("Management", "RemoveContact", "Customer", CustID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {CustID} Id'li firmadan {_RemoveRowID} Id'li yetkili kişiyi sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return PartialView("~/Views/Customer/_PartialMusteriler.cshtml", model);
        }

        public PartialViewResult County(string ID)
        {
            var cityID = Convert.ToInt32(ID);
            var searchCounty = Db.County.Where(x => x.sehir == cityID).ToList();
            return PartialView("~/Views/Customer/_PartialCounty.cshtml", searchCounty);
        }

        [HttpPost]
        public ActionResult Update(CustomerControlModel model)
        {
            var updatedCustomer = Db.Customers.FirstOrDefault(i => i.ID == model.CustomerID);

            if (updatedCustomer != null)
            {
                updatedCustomer.IBAN = model.IBAN;
                updatedCustomer.OpeningDate = model.OpeningDate;
                updatedCustomer.Phone = model.Phone;
                updatedCustomer.TaxNumber = model.TaxNumber;
                updatedCustomer.TaxOffice = model.TaxOffice;
                updatedCustomer.FaxNumber = model.FaxNumber;
                updatedCustomer.EMail = model.EMail;
                updatedCustomer.CustomerFullName = model.CustomerFullName;
                updatedCustomer.Address = model.Address;
                updatedCustomer.OpeningBalance = model.OpeningBalance;
                updatedCustomer.CityID = model.CityID;
                updatedCustomer.CountyID = model.CountyId;


                Db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int customerID)
        {
            var deletedCustomer = Db.Customers.FirstOrDefault(i => i.ID == customerID);
            if (deletedCustomer != null)
            {
                deletedCustomer.IsActive = false;
                Db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult ExportToExcelCustomers()
        {
            var gv = new GridView();
            var AllOperatorList = Db.Customers.Where(i => i.IsActive == true).ToList();

            var list = (from e in Db.Customers
                        where e.IsActive == true
                        select new
                        {
                            Ünvanı = e.CustomerFullName,
                            Firma_Kodu = e.AccountCode != null ? e.AccountCode : null,
                            Adres = e.Address != null ? e.Address : null,

                        }).ToList();
            gv.DataSource = list;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", $"attachment; filename={DateTime.Now.ToShortDateString()}_FirmaListesi.xls");
            Response.ContentType = "application/ms-excel";
            //Response.ContentEncoding = Encoding.GetEncoding("iso-8859-9");
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            OypaHelper.AddApplicationLog("Management", "ExportToExcelCustomers", "Customer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Firma listesini excel olarak indirdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View();
        }

        public ActionResult GetCustomerFromLogo()
        {
            try
            {
                //İlgili firmalar neye göre çekilecek, netleştirilecek.
                OypaService.Authentication auth = new OypaService.Authentication();
                auth.Username = "testUser";
                auth.Password = "testKullanici";
                auth.FirmNR = "124";
                auth.PeriodNR = "1";

                OypaService.LOGOTIGERSoapClient client = new OypaService.LOGOTIGERSoapClient();
                client.Open();
               
                var list = client.GetClientCardList(auth);
                List<Customers> customerList = new List<Customers>();

                //var customerList4 = list.Where(t => (t.CODE.Contains("1.120") || t.CODE.Contains("1.2.4002") || t.CODE.Contains("1.340") || t.CODE.Contains("1.159") || t.CODE.Contains("1.195")).ToList();
                var logoCustomerList = list.Where(t => (t.CODE.Contains("1.120") == true) || (t.CODE.Contains("1.2.4002") == true) || (t.CODE.Contains("1.340") == true) || (t.CODE.Contains("1.159") == true) || (t.CODE.Contains("1.195") == true)).ToList();
                List<Customers> newCustomerList = new List<Customers>();

                var ourCustomer = Db.Customers.Where(t => t.IsActive == true).ToList();

                if (logoCustomerList != null && logoCustomerList.Count() > 0)
                {
                    if (logoCustomerList.Count() != ourCustomer.Count())
                    {
                        foreach (var item in logoCustomerList)
                        {
                            if (ourCustomer.Any(t => t.AccountCode == item.CODE) == false)
                            {
                                //Yeni eklenmiş bir müşteri, db ye ekle!
                                Customers newCustomer = new Customers();
                                newCustomer.AccountCode = item.CODE;
                                newCustomer.CustomerFullName = item.DEFINITION_;
                                newCustomer.RegionID = 1;
                                newCustomer.CategoryID = 1;
                                newCustomer.EMail = item.EMAILADDR;
                                newCustomer.Phone = item.TELNRS1;
                                newCustomer.Address = item.ADDR1;
                                newCustomer.CityID = null;
                                newCustomer.CountyID = null;
                                newCustomer.PostCode = item.POSTCODE;
                                newCustomer.TaxOffice = item.TAXOFFICE;
                                newCustomer.TaxNumber = item.TAXNR;
                                newCustomer.LogoCode = item.LOGICALREF;
                                newCustomer.FaxNumber = item.FAXNR;
                                newCustomer.LogoID = null;
                                newCustomer.Title = null;
                                newCustomer.IBAN = null;
                                newCustomer.OpeningBalance = null;
                                newCustomer.OpeningDate = null;
                                newCustomer.CurrencyType = null;
                                newCustomer.IsActive = true;
                                newCustomer.UpdateDate = DateTime.Now;
                                newCustomer.UpdateUserID = OypaHelper.RecordEmployee();
                                newCustomer.OpeningDate = DateTime.Now;

                                newCustomerList.Add(newCustomer);
                            }
                            //else
                            //{
                            //    //Db de kayıtı olan bir ürün ise git bak bakalım değişen verisi var mı 
                            //    var selectedCustomer = ourCustomer.FirstOrDefault(t => t.IsActive == true && t.AccountCode == item.CODE);
                            //    selectedCustomer.AccountCode = item.CODE;
                            //    selectedCustomer.CustomerFullName = item.DEFINITION_;
                            //    selectedCustomer.RegionID = 1;
                            //    selectedCustomer.CategoryID = 1;
                            //    selectedCustomer.EMail = item.EMAILADDR;
                            //    selectedCustomer.Phone = item.TELNRS1;
                            //    selectedCustomer.Address = item.ADDR1;
                            //    selectedCustomer.CityID = null;
                            //    selectedCustomer.CountyID = null;
                            //    selectedCustomer.PostCode = item.POSTCODE;
                            //    selectedCustomer.TaxOffice = item.TAXOFFICE;
                            //    selectedCustomer.TaxNumber = item.TAXNR;
                            //    selectedCustomer.LogoCode = item.LOGICALREF;
                            //    selectedCustomer.FaxNumber = item.FAXNR;
                            //    selectedCustomer.LogoID = null;
                            //    selectedCustomer.Title = null;
                            //    selectedCustomer.IBAN = null;
                            //    selectedCustomer.OpeningBalance = null;
                            //    selectedCustomer.OpeningDate = null;
                            //    selectedCustomer.CurrencyType = null;
                            //    selectedCustomer.IsActive = true;
                            //    selectedCustomer.UpdateDate = DateTime.Now;
                            //    selectedCustomer.UpdateUserID = OypaHelper.RecordEmployee();
                            //    selectedCustomer.OpeningDate = DateTime.Now;
                            //    int s = Db.SaveChanges();
                            //}

                        }

                        foreach (var item in ourCustomer)
                        {
                            if (logoCustomerList.Any(t => t.CODE == item.AccountCode) == false)
                            {
                                item.IsActive = false;
                                //int s = Db.SaveChanges();
                            }
                        }


                        Db.Customers.AddRange(newCustomerList);
                        int save = Db.SaveChanges();
                        if (save > 0)
                        {
                            OypaHelper.AddApplicationLog("Management", "GetCustomerFromLogo", "Customer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Firma listesini güncellemek için logoya istekte bulundu ve yeni açılmış {newCustomerList.Count} adet firma uygulamaya dahil edilidi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Success", message = $"Firma bilgileri güncellendi ve logo üzerinden yeni açılmış {newCustomerList.Count} adet firma uygulamaya dahil edilidi." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "GetCustomerFromLogo", "Customer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; Firma listesini güncellemek için logoya istekte bulundu, Firma bilgilerinin güncel durumda olduğu mesajını aldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            return Json(new { result = "Success", message = $"Firma bilgileri güncel durumdadır." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "GetCustomerFromLogo", "Customer", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; Firma listesini güncellemek için logoya istekte bulundu, Firma bilgilerinin güncel durumda olduğu mesajını aldı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return Json(new { result = "Success", message = $"Firma bilgileri güncel durumdadır." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "GetCustomerFromLogo", "Customer", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı; Firma listesini güncellemek için logoya istekte bulundu, Logodan güncel firma bilgileri çekilemedi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return Json(new { result = "Fail", message = $"Logodan güncel firma bilgileri çekilemedi, lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex )
            {
                OypaHelper.AddApplicationLog("Management", "GetCustomerFromLogo", "Customer", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Firma listesini logodan çekerken bir hata ile karşılaştı! exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                OypaHelper.SendErrorMail("TYHErrorGetCustomerFromLogo", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Firma listesini logodan çekerken bir hata ile karşılaştı! exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");

                return Json(new { result = "Fail", message = $"Logodan güncel firma bilgileri getirilirken bir hata oluştu, lütfen tekrar deneyiniz!" }, JsonRequestBehavior.AllowGet);

            }

        
        }



    }
}