﻿using iTextSharp.text.pdf.qrcode;
using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace OykaCrm.Controllers
{
    public class OpportunityController : BaseController
    {
        // GET: Opportunity
        public ActionResult Index(bool? MessageType, string Message)
        {
            if (MessageType != null)
            {
                if (MessageType.Value == true)
                {
                    ViewBag.SMessage = Message;
                }
                else
                {
                    ViewBag.EMessage = Message;
                }
            }

            var oplist = Db.Opportunity.OrderByDescending(i => i.ID).ToList();
            var offerList = Db.Offer.ToList();
            List<Opportunity> list = new List<Opportunity>();
            foreach (var item in oplist)
            {
                if (offerList.Any(t => t.OpportunityID == item.ID) == false)
                {
                    list.Add(item);
                }
            }

            List<Opportunity> model = list.OrderByDescending(i => i.ID).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Opportunity", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Fırsat Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string str)
        {
            var Opportunity = Db.Opportunity.Where(i => (i.OpportunityName.Contains(str) || i.Customers.CustomerFullName.Contains(str) || i.OpportunityNumber.Contains(str)) && (i.StatusLevel == 1 || i.StatusLevel == 2)).ToList();

            OypaHelper.AddApplicationLog("Management", "Index", "Opportunity", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, Fırsat Listesi sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(Opportunity);
        }

        public ActionResult New(string error)
        {
            OypaHelper helper = new OypaHelper();
            OpportunityNewViewModel model = new OpportunityNewViewModel();

            if (error != null)
            {
                ViewBag.ErrorMessage = error;
            }
            model.ProjectList = Db.Projects.Where(i => i.IsActive == true).ToList();
            model.CustomersList = Db.Customers.Where(t=>t.IsActive==true).ToList();
            model.OpportunityNumber = helper.GetOpportunityNumber();
            model.CityList = Db.Cities.ToList();

            var perm = Db.RoleGroupPermissions.FirstOrDefault(x => x.RoleGroupID == model.CurrentUser.RolGroupID && x.PermissionID == 11);
            model.StatusList = Db.OpportunityStatus.Where(t => t.IsActive == true).ToList();

            if (perm != null)
            {
                model.StatusList = model.StatusList.Where(x => x.StatusLevel <= perm.MaxStatusLevel).ToList();
            }

            OypaHelper.AddApplicationLog("Management", "New", "Opportunity", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, yeni fırsat oluşturma sayfasını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            return View(model);
        }


        [HttpPost]
        public ActionResult Add(OpportunityAddViewModel model, HttpPostedFileBase[] uploadFiles)
        {
            OypaHelper helper = new OypaHelper();
            try
            { 
                if (model.IsOneTimeForm == 0)
                {
                    model.IsOneTime = true;
                }
                else
                {
                    model.IsOneTime = false;
                }
                if (model.CustomerID != 0 && model.ProjectID != 0)
                {
                    Opportunity opportunity = new Opportunity();
                    if (model.IsOneTime)
                    {
                        opportunity.IsOneTime = new bool?(true);
                        opportunity.DateBegin = new DateTime?(model.DateBegin);
                        opportunity.DateEnd = new DateTime?(model.DateEnd);
                    }
                    else
                    {
                        opportunity.IsOneTime = new bool?(false);
                        opportunity.IsOneTime = new bool?(model.IsOneTime);
                        opportunity.DateBegin = new DateTime?(model.DateBegin);
                        opportunity.DateEnd = new DateTime?(model.DateBegin);
                    }
                    ActionResult result;
                    if (opportunity.DateEnd < opportunity.DateBegin)
                    {
                        OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", 0L, null, false, string.Concat(new string[]
                        {
                            " ",
                            OypaHelper.RecordEmployeeFullName(),
                            " isimli kullanıcı ",
                            opportunity.OpportunityNumber,
                            " nolu fırsatı kaydederken 'Bitiş tarihi düzenleme tarihinden önce olamaz!'uyarısı ile karşılaştı"
                        }), OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        result = base.RedirectToAction("New", "Opportunity", new
                        {
                            error = "Bitiş tarihi düzenleme tarihinden önce olamaz!"
                        });
                        return result;
                    }
                    opportunity.RegionID = helper.EmployeeRegionID();
                    opportunity.RecordDate = new DateTime?(DateTime.Now);
                    opportunity.OpportunityNumber = model.OpportunityNumber;
                    opportunity.IsActive = new bool?(true);
                    opportunity.MainContent = model.MainContent;
                    opportunity.EmployeeID = new int?(OypaHelper.RecordEmployee());
                    opportunity.RecordEmployeeID = new int?(OypaHelper.RecordEmployee());
                    opportunity.OpportunityName = model.OpportunityName;
                    opportunity.RecordIP = Request.UserHostAddress;
                    opportunity.StatusLevel = new int?(model.StatusLevel);
                    opportunity.CustomerID = new int?(model.CustomerID);
                    opportunity.ProjectID = new int?(model.ProjectID);
                    base.Db.Opportunity.Add(opportunity);
                    int IsSave = base.Db.SaveChanges();
                    if (uploadFiles != null && uploadFiles.Count() > 0)
                    {
                        foreach (var uploadFile in uploadFiles)
                        {
                            if (uploadFile != null)
                            {
                                Documents doc = new Documents();
                                Guid g = Guid.NewGuid();
                                var fileName = Path.GetFileName(g.ToString() + uploadFile.FileName);
                                //Dosya belirtilen yola kaydediliyor.
                                var filePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadData/");
                                uploadFile.SaveAs(Path.Combine(filePath, fileName));
                                doc.CategoryID = 1;
                                doc.DocumentName = Path.GetFileName(fileName);
                                doc.FileName = Path.GetFileName(fileName);
                                doc.FilePath = Path.Combine(filePath, fileName);
                                doc.OpportunityID = opportunity.ID;
                                doc.Date = DateTime.Now;
                                doc.RecordDate = DateTime.Now;
                                doc.TypeID = null;
                                doc.OfferID = null;
                                doc.RecordEmployee = OypaHelper.RecordEmployee();
                                doc.RecordIP = HttpContext.Request.UserHostAddress;
                                doc.IsActive = true;
                                doc.ProjectID = opportunity.ProjectID;
                                Db.Documents.Add(doc);
                                Db.SaveChanges();
                            }
                        }
                    }
                    if (IsSave > 0)
                    {
                        string CreatedBy = Db.Employee.Where(t => t.ID == opportunity.RecordEmployeeID).FirstOrDefault().FullName;
                        string OppName = opportunity.OpportunityNumber + " - " + opportunity.OpportunityName;

                        var mailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 1 && t.IsActive == true).ToList();
                        var mailres = false;
                        var mailCounter = 0;
                        if (mailList != null && mailList.Count() > 0)
                        {
                            foreach (var item in mailList)
                            {
                                mailres = OypaHelper.FillinEmailContent($"{CreatedBy} tarafından {OppName} isimli bir fırsat oluşturuldu.",
                              "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak fırsatı inceleyiniz.",
                              $"Sn.{item.FullName}", $"{item.Email}", //gsahin@oypa.com.tr
                              "TYH - Fırsat Oluşturuldu",
                              "http://194.100.100.21/", null, "CreatedOpportunity.html");
                                if (mailres)
                                {
                                    mailCounter++;

                                    OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", opportunity.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu fırsat bildirimi için {item.Email} adresine mail başarıyla gönderdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                }
                                else
                                {
                                    OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", opportunity.ID, null, false, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu  fırsat bildirimi için {item.Email} adresine mail gönderirken hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                                    OypaHelper.SendErrorMail("TYHErrorAddOpportunity", $"Fırsat kaydedildi fakat {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu  fırsat bildirimi için {item.Email} adresine mail gönderirken hata ile karşılaştı.");
                                }
                            }
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", opportunity.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu fırsatı ilgiliye bildirirken 'Sistemde fırsat süreci için ilgili kişinin mail bilgileri tanımlı değil, Sistem yöneticisi ile iletişime geçiniz!' uyarısı ile karşılaştı", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            ViewBag.EMessage = "Sistemde fırsat süreci için ilgili kişinin mail bilgileri tanımlı değil, Sistem yöneticisi ile iletişime geçiniz!";

                            return RedirectToAction("Index", "Opportunity", new { MessageType = false, Message = ViewBag.EMessage });
                        }

                        if (mailCounter == mailList.Count())
                        {
                            OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", opportunity.ID, null, true, $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu fırsatı direktöre bildirdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            ViewBag.SMessage = "Fırsat Direktöre Bildirildi.";
                            return RedirectToAction("Index", "Opportunity", new { MessageType = true, Message = ViewBag.SMessage });
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", opportunity.ID, null, false, $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu fırsatı direktöre mail ile bildirilirken hata ile karşılaştı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                            ViewBag.EMessage = "Fırsat oluşturuldu fakat direktöre bildirilemedi.";
                            return RedirectToAction("Index", "Opportunity", new { MessageType = false, Message = ViewBag.EMessage });
                        }
                    }
                    else
                    {
                        OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", opportunity.ID, null, false, $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu fırsatı oluştururken bir hata ile karşılaştı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        ViewBag.EMessage = "Fırsat kaydedilirken bir hata oluştu, Lütfen tekrar deneyiniz'";
                        OypaHelper.SendErrorMail("TYHErrorAddOpportunity", $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {opportunity.OpportunityNumber} nolu fırsatı oluştururken bir hata ile karşılaştı!");

                        return RedirectToAction("Index", "Opportunity", new { MessageType = false, Message = ViewBag.EMessage });
                    }
                }
            }
            catch (Exception ex)
            {
                OypaHelper.AddApplicationLog("Management", "Add", "Opportunity", 0, null, false, $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.OpportunityNumber} nolu fırsatı direktöre bildirirken hata ile karşılaştı!", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                OypaHelper.SendErrorMail("TYHErrorAddOpportunity", $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.OpportunityNumber} nolu fırsatı direktöre bildirirken hata ile karşılaştı! exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");
               
            }
            

            return RedirectToAction("Index", "Opportunity");
        }

        public ActionResult Detail(int? ID, bool? MessageType, string Message)
        {
            if (MessageType != null)
            {
                if (MessageType.Value == true)
                {
                    ViewBag.SMessage = Message;
                }
                else
                {
                    ViewBag.EMessage = Message;
                }
            }

            OpportunityDetailViewModel model = new OpportunityDetailViewModel();
            model.SingleOpportunity = Db.Opportunity.Any(i => i.ID == ID) ? Db.Opportunity.FirstOrDefault(i => i.ID == ID) : null;
            if (model.SingleOpportunity != null)
            {
                model.RolePermissionList = Db.RoleGroupPermissions.Where(i => i.RoleGroupID == model.CurrentUser.RolGroupID && i.MaxStatusLevel > 0).ToList();

                model.CustomersList = Db.Customers.Where(t=>t.IsActive==true).ToList();
                model.StatusList = Db.OpportunityStatus.Where(t => t.IsActive == true).ToList();
                model.DocumentsList = Db.Documents.Where(t => t.OpportunityID == ID).ToList();
                model.ProjectList = Db.Projects.Where(i => i.IsActive == true).ToList();

                if (model.SingleOpportunity.DateBegin != null)
                {
                    var date = model.SingleOpportunity.DateBegin.Value;

                    model.FormatDateBegin = date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                }

                if (model.SingleOpportunity.DateEnd != null)
                {
                    var date = model.SingleOpportunity.DateEnd.Value;

                    model.FormatDateEnd = date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                }

                OypaHelper.AddApplicationLog("Management", "Detail", "Opportunity", model.SingleOpportunity.ID, null, true, $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.SingleOpportunity.OpportunityNumber} nolu fırsatın detayını görüntüledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                return View(model);
            }
            else
            {
                OypaHelper.AddApplicationLog("Management", "Detail", "Opportunity", 0, null, false, $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {ID} id li  fırsatın detayını görüntülerken 'Böyle bir fırsat bulunmamaktadır.' uyarısı ile karşılaştı. ", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                ViewBag.ErrorMessage = "Böyle bir fırsat bulunmamaktadır.";
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(Opportunity model, bool IsOneTimee = false) //False ise tek seferlik teklif
        {
            OpportunityNewViewModel modelx = new OpportunityNewViewModel();
            try
            {
         
                var selfopportunity = Db.Opportunity.FirstOrDefault(i => i.ID == model.ID);
                if (selfopportunity != null)
                {
                    Opportunity selfop = new Opportunity()
                    {
                        ID = selfopportunity.ID,
                        CustomerID = selfopportunity.CustomerID,
                        ProjectID = selfopportunity.ProjectID,
                        DateBegin = selfopportunity.DateBegin,
                        DateEnd = selfopportunity.DateEnd,
                        EnvironmentID = selfopportunity.EnvironmentID,
                        IsActive = selfopportunity.IsActive,
                        MainContent = selfopportunity.MainContent,
                        RecordDate = selfopportunity.RecordDate,
                        OpportunityNumber = selfopportunity.OpportunityNumber,
                        RecordEmployeeID = selfopportunity.RecordEmployeeID,
                        RegionID = selfopportunity.RegionID,
                        StatusLevel = selfopportunity.StatusLevel,
                        Summary = selfopportunity.Summary,
                        OpportunityName = selfopportunity.OpportunityName,

                    };

                    selfopportunity.MainContent = model.MainContent;
                    selfopportunity.IsActive = true;
                    selfopportunity.OpportunityName = model.OpportunityName;
                    selfopportunity.OpportunityNumber = model.OpportunityNumber;
                    selfopportunity.StatusLevel = model.StatusLevel;
                    selfopportunity.Summary = model.Summary;
                    selfopportunity.EnvironmentID = model.EnvironmentID;
                    selfopportunity.ProjectID = model.ProjectID;
                    if (IsOneTimee == false) //False ise tek seferlik teklif
                    {
                        selfopportunity.IsOneTime = false;
                        selfopportunity.DateBegin = model.DateBegin;
                        selfopportunity.DateEnd = model.DateBegin;
                    }
                    else // True ise tek seferlik teklif
                    {
                        selfopportunity.IsOneTime = true;
                        selfopportunity.DateBegin = model.DateBegin;
                        selfopportunity.DateEnd = model.DateEnd;
                    }

                    selfopportunity.CustomerID = model.CustomerID;

                    int IsSave = Db.SaveChanges();
                    bool mailres = false;

                    var mailContentTitle = Db.Employee.FirstOrDefault(t => t.ID == selfopportunity.RecordEmployeeID).FullName;
                    string OppName = selfopportunity.OpportunityNumber + " - " + selfopportunity.OpportunityName;

                    mailres = OypaHelper.FillinEmailContent($"Sn.{ OypaHelper.RecordEmployeeFullName()} tarafından {OppName} isimli  fırsatın durumu {selfopportunity.OpportunityStatus.StatusName} olarak değiştirildi.",
                                   "Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak fırsatı inceleyiniz.",
                                   $"Sn. {(mailContentTitle != null ? mailContentTitle : "")}", selfopportunity.Employee.EMail, //gsahin@oypa.com.tr
                                   "TYH - Fırsat Durum Değişikliği",
                                   "http://194.100.100.21/", null, "CreatedOpportunity.html");

 
                    if(model.StatusLevel==3)
                    {
                        #region Fırsatı direktör onayladığında Sn Arslan Coşar'a atılacak mail 
                        string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Email/InfoGeneralManager.html"));
                        //#To# Hitap edilecek kişi
                        // Message1 #CreatedBy# tarafından #OpportunityName# isimli bir fırsat oluşturuldu.
                        // Message2 Lütfen Tesis Yönetim Hizmeti Web Uygulamasına giriş yaparak..

                        Body = Body.Replace("#MailContentTitle#", "Sn. Arslan Coşar"); //Örn Sn Direktör
                        Body = Body.Replace("#Message1#", $"Tesis Yönetim Hizmetleri uygulamsında oluşturulan {OppName} isimli fırsat teklif sürecine dönüştürülmüştür.");


                        //
                        bool generalManagerMailRes = OypaHelper.SendMail("TYH-Teklif Bilgilendirme", Body, "acosar@oypa.com.tr", null);
                        if(generalManagerMailRes==true)
                        {
                            OypaHelper.AddApplicationLog("Management", "Update", "Opportunity", selfopportunity.ID, "RoleGroupPermissions", true, $"{ OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selfopportunity.OpportunityNumber} nolu fırsatın direktör onay bilgisi acosar@oypa.com.tr adresine iletildi.", modelx.CurrentUser.ID, OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "Update", "Opportunity", selfopportunity.ID, "RoleGroupPermissions", true, $"{ OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {selfopportunity.OpportunityNumber} nolu fırsatın direktör onay bilgisi acosar@oypa.com.tr adresine iletilirken bir hata ile karşılaşıldı.", modelx.CurrentUser.ID, OypaHelper.UserIpAdress(), null);
                        }
                      
                        #endregion
                    }




                    var isequal = OypaHelper.PublicInstancePropertiesEqual<Opportunity>(selfop, selfopportunity, OypaHelper.getIgnorelist());

                    OypaHelper.AddApplicationLog("Management", "Update", "Opportunity", selfopportunity.ID, "RoleGroupPermissions", true, $"{ OypaHelper.RecordEmployeeFullName()} isimli kullancı{selfopportunity.OpportunityNumber} nolu fırsatı güncelledi.", modelx.CurrentUser.ID, OypaHelper.UserIpAdress(), null);

                    if (mailres)
                    {
                        ViewBag.SMessage = "Fırsat Güncellendi.";
                        return RedirectToAction("Detail", "Opportunity", new
                        {
                            ID = selfopportunity.ID,
                            MessageType = true,
                            Message = ViewBag.SMessage
                        });
                    }
                    else
                    {

                        OypaHelper.AddApplicationLog("Management", "Update", "Opportunity", selfopportunity.ID, "RoleGroupPermissions", true, $"{ OypaHelper.RecordEmployeeFullName()} isimli kullancı{selfopportunity.OpportunityNumber} nolu fırsatı güncellerken hata ile karşılaştı", modelx.CurrentUser.ID, OypaHelper.UserIpAdress(), null);

                        ViewBag.EMessage = "Fırsat Güncellenmedi";
                        return RedirectToAction("Detail", "Opportunity", new { ID = selfopportunity.ID, MessageType = false, Message = ViewBag.EMessage });
                    }
                }
            }
            catch (Exception ex )
            {
                OypaHelper.SendErrorMail("TYHErrorEditOpportunity", $" {OypaHelper.RecordEmployeeFullName()} isimli kullanıcı {model.OpportunityNumber} nolu fırsatı güncellenidrken hata ile karşılaştı! exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");
            
            }

            return RedirectToAction("Index", "Opportunity");
        }

        public ActionResult DownloadFile(string filePath)
        {
            string fullName = Server.MapPath("~/UploadData/" + filePath);

            OypaHelper.AddApplicationLog("Management", "DownloadFile", "Opportunity", 0, "Opportunity", true, $"{ OypaHelper.RecordEmployeeFullName()} isimli kullancı {filePath} isimli belgeyi indirdi", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

            byte[] fileBytes = GetFile(fullName);
            return File(
                fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filePath);
        }

        byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        //Proje Kodu Oluşturmak İçin Mail Gönder
        [HttpPost]
        public ActionResult SendMailForGenerateProjectCode(GenerateProjectCodeModel model)
        {
            if (model != null)
            {
                var cityName = "";
                var countyName = "";

                if (model.CityId != 0)
                {
                    cityName = Db.Cities.Where(x => x.id == model.CityId).Select(x => x.sehir).FirstOrDefault();
                }
                if (model.CountyId != 0)
                {
                    countyName = Db.County.Where(x => x.id == model.CountyId).Select(x => x.ilce).FirstOrDefault();
                }

                var mailList = Db.MailProcess.Where(t => t.MailProcessTypeId == 2 && t.IsActive == true).ToList();

                var mailCounter = 0;
                if (mailList != null && mailList.Count() > 0)
                {
                    var icerik = Session["username"].ToString() + " tarafından  " + model.CustomerName + " müşterisi için " + model.OpportunityNumber + " numaralı fırsata " + cityName + " - " + countyName + "  ili için tarafınızdan bir proje kodu üretmeniz isteniyor";

                    foreach (var item in mailList)
                    {
                        var mailRes = OypaHelper.FillinEmailContent("", icerik, $"Sayın {item.FullName}, ", $"{item.Email}", "Yeni Proje Kodu İsteği", "", null, "CustomerMail.html");

                        if (mailRes)
                        {
                            mailCounter++;

                            OypaHelper.AddApplicationLog("Management", "SendMailForGenerateProjectCode", "Opportunity", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isim li kullanıcı {item.Email} adresine proje kodu talebini başarıyla gönderdi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                        else
                        {
                            OypaHelper.AddApplicationLog("Management", "SendMailForGenerateProjectCode", "Opportunity", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} isim li kullanıcı {item.Email} adresine proje kodu talebini gönderirken hata ile karşılaştı.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                        }
                    }
                    if (mailCounter == mailList.Count())
                    {
                        return Json(new { result = "true" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { result = "false" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    OypaHelper.AddApplicationLog("Management", "SendMailForGenerateProjectCode", "Opportunity", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} isim li kullanıcı proje kodu talep ederken 'ilgili birimin mail adresi sistemde tanımlı değil' uyarısı ile karşılaştı", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                    return Json(new { result = "missingMail" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetCounty(int cityId)
        {
            var searchCounty = Db.County.Where(x => x.sehir == cityId).ToList();
            return PartialView("~/Views/Customer/_PartialCounty.cshtml", searchCounty);
        }


    }
}
