﻿using OykaCrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using RoleGroup = OykaCrm.Models.RoleGroup;

namespace OykaCrm.Controllers
{
    public class RoleController : BaseController
    {
        // GET: Role
        public ActionResult Index()
        {
            var model = new List<RoleModel>();

            var list = Db.RoleGroup.Where(x => x.IsActive == true).ToList();
            if (list != null)
            {
                foreach (var item in list)
                {
                    var md = new RoleModel();
                    md.RoleName = item.GroupName;
                    md.Id = item.ID;
                    model.Add(md);
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteRole(int? id)
        {
            if (id != 0)
            {
                var role = Db.RoleGroup.Where(x => x.ID == id && x.IsActive == true).FirstOrDefault();
                if (role != null)
                {
                    role.IsActive = false;
                    var isSaved = Db.SaveChanges();
                    if (isSaved > 0)
                    {
                        OypaHelper.AddApplicationLog("Management", "DeleteRole", "Role", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {role.ID} Id'li rol grubunu sildi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return Json(true, JsonRequestBehavior.AllowGet);
                    }

                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddRole()
        {
            var listRoleLevel = Db.RoleLevel.Where(x => x.IsActive == true).ToList();

            if (listRoleLevel != null)
            {
                var model = new List<RoleLevelList>();
                foreach (var item in listRoleLevel)
                {
                    var md = new RoleLevelList();
                    md.Id = item.ID;
                    md.Name = item.Name;

                    model.Add(md);
                }
                return View(model);
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddRole(RoleModel model)
        {
            try
            {
                if (model != null)
                {
                    var role = new RoleGroup();
                    role.GroupName = model.RoleName;
                    role.RoleLevel = model.RoleLevelId;
                    role.IsActive = true;
              
                    Db.RoleGroup.Add(role);
                    var isSaved = Db.SaveChanges();
                    if (isSaved > 0)
                    {
                        OypaHelper.AddApplicationLog("Management", "AddRole", "Role", role.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {role.GroupName} adlı yeni rol grubu ekledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return RedirectToAction("Index", "Role");
                    }
                }
            }
            catch (Exception ex)
            {
                OypaHelper.AddApplicationLog("Management", "AddRole", "Role", 0, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.RoleName}-{model.RoleLevelId} adlı yeni rol grubu eklerken hata ile karşılaştı. hata:{ex.Message}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);
                OypaHelper.SendErrorMail("AddRolePostError", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.RoleName}-{model.RoleLevelId} adlı yeni rol grubu eklerken hata ile karşılaştı. exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace:{ex.StackTrace}");
            
            }
            return RedirectToAction("Index", "Role");

        }

        public ActionResult EditRole(int? id)
        {
            if (id != 0)
            {
                var role = Db.RoleGroup.Where(x => x.ID == id && x.IsActive == true).FirstOrDefault();

                if (role != null)
                {
                    var model = new RoleModel();
                    model.Id = role.ID;
                    model.RoleName = role.GroupName;
                    model.RoleLevelId = role.RoleLevel;
                    var list = Db.RoleLevel.Where(x => x.IsActive == true).ToList();

                    foreach (var item in list)
                    {
                        var md = new RoleLevelList();
                        md.Id = item.ID;
                        md.Name = item.Name;

                        model.RoleLevelList.Add(md);
                    }

                    return View(model);
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult EditRole(RoleModel model)
        {
            try
            {
                if (model != null)
                {
                    var role = Db.RoleGroup.Where(x => x.ID == model.Id && x.IsActive == true).FirstOrDefault();

                    if (role != null)
                    {
                        role.GroupName = model.RoleName;
                        role.RoleLevel = model.RoleLevelId;
                        Db.SaveChanges();

                        OypaHelper.AddApplicationLog("Management", "EditRole", "Role", role.ID, null, true, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {role.ID} Id'li rol grubunu güncelledi.", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                        return RedirectToAction("Index", "Role");
                    }
                }
            }
            catch (Exception ex )
            {
                OypaHelper.AddApplicationLog("Management", "EditRole", "Role", 0, null, false, $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.RoleName}-{model.RoleLevelId} adlı rolü güncellerken hata ile karşılaştı. hata:{ex.Message}", OypaHelper.RecordEmployee(), OypaHelper.UserIpAdress(), null);

                OypaHelper.SendErrorMail("EditRolePostError", $"{OypaHelper.RecordEmployeeFullName()} adlı kullanıcı, {model.RoleName}-{model.RoleLevelId} adlı rolü güncellerken hata ile karşılaştı.  exMessage:{ex.Message}, innerEx:{ex.InnerException}, StakTrace: {ex.StackTrace}");

      
            }
           

            return View();
        }

    }
}